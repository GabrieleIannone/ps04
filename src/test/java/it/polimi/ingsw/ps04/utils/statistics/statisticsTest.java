package it.polimi.ingsw.ps04.utils.statistics;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class statisticsTest {

	Ranking expectedRanking;

	@Test
	public void testMoreVictories() {
		List<PlayerStatistics> statistics = new ArrayList<>();
		PlayerStatistics firstPlayer = new PlayerStatistics("player1", 2, 3, 70);
		PlayerStatistics secondPlayer = new PlayerStatistics("player2", 1, 3, 80);
		statistics.add(secondPlayer);
		statistics.add(firstPlayer);
		Ranking ranking = new Ranking(statistics);
		assertEquals(firstPlayer, ranking.getRanking().get(0));
		assertEquals(secondPlayer, ranking.getRanking().get(1));
	}

	@Test
	public void testEqualsVictories() {
		List<PlayerStatistics> statistics = new ArrayList<>();
		PlayerStatistics firstPlayer = new PlayerStatistics("player1", 2, 4, 70);
		PlayerStatistics secondPlayer = new PlayerStatistics("player2", 2, 4, 80);
		statistics.add(secondPlayer);
		statistics.add(firstPlayer);
		Ranking ranking = new Ranking(statistics);
		assertEquals(firstPlayer, ranking.getRanking().get(0));
		assertEquals(secondPlayer, ranking.getRanking().get(1));
	}

	
}
