package it.polimi.ingsw.ps04.utils.login;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import javax.security.auth.login.FailedLoginException;
import javax.security.auth.login.LoginException;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import it.polimi.ingsw.ps04.utils.db.DBConnector;
import it.polimi.ingsw.ps04.utils.login.LoginManager;
import it.polimi.ingsw.ps04.utils.message.login.CredentialsMessage;
import it.polimi.ingsw.ps04.utils.message.login.DeleteUserMessage;
import it.polimi.ingsw.ps04.utils.message.login.LoginMessage;
import it.polimi.ingsw.ps04.utils.message.login.SignupMessage;

public class TestLoginManager {

	@BeforeClass
	public static void setUpBefore() {
		try {
			DBConnector.getInstance().setAutocommit(false);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@After
	public void deleteChanges() {

		try {
			DBConnector.getInstance().rollback();
			LoginManager.getInstance().logoutAll();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testLoginAfterSignup() throws LoginException, SQLException {
		CredentialsMessage signupMessage = new SignupMessage("user", "email", "password");
		LoginManager.getInstance().executeCredentialsMessage(signupMessage);
		CredentialsMessage loginMessage = new LoginMessage("user", "password");
		boolean loginResult = LoginManager.getInstance().executeCredentialsMessage(loginMessage);
		assertEquals(true, loginResult);
	}

	@Test
	public void testBadSignup() throws SQLException {
		CredentialsMessage signupMessage = new SignupMessage("user", "email", "password");
		boolean firstSignupOk = true;
		try {
			LoginManager.getInstance().executeCredentialsMessage(signupMessage);
		} catch (LoginException e) {
			firstSignupOk = false;
		}
		CredentialsMessage signupMessage2 = new SignupMessage("user2", "email", "password2");

		boolean secondSingupOk = true;
		try {
			LoginManager.getInstance().executeCredentialsMessage(signupMessage2);
		} catch (LoginException e) {
			secondSingupOk = false;
		}
		if (firstSignupOk) {
			assertEquals(false, secondSingupOk);
		}
	}

	@Test
	public void testBadUserLogin() throws LoginException, SQLException {
		LoginManager.getInstance().executeCredentialsMessage(new SignupMessage("user", "email", "password"));
		boolean loginResult = true;
		try {
			LoginManager.getInstance().executeCredentialsMessage(new LoginMessage("baduser", "password"));
		} catch (FailedLoginException e) {
			loginResult = false;
		}
		assertEquals(false, loginResult);
	}

	@Test
	public void testBadPasswordLogin() throws LoginException, SQLException {
		LoginManager.getInstance().executeCredentialsMessage(new SignupMessage("user", "email", "password"));
		boolean loginResult = true;
		try {
			LoginManager.getInstance().executeCredentialsMessage(new LoginMessage("user", "badpassword"));
		} catch (FailedLoginException e) {
			loginResult = false;
		}
		assertEquals(false, loginResult);
	}

	@Test
	public void testDeletingUser() throws LoginException, SQLException {
		CredentialsMessage deleteMessage = new DeleteUserMessage("user", "password");
		LoginManager.getInstance().executeCredentialsMessage(new SignupMessage("user", "email", "password"));
		LoginManager.getInstance().executeCredentialsMessage(deleteMessage);
		boolean loginResult = true;
		try {
			loginResult = LoginManager.getInstance().executeCredentialsMessage(new LoginMessage("user", "password"));
		} catch (FailedLoginException e) {
			loginResult = false;
		}
		assertEquals(false, loginResult);
	}

	@Test
	public void testOnlineUsers() throws LoginException, SQLException {
		LoginManager.getInstance().executeCredentialsMessage(new SignupMessage("user1", "email1", "password1"));
		LoginManager.getInstance().executeCredentialsMessage(new LoginMessage("user1", "password1"));
		LoginManager.getInstance().executeCredentialsMessage(new SignupMessage("user2", "email2", "password2"));
		LoginManager.getInstance().executeCredentialsMessage(new LoginMessage("user2", "password2"));
		Set<String> actualOnlineUsers = new HashSet<String>(LoginManager.getInstance().onlineUsers.values());
		Set<String> expectedOnlineUsers = new HashSet<>();
		expectedOnlineUsers.add("user1");
		expectedOnlineUsers.add("user2");
		assertEquals(expectedOnlineUsers, actualOnlineUsers);

	}

	@Test
	public void testLogout() throws LoginException, SQLException {
		LoginManager.getInstance().executeCredentialsMessage(new SignupMessage("user", "email", "password"));
		LoginManager.getInstance().executeCredentialsMessage(new LoginMessage("user", "password"));
		LoginManager.getInstance().logout("user", "password");
		assertEquals(false, LoginManager.getInstance().onlineUsers.containsValue("user"));
	}
	
	@Test
	public void testDisconnectUser() throws LoginException, SQLException {
		LoginManager.getInstance().executeCredentialsMessage(new SignupMessage("user", "email", "password"));
		LoginManager.getInstance().executeCredentialsMessage(new LoginMessage("user", "password"));
		LoginManager.getInstance().disconnectUser("user");
		assertEquals(false, LoginManager.getInstance().onlineUsers.containsValue("user"));
	}
}
