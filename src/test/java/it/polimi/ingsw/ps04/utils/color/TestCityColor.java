package it.polimi.ingsw.ps04.utils.color;

import static org.junit.Assert.*;

import java.awt.Color;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import it.polimi.ingsw.ps04.model.bonus.Bonus;
import it.polimi.ingsw.ps04.model.bonus.simple.AssistantsBonus;
import it.polimi.ingsw.ps04.model.bonus.simple.CoinsBonus;

public class TestCityColor {

	CityColor expectedCityColor;

	@Test
	public void testCityColorConstructors() {
		Color red = new Color(255,0,0);
		Set<Bonus> bonuses = new HashSet<>();
		bonuses.add(new AssistantsBonus(1));
		bonuses.add(new CoinsBonus(1));
		expectedCityColor = new CityColor("red", red, bonuses ) ;
		NamedColor namedColor = new NamedColor("red", 255, 0, 0);
		CityColor actualCityColor = new CityColor(namedColor, bonuses);
		assertEquals(expectedCityColor,actualCityColor);
	}
}
