package it.polimi.ingsw.ps04.creation;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;

import it.polimi.ingsw.ps04.model.GameFactory;
import it.polimi.ingsw.ps04.model.Model;
import it.polimi.ingsw.ps04.model.board.map.Map;
import it.polimi.ingsw.ps04.model.board.map.MapFactory;
import it.polimi.ingsw.ps04.model.bonus.BonusFactory;
import it.polimi.ingsw.ps04.model.deck.business.BusinessDeckFactory;
import it.polimi.ingsw.ps04.model.deck.politic.PoliticCard;
import it.polimi.ingsw.ps04.model.deck.politic.PoliticDeck;
import it.polimi.ingsw.ps04.model.nobilitytrack.NobilityTrack;
import it.polimi.ingsw.ps04.model.nobilitytrack.NobilityTrackFactory;
import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.utils.ConfigHelper;
import it.polimi.ingsw.ps04.utils.color.ColorsManager;
import it.polimi.ingsw.ps04.utils.color.ColorsManagerFactory;
import it.polimi.ingsw.ps04.utils.exception.BadConfigFileException;

public class TestGameModelCreation {

	List<String> playerNames;
	Model expectedModel;
	String TEST_CONFING_FILE = "src" + File.separator + "test" + File.separator + "TestGameCreation.json";
	ConfigHelper configHelper = new ConfigHelper(new File(TEST_CONFING_FILE));

	@Before
	public void setUp() throws JsonProcessingException, IOException, BadConfigFileException {
		/*
		 * I can use only one player because the create game shuffle the
		 * playerNames list
		 */
		String player1 = "player1";
		playerNames = new ArrayList<>();
		playerNames.add(player1);
		expectedModel = new Model();
		BonusFactory bonusFactory = new BonusFactory(expectedModel, configHelper);
		ColorsManager colorsManager = new ColorsManagerFactory(configHelper, bonusFactory).factoryMethod();
		PoliticDeck politicCardsDeck = new PoliticDeck(colorsManager.getCorruptionColors());
		expectedModel.setPoliticCardsDeck(politicCardsDeck);
		politicCardsDeck = expectedModel.getPoliticDeck();
		List<Player> players = new ArrayList<>();
		NobilityTrackFactory nobilityTrackFactory;
		BusinessDeckFactory businessDeckFactory = new BusinessDeckFactory(bonusFactory);
		Map map = null;
		NobilityTrack nobilityTrack = null;

		nobilityTrackFactory = new NobilityTrackFactory(configHelper, bonusFactory, 0);
		nobilityTrack = nobilityTrackFactory.factoryMethod();
		map = new MapFactory(colorsManager, configHelper, bonusFactory, businessDeckFactory, politicCardsDeck)
				.importMap("map_name_1");

		int playerTurn;
		int citiesNumber = map.getCitiesNumber();
		for (int i = 0; i < playerNames.size(); i++) {
			String playerName = playerNames.get(i);
			playerTurn = i + 1;
			List<PoliticCard> initialCards = politicCardsDeck.drawInitalCards();
			players.add(new Player(playerName, citiesNumber, playerTurn, initialCards));
		}
		expectedModel.setMap(map);
		expectedModel.setPlayers(players);
		expectedModel.setNobilityTrack(nobilityTrack);
	}

	@Test
	public void testGameModelCreation() {

		Model actualModel = null;
		List<String> playerNamesCopy = new ArrayList<>(playerNames);
		try {
			actualModel = GameFactory.createGame(playerNamesCopy, configHelper);
		} catch (IOException | BadConfigFileException e) {
			e.printStackTrace();
		}
		assertEquals(expectedModel, actualModel);
	}

}
