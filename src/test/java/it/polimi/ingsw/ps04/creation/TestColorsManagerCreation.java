package it.polimi.ingsw.ps04.creation;

import static org.junit.Assert.*;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.ps04.model.Model;
import it.polimi.ingsw.ps04.model.bonus.BonusFactory;
import it.polimi.ingsw.ps04.utils.ConfigHelper;
import it.polimi.ingsw.ps04.utils.color.ColorsManager;
import it.polimi.ingsw.ps04.utils.color.ColorsManagerFactory;
import it.polimi.ingsw.ps04.utils.color.NamedColor;

public class TestColorsManagerCreation {

	private Model model;
	String TEST_CONFING_FILE = "src" + File.separator + "test" + File.separator + "TestGameCreation.json";
	ConfigHelper configHelper;
	BonusFactory bf;
	Map<String, Color> availableColors;
	Set<NamedColor> corruptionColors;

	private void addAvailableColors(Map<String, Color> availableColors) {
		availableColors.put("black", new Color(0, 0, 0));
		availableColors.put("orange", new Color(255, 165, 0));
		availableColors.put("pink", new Color(255, 192, 203));
		availableColors.put("purple", new Color(128, 0, 128));
		availableColors.put("blue", new Color(0, 0, 255));
		availableColors.put("gold", new Color(255, 215, 0));
		availableColors.put("silver", new Color(218, 218, 218));
		availableColors.put("bronze", new Color(201, 174, 93));
	}

	private void addCorruptionColors(Set<NamedColor> corruptionColors) {
		corruptionColors.add(new NamedColor("black", new Color(0, 0, 0)));
		corruptionColors.add(new NamedColor("orange", new Color(255, 165, 0)));
		corruptionColors.add(new NamedColor("pink", new Color(255, 192, 203)));
		corruptionColors.add(new NamedColor("purple", new Color(128, 0, 128)));
		corruptionColors.add(new NamedColor("blue", new Color(0, 0, 255)));
	}

	private void setColorsManager() {
		availableColors = new HashMap<>();
		addAvailableColors(availableColors);
		corruptionColors = new HashSet<>();
		addCorruptionColors(corruptionColors);
	}

	@Before
	public void setUp() {
		model = new Model();
		configHelper = new ConfigHelper(new File(TEST_CONFING_FILE));
		bf = new BonusFactory(model, configHelper);
		setColorsManager();
	}

	@Test
	public void testColorsManagerCreation() {
		ColorsManager expectedColorsManager = new ColorsManager(availableColors, corruptionColors, bf);
		ColorsManager colorsManagerFromFile = null;
		try {
			colorsManagerFromFile = new ColorsManagerFactory(configHelper, bf).factoryMethod();
		} catch (IOException e) {
			e.printStackTrace();
		}
		assertEquals(expectedColorsManager, colorsManagerFromFile);
	}
	
	@Test
	public void testColorsManagerCreationWithId() {
		ColorsManager expectedColorsManager = new ColorsManager(availableColors, corruptionColors, bf);
		ColorsManager colorsManagerFromFile = null;
		try {
			colorsManagerFromFile = new ColorsManagerFactory(configHelper, bf, "config1").factoryMethod();
		} catch (IOException e) {
			e.printStackTrace();
		}
		assertEquals(expectedColorsManager, colorsManagerFromFile);
	}

	@Test
	public void testColorsManagerCreationWithDifferentCorruptionColor() {
		corruptionColors.add(new NamedColor("bad_color", new Color(3, 4, 5)));
		ColorsManager expectedColorsManager = new ColorsManager(availableColors, corruptionColors, bf);
		ColorsManager colorsManagerFromFile = null;
		try {
			colorsManagerFromFile = new ColorsManagerFactory(configHelper, bf).factoryMethod();
		} catch (IOException e) {
			e.printStackTrace();
		}
		assertFalse(expectedColorsManager.equals(colorsManagerFromFile));
	}

	@Test
	public void testColorsManagerCreationWithDifferentAvailableColor() {
		availableColors.put("bad_color", new Color(3, 4, 5));
		ColorsManager expectedColorsManager = new ColorsManager(availableColors, corruptionColors, bf);
		ColorsManager colorsManagerFromFile = null;
		try {
			colorsManagerFromFile = new ColorsManagerFactory(configHelper, bf).factoryMethod();
		} catch (IOException e) {
			e.printStackTrace();
		}
		assertFalse(expectedColorsManager.equals(colorsManagerFromFile));
	}

}
