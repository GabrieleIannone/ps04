package it.polimi.ingsw.ps04.action.main;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.awt.Color;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.ps04.model.Model;
import it.polimi.ingsw.ps04.model.action.Action;
import it.polimi.ingsw.ps04.model.action.actionVisitor.ActionVisitor;
import it.polimi.ingsw.ps04.model.action.actionVisitor.ConcreteActionVisitor;
import it.polimi.ingsw.ps04.model.action.main.ElectCouncillor;
import it.polimi.ingsw.ps04.model.board.Council;
import it.polimi.ingsw.ps04.model.board.Councillor;
import it.polimi.ingsw.ps04.model.board.Region;
import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.setUpUtils.SetUp;
import it.polimi.ingsw.ps04.utils.color.NamedColor;

public class TestElectCouncillor {
	Model initialModel;
	Model expectedModel;
	ActionVisitor actionVisitor;
	Councillor addedCouncillor;

	@Before
	public void setUp() {
		initialModel = SetUp.getInitialModel();
		actionVisitor = new ConcreteActionVisitor(initialModel);
		Region initialCoast = initialModel.getMap().getRegionReference(new Region("coast"));

		Council initialCoastCouncil = initialCoast.getCouncil();

		expectedModel = SetUp.getInitialModel();
		Player expectedFirstPlayer = expectedModel.getPlayersManager().getFirstActivePlayer();
		expectedFirstPlayer.getCoins().addCoins(4);

		Region expectedCoast = expectedModel.getMap().getRegionReference(new Region("coast"));
		Council expectedCoastCouncil = expectedCoast.getCouncil();
		expectedCoastCouncil.getCouncillors().clear();
		expectedCoastCouncil.getCouncillors().addAll(initialCoastCouncil.getCouncillors());
		expectedCoastCouncil.getCouncillors().remove();
		addedCouncillor = new Councillor(new NamedColor("black", Color.BLACK));
		expectedCoastCouncil.getCouncillors().add(addedCouncillor);
	}

	@Test
	public void testAcquireBusinessPermitTile1() {
		Action action = new ElectCouncillor(new Region("coast"), addedCouncillor);
		action.accept(actionVisitor);
		initialModel.doAction(action);
		assertEquals(expectedModel, initialModel);
	}

	@Test
	public void testAcquireBusinessPermitTile2() {
		Action action = new ElectCouncillor(new Region("coast"), addedCouncillor);
		action.accept(actionVisitor);
		initialModel.doAction(action);
		initialModel.getPlayersManager().getFirstActivePlayer().getCoins().addCoins(1);
		assertFalse(expectedModel.equals(initialModel));
	}

}
