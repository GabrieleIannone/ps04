package it.polimi.ingsw.ps04.action;

import static org.junit.Assert.*;

import java.awt.Color;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.ps04.controller.ServerController;
import it.polimi.ingsw.ps04.controller.ServerControllerCreatorForTest;
import it.polimi.ingsw.ps04.model.Model;
import it.polimi.ingsw.ps04.model.action.Action;
import it.polimi.ingsw.ps04.model.action.main.ElectCouncillor;
import it.polimi.ingsw.ps04.model.action.quick.EngageAssistant;
import it.polimi.ingsw.ps04.model.board.Council;
import it.polimi.ingsw.ps04.model.board.Councillor;
import it.polimi.ingsw.ps04.model.board.Region;
import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.setUpUtils.SetUp;
import it.polimi.ingsw.ps04.utils.color.NamedColor;
import it.polimi.ingsw.ps04.utils.exception.ActionNotAvailableException;
import it.polimi.ingsw.ps04.utils.message.ActionMessage;
import it.polimi.ingsw.ps04.utils.message.market.SellingOffersMessage;
import it.polimi.ingsw.ps04.view.rmi.RMIView;

public class TestTwoPlayersOneTurn {
	ServerController initialController;
	SellingOffersMessage initialOffersMessage1;
	SellingOffersMessage initialOffersMessage2;
	RMIView initialRMIView1;
	RMIView initialRMIView2;
	Model initialModel;
	Model expectedModel;
	ActionMessage mainActionMessage;
	ActionMessage quickActionMessage;

	@Before
	public void setUp() {
		// Model SetUp
		initialModel = SetUp.getTwoPlayersModel();
		SetUp.populateModel(initialModel);
		expectedModel = SetUp.getTwoPlayersModel();
		SetUp.populateModel(expectedModel);

		// Controller SetUp
		initialController = ServerControllerCreatorForTest.createServerController(initialModel);

		// Player1 View SetUp
		Player initialPlayer1 = initialModel.getPlayersManager().getPlayerReference(new Player("player1"));
		initialRMIView1 = new RMIView(initialPlayer1, null);

		// Player2 View SetUp
		Player initialPlayer2 = initialModel.getPlayersManager().getPlayerReference(new Player("player2"));
		initialRMIView2 = new RMIView(initialPlayer2, null);

		// I pick the player in expectedModel who will do the turn
		Player initialActivePlayer = initialModel.getPlayersManager().getActivePlayer();
		Player expectedActivePlayer = expectedModel.getPlayersManager().getPlayerReference(initialActivePlayer);
		
		//I set assistant crew of expected active player in order to reflect the initial one
		int expectedAssistantNumber = expectedActivePlayer.getAssistantCrew().getMembersNumber();
		expectedActivePlayer.getAssistantCrew().sendAssistants(expectedAssistantNumber);
		int initialAssistantNumber = initialActivePlayer.getAssistantCrew().getMembersNumber();
		expectedActivePlayer.getAssistantCrew().engageAssistants(initialAssistantNumber);

		//I set coins of expected active player in order to reflect the initial one
		int expectedCoinsNumber = expectedActivePlayer.getCoins().getCoinsNumber();
		expectedActivePlayer.getCoins().payFor(expectedCoinsNumber);
		int initialCoinsNumber = initialActivePlayer.getCoins().getCoinsNumber();
		expectedActivePlayer.getCoins().addCoins(initialCoinsNumber);
		
		// Main action message creation: I choose Elect council
		Region initialCoast = initialModel.getMap().getRegionReference(new Region("coast"));
		Council initialCoastCouncil = initialCoast.getCouncil();
		Councillor addedCouncillor = new Councillor(new NamedColor("black", Color.BLACK));
		Action mainAction = new ElectCouncillor(new Region("coast"), addedCouncillor);
		mainActionMessage = new ActionMessage(mainAction);

		// I execute the choosen main action in expected model
		Region expectedCoast = expectedModel.getMap().getRegionReference(new Region("coast"));
		Council expectedCoastCouncil = expectedCoast.getCouncil();
		expectedCoastCouncil.getCouncillors().clear();
		expectedCoastCouncil.getCouncillors().addAll(initialCoastCouncil.getCouncillors());
		expectedCoastCouncil.getCouncillors().remove();
		expectedCoastCouncil.getCouncillors().add(addedCouncillor);
		mainAction = new ElectCouncillor(new Region("coast"), addedCouncillor);
		expectedActivePlayer.getCoins().addCoins(4);
		try {
			expectedActivePlayer.getActionsCounters().doMainAction();
		} catch (ActionNotAvailableException e) {
		}

		// Quick action message creation: I choose Engage new assistant
		Action quickAction = new EngageAssistant();
		quickActionMessage = new ActionMessage(quickAction);

		// I execute the choosen quick action in expected model
		expectedActivePlayer.getCoins().payFor(3);
		expectedActivePlayer.getAssistantCrew().engageAssistants(1);
		try {
			expectedActivePlayer.getActionsCounters().doQuickAction();
		} catch (ActionNotAvailableException e) {
		}

		// I pass the turn

	}

	@Test
	public void testTwoPlayersOneTurn() {
		
		initialController.initGame();
		
		// I get the active player
		Player initialActivePlayer = initialModel.getPlayersManager().getActivePlayer();
		Player expectedActivePlayer = expectedModel.getPlayersManager().getPlayerReference(initialActivePlayer);

		// Active player execute main action
		if (initialActivePlayer.getName().equals("player1")) {
			initialController.update(initialRMIView1, mainActionMessage);
		} else {
			initialController.update(initialRMIView2, mainActionMessage);
		}
		// Active player execute quick action
		if (initialActivePlayer.getName().equals("player1")) {
			initialController.update(initialRMIView1, quickActionMessage);
		} else {
			initialController.update(initialRMIView2, quickActionMessage);
		}
		
		//I check the number of coins
		int initialCoins = initialActivePlayer.getCoins().getCoinsNumber();
		int expectedCoins = expectedActivePlayer.getCoins().getCoinsNumber();
		assertTrue(initialCoins == expectedCoins);

		//I check the number of assistants
		int initialAssistants = initialActivePlayer.getAssistantCrew().getMembersNumber();
		int expectedAssistants = expectedActivePlayer.getAssistantCrew().getMembersNumber();
		assertTrue(initialAssistants == expectedAssistants);
		
		//I check if the turn has passed
		assertFalse(initialModel.getPlayersManager().getActivePlayer().getName().equals(expectedActivePlayer.getName()));

	}

}
