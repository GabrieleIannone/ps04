package it.polimi.ingsw.ps04.action.quick;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.ps04.model.Model;
import it.polimi.ingsw.ps04.model.action.Action;
import it.polimi.ingsw.ps04.model.action.actionVisitor.ActionVisitor;
import it.polimi.ingsw.ps04.model.action.actionVisitor.ConcreteActionVisitor;
import it.polimi.ingsw.ps04.model.action.quick.ChangeBusinessPermitTiles;
import it.polimi.ingsw.ps04.model.board.Region;
import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.setUpUtils.SetUp;

public class TestChangeBusinessPermitTiles {
	Model initialModel;
	Model expectedModel;
	Region initialCoast;
	ActionVisitor actionVisitor;
	
	@Before
	public void setUp(){
		initialModel = SetUp.getInitialModel();
		expectedModel = SetUp.getInitialModel();
		initialCoast = initialModel.getMap().getRegionReference(new Region("coast"));
		actionVisitor = new ConcreteActionVisitor(initialModel);
		
		Player expectedPlayer = expectedModel.getPlayersManager().getFirstActivePlayer();
		expectedPlayer.getAssistantCrew().sendAssistants(1);
	}
	
	@Test
	public void testChangeBusinessPermitTiles1(){
		Action action = new ChangeBusinessPermitTiles(new Region("coast"));
		action.accept(actionVisitor);
		initialModel.doAction(action);
		assertEquals(expectedModel, initialModel);
	}
	
	@Test
	public void testChangeBusinessPermitTiles2(){
		Action action = new ChangeBusinessPermitTiles(new Region("coast"));
		action.accept(actionVisitor);
		initialModel.doAction(action);
		initialModel.getPlayersManager().getFirstActivePlayer().getCoins().addCoins(1);
		assertFalse(expectedModel.equals(initialCoast));
	}


}
