package it.polimi.ingsw.ps04.action.quick;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.ps04.model.Model;
import it.polimi.ingsw.ps04.model.action.Action;
import it.polimi.ingsw.ps04.model.action.quick.EngageAssistant;
import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.setUpUtils.SetUp;

public class TestEngageAssistant {
	Model expectedModel;

	@Before
	public void setUp() {
		expectedModel = SetUp.getInitialModel();
		Player firstPlayer = expectedModel.getPlayersManager().getFirstActivePlayer();
		firstPlayer.getAssistantCrew().engageAssistants(1);
		firstPlayer.getCoins().payFor(3);
	}

	@Test
	public void testEngageAssistant() {
		Model actualModel = SetUp.getInitialModel();
		Action action = new EngageAssistant();
		actualModel.doAction(action);
		assertEquals(expectedModel, actualModel);
	}
	
	@Test
	public void testEngageAssistantWithDifferentCoins() {
		Model actualModel = SetUp.getInitialModel();
		Action action = new EngageAssistant();
		actualModel.doAction(action);
		Player firstPlayer = actualModel.getPlayersManager().getFirstActivePlayer();
		firstPlayer.getAssistantCrew().engageAssistants(1);
		firstPlayer.getCoins().addCoins(1);
		assertFalse(expectedModel.equals(actualModel));
	}
}
