package it.polimi.ingsw.ps04.action.quick;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.ps04.model.Model;
import it.polimi.ingsw.ps04.model.action.Action;
import it.polimi.ingsw.ps04.model.action.actionVisitor.ActionVisitor;
import it.polimi.ingsw.ps04.model.action.actionVisitor.ConcreteActionVisitor;
import it.polimi.ingsw.ps04.model.action.quick.GetAnotherPrimaryAction;
import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.setUpUtils.SetUp;

public class TestGetAnotherPrimaryAction {
	Model initialModel;
	Model expectedModel;
	ActionVisitor actionVisitor;

	@Before
	public void setUp(){
		initialModel = SetUp.getInitialModel();
		expectedModel = SetUp.getInitialModel();
		
		actionVisitor = new ConcreteActionVisitor(initialModel);
		
		Player initialPlayer = initialModel.getPlayersManager().getFirstActivePlayer(); 
		Player expectedPlayer = expectedModel.getPlayersManager().getFirstActivePlayer();
		
		initialPlayer.getAssistantCrew().engageAssistants(3);
		expectedPlayer.getActionsCounters().additionalMainAction();
	}
	
	@Test
	public void testGetAnotherPrimaryAction1(){
		Action action = new GetAnotherPrimaryAction();
		action.accept(actionVisitor);
		initialModel.doAction(action);
		assertEquals(expectedModel, initialModel);
	}
	
	@Test
	public void testGetAnotherPrimaryAction2(){
		Action action = new GetAnotherPrimaryAction();
		action.accept(actionVisitor);
		initialModel.doAction(action);
		initialModel.getPlayersManager().getFirstActivePlayer().getCoins().addCoins(1);
		assertFalse(expectedModel.equals(initialModel));
	}

}
