package it.polimi.ingsw.ps04.action.main;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.ps04.model.Model;
import it.polimi.ingsw.ps04.model.action.Action;
import it.polimi.ingsw.ps04.model.action.actionVisitor.ActionVisitor;
import it.polimi.ingsw.ps04.model.action.actionVisitor.ConcreteActionVisitor;
import it.polimi.ingsw.ps04.model.action.main.BuildEmporiumWithKing;
import it.polimi.ingsw.ps04.model.board.City;
import it.polimi.ingsw.ps04.model.board.Council;
import it.polimi.ingsw.ps04.model.board.Councillor;
import it.polimi.ingsw.ps04.model.deck.politic.PoliticCard;
import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.setUpUtils.SetUp;
import it.polimi.ingsw.ps04.utils.exception.PlayerHasAlreadyBuiltException;
import it.polimi.ingsw.ps04.model.bonus.Bonus;
import it.polimi.ingsw.ps04.model.bonus.bonusVisitor.ServerBonusVisitor;


public class TestBuildEmporiumWithKing {
	Model initialModel;
	Model expectedModel;
	ActionVisitor actionVisitor;
	List<PoliticCard> usedCards = new ArrayList<PoliticCard>();

	@Before
	public void setUp(){
		initialModel = SetUp.getInitialModel();
		expectedModel = SetUp.getInitialModel();
		actionVisitor = new ConcreteActionVisitor(initialModel);
		
		Player initialPlayer = initialModel.getPlayersManager().getFirstActivePlayer(); 
		Player expectedPlayer = expectedModel.getPlayersManager().getFirstActivePlayer();
		
		initialPlayer.getPoliticCardsHand().getHand().clear();
		expectedPlayer.getPoliticCardsHand().getHand().clear();
		
		Council kingCouncil = initialModel.getMap().getKing().getCouncil();
		for(Councillor kingCouncillor: kingCouncil.getCouncillors()){
			usedCards.add(new PoliticCard(kingCouncillor.getColor()));
		}
		initialPlayer.getPoliticCardsHand().getHand().addAll(usedCards);
		Set<Bonus> activatedBonuses = initialModel.getMap().getCityReference(new City("esti")).getBonus();
		Set<Bonus> colorBonuses = initialModel.getMap().getCityReference(new City("esti")).getColour().getBonus();
		ServerBonusVisitor bonusVisitor = new ServerBonusVisitor();
		for(Bonus bonus: activatedBonuses){
			bonus.accept(bonusVisitor, expectedPlayer);
		}
		for(Bonus bonus: colorBonuses){
			bonus.accept(bonusVisitor, expectedPlayer);
		}
		try {
			expectedModel.getMap().getCityReference(new City("esti")).build(expectedPlayer);
		} catch (PlayerHasAlreadyBuiltException e) {
		}
		expectedPlayer.getCoins().payFor(2);
		expectedPlayer.getUnusedEmporiums().removeEmporium();
		expectedModel.getMap().getKing().setActualCity(expectedModel.getMap().getCityReference(new City("esti")));
	}
	
	@Test
	public void testBuildEmporiumWithKing1(){
		Action action = new BuildEmporiumWithKing(new City("esti"), usedCards);
		action.accept(actionVisitor);
		initialModel.doAction(action);
		assertEquals(expectedModel, initialModel);
	}
	
	@Test
	public void testBuildEmporiumWithKing2(){
		Action action = new BuildEmporiumWithKing(new City("framek"), usedCards);
		action.accept(actionVisitor);
		initialModel.doAction(action);
		assertFalse(expectedModel.equals(initialModel));
	}

}
