package it.polimi.ingsw.ps04.action.main;

import static org.junit.Assert.*;

import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.ps04.model.Model;
import it.polimi.ingsw.ps04.model.action.Action;
import it.polimi.ingsw.ps04.model.action.actionVisitor.ActionVisitor;
import it.polimi.ingsw.ps04.model.action.actionVisitor.ConcreteActionVisitor;
import it.polimi.ingsw.ps04.model.action.main.BuildEmporium;
import it.polimi.ingsw.ps04.model.board.City;
import it.polimi.ingsw.ps04.model.board.Region;
import it.polimi.ingsw.ps04.model.bonus.Bonus;
import it.polimi.ingsw.ps04.model.bonus.bonusVisitor.ServerBonusVisitor;
import it.polimi.ingsw.ps04.model.deck.business.BusinessPermitTile;
import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.setUpUtils.SetUp;
import it.polimi.ingsw.ps04.utils.exception.AlreadyFaceDownException;
import it.polimi.ingsw.ps04.utils.exception.PlayerHasAlreadyBuiltException;

public class TestBuildEmporium {
	Model initialModel;
	Model expectedModel;
	ActionVisitor actionVisitor;
	BusinessPermitTile initialTile;
	City initialArkon;
	
	@Before
	public void setUp() {
		initialModel = SetUp.getInitialModel();
		actionVisitor = new ConcreteActionVisitor(initialModel);
		Region initialCoast = initialModel.getMap().getRegionReference(new Region("coast"));

		initialTile = initialCoast.getBusinessDeck().getRevealedTiles().get(0);
		initialCoast.getBusinessDeck().getRevealedTiles().remove(0);

		Player initialFirstPlayer = initialModel.getPlayersManager().getFirstActivePlayer();
		initialFirstPlayer.getBusinessPermitTilePool().addPermitTile(initialTile);

		expectedModel = SetUp.getInitialModel();
		Region expectedCoast = expectedModel.getMap().getRegionReference(new Region("coast"));

		BusinessPermitTile expectedTile = expectedCoast.getBusinessDeck().getRevealedTiles().get(0);

		Player expectedFirstPlayer = expectedModel.getPlayersManager().getFirstActivePlayer();
		expectedFirstPlayer.getBusinessPermitTilePool().addPermitTile(expectedTile);

		expectedCoast.getBusinessDeck().getRevealedTiles().remove(0);
		try {
			expectedTile.turnFaceDown();
		} catch (AlreadyFaceDownException e) {
			
		}
		City expectedArkon = expectedModel.getMap().getCityReference(new City("arkon"));
		try {
			expectedArkon.build(expectedFirstPlayer);
		} catch (PlayerHasAlreadyBuiltException e) {
			
		}
		expectedFirstPlayer.getUnusedEmporiums().removeEmporium();
		initialArkon = initialModel.getMap().getCityReference(expectedArkon);
		Set<Bonus> activatedBonuses = initialArkon.getBonus();
		Set<Bonus> colorBonuses = initialArkon.getColour().getBonus();
		ServerBonusVisitor expectedBonusVisitor = new ServerBonusVisitor();
		for (Bonus bonus : activatedBonuses) {
			bonus.accept(expectedBonusVisitor, expectedFirstPlayer);
		}
		for (Bonus bonus : colorBonuses) {
			bonus.accept(expectedBonusVisitor, expectedFirstPlayer);
		}
	}

	@Test
	public void testBuildEmporium1() {
		Action action = new BuildEmporium(initialArkon, initialTile);
		action.accept(actionVisitor);
		initialModel.doAction(action);
		assertEquals(expectedModel, initialModel);
	}
	
	@Test
	public void testBuildEmporium2() {
		Action action = new BuildEmporium(initialArkon, initialTile);
		action.accept(actionVisitor);
		initialModel.doAction(action);
		initialModel.getPlayersManager().getFirstActivePlayer().getCoins().addCoins(1);
		assertFalse(expectedModel.equals(initialModel));
	}

}
