package it.polimi.ingsw.ps04.action.main;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.ps04.model.Model;
import it.polimi.ingsw.ps04.model.action.Action;
import it.polimi.ingsw.ps04.model.action.actionVisitor.ActionVisitor;
import it.polimi.ingsw.ps04.model.action.actionVisitor.ConcreteActionVisitor;
import it.polimi.ingsw.ps04.model.action.main.AcquireBusinessPermitTile;
import it.polimi.ingsw.ps04.model.board.Council;
import it.polimi.ingsw.ps04.model.board.Councillor;
import it.polimi.ingsw.ps04.model.board.Region;
import it.polimi.ingsw.ps04.model.deck.business.BusinessPermitTile;
import it.polimi.ingsw.ps04.model.deck.politic.PoliticCard;
import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.setUpUtils.SetUp;

public class TestAcquireBusinessPermitTile {
	Model initialModel;
	Model expectedModel;
	ActionVisitor actionVisitor;
	List<PoliticCard> usedCards = new ArrayList<PoliticCard>();

	@Before
	public void setUp() {
		initialModel = SetUp.getInitialModel();
		actionVisitor = new ConcreteActionVisitor(initialModel);
		Region initialCoast = initialModel.getMap().getRegionReference(new Region("coast"));
		
		BusinessPermitTile tile = initialCoast.getBusinessDeck().getRevealedTiles().get(0);
		
		Council coastCouncil = initialCoast.getCouncil();
		Player initialFirstPlayer = initialModel.getPlayersManager().getFirstActivePlayer();
		initialFirstPlayer.getPoliticCardsHand().getHand().clear();
		for(Councillor councillor: coastCouncil.getCouncillors()){
			PoliticCard politicCard = new PoliticCard(councillor.getColor());
			usedCards.add(politicCard);
			initialFirstPlayer.addPoliticCard(politicCard);
		}
		expectedModel = SetUp.getInitialModel();
		Player expectedFirstPlayer = expectedModel.getPlayersManager().getFirstActivePlayer();
		expectedFirstPlayer.getPoliticCardsHand().getHand().clear();
		expectedFirstPlayer.getBusinessPermitTilePool().addPermitTile(tile);
		expectedFirstPlayer.getCoins().addCoins(5);
		expectedFirstPlayer.getPoints().addPoints(6);
		
		Region expectedCoast = expectedModel.getMap().getRegionReference(new Region("coast"));
		expectedCoast.getBusinessDeck().getRevealedTiles().remove(0);
	}

	@Test
	public void testAcquireBusinessPermitTile1() {
		Action action = new AcquireBusinessPermitTile(usedCards, new Region("coast"), 1);
		action.accept(actionVisitor);
		initialModel.doAction(action);
		assertEquals(expectedModel, initialModel);
	}

	@Test
	public void testAcquireBusinessPermitTile2() {
		Action action = new AcquireBusinessPermitTile(usedCards, new Region("coast"), 1);
		action.accept(actionVisitor);
		initialModel.doAction(action);
		expectedModel.getPlayersManager().getFirstActivePlayer().getCoins().addCoins(1);
		assertFalse(expectedModel.equals(initialModel));
	}

}
