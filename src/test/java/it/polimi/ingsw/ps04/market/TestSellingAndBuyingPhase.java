package it.polimi.ingsw.ps04.market;

import static org.junit.Assert.*;

import java.awt.Color;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.ps04.controller.ServerController;
import it.polimi.ingsw.ps04.controller.ServerControllerCreatorForTest;
import it.polimi.ingsw.ps04.model.Model;
import it.polimi.ingsw.ps04.model.deck.business.BusinessPermitTile;
import it.polimi.ingsw.ps04.model.deck.politic.PoliticCard;
import it.polimi.ingsw.ps04.model.market.AssistantsonSale;
import it.polimi.ingsw.ps04.model.market.BusinessPermitTileonSale;
import it.polimi.ingsw.ps04.model.market.BuyingOrder;
import it.polimi.ingsw.ps04.model.market.Market;
import it.polimi.ingsw.ps04.model.market.PoliticCardonSale;
import it.polimi.ingsw.ps04.model.market.SellingOffers;
import it.polimi.ingsw.ps04.model.player.AssistantsCrew;
import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.setUpUtils.SetUp;
import it.polimi.ingsw.ps04.utils.color.NamedColor;
import it.polimi.ingsw.ps04.utils.message.market.BuyingOrdersMessage;
import it.polimi.ingsw.ps04.utils.message.market.SellingOffersMessage;
import it.polimi.ingsw.ps04.view.View;
import it.polimi.ingsw.ps04.view.rmi.RMIView;

public class TestSellingAndBuyingPhase {
	ServerController initialController;
	SellingOffersMessage initialOffersMessage1;
	SellingOffersMessage initialOffersMessage2;
	View initialRMIView1;
	View initialRMIView2;
	Market initialMarket;
	Market expectedMarket;
	Model initialModel;
	BuyingOrdersMessage initialBuyingMessage1;
	BuyingOrdersMessage initialBuyingMessage2;

	@Before
	public void setUp() {
		// Model SetUp
		initialModel = SetUp.getTwoPlayersModel();
		SetUp.populateModel(initialModel);
		Model expectedModel = SetUp.getTwoPlayersModel();
		SetUp.populateModel(expectedModel);

		// Controller SetUp
		initialController = ServerControllerCreatorForTest.createServerController(initialModel);

		// Starting Market
		initialController.startMarketPhase();
		initialMarket = initialModel.getMarket();
		expectedModel.startMarketPhase();
		expectedMarket = expectedModel.getMarket();

		// Player1 View SetUp
		Player initialPlayer1 = initialModel.getPlayersManager().getPlayerReference(new Player("player1"));
		initialRMIView1 = new RMIView(initialPlayer1, null);

		// Player2 View SetUp
		Player initialPlayer2 = initialModel.getPlayersManager().getPlayerReference(new Player("player2"));
		initialRMIView2 = new RMIView(initialPlayer2, null);

		// Player1 ExpectedOffer SetUp
		Player expectedPlayer1 = expectedModel.getPlayersManager().getPlayerReference(new Player("player1"));
		AssistantsonSale expectedAssistants1 = new AssistantsonSale(new AssistantsCrew(1), 1);
		BusinessPermitTile expectedTile1 = expectedPlayer1.getBusinessPermitTilePool().getBusinessPermitTileAt(0);
		BusinessPermitTileonSale expectedTileonSale1 = new BusinessPermitTileonSale(expectedTile1, 1);
		PoliticCardonSale expectedPoliticCardonSale1 = new PoliticCardonSale(
				new PoliticCard(new NamedColor("black", Color.BLACK)), 1);
		SellingOffers expectedOffer1 = new SellingOffers();
		expectedOffer1.setAssistantsonSale(expectedAssistants1);
		expectedOffer1.addTileOnSale(expectedTileonSale1);
		expectedOffer1.addCardOnSale(expectedPoliticCardonSale1);
		expectedModel.getMarket().getOffers().put(expectedPlayer1.getName(), expectedOffer1);

		// Player2 Expected Offer SetUp
		Player expectedPlayer2 = expectedModel.getPlayersManager().getPlayerReference(new Player("player2"));
		AssistantsonSale expectedAssistants2 = new AssistantsonSale(new AssistantsCrew(2), 2);
		SellingOffers expectedOffer2 = new SellingOffers();
		expectedOffer2.setAssistantsonSale(expectedAssistants2);
		expectedModel.getMarket().getOffers().put(expectedPlayer2.getName(), expectedOffer2);

		// Player1 Initial Offer SetUp
		AssistantsonSale initialAssistants1 = new AssistantsonSale(new AssistantsCrew(2), 1);
		BusinessPermitTile initialTile1 = initialPlayer1.getBusinessPermitTilePool().getBusinessPermitTileAt(0);
		BusinessPermitTileonSale initialTileonSale1 = new BusinessPermitTileonSale(initialTile1, 1);
		PoliticCardonSale initialPoliticCardonSale1 = new PoliticCardonSale(
				new PoliticCard(new NamedColor("black", Color.BLACK)), 1);
		SellingOffers initialOffer1 = new SellingOffers();
		initialOffer1.setAssistantsonSale(initialAssistants1);
		initialOffer1.addTileOnSale(initialTileonSale1);
		initialOffer1.addCardOnSale(initialPoliticCardonSale1);

		initialOffersMessage1 = new SellingOffersMessage(initialOffer1);

		// Player2 Initial Offer SetUp
		AssistantsonSale initialAssistants2 = new AssistantsonSale(new AssistantsCrew(3), 2);
		SellingOffers initialOffer2 = new SellingOffers();
		initialOffer2.setAssistantsonSale(initialAssistants2);

		initialOffersMessage2 = new SellingOffersMessage(initialOffer2);

		BuyingOrder initialOrder1 = new BuyingOrder();
		initialOrder1.setAssistants(new AssistantsCrew(1));
		HashMap<String, BuyingOrder> buyingOrder1 = new HashMap<String, BuyingOrder>();
		buyingOrder1.put(initialPlayer2.getName(), initialOrder1);
		initialBuyingMessage1 = new BuyingOrdersMessage(buyingOrder1);

		BuyingOrder initialOrder2 = new BuyingOrder();
		initialOrder2.setAssistants(new AssistantsCrew(1));
		HashMap<String, BuyingOrder> buyingOrder2 = new HashMap<String, BuyingOrder>();
		buyingOrder2.put(initialPlayer1.getName(), initialOrder1);
		initialBuyingMessage2 = new BuyingOrdersMessage(buyingOrder2);

		expectedPlayer1.getCoins().payFor(1);
		expectedPlayer2.getCoins().addCoins(1);
	}

	@Test
	public void testSellingPhase1() {

		int startingAssistant1 = initialMarket.getPlayersManager().getPlayerReference(new Player("player1"))
				.getAssistantCrew().getMembersNumber();
		int startingCoins1 = initialMarket.getPlayersManager().getPlayerReference(new Player("player1")).getCoins()
				.getCoinsNumber();

		int startingAssistant2 = initialMarket.getPlayersManager().getPlayerReference(new Player("player2"))
				.getAssistantCrew().getMembersNumber();
		int startingCoins2 = initialMarket.getPlayersManager().getPlayerReference(new Player("player2")).getCoins()
				.getCoinsNumber();

		// Selling Phase
		Player firstPlayer = initialModel.getMarket().getPlayersManager().getActivePlayer();
		if (firstPlayer.getName().equals("player1")) {
			initialController.update(initialRMIView1, initialOffersMessage1);
			initialController.update(initialRMIView2, initialOffersMessage2);
		} else {
			initialController.update(initialRMIView2, initialOffersMessage2);
			initialController.update(initialRMIView1, initialOffersMessage1);
		}
		Map<String, SellingOffers> expectedOffers = expectedMarket.getOffers();
		Map<String, SellingOffers> initialOffers = initialMarket.getOffers();

		// Buying Phase
		firstPlayer = initialModel.getMarket().getPlayersManager().getActivePlayer();
		if (firstPlayer.getName().equals("player1")) {
			initialController.update(initialRMIView1, initialBuyingMessage1);

			SellingOffers expectedOffers2 = expectedOffers.get("player2");
			SellingOffers initialOffers2 = initialOffers.get("player2");
			assertEquals(expectedOffers2, initialOffers2);

			initialController.update(initialRMIView2, initialBuyingMessage2);
		} else {
			initialController.update(initialRMIView2, initialBuyingMessage2);

			SellingOffers expectedOffers1 = expectedOffers.get("player1");
			SellingOffers initialOffers1 = initialOffers.get("player1");
			assertEquals(expectedOffers1, initialOffers1);

			initialController.update(initialRMIView1, initialBuyingMessage1);
		}

		SellingOffers initialOffers1 = initialOffers.get("player1");
		SellingOffers initialOffers2 = initialOffers.get("player2");

		int finalAssistant1 = initialMarket.getPlayersManager().getPlayerReference(new Player("player1"))
				.getAssistantCrew().getMembersNumber();
		int finalCoins1 = initialMarket.getPlayersManager().getPlayerReference(new Player("player1")).getCoins()
				.getCoinsNumber();

		int finalAssistant2 = initialMarket.getPlayersManager().getPlayerReference(new Player("player2"))
				.getAssistantCrew().getMembersNumber();
		int finalCoins2 = initialMarket.getPlayersManager().getPlayerReference(new Player("player2")).getCoins()
				.getCoinsNumber();

		assertTrue(initialOffers1 == null && initialOffers2 == null);
		assertTrue(startingAssistant1 == finalAssistant1 && startingAssistant2 == finalAssistant2
				&& startingCoins1 == finalCoins1 + 1 && startingCoins2 == finalCoins2 - 1);
	}

}
