package it.polimi.ingsw.ps04.market;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.ps04.model.Model;
import it.polimi.ingsw.ps04.model.market.AssistantsonSale;
import it.polimi.ingsw.ps04.model.market.SellingOffers;
import it.polimi.ingsw.ps04.model.player.AssistantsCrew;
import it.polimi.ingsw.ps04.setUpUtils.SetUp;
import it.polimi.ingsw.ps04.utils.message.market.SellingOffersMessage;

public class SellAnAssistant {
	Model initialModel;
	Model expectedModel;
	SellingOffersMessage offerMessage;

	@Before
	public void SetUp(){
		initialModel = SetUp.getInitialModel();
		expectedModel = SetUp.getInitialModel();
		AssistantsCrew assistants = new AssistantsCrew(1);
		AssistantsonSale assistantsonSale = new AssistantsonSale(assistants, 2);
		SellingOffers offer = new SellingOffers();
		offer.setAssistantsonSale(assistantsonSale);
		offerMessage = new SellingOffersMessage(offer);
		expectedModel.getMarket().startSellingPhase();
	}
	
	@Test
	public void test(){
		initialModel.startMarketPhase();
		initialModel.manageMarket(offerMessage);
		assertEquals(expectedModel, initialModel);
	}

}
