package it.polimi.ingsw.ps04.market;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.awt.Color;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.ps04.controller.ServerController;
import it.polimi.ingsw.ps04.controller.ServerControllerCreatorForTest;
import it.polimi.ingsw.ps04.model.Model;
import it.polimi.ingsw.ps04.model.deck.business.BusinessPermitTile;
import it.polimi.ingsw.ps04.model.deck.politic.PoliticCard;
import it.polimi.ingsw.ps04.model.market.AssistantsonSale;
import it.polimi.ingsw.ps04.model.market.BusinessPermitTileonSale;
import it.polimi.ingsw.ps04.model.market.Market;
import it.polimi.ingsw.ps04.model.market.PoliticCardonSale;
import it.polimi.ingsw.ps04.model.market.SellingOffers;
import it.polimi.ingsw.ps04.model.player.AssistantsCrew;
import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.setUpUtils.SetUp;
import it.polimi.ingsw.ps04.utils.color.NamedColor;
import it.polimi.ingsw.ps04.utils.message.market.SellingOffersMessage;
import it.polimi.ingsw.ps04.view.rmi.RMIView;

public class TestSellingPhase {
	ServerController initialController;
	SellingOffersMessage initialOffersMessage1;
	SellingOffersMessage initialOffersMessage2;
	RMIView initialRMIView1;
	RMIView initialRMIView2;
	Market initialMarket;
	Market expectedMarket;
	Model initialModel;

	@Before
	public void setUp() {
		initialModel = SetUp.getTwoPlayersModel();
		SetUp.populateModel(initialModel);
		Model expectedModel = SetUp.getTwoPlayersModel();
		SetUp.populateModel(expectedModel);

		initialController = ServerControllerCreatorForTest.createServerController(initialModel);

		initialController.startMarketPhase();
		initialMarket = initialModel.getMarket();
		expectedModel.startMarketPhase();
		expectedMarket = expectedModel.getMarket();

		Player initialPlayer1 = initialModel.getPlayersManager().getPlayerReference(new Player("player1"));
		initialRMIView1 = new RMIView(initialPlayer1, null);

		Player initialPlayer2 = initialModel.getPlayersManager().getPlayerReference(new Player("player2"));
		initialRMIView2 = new RMIView(initialPlayer2, null);

		Player expectedPlayer1 = expectedModel.getPlayersManager().getPlayerReference(new Player("player1"));
		AssistantsonSale expectedAssistants1 = new AssistantsonSale(new AssistantsCrew(1), 1);
		BusinessPermitTile expectedTile1 = expectedPlayer1.getBusinessPermitTilePool().getBusinessPermitTileAt(0);
		BusinessPermitTileonSale expectedTileonSale1 = new BusinessPermitTileonSale(expectedTile1, 1);
		PoliticCardonSale expectedPoliticCardonSale1 = new PoliticCardonSale(
				new PoliticCard(new NamedColor("black", Color.BLACK)), 1);
		SellingOffers expectedOffer1 = new SellingOffers();
		expectedOffer1.setAssistantsonSale(expectedAssistants1);
		expectedOffer1.addTileOnSale(expectedTileonSale1);
		expectedOffer1.addCardOnSale(expectedPoliticCardonSale1);
		expectedModel.getMarket().getOffers().put(expectedPlayer1.getName(), expectedOffer1);

		Player expectedPlayer2 = expectedModel.getPlayersManager().getPlayerReference(new Player("player2"));
		AssistantsonSale expectedAssistants2 = new AssistantsonSale(new AssistantsCrew(2), 2);
		SellingOffers expectedOffer2 = new SellingOffers();
		expectedOffer2.setAssistantsonSale(expectedAssistants2);
		expectedModel.getMarket().getOffers().put(expectedPlayer2.getName(), expectedOffer2);

		AssistantsonSale initialAssistants1 = new AssistantsonSale(new AssistantsCrew(1), 1);
		BusinessPermitTile initialTile1 = initialPlayer1.getBusinessPermitTilePool().getBusinessPermitTileAt(0);
		BusinessPermitTileonSale initialTileonSale1 = new BusinessPermitTileonSale(initialTile1, 1);
		PoliticCardonSale initialPoliticCardonSale1 = new PoliticCardonSale(
				new PoliticCard(new NamedColor("black", Color.BLACK)), 1);
		SellingOffers initialOffer1 = new SellingOffers();
		initialOffer1.setAssistantsonSale(initialAssistants1);
		initialOffer1.addTileOnSale(initialTileonSale1);
		initialOffer1.addCardOnSale(initialPoliticCardonSale1);

		initialOffersMessage1 = new SellingOffersMessage(initialOffer1);

		AssistantsonSale initialAssistants2 = new AssistantsonSale(new AssistantsCrew(2), 2);
		SellingOffers initialOffer2 = new SellingOffers();
		initialOffer2.setAssistantsonSale(initialAssistants2);

		initialOffersMessage2 = new SellingOffersMessage(initialOffer2);
	}

	@Test
	public void testSellingPhase1() {
		Player firstPlayer = initialModel.getMarket().getPlayersManager().getActivePlayer();
		if (firstPlayer.getName().equals("player1")) {
			initialController.update(initialRMIView1, initialOffersMessage1);
			initialController.update(initialRMIView2, initialOffersMessage2);
		} else {
			initialController.update(initialRMIView2, initialOffersMessage2);
			initialController.update(initialRMIView1, initialOffersMessage1);
		}
		Map<String, SellingOffers> expectedOffers = expectedMarket.getOffers();
		Map<String, SellingOffers> initialOffers = initialMarket.getOffers();
		SellingOffers expectedOffers1 = expectedOffers.get("player1");
		SellingOffers expectedOffers2 = expectedOffers.get("player2");

		SellingOffers initialOffers1 = initialOffers.get("player1");
		SellingOffers initialOffers2 = initialOffers.get("player2");

		assertTrue(expectedOffers1.equals(initialOffers1) && expectedOffers2.equals(initialOffers2));
	}

	@Test
	public void testSellingPhase2() {
		initialOffersMessage2.getSellingOffers().getAssistantsonSale().getAssistantCrew().engageAssistants(1);
		Player firstPlayer = initialModel.getMarket().getPlayersManager().getActivePlayer();
		if (firstPlayer.getName().equals("player1")) {
			initialController.update(initialRMIView1, initialOffersMessage1);
			initialController.update(initialRMIView2, initialOffersMessage2);
		} else {
			initialController.update(initialRMIView2, initialOffersMessage2);
			initialController.update(initialRMIView1, initialOffersMessage1);
		}
		Map<String, SellingOffers> expectedOffers = expectedMarket.getOffers();
		Map<String, SellingOffers> initialOffers = initialMarket.getOffers();
		SellingOffers expectedOffers1 = expectedOffers.get("player1");
		SellingOffers expectedOffers2 = expectedOffers.get("player2");

		SellingOffers initialOffers1 = initialOffers.get("player1");
		SellingOffers initialOffers2 = initialOffers.get("player2");

		assertFalse(expectedOffers1.equals(initialOffers1) && expectedOffers2.equals(initialOffers2));
	}

}
