package it.polimi.ingsw.ps04.setUpUtils;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import it.polimi.ingsw.ps04.controller.ServerController;
import it.polimi.ingsw.ps04.controller.ServerControllerCreatorForTest;
import it.polimi.ingsw.ps04.model.GameFactory;
import it.polimi.ingsw.ps04.model.GameModelView;
import it.polimi.ingsw.ps04.model.Model;
import it.polimi.ingsw.ps04.model.board.City;
import it.polimi.ingsw.ps04.model.bonus.Bonus;
import it.polimi.ingsw.ps04.model.deck.business.BusinessPermitTile;
import it.polimi.ingsw.ps04.model.deck.politic.PoliticCard;
import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.utils.ConfigHelper;
import it.polimi.ingsw.ps04.utils.color.NamedColor;
import it.polimi.ingsw.ps04.utils.exception.BadConfigFileException;
import it.polimi.ingsw.ps04.view.TestView;
import it.polimi.ingsw.ps04.view.View;

public class SetUp {

	static String TEST_CONFING_FILE = "src" + File.separator + "test" + File.separator + "TestGameCreation.json";
	static String COMPLEX_MODEL_CONFIG_FILE = "src" + File.separator + "main" + File.separator + "config.json";

	public static Model getInitialModel() {
		List<String> playersNames = new ArrayList<>();
		playersNames.add("player1");
		try {
			return GameFactory.createGame(playersNames, new ConfigHelper(new File(TEST_CONFING_FILE)));
		} catch (IOException | BadConfigFileException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static void connectMVC(View view, ServerController controller, Model model, GameModelView gmv, TestView testView) {
		controller = ServerControllerCreatorForTest.createServerController(model);
		model.addObserver(gmv);
		view.addObserver(controller);
		gmv.addObserver(testView);
	}

	public static Model getTwoPlayersModel() {
		List<String> playersNames = new ArrayList<>();
		playersNames.add("player1");
		playersNames.add("player2");
		try {
			return GameFactory.createGame(playersNames, new ConfigHelper(new File(TEST_CONFING_FILE)));
		} catch (IOException | BadConfigFileException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static void populateModel(Model model) {

		Player player1 = model.getPlayersManager().getPlayerReference(new Player("player1"));
		Player player2 = model.getPlayersManager().getPlayerReference(new Player("player2"));

		Set<City> cities1 = new HashSet<City>();
		cities1.add(new City("city1"));
		cities1.add(new City("city2"));

		Set<Bonus> bonuses1 = new HashSet<Bonus>();

		BusinessPermitTile fakeTile1 = new BusinessPermitTile(cities1, bonuses1);
		player1.getBusinessPermitTilePool().addPermitTile(fakeTile1);

		Set<City> cities2 = new HashSet<City>();
		cities2.add(new City("city1"));
		cities2.add(new City("city3"));

		BusinessPermitTile fakeTile2 = new BusinessPermitTile(cities2, bonuses1);
		player2.getBusinessPermitTilePool().addPermitTile(fakeTile2);

		player1.getAssistantCrew().engageAssistants(5);
		player2.getAssistantCrew().engageAssistants(3);

		player1.getCoins().addCoins(10);
		player2.getCoins().addCoins(15);

		player1.getPoliticCardsHand().getHand().clear();
		player1.getPoliticCardsHand().getHand().add(new PoliticCard(new NamedColor("black", Color.BLACK)));
		player1.getPoliticCardsHand().getHand().add(new PoliticCard(new NamedColor("orange", Color.ORANGE)));
		player1.getPoliticCardsHand().getHand().add(new PoliticCard(new NamedColor("pink", Color.PINK)));

		player2.getPoliticCardsHand().getHand().clear();
		player2.getPoliticCardsHand().getHand().add(new PoliticCard(new NamedColor("black", Color.BLACK)));
		player2.getPoliticCardsHand().getHand().add(new PoliticCard(new NamedColor("black", Color.BLACK)));
		player2.getPoliticCardsHand().getHand().add(new PoliticCard(new NamedColor("pink", Color.PINK)));
	}

}
