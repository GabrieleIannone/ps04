package it.polimi.ingsw.ps04.model.bonus;

import static org.junit.Assert.*;

import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.ps04.controller.ServerController;
import it.polimi.ingsw.ps04.controller.ServerControllerCreatorForTest;
import it.polimi.ingsw.ps04.model.Model;
import it.polimi.ingsw.ps04.model.board.City;
import it.polimi.ingsw.ps04.model.bonus.bonusVisitor.BonusVisitor;
import it.polimi.ingsw.ps04.model.bonus.bonusVisitor.ServerBonusVisitor;
import it.polimi.ingsw.ps04.model.bonus.complex.BonusfromCityBonus;
import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.setUpUtils.SetUp;
import it.polimi.ingsw.ps04.utils.message.Message;
import it.polimi.ingsw.ps04.utils.message.RequestforBonus;
import it.polimi.ingsw.ps04.view.View;
import it.polimi.ingsw.ps04.view.rmi.RMIView;

public class TestComplexBonus {
	Model initialModel;
	Model expectedModel;
	View initialRMIView;
	ServerController initialController;
	BonusVisitor bonusVisitor;
	Player initialActivePlayer;
	Player expectedActivePlayer;
	BonusfromCityBonus complexBonus;
	
	@Before
	public void setUp() {
		//Model SetUp
		initialModel = SetUp.getInitialModel();
		Model expectedModel = SetUp.getInitialModel();
		
		//Initial Active Player Set Up
		initialActivePlayer = initialModel.getPlayersManager().getActivePlayer();
		
		//Controller SetUp
		initialController = ServerControllerCreatorForTest.createServerController(initialModel);

		//Player1 View SetUp
		initialRMIView = new RMIView(initialActivePlayer, null);
		
		//Bonus Visitor SetUp
		bonusVisitor = new ServerBonusVisitor();
		
		//Complex Bonus SetUp. I choose BonusFromCityBonus
		complexBonus = new BonusfromCityBonus(1, initialModel);
		
		//Expected Active Player Set Up
		expectedActivePlayer = expectedModel.getPlayersManager().getActivePlayer();
		City burgen = initialModel.getMap().getCityReference(new City("burgen"));
		Set<Bonus> burgenBonuses = burgen.getBonus();
		for(Bonus bonus: burgenBonuses){
			bonus.accept(bonusVisitor, expectedActivePlayer);
		}
	}
	
	@Test
	public void testComplexBonus(){
		//Accepting bonus
		complexBonus.accept(bonusVisitor, initialActivePlayer);
		
		//Compiling Bonus
		complexBonus.getCities().add(new City("burgen"));
		Message message = new RequestforBonus(complexBonus);
		
		//Sending Bonus
		initialController.update(initialRMIView, message);
		
		assertEquals(initialActivePlayer, expectedActivePlayer);
	}
}
