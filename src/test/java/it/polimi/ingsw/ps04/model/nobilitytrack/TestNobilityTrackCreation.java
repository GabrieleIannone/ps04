package it.polimi.ingsw.ps04.model.nobilitytrack;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.ps04.model.Model;
import it.polimi.ingsw.ps04.model.bonus.Bonus;
import it.polimi.ingsw.ps04.model.bonus.BonusFactory;
import it.polimi.ingsw.ps04.model.bonus.simple.AssistantsBonus;
import it.polimi.ingsw.ps04.model.bonus.simple.VictoryPointsBonus;
import it.polimi.ingsw.ps04.model.nobilitytrack.NobilityTrack;
import it.polimi.ingsw.ps04.model.nobilitytrack.NobilityTrackFactory;
import it.polimi.ingsw.ps04.utils.ConfigHelper;
import it.polimi.ingsw.ps04.utils.exception.BadConfigFileException;

public class TestNobilityTrackCreation {

	private Model model;
	String TEST_CONFING_FILE = "src" + File.separator + "test" + File.separator + "TestGameCreation.json";
	ConfigHelper configHelper;
	BonusFactory bf;

	private void setNobilityTrack() {
		Map<Integer, Set<Bonus>> track = new HashMap<>();
		Set<Bonus> bonuses1 = new HashSet<>();
		bonuses1.add(new VictoryPointsBonus(1));
		bonuses1.add(new AssistantsBonus(1));
		track.put(1, bonuses1);

		Set<Bonus> bonuses3 = new HashSet<>();
		bonuses3.add(new AssistantsBonus(1));
		track.put(3, bonuses3);
		NobilityTrack nobilityTrack = new NobilityTrack(track);
		model.setNobilityTrack(nobilityTrack);
	}

	@Before
	public void setUp() {
		model = new Model();
		configHelper = new ConfigHelper(new File(TEST_CONFING_FILE));
		bf = new BonusFactory(model, configHelper);
		setNobilityTrack();
	}

	
	@Test
	public void testNobilityTrackCreationFromFile() {
		try {
			NobilityTrackFactory ntf = new NobilityTrackFactory(configHelper, bf, 0);
			NobilityTrack nt = ntf.factoryMethod();
			assertEquals(model.getNobilityTrack(), nt);
		} catch (IOException | BadConfigFileException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testNobilityTrackCreationFromFileWithDifferentSteps() {
		try {
			NobilityTrackFactory ntf = new NobilityTrackFactory(configHelper, bf, 1);
			NobilityTrack nt = ntf.factoryMethod();
			assertFalse(model.getNobilityTrack().equals(nt));
		} catch (IOException | BadConfigFileException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testNobilityTrackCreationFromFileWithDifferentBonus() {
		try {
			NobilityTrackFactory ntf = new NobilityTrackFactory(configHelper, bf, 2);
			NobilityTrack nt = ntf.factoryMethod();
			assertFalse(model.getNobilityTrack().equals(nt));
		} catch (IOException | BadConfigFileException e) {
			e.printStackTrace();
		}
	}

}
