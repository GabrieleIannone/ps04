package it.polimi.ingsw.ps04.model.deck.politic;

import static org.junit.Assert.*;

import java.awt.Color;
import java.util.HashSet;
import java.util.Set;


import org.junit.Test;

import it.polimi.ingsw.ps04.utils.color.NamedColor;

public class TestPoliticDeck {

	@Test
	public void testGetCorruptionsColor(){
		Set<NamedColor> expectedCorruptionColors = new HashSet<>();
		expectedCorruptionColors.add(new NamedColor("black", Color.BLACK));
		expectedCorruptionColors.add(new NamedColor("blue", Color.BLUE));
		PoliticDeck deck = new PoliticDeck(expectedCorruptionColors);
		assertEquals(expectedCorruptionColors, deck.getCorruptionColors());
	}
	
	@Test
	public void testGetCorruptionColor(){
		Set<NamedColor> expectedCorruptionColors = new HashSet<>();
		NamedColor black = new NamedColor("black", Color.BLACK);
		expectedCorruptionColors.add(black);
		expectedCorruptionColors.add(new NamedColor("blue", Color.BLUE));
		PoliticDeck deck = new PoliticDeck(expectedCorruptionColors);
		assertEquals(black, deck.getCorruptionColor("black"));
	}
}
