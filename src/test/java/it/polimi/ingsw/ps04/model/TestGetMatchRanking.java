package it.polimi.ingsw.ps04.model;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.setUpUtils.SetUp;

public class TestGetMatchRanking {
	Model model;

	@Before
	public void setUp() {
		model = SetUp.getTwoPlayersModel();
	}

	@Test
	public void testHighestNobilityLevel() {
		List<Player> players = model.getPlayersManager().getAllPlayers();
		Player player1 = players.get(0);
		player1.getNobilityLevel().increaseLevel();
		assertEquals(player1.getName(), model.getMatchRanking().get(0).getName());
	}
	
	@Test
	public void testEqualNobilityLevel() {
		List<Player> players = model.getPlayersManager().getAllPlayers();
		Player player1 = players.get(0);
		Player player2 = players.get(0);
		player1.getNobilityLevel().increaseLevel();
		player2.getNobilityLevel().increaseLevel();
		player2.getAssistantCrew().engageAssistants(20);
		assertEquals(player2.getName(), model.getMatchRanking().get(0).getName());
	}

}
