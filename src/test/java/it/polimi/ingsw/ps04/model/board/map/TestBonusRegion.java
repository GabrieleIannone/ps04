package it.polimi.ingsw.ps04.model.board.map;

import static org.junit.Assert.assertEquals;

import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.ps04.model.Model;
import it.polimi.ingsw.ps04.model.board.City;
import it.polimi.ingsw.ps04.model.bonus.Bonus;
import it.polimi.ingsw.ps04.model.bonus.bonusVisitor.ServerBonusVisitor;
import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.setUpUtils.SetUp;
import it.polimi.ingsw.ps04.utils.exception.PlayerHasAlreadyBuiltException;

public class TestBonusRegion {

	Model model;
	Player actualPlayer;
	Player expectedPlayer;
	
	private void activateExpectedBonuses(){
		Set<Bonus> estiBonuses = model.getMap().getCityReference(new City("esti")).getBonus();
		Set<Bonus> framekBonuses = model.getMap().getCityReference(new City("framek")).getBonus();
		Set<Bonus> colorBonuses = model.getMap().getCityReference(new City("esti")).getColour().getBonus();
		Set<Bonus> regionBonuses = model.getMap().getCityReference(new City("esti")).getRegion().getBonuses();
		ServerBonusVisitor bonusVisitor = new ServerBonusVisitor();
		for(Bonus bonus: estiBonuses){
			bonus.accept(bonusVisitor, expectedPlayer);
		}
		for(Bonus bonus: framekBonuses){
			bonus.accept(bonusVisitor, expectedPlayer);
		}
		for(Bonus bonus: colorBonuses){
			bonus.accept(bonusVisitor, expectedPlayer);
		}
		for(Bonus bonus: regionBonuses){
			bonus.accept(bonusVisitor, expectedPlayer);
		}
	}
	
	@Before
	public void setUp(){
		model = SetUp.getInitialModel();
	}
	
	@Test
	public void testBonusRegion(){
		actualPlayer = model.getPlayersManager().getFirstActivePlayer();
		expectedPlayer= new Player(actualPlayer.getName(), model.getMap().getCitiesNumber(), 1, actualPlayer.getPoliticCardsHand().getHand());
		activateExpectedBonuses();
		City framek = model.getMap().getCityReference(new City("framek"));
		City esti = model.getMap().getCityReference(new City("esti"));
		try {
			framek.build(actualPlayer);
			model.getMap().activateCitiesBonuses(actualPlayer, framek);
			esti.build(actualPlayer);
			model.getMap().activateCitiesBonuses(actualPlayer, esti);
		} catch (PlayerHasAlreadyBuiltException e) {
			e.printStackTrace();
		}
		assertEquals(expectedPlayer, actualPlayer);
	}
}
