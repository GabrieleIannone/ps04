package it.polimi.ingsw.ps04.model;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.ps04.setUpUtils.SetUp;

public class TestGameModelView {
	
	Model expectedModel;
	
	@Before
	public void setUp(){
		 expectedModel = SetUp.getTwoPlayersModel();
	}

	@Test
	public void gmvTest(){
		GameModelView gmv = new GameModelView();
		expectedModel.addObserver(gmv);
		expectedModel.notifyModel();
		assertEquals(expectedModel, gmv.getModel());
	}
}
