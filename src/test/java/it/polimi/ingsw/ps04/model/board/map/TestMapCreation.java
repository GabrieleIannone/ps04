package it.polimi.ingsw.ps04.model.board.map;

import static org.junit.Assert.*;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

import org.jgrapht.UndirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;
import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.ps04.model.Model;
import it.polimi.ingsw.ps04.model.board.City;
import it.polimi.ingsw.ps04.model.board.Council;
import it.polimi.ingsw.ps04.model.board.Councillor;
import it.polimi.ingsw.ps04.model.board.King;
import it.polimi.ingsw.ps04.model.board.Region;
import it.polimi.ingsw.ps04.model.board.map.Map;
import it.polimi.ingsw.ps04.model.board.map.MapFactory;
import it.polimi.ingsw.ps04.model.bonus.Bonus;
import it.polimi.ingsw.ps04.model.bonus.BonusFactory;
import it.polimi.ingsw.ps04.model.bonus.simple.AssistantsBonus;
import it.polimi.ingsw.ps04.model.bonus.simple.CoinsBonus;
import it.polimi.ingsw.ps04.model.bonus.simple.PoliticCardsBonus;
import it.polimi.ingsw.ps04.model.bonus.simple.VictoryPointsBonus;
import it.polimi.ingsw.ps04.model.deck.business.BusinessDeck;
import it.polimi.ingsw.ps04.model.deck.business.BusinessDeckCreatorForTest;
import it.polimi.ingsw.ps04.model.deck.business.BusinessDeckFactory;
import it.polimi.ingsw.ps04.model.deck.business.BusinessPermitTile;
import it.polimi.ingsw.ps04.model.deck.politic.PoliticDeck;
import it.polimi.ingsw.ps04.utils.ConfigHelper;
import it.polimi.ingsw.ps04.utils.color.CityColor;
import it.polimi.ingsw.ps04.utils.color.ColorsManager;
import it.polimi.ingsw.ps04.utils.color.ColorsManagerFactory;
import it.polimi.ingsw.ps04.utils.color.NamedColor;
import it.polimi.ingsw.ps04.utils.exception.BadConfigFileException;

public class TestMapCreation {

	private Model model;
	String TEST_CONFING_FILE = "src" + File.separator + "test" + File.separator + "TestGameCreation.json";
	ColorsManager colorsManager;
	BonusFactory bonusFactory;
	ConfigHelper configHelper;
	BusinessDeckFactory businessDeckFactory;
	PoliticDeck politicCardsDeck;
	UndirectedGraph<City, DefaultEdge> graph = new SimpleGraph<>(DefaultEdge.class);
	City capital;
	King king;
	String mapName;
	List<Region> regions;

	private void setMap() {
		regions = new ArrayList<>();
		addCityColors();
		setRegions();
		try {
			bonusFactory.addBonuses(graph.vertexSet(), capital);
		} catch (BadConfigFileException | IOException e) {
			e.printStackTrace();
		}
		setEdges();
		mapName = "map_name_1";
		king = new King(capital, new Council(politicCardsDeck));

	}

	private Region prepareCoast() {
		Set<Bonus> coastBonus = new HashSet<>();
		coastBonus.add(new VictoryPointsBonus(20));
		Region coast = new Region("coast", new Council(politicCardsDeck), coastBonus);

		// I add cities
		City arkon = new City("arkon", coast, colorsManager.getCityColor("blue"));
		coast.addCity(arkon);
		graph.addVertex(arkon);
		City burgen = new City("burgen", coast, colorsManager.getCityColor("gold"));
		coast.addCity(burgen);
		graph.addVertex(burgen);

		List<BusinessPermitTile> deck = new ArrayList<>();
		Set<City> cities = new HashSet<>();
		cities.add(arkon);
		Set<Bonus> bonuses = new HashSet<>();
		bonuses.add(new VictoryPointsBonus(6));
		bonuses.add(new CoinsBonus(5));
		deck.add(new BusinessPermitTile(cities, bonuses));
		BusinessDeck businessDeck = BusinessDeckCreatorForTest.createBusinessDeck(deck);
		coast.setBusinessDeck(businessDeck);
		return coast;
	}

	private Region prepareHills() {
		Set<Bonus> hillsBonus = new HashSet<>();
		hillsBonus.add(new VictoryPointsBonus(7));
		Region hills = new Region("hills", new Council(politicCardsDeck), hillsBonus);

		// I add cities
		City castrum = new City("castrum", hills, colorsManager.getCityColor("silver"));
		hills.addCity(castrum);
		graph.addVertex(castrum);
		City dorful = new City("dorful", hills, colorsManager.getCityColor("silver"));
		hills.addCity(dorful);
		graph.addVertex(dorful);
		City juvelar = new City("juvelar", hills, null);
		hills.addCity(juvelar);
		graph.addVertex(juvelar);
		capital = juvelar;

		List<BusinessPermitTile> deck = new ArrayList<>();
		Set<City> cities = new HashSet<>();
		cities.add(castrum);
		Set<Bonus> bonuses = new HashSet<>();
		bonuses.add(new AssistantsBonus(2));
		bonuses.add(new CoinsBonus(3));
		deck.add(new BusinessPermitTile(cities, bonuses));
		BusinessDeck businessDeck = BusinessDeckCreatorForTest.createBusinessDeck(deck);
		hills.setBusinessDeck(businessDeck);
		return hills;
	}

	private Region prepareMountains() {
		Set<Bonus> mountainsBonus = new HashSet<>();
		mountainsBonus.add(new VictoryPointsBonus(8));
		Region mountains = new Region("mountains", new Council(politicCardsDeck), mountainsBonus);

		// I add cities
		City framek = new City("framek", mountains, colorsManager.getCityColor("gold"));
		mountains.addCity(framek);
		graph.addVertex(framek);
		City esti = new City("esti", mountains, colorsManager.getCityColor("bronze"));
		mountains.addCity(esti);
		graph.addVertex(esti);

		List<BusinessPermitTile> deck = new ArrayList<>();
		Set<City> cities = new HashSet<>();
		cities.add(framek);
		Set<Bonus> bonuses = new HashSet<>();
		bonuses.add(new PoliticCardsBonus(2, politicCardsDeck));
		bonuses.add(new CoinsBonus(4));
		deck.add(new BusinessPermitTile(cities, bonuses));
		BusinessDeck businessDeck = BusinessDeckCreatorForTest.createBusinessDeck(deck);
		mountains.setBusinessDeck(businessDeck);
		return mountains;
	}

	private void setEdges() {
		graph.addEdge(new City("arkon"), new City("castrum"));
		graph.addEdge(new City("arkon"), new City("burgen"));
		graph.addEdge(new City("burgen"), new City("dorful"));
		graph.addEdge(new City("burgen"), new City("esti"));
		graph.addEdge(new City("castrum"), new City("framek"));
		graph.addEdge(new City("framek"), new City("juvelar"));
		graph.addEdge(new City("esti"), new City("juvelar"));
	}

	private void setRegions() {
		regions.add(prepareCoast());
		regions.add(prepareHills());
		regions.add(prepareMountains());
	}

	private void addCityColors() {
		Set<Bonus> goldBonuses = new HashSet<>();
		goldBonuses.add(new VictoryPointsBonus(20));
		colorsManager.addCityColor(new CityColor("gold", colorsManager.getColor("gold"), goldBonuses));
		Set<Bonus> silverBonuses = new HashSet<>();
		goldBonuses.add(new VictoryPointsBonus(12));
		colorsManager.addCityColor(new CityColor("silver", colorsManager.getColor("silver"), silverBonuses));
		Set<Bonus> bronzeBonuses = new HashSet<>();
		goldBonuses.add(new VictoryPointsBonus(8));
		colorsManager.addCityColor(new CityColor("bronze", colorsManager.getColor("bronze"), bronzeBonuses));
		Set<Bonus> blueBonuses = new HashSet<>();
		goldBonuses.add(new VictoryPointsBonus(5));
		colorsManager.addCityColor(new CityColor("blue", colorsManager.getColor("blue"), blueBonuses));
	}

	@Before
	public void setUp() {
		model = new Model();
		configHelper = new ConfigHelper(new File(TEST_CONFING_FILE));
		bonusFactory = new BonusFactory(model, configHelper);
		businessDeckFactory = new BusinessDeckFactory(bonusFactory);
		try {
			colorsManager = new ColorsManagerFactory(configHelper, bonusFactory).factoryMethod();
		} catch (IOException e) {
			e.printStackTrace();
		}
		politicCardsDeck = new PoliticDeck(colorsManager.getCorruptionColors());
		setMap();
	}

	@Test
	public void testMapCreationFromFile() {
		Map expectedMap = new Map(graph, king, mapName, regions);
		MapFactory mf = new MapFactory(colorsManager, configHelper, bonusFactory, businessDeckFactory,
				politicCardsDeck);
		Map actualMap = null;
		try {
			actualMap = mf.importMap(mapName);
		} catch (IOException | BadConfigFileException e) {
			e.printStackTrace();
		}
		assertEquals(expectedMap, actualMap);
	}

	@Test
	public void testMapToString() throws IOException {
		MapFactory mf = new MapFactory(colorsManager, configHelper, bonusFactory, businessDeckFactory,
				politicCardsDeck);
		Map actualMap = null;
		try {
			actualMap = mf.importMap(mapName);
		} catch (IOException | BadConfigFileException e) {
			e.printStackTrace();
		}
		King king = actualMap.getKing();
		Queue<Councillor> councillors = new LinkedList<>();
		for (int i = 0; i < 4; i++)
			councillors.add(new Councillor(new NamedColor("white", Color.WHITE)));
		Council fakeCouncil = new Council(councillors);
		King kingWithFakeCouncil = new King(king.getActualCity(), fakeCouncil);
		List<Region> regionsWithFakeCouncil = new ArrayList<>();
		for (Region region : actualMap.getRegions()) {
			Region regionwfc = new Region(region.getName(), fakeCouncil, region.getBonuses());
			regionwfc.setBonuses(region.getBonuses());
			regionwfc.setBonusTaken(region.isBonusTaken());
			regionwfc.setBusinessDeck(region.getBusinessDeck());
			regionsWithFakeCouncil.add(regionwfc);
		}
		Map mapWithFakeCouncil = new Map(actualMap.getGraph(), kingWithFakeCouncil, actualMap.getName(),
				regionsWithFakeCouncil);
		String expectedString = new String(
				Files.readAllBytes(Paths.get("src" + File.separator + "test" + File.separator + "mapStringTest.txt")));
		assertEquals(expectedString, mapWithFakeCouncil.toString());
	}

	@Test
	public void testGetEdgesString() {
		MapFactory mf = new MapFactory(colorsManager, configHelper, bonusFactory, businessDeckFactory,
				politicCardsDeck);
		Map actualMap = null;
		try {
			actualMap = mf.importMap(mapName);
		} catch (IOException | BadConfigFileException e) {
			e.printStackTrace();
		}
		Set<String> edgesString = new HashSet<>();
		edgesString.add("\"arkon <-> castrum\"");
		edgesString.add("\"arkon <-> burgen\"");
		edgesString.add("\"burgen <-> dorful\"");
		edgesString.add("\"burgen <-> esti\"");
		edgesString.add("\"castrum <-> framek\"");
		edgesString.add("\"framek <-> juvelar\"");
		edgesString.add("\"esti <-> juvelar\"");
		Set<String> actualEdgesStrings = new HashSet<>(actualMap.getEdgesStrings());
		assertEquals(edgesString, actualEdgesStrings);

	}

	@Test
	public void testGetCities() {
		MapFactory mf = new MapFactory(colorsManager, configHelper, bonusFactory, businessDeckFactory,
				politicCardsDeck);
		Map actualMap = null;
		try {
			actualMap = mf.importMap(mapName);
		} catch (IOException | BadConfigFileException e) {
			e.printStackTrace();
		}
		Set<City> actualCities = actualMap.getCities(new Region("coast"));
		City arkon = new City("arkon");
		City burgen = new City("burgen");
		Set<City> expectedCities = new HashSet<>();
		expectedCities.add(arkon);
		expectedCities.add(burgen);
		assertEquals(expectedCities, actualCities);
	}

	@Test
	public void testGetCitiesNumber() {
		MapFactory mf = new MapFactory(colorsManager, configHelper, bonusFactory, businessDeckFactory,
				politicCardsDeck);
		Map actualMap = null;
		try {
			actualMap = mf.importMap(mapName);
		} catch (IOException | BadConfigFileException e) {
			e.printStackTrace();
		}
		assertEquals(2, actualMap.getCitiesNumber(new Region("coast")));
	}

	@Test
	public void testFindCity() throws BadConfigFileException {
		MapFactory mf = new MapFactory(colorsManager, configHelper, bonusFactory, businessDeckFactory,
				politicCardsDeck);
		Map actualMap = null;
		try {
			actualMap = mf.importMap(mapName);
		} catch (IOException | BadConfigFileException e) {
			e.printStackTrace();
		}
		Region coast = actualMap.getRegionReference(new Region("coast"));
		assertEquals(new City("arkon"), coast.findCity("arkon"));
	}

	@Test
	public void testMapCreationFromFileWithDifferentEdge() {
		graph.addEdge(new City("juvelar"), new City("arkon"));
		Map expectedMap = new Map(graph, king, mapName, regions);
		MapFactory mf = new MapFactory(colorsManager, configHelper, bonusFactory, businessDeckFactory,
				politicCardsDeck);
		Map actualMap = null;
		try {
			actualMap = mf.importMap(mapName);
		} catch (IOException | BadConfigFileException e) {
			e.printStackTrace();
		}
		assertFalse(expectedMap.equals(actualMap));
	}

}
