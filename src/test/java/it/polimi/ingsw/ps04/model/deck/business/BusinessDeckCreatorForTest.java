package it.polimi.ingsw.ps04.model.deck.business;

import java.util.List;

public class BusinessDeckCreatorForTest {

	public static BusinessDeck createBusinessDeck(List<BusinessPermitTile> deck){
		return new BusinessDeck(deck);
	}
}
