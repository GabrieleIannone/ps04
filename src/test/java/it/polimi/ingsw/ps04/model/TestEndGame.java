package it.polimi.ingsw.ps04.model;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.setUpUtils.SetUp;

public class TestEndGame {
	Model model;

	@Before
	public void setUp() {
		model = SetUp.getInitialModel();
	}

	@Test
	public void testEndGame() {
		Player winner = model.getPlayersManager().getActivePlayer();
		model.endGame();
		assertTrue(winner.getPoints().getVictoryPoints() == 11);
	}

}
