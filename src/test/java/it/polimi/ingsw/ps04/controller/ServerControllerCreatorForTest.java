package it.polimi.ingsw.ps04.controller;

import it.polimi.ingsw.ps04.model.Model;

public class ServerControllerCreatorForTest {
	public static ServerController createServerController(Model model) {
		return new ServerController(model);
	}
}
