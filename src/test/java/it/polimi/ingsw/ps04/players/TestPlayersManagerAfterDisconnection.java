package it.polimi.ingsw.ps04.players;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.model.player.PlayersManager;
import it.polimi.ingsw.ps04.utils.exception.ActivePlayerDisconnectedException;
import it.polimi.ingsw.ps04.utils.exception.NoActivePlayersException;

public class TestPlayersManagerAfterDisconnection {
	List<Player> players;

	@Before
	public void setUp() {
		players = new ArrayList<>();
		players.add(new Player("player1"));
		players.add(new Player("player2"));
		players.add(new Player("player3"));
		players.add(new Player("player4"));
	}

	@Test
	public void firstPlayerDisconnectedTest() {
		PlayersManager playersManager = new PlayersManager(players);
		Player activePlayer = playersManager.getActivePlayer();
		try {
			playersManager.disconnectPlayer(activePlayer);
		} catch (ActivePlayerDisconnectedException | NoActivePlayersException e) {

		}
		assertEquals(new Player("player2"), playersManager.getActivePlayer());
	}

	@Test
	public void allPlayersDisconnectedTest() {
		PlayersManager playersManager = new PlayersManager(players);
		try {
			playersManager.disconnectPlayer(new Player("player1"));
		} catch (ActivePlayerDisconnectedException | NoActivePlayersException e1) {
			
		}
		try {
			playersManager.disconnectPlayer(new Player("player2"));
		} catch (ActivePlayerDisconnectedException | NoActivePlayersException e1) {
			
		}
		try {
			playersManager.disconnectPlayer(new Player("player3"));
		} catch (ActivePlayerDisconnectedException | NoActivePlayersException e1) {
			
		}
		try {
			playersManager.disconnectPlayer(new Player("player4"));
		} catch (ActivePlayerDisconnectedException e1) {
			
		} catch (NoActivePlayersException e1) {
			assertEquals(0, playersManager.getActivePlayers().size());
		}
	}
}
