package it.polimi.ingsw.ps04.players.turn;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.model.player.PlayersManager;
import it.polimi.ingsw.ps04.utils.exception.RoundFinishedException;

public class TestTurn {

	List<Player> players;
	
	@Before
	public void setUp() {
		players= new ArrayList<>();
		players.add(new Player("player1"));
		players.add(new Player("player2"));
		players.add(new Player("player3"));
		players.add(new Player("player4"));
	}
	
	@Test
	public void testFirstPlayer() {
		PlayersManager playersManager = new PlayersManager(players);
		Player activePlayer= playersManager.getActivePlayer();
		assertEquals(players.get(0), activePlayer);
	}
	
	@Test
	public void testSecondPlayer() {
		PlayersManager playersManager = new PlayersManager(players);
		try {
			playersManager.updateActivePlayer();
		} catch (RoundFinishedException e1) {
			e1.printStackTrace();
		}
		Player activePlayer=null;
		activePlayer = playersManager.getActivePlayer();
		
		assertEquals(players.get(1), activePlayer);
	}
	
	
	@Test
	public void testRoundFinished() {
		PlayersManager playersManager = new PlayersManager(players);
		for(int i=1;i<players.size();i++){
			try {
				playersManager.updateActivePlayer();
			} catch (RoundFinishedException e1) {
				e1.printStackTrace();
			}
		}
		Player activePlayer=playersManager.getActivePlayer();
		Player lastPlayer =players.get(players.size()-1);
		assertEquals(lastPlayer, activePlayer);
		try {
			playersManager.updateActivePlayer();
		} catch (RoundFinishedException e) {
			assertEquals(players.get(0), playersManager.getActivePlayer());
		}
		
	}

}
