package it.polimi.ingsw.ps04.view;

import static org.junit.Assert.assertEquals;


import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.ps04.controller.ServerController;
import it.polimi.ingsw.ps04.model.GameModelView;
import it.polimi.ingsw.ps04.model.Model;
import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.setUpUtils.SetUp;
import it.polimi.ingsw.ps04.utils.message.ModelRequestMessage;
import it.polimi.ingsw.ps04.view.View;
import it.polimi.ingsw.ps04.view.socket.SocketConnection;
import it.polimi.ingsw.ps04.view.socket.SocketView;

public class TestRestoreModel {

	View view;
	ServerController controller;
	Model model;
	GameModelView gmv;TestView testView;

	@Before
	public void setUp() {
		Player player = new Player("player1");
		SocketConnection conn = new SocketConnection(null);
		view = new SocketView(player, conn);
		model = SetUp.getTwoPlayersModel();
		gmv = new GameModelView();
		testView = new TestView();
		SetUp.connectMVC(view, controller, model, gmv, testView);
	}
	

	@Test
	public void test(){
		view.notifyMessage(new ModelRequestMessage());
		assertEquals(model, testView.getGmv().getModel());
	}
}
