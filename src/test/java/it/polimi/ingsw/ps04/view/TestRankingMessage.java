package it.polimi.ingsw.ps04.view;

import static org.junit.Assert.assertEquals;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.security.auth.login.LoginException;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import it.polimi.ingsw.ps04.controller.ServerController;
import it.polimi.ingsw.ps04.model.GameModelView;
import it.polimi.ingsw.ps04.model.Model;
import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.setUpUtils.SetUp;
import it.polimi.ingsw.ps04.utils.db.DBConnector;
import it.polimi.ingsw.ps04.utils.login.LoginManager;
import it.polimi.ingsw.ps04.utils.message.login.SignupMessage;
import it.polimi.ingsw.ps04.utils.message.statistics.PlayerStatisticsMessage;
import it.polimi.ingsw.ps04.utils.message.statistics.RankingMessage;
import it.polimi.ingsw.ps04.utils.message.statistics.StatisticsMessage;
import it.polimi.ingsw.ps04.utils.statistics.PlayerStatistics;
import it.polimi.ingsw.ps04.utils.statistics.Ranking;
import it.polimi.ingsw.ps04.view.socket.SocketConnection;
import it.polimi.ingsw.ps04.view.socket.SocketView;

public class TestRankingMessage {

	Ranking expectedRanking;
	View view;
	ServerController controller;
	Model model;
	GameModelView gmv;
	TestView testView;

	@BeforeClass
	public static void setUpBefore() {
		try {
			DBConnector.getInstance().setAutocommit(false);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Before
	public void setUp() {
		Player player = new Player("player1");
		SocketConnection conn = new SocketConnection(null);
		view = new SocketView(player, conn);
		model = SetUp.getTwoPlayersModel();
		gmv = new GameModelView();
		testView = new TestView();
		SetUp.connectMVC(view, controller, model, gmv, testView);
	}

	@Test
	public void testMoreVictoriesWithMVC() throws LoginException, SQLException {
		DBConnector.getInstance().update("DELETE FROM users");
		LoginManager.getInstance().executeCredentialsMessage(new SignupMessage("player1", "password1", "email1"));
		LoginManager.getInstance().executeCredentialsMessage(new SignupMessage("player2", "password2", "email2"));
		DBConnector.getInstance().incrementWonGames("player1");
		DBConnector.getInstance().incrementPlayedGames("player1");
		DBConnector.getInstance().incrementPlayedGames("player2");

		view.notifyMessage(new RankingMessage());
		List<PlayerStatistics> statistics = new ArrayList<>();
		PlayerStatistics firstPlayer = new PlayerStatistics("player1", 1, 1, 0);
		PlayerStatistics secondPlayer = new PlayerStatistics("player2", 0, 1, 0);
		statistics.add(secondPlayer);
		statistics.add(firstPlayer);
		expectedRanking = new Ranking(statistics);
		RankingMessage rankingMessage = (RankingMessage) testView.getMessage();
		Ranking actualRanking = rankingMessage.getRanking();
		assertEquals(actualRanking, expectedRanking);
	}
	
	@Test
	public void testPlayerStatisticsWithMVC() throws LoginException, SQLException {
		DBConnector.getInstance().update("DELETE FROM users");
		LoginManager.getInstance().executeCredentialsMessage(new SignupMessage("player1", "password1", "email1"));
		DBConnector.getInstance().incrementWonGames("player1");
		DBConnector.getInstance().incrementPlayedGames("player1");
		DBConnector.getInstance().incrementGameMinutes("player1",1);

		StatisticsMessage statisticsMessage = new PlayerStatisticsMessage("player1");
		view.notifyMessage(statisticsMessage);
		PlayerStatistics expectedPlayerStatistics = new PlayerStatistics("player1", 1, 1, 1);
		PlayerStatisticsMessage actualStatisticsMessage = (PlayerStatisticsMessage) testView.getMessage();
		assertEquals(expectedPlayerStatistics, actualStatisticsMessage.getStatistics());
	}
}
