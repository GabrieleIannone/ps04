package it.polimi.ingsw.ps04.view;

import java.util.Observable;
import java.util.Observer;

import it.polimi.ingsw.ps04.model.GameModelView;
import it.polimi.ingsw.ps04.utils.message.GameFinishedMessage;
import it.polimi.ingsw.ps04.utils.message.Message;

public class TestView implements Observer {

	GameModelView gmv;
	Message message;
	boolean gameFinishedMessage;
	
	@Override
	public synchronized void update(Observable o, Object arg1) {
		if (!(o instanceof GameModelView)) {
			throw new IllegalArgumentException();
		}
		if (arg1 == null) {
			gmv =(GameModelView) o;
		} else if (arg1 instanceof GameFinishedMessage) {
			gameFinishedMessage = true;
		} else if (arg1 instanceof Message) {
			message =(Message) arg1;
		}
	}

	public GameModelView getGmv() {
		return gmv;
	}

	public Message getMessage() {
		return message;
	}

	public boolean isGameFinishedMessage() {
		return gameFinishedMessage;
	}
	
	
}
