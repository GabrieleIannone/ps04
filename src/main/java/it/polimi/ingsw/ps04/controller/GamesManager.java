package it.polimi.ingsw.ps04.controller;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.ps04.client.view.connection.rmi.ClientRMIReceiverInterface;
import it.polimi.ingsw.ps04.model.GameFactory;
import it.polimi.ingsw.ps04.model.Model;
import it.polimi.ingsw.ps04.model.GameModelView;
import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.utils.exception.BadConfigFileException;
import it.polimi.ingsw.ps04.utils.log.Handlers;
import it.polimi.ingsw.ps04.utils.time.Timing;
import it.polimi.ingsw.ps04.utils.time.WaitingThread;
import it.polimi.ingsw.ps04.view.View;
import it.polimi.ingsw.ps04.view.WaitingConnections;
import it.polimi.ingsw.ps04.view.rmi.RMIView;
import it.polimi.ingsw.ps04.view.socket.SocketConnection;
import it.polimi.ingsw.ps04.view.socket.SocketView;

/**
 * this class manages the games. It stores the waiting connections and starts
 * the match at the right moment. In addition to this, it creates all the MVC
 * system.
 */
public class GamesManager implements Timing {
	private final static int RMI_PORT = 52365;
	private static final Logger log = Logger.getLogger(ServerController.class.getName());
	private static GamesManager instance = null;
	/*
	 * connections that wait for other players. I use HashTable because it is
	 * thread-safe. String is the name of the player, here I cannot create a
	 * Map<Player, Connection> because in order to create the player I need to
	 * know the turn and the number of players
	 */
	private WaitingConnections waitingConnections;
	private List<Model> games = new ArrayList<>();

	/*
	 * when 2 players connect to the game, a timeout with these seconds starts.
	 * The match starts when there are 4 players or when this timeout ends.
	 */
	private final Duration timeout = Duration.ofSeconds(20);

	/**
	 * Constructs a games manager. This constructor is private because of the
	 * Singleton Pattern.
	 */
	private GamesManager() {
		this.waitingConnections = new WaitingConnections();
		log.addHandler(Handlers.getInstance().SERVER_HANDLER);
	}

	/**
	 * Returns the instance of GamesManager
	 * 
	 * @return the instanceof GamesManager
	 */
	public static GamesManager getInstance() {
		if (instance == null) {
			instance = new GamesManager();
		}
		return instance;
	}

	/**
	 * registers a socket connection
	 * 
	 * @param name
	 *            the name of the player
	 * @param connection
	 *            the connection that you want to register
	 */
	public void registerSocketConnection(String name, SocketConnection connection) {
		synchronized (waitingConnections) {
			waitingConnections.getWaitingSocketConnections().put(name, connection);
			checkWaitingConnections();
		}
	}

	/**
	 * registers an RMI connection
	 * 
	 * @param name
	 *            the name of the player
	 * @param clientStub
	 *            the client stub of the RMI connection
	 */
	public void registerRMIConnection(String name, ClientRMIReceiverInterface clientStub) {
		synchronized (waitingConnections) {
			waitingConnections.getWaitingRMIConnections().put(name, clientStub);
			checkWaitingConnections();
		}
	}

	/**
	 * it checks if there are 2 waiting connections. if there are, then the
	 * countdown of the match start
	 */
	private void checkWaitingConnections() {
		if (waitingConnections.numofWaiting() == 2) {
			log.log(Level.FINE, "there are 2 players, I wait");
			WaitingThread wt = new WaitingThread(timeout, this);
			wt.start();
			// during this time other connections can be added to the waiting
			// connections
		}
	}

	/**
	 * starts the match and implements the connection of the MVC
	 */
	private void startMatch() {
		Map<String, SocketConnection> playingSocketConnections;
		Map<String, ClientRMIReceiverInterface> playingRMIConnections;
		synchronized (waitingConnections) {
			/*
			 * I create a copy of the waiting connections because then I'm going
			 * to clear them
			 */
			playingSocketConnections = new HashMap<String, SocketConnection>(
					waitingConnections.getWaitingSocketConnections());
			playingRMIConnections = new HashMap<String, ClientRMIReceiverInterface>(
					waitingConnections.getWaitingRMIConnections());
			waitingConnections.clearConnections();
		}
		Set<String> playerNames = new HashSet<String>(playingSocketConnections.keySet());
		playerNames.addAll(playingRMIConnections.keySet());

		log.log(Level.FINE, "player names: " + playerNames);
		Model gameModel;
		try {
			gameModel = GameFactory.createGame(new ArrayList<String>(playerNames));

			ServerController controller = new ServerController(gameModel);
			GameModelView gameModelView = new GameModelView();
			gameModel.addObserver(gameModelView);

			createSocketViews(playingSocketConnections, controller, gameModelView);
			createRMIViews(playingRMIConnections, controller, gameModelView);

			log.log(Level.FINE, "creation completed");
			controller.initGame();
			games.add(gameModel);
		} catch (IOException | BadConfigFileException e) {
			e.printStackTrace();
		}
	}

	/**
	 * creates the socket views
	 * 
	 * @param playingSocketConnections
	 *            the socket connections of the players of the current match
	 * @param controller
	 *            the server controller of the current match
	 * @param gameModelView
	 *            the game model view of the current match
	 */
	private void createSocketViews(Map<String, SocketConnection> playingSocketConnections, ServerController controller,
			GameModelView gameModelView) {
		for (String playerName : playingSocketConnections.keySet()) {
			View socketView = new SocketView(new Player(playerName), playingSocketConnections.get(playerName));
			socketView.addObserver(controller);
			gameModelView.addObserver(socketView);
		}
	}

	/**
	 * creates the RMI views
	 * 
	 * @param playingRMIConnections
	 *            the RMI connections of the players of the current match
	 * @param controller
	 *            the server controller of the current match
	 * @param gameModelView
	 *            the game model view of the current match
	 */
	private void createRMIViews(Map<String, ClientRMIReceiverInterface> playingRMIConnections,
			ServerController controller, GameModelView gameModelView) {

		for (String playerName : playingRMIConnections.keySet()) {
			RMIView rmiView = new RMIView(new Player(playerName), playingRMIConnections.get(playerName));

			try {
				/* RMIViewInterface viewInterface = (RMIViewInterface) */ UnicastRemoteObject.exportObject(rmiView, 0);
				Registry registry = LocateRegistry.getRegistry(RMI_PORT);
				registry.bind(playerName, rmiView);
			} catch (RemoteException e) {
				e.printStackTrace();
			} catch (AlreadyBoundException e) {
				e.printStackTrace();
			}
			try {
				playingRMIConnections.get(playerName).startGame(InetAddress.getLocalHost().getHostAddress(), RMI_PORT,
						playerName);
			} catch (RemoteException | UnknownHostException | NotBoundException e) {
				e.printStackTrace();
			}
			rmiView.addObserver(controller);
			gameModelView.addObserver(rmiView);
		}
	}

	/**
	 * close all games correctly
	 */
	public void closeAllGames() {
		for (Model game : games) {
			game.endGame();
		}
		synchronized (waitingConnections) {
			for (SocketConnection conn : waitingConnections.getWaitingSocketConnections().values()) {
				conn.closeConnection();
			}
		}
	}

	/**
	 * starts the match when the time for the preparation of the match expires
	 */
	@Override
	public void onTimesUp() {
		startMatch();

	}
}
