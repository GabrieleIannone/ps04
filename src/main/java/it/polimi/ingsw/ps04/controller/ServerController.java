package it.polimi.ingsw.ps04.controller;

import java.util.Observable;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.ps04.model.Model;
import it.polimi.ingsw.ps04.model.action.Action;
import it.polimi.ingsw.ps04.model.action.PassTurn;
import it.polimi.ingsw.ps04.utils.exception.ActivePlayerDisconnectedException;
import it.polimi.ingsw.ps04.utils.exception.NoActivePlayersException;
import it.polimi.ingsw.ps04.utils.exception.NotYourTurnException;
import it.polimi.ingsw.ps04.utils.log.Handlers;
import it.polimi.ingsw.ps04.utils.message.ActionMessage;
import it.polimi.ingsw.ps04.utils.message.Message;
import it.polimi.ingsw.ps04.utils.message.ModelRequestMessage;
import it.polimi.ingsw.ps04.utils.message.PlayerDisconnectedMessage;
import it.polimi.ingsw.ps04.utils.message.RequestforBonus;
import it.polimi.ingsw.ps04.utils.message.market.MarketMessage;
import it.polimi.ingsw.ps04.utils.message.market.SellingOffersMessage;
import it.polimi.ingsw.ps04.utils.message.market.SkipBuyMessage;
import it.polimi.ingsw.ps04.utils.message.statistics.StatisticsMessage;
import it.polimi.ingsw.ps04.utils.statistics.StatisticsMessageManager;
import it.polimi.ingsw.ps04.utils.time.Timing;
import it.polimi.ingsw.ps04.utils.time.WaitingThread;
import it.polimi.ingsw.ps04.view.View;

/**
 * this class implements the controller of the server
 *
 */
public class ServerController extends Controller implements Timing {
	private static final Logger log = Logger.getLogger(ServerController.class.getName());
	private WaitingThread wt;

	/**
	 * constructs a server controller with that specified model
	 * 
	 * @param model
	 *            the model that you want to to assign to the controller
	 */
	protected ServerController(Model model) {
		super(model);
		log.addHandler(Handlers.getInstance().SERVER_HANDLER);

	}

	/**
	 * updates the messages that come from the observed object
	 */
	@Override
	public synchronized void update(Observable o, Object arg) {
		log.log(Level.FINE, "Controller: message received!");
		if (!(o instanceof View) || !(arg instanceof Message)) {
			throw new IllegalArgumentException();
		}

		else if (arg instanceof PlayerDisconnectedMessage) {
			log.log(Level.FINE, "PlayerDisconnectedMessage");
			PlayerDisconnectedMessage disconnessionMessage = (PlayerDisconnectedMessage) arg;
			log.log(Level.FINE, disconnessionMessage.toString());
			View view = (View) o;
			view.deleteObserver(this);
			disconnessionMessage.setView(view);
			model.notifyMessage(disconnessionMessage);
			try {
				model.getPlayersManager().disconnectPlayer(disconnessionMessage.getPlayer());
			} catch (ActivePlayerDisconnectedException e) {
				System.out
						.println("controller: active player: " + model.getPlayersManager().getActivePlayer().getName());
			} catch (NoActivePlayersException e) {
				model.endGame();
			}

		}

		// I obtained an action or bonus message. I check if the sender is the
		// active player
		else if (((arg instanceof ActionMessage) || (arg instanceof RequestforBonus))
				&& !(((View) o).getPlayer().getName().equals(model.getPlayersManager().getActivePlayer().getName()))) {
			throw new NotYourTurnException();
		}

		// I obtained an action or bonus message. I check if market is close
		else if (((arg instanceof ActionMessage) || (arg instanceof RequestforBonus))
				&& model.getMarket().isMarketisOpen()) {
			throw new NotYourTurnException("Market is open");
		}

		// I obtained a market message. I check if market is open
		else if (arg instanceof MarketMessage && !model.getMarket().isMarketisOpen()) {
			throw new NotYourTurnException("Market is close");
		}

		// I obtained a market message. I check if the sender is the active
		// player for the market
		else if (arg instanceof MarketMessage && !(((View) o).getPlayer().getName()
				.equals(model.getMarket().getPlayersManager().getActivePlayer().getName()))) {
			throw new NotYourTurnException();
		}

		// I obtained an action message. I execute the action
		else if (arg instanceof ActionMessage) {
			log.log(Level.FINE, "ActionMessage");
			Action action = ((ActionMessage) arg).getAction();
			executeAction(action);
		}

		// I obtained a market message. I process the message
		else if (arg instanceof MarketMessage) {
			MarketMessage message = (MarketMessage) arg;

			manageMarket(message);
		}

		// I obtained a bonus message. I execute the action
		else if (arg instanceof RequestforBonus) {
			log.log(Level.FINE, "Controller: It's a bonus!");
			boolean endofTurn = model.executeBonus((RequestforBonus) arg);
			if (endofTurn)
				nextTurn();
		}

		else if (arg instanceof StatisticsMessage) {
			StatisticsMessage message = (StatisticsMessage) arg;
			View view = (View) o;
			StatisticsMessageManager smm = new StatisticsMessageManager(message, view.getPlayer().getName());

			model.notifyMessage(smm.getResponseMessage());
		}

		else if (arg instanceof ModelRequestMessage) {
			model.notifyModel();
		}
	}

	/**
	 * initializes the game
	 */
	public void initGame() {
		initTurn();
		model.notifyModel();
	}

	/**
	 * it send the message to the logger
	 */
	@Override
	protected void showMessage(Message message) {
		log.log(Level.FINE, message.toString());
	}

	/**
	 * this functions is called when the time for the turn of a user expires.
	 * The player passes the turn if the market is not opened, while if the
	 * market is opened the player skips the sell phase or the buy phase
	 */
	@Override
	public void onTimesUp() {
		if (!model.getMarket().isMarketisOpen()) {
			if (!model.isGameFinished())
				executeAction(new PassTurn());
		} else {
			if (model.getMarket().isSellingPhase())
				manageMarket(new SellingOffersMessage());
			else
				manageMarket(new SkipBuyMessage());
		}
	}

	/**
	 * starts the timer
	 */
	@Override
	protected void startTimer() {
		wt = new WaitingThread(model.getTurnTimeout(), this);
		wt.start();
	}

	/**
	 * stops the timer
	 */
	@Override
	protected void stopTimer() {
		wt.interrupt();
	}

}