package it.polimi.ingsw.ps04.controller;

import java.util.Observer;

import it.polimi.ingsw.ps04.model.Model;
import it.polimi.ingsw.ps04.model.action.Action;
import it.polimi.ingsw.ps04.model.action.actionVisitor.ConcreteActionVisitor;
import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.utils.exception.ActionNotCompletedException;
import it.polimi.ingsw.ps04.utils.exception.RoundFinishedException;
import it.polimi.ingsw.ps04.utils.message.Message;
import it.polimi.ingsw.ps04.utils.message.SimpleMessage;
import it.polimi.ingsw.ps04.utils.message.market.MarketMessage;
import it.polimi.ingsw.ps04.utils.message.market.UpdateMarketMessage;

/**
 * this class collects the things that the client controller and the server
 * controller have in common
 */
public abstract class Controller implements Observer {

	protected Model model;
	protected ConcreteActionVisitor visitor;

	/**
	 * constructs a controller with a given model
	 * 
	 * @param model
	 *            the model that you want to assign to the controller
	 */
	public Controller(Model model) {
		super();
		this.model = model;
		visitor = new ConcreteActionVisitor(model);
	}

	/**
	 * starts the market phase
	 */
	public void startMarketPhase() {
		model.startMarketPhase();
		startTimer();
	}

	/**
	 * This method will be called at the beginning of each turn. It will change
	 * the active player, reset action counter and give a politic card to the
	 * new active player
	 */
	private void endTurn() throws RoundFinishedException {
		stopTimer();
		model.getPlayersManager().updateActivePlayer();
	}

	/**
	 * stops the timer
	 */
	protected abstract void stopTimer();

	/**
	 * execute an action. this method calls the doAction of the model
	 * 
	 * @param action
	 *            the action that you want to execute
	 * @see Model.doAction()
	 */
	protected void executeAction(Action action) {
		/*
		 * I accept a visitor in order to update the parameter of the action
		 * with the correct references of the server model
		 */
		try {
			action.accept(visitor);
			boolean endofTurn;
			try {
				endofTurn = model.doAction(action);
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}
			if (endofTurn) {
				nextTurn();
			}

		} catch (IllegalArgumentException e) {
			throw new ActionNotCompletedException(e.getMessage());
		}
	}

	/**
	 * Manage a market message. This method will calls the manageMarket method
	 * in the model and, once it finished, update the market PlayersManager. If
	 * the selling phase is over starts the buying phase. If the buying phase is
	 * over it closes the market and return to the normal round.
	 * 
	 * @param message
	 *            The message received from the client.
	 */
	protected void manageMarket(MarketMessage message) {
		model.manageMarket(message);
		try {
			stopTimer();
			model.getMarket().getPlayersManager().updateActivePlayer();
			notifyActivePlayer();
			startTimer();
		} catch (RoundFinishedException e) {
			if (model.getMarket().isSellingPhase()) {
				System.out.println("Selling phase is over. Starting buying phase");
				model.getMarket().startBuyingPhase();
				startTimer();
				model.notifyMessage(new UpdateMarketMessage(model.getMarket()));
			} else {
				model.getMarket().closeMarket();
				System.out.println("Market close!");
				initTurn();
			}
		}

	}

	/**
	 * passes to the other turn. if the round is finished then the market phase
	 * starts
	 */
	protected void nextTurn() {
		try {
			endTurn();
			initTurn();
		} catch (RoundFinishedException e) {
			System.out.println("Round finished, start market");
			startMarketPhase();
			notifyActivePlayer();
		}
	}

	/**
	 * starts the timer
	 */
	protected abstract void startTimer();

	/**
	 * initializes the turn. It checks if the game is ended, if not the player
	 * active player is updated, it draws and the timer is started
	 */
	protected void initTurn() {
		if (!model.getPlayersManager().getActivePlayer().getUnusedEmporiums().hasEmporium())
			model.endGame();
		/*
		 * here I verify if the game is finished because the game can finish
		 * also because every player disconnected
		 */
		if (!model.isGameFinished()) {
			Player activePlayer = model.getPlayersManager().getActivePlayer();
			activePlayer.getActionsCounters().resetActionsCounters();
			activePlayer.addPoliticCard(model.getPoliticDeck().drawCard());
			notifyActivePlayer();
			startTimer();
		}
	}

	/**
	 * notifies the active player
	 */
	protected void notifyActivePlayer() {
		if (!model.getMarket().isMarketisOpen()) {
			showMessage(new SimpleMessage(model.getPlayersManager().getActivePlayer().getName() + " it's your turn!"));
		} else
			showMessage(new SimpleMessage(
					model.getMarket().getPlayersManager().getActivePlayer().getName() + " it's your turn!"));
	}

	/**
	 * shows a given message
	 * 
	 * @param message
	 *            the message that you want to show
	 */
	protected abstract void showMessage(Message message);

}