package it.polimi.ingsw.ps04.view;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.time.StopWatch;

import it.polimi.ingsw.ps04.model.GameModelView;
import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.utils.db.DBConnector;
import it.polimi.ingsw.ps04.utils.message.GameFinishedMessage;
import it.polimi.ingsw.ps04.utils.message.Message;
import it.polimi.ingsw.ps04.utils.message.PlayerDisconnectedMessage;
import it.polimi.ingsw.ps04.utils.message.statistics.StatisticsMessage;

/**
 * This class implements the view of the MVC pattern
 */
public abstract class View extends Observable implements Observer {

	private Player player;
	private boolean disconnected = false;
	private StopWatch stopWatch = new StopWatch();

	/**
	 * Constructs a view with the given player
	 * 
	 * @param player
	 *            the player that you want to assign to this view
	 */
	protected View(Player player) {
		this.player = player;
		stopWatch.start();
	}

	/**
	 * Returns the player of the view
	 * 
	 * @return the player of the view
	 */
	public Player getPlayer() {
		return player;
	}

	/**
	 * Notifies the observers. This method will be called by the concrete view
	 * 
	 * @param message
	 *            the message that you want to notify
	 */
	protected void notifyMessage(Message message) {
		setChanged();
		try {
			notifyObservers(message);
		} catch (RuntimeException e) {
			/*
			 * I catch the exception here in order to notify only the player
			 * that did the action
			 */
			try {
				sendObject(e);
			} catch (IOException exc) {
				notifyDisconnection();
			}
		}
	}

	/**
	 * notifies the observer of a disconnection
	 */
	protected void notifyDisconnection() {
		if (!disconnected) {
			registerTime();
			disconnected = true;
			setChanged();
			notifyObservers(new PlayerDisconnectedMessage(player));
			try {
				close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * registers the time that a player played to a match
	 */
	private void registerTime() {
		stopWatch.stop();
		long millis = stopWatch.getTime();
		long minutes = TimeUnit.MILLISECONDS.toMinutes(millis);
		try {
			DBConnector.getInstance().incrementGameMinutes(player.getName(), (int) minutes);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * closes the view
	 * 
	 * @throws IOException
	 *             if there was a problem with the I/O
	 */
	protected abstract void close() throws IOException;

	/**
	 * sends an object to the client of the player of this view
	 * 
	 * @param obj
	 *            the object that you want to send
	 * @throws IOException
	 *             if there was a problem with the I/O
	 */
	protected abstract void sendObject(Object obj) throws IOException;

	/**
	 * updates the notifies from the observed objects (the game model view in
	 * this case)
	 */
	@Override
	public synchronized void update(Observable o, Object arg1) {
		if (!(o instanceof GameModelView)) {
			throw new IllegalArgumentException();
		}
		try {
			if (arg1 == null) {
				sendObject((GameModelView) o);
			} else if (arg1 instanceof GameFinishedMessage) {
				registerTime();
				close();
			} else if (arg1 instanceof StatisticsMessage) {
				StatisticsMessage sm = (StatisticsMessage) arg1;
				if (player.getName().equals(sm.getRequestOwner())) {
					sendObject(sm);
				}
			} else if (arg1 instanceof Message) {
				sendObject((Message) arg1);
			}
		} catch (IOException e) {
			notifyDisconnection();
		}
	}
}