package it.polimi.ingsw.ps04.view;

import java.util.HashMap;
import java.util.Map;

import it.polimi.ingsw.ps04.client.view.connection.rmi.ClientRMIReceiverInterface;
import it.polimi.ingsw.ps04.view.socket.SocketConnection;

/**
 * this class manages the waiting connections, that is the players that are
 * waiting to join to a match.
 */
public class WaitingConnections {

	private Map<String, SocketConnection> waitingSocketConnections = new HashMap<>();
	private Map<String, ClientRMIReceiverInterface> waitingRMIConnections = new HashMap<>();

	/**
	 * Returns the number of the waiting connections
	 * 
	 * @return the number of the waiting connections
	 */
	public int numofWaiting() {
		return waitingSocketConnections.size() + waitingRMIConnections.size();
	}

	/**
	 * removes all the connections
	 */
	public void clearConnections() {
		waitingSocketConnections.clear();
		waitingRMIConnections.clear();
	}

	// Getters
	/**
	 * Returns the waiting RMI connections
	 * 
	 * @return the waiting RMI connections
	 */
	public Map<String, ClientRMIReceiverInterface> getWaitingRMIConnections() {
		return waitingRMIConnections;
	}

	/**
	 * Returns the waiting Socket connections
	 * 
	 * @return the waiting Socket connections
	 */
	public Map<String, SocketConnection> getWaitingSocketConnections() {
		return waitingSocketConnections;
	}

}
