package it.polimi.ingsw.ps04.view.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

import javax.security.auth.login.LoginException;

import it.polimi.ingsw.ps04.client.view.connection.rmi.ClientRMIReceiverInterface;
import it.polimi.ingsw.ps04.utils.message.login.CredentialsMessage;

/**
 * This class represent the interface of the RMI Game
 * 
 * @see RMIGame
 */
public interface RMIGameInterface extends Remote {

	/**
	 * Registers a client
	 * 
	 * @param credentialsMessage
	 *            the credentials message sent from the client
	 * @param clientStub
	 *            the client stub of the RMI
	 * @return true if the client was registered
	 * @throws RemoteException
	 *             if there is a problem with the network
	 * @throws LoginException
	 *             if the login was not successful
	 */
	public boolean registerClient(CredentialsMessage credentialsMessage, ClientRMIReceiverInterface clientStub)
			throws RemoteException, LoginException;

}
