package it.polimi.ingsw.ps04.view;

import java.io.IOException;
import java.util.Scanner;

import it.polimi.ingsw.ps04.utils.login.LoginManager;

/**
 * This class implements a cli for the server
 *
 */
public class MenuThread extends Thread {

	private Server server;

	/**
	 * Constructs a menu thread for the given server
	 * 
	 * @param server
	 *            the server that you want to assign to this menu thread
	 */
	public MenuThread(Server server) {
		this.server = server;
	}

	/**
	 * lets the server administrator choose what to do
	 * 
	 * @param scanner
	 *            the scanner where you want to take the input
	 * @throws IOException
	 *             if there is a problem with the I/O
	 */
	private void chooseService(Scanner scanner) throws IOException {
		while (true) {
			System.out.println("Enter:\n1\tPrint Online users\n2\tstop server");
			String userInput = scanner.nextLine();
			if (userInput.equals("1") || userInput.equalsIgnoreCase("Print Online users")) {
				System.out.println(LoginManager.getInstance().getOnlineUsers());
			} else if (userInput.equals("2") || userInput.equalsIgnoreCase("stop server")) {
				server.stop();
			}
		}
	}

	/**
	 * starts the cli of the server
	 */
	public void run() {
		Scanner scanner = new Scanner(System.in);
		try {
			chooseService(scanner);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}