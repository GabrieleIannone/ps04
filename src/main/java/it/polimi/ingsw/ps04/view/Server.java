package it.polimi.ingsw.ps04.view;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.sql.SQLException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import it.polimi.ingsw.ps04.controller.GamesManager;
import it.polimi.ingsw.ps04.utils.db.DBConnector;
import it.polimi.ingsw.ps04.view.rmi.RMIGame;
import it.polimi.ingsw.ps04.view.socket.SocketConnection;

/**
 * Listen on a specific port and manage the distribution of the connections
 */
public class Server {
	private static final int PORT = 12345;
	private final static int RMI_PORT = 52365;
	private ServerSocket serverSocket;
	protected volatile boolean isStopped = false;
	private static final int MAX_PLAYERS_NUMBER = 128;

	// I use a fixed number of thread in order to ensure performance
	private ExecutorService executor = Executors.newFixedThreadPool(MAX_PLAYERS_NUMBER);

	/**
	 * Constructs a server and binds it to the default port
	 * 
	 * @throws IOException
	 *             if there was a problem with the I/O
	 */
	public Server() throws IOException {
		this.serverSocket = new ServerSocket(PORT);
	}

	/**
	 * starts to listen to new sockets
	 */
	private void startSocket() {
		while (!isStopped()) {
			try {
				System.out.println("server is ready");
				Socket newSocket = serverSocket.accept();
				System.out.println("I accept " + newSocket);
				SocketConnection connection = new SocketConnection(newSocket);
				executor.submit(connection);// it is equivalent to
											// Thread(connection).start();
			} catch (IOException e) {
				System.err.println("Connession error!");
			}
		}
	}

	/**
	 * Starts the RMI process
	 * 
	 * @throws RemoteException
	 *             if there is a problem with the network
	 * @throws AlreadyBoundException
	 *             if you to bind this object in the registry to a name that
	 *             already has an associated binding
	 */
	private void startRMI() throws RemoteException, AlreadyBoundException {
		Registry registry = LocateRegistry.createRegistry(RMI_PORT);
		RMIGame game = new RMIGame();
		UnicastRemoteObject.exportObject(game, 0);
		registry.bind("game", game);
	}

	/**
	 * checks if the server is stopped
	 * 
	 * @return true if the server is stopped
	 */
	private synchronized boolean isStopped() {
		return this.isStopped;
	}

	/**
	 * stops the server
	 */
	protected synchronized void stop() {
		this.isStopped = true;
		try {
			DBConnector.getInstance().shutdown();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		GamesManager.getInstance().closeAllGames();
	}

	/**
	 * starts the server
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		Server server;
		try {
			server = new Server();
			MenuThread mt = new MenuThread(server);
			mt.start();
			try {
				server.startRMI();
			} catch (AlreadyBoundException e) {
				e.printStackTrace();
			}
			server.startSocket();

		} catch (IOException e) {
			System.err.println("Inizialization of the server not possible: " + e.getMessage() + "!");
		}
	}

}