package it.polimi.ingsw.ps04.view.socket;

import java.io.IOException;

import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.utils.message.Message;
import it.polimi.ingsw.ps04.utils.message.PlayerDisconnectedMessage;
import it.polimi.ingsw.ps04.utils.observ.Observer;
import it.polimi.ingsw.ps04.view.View;

/**
 * The class that implements the socket version of the view
 */
public class SocketView extends View implements Observer<Message> {

	private SocketConnection connection;

	/**
	 * Constructs a socket view with the given player and socket connection
	 * 
	 * @param player
	 *            the player that you want to assign to this view
	 * @param socketConnection
	 *            the socket connection that you want to assign to this view
	 */
	public SocketView(Player player, SocketConnection socketConnection) {
		super(player);
		this.connection = socketConnection;
		socketConnection.register(this);
	}

	/**
	 * sends an object to the other side of the connection
	 */
	@Override
	protected void sendObject(Object obj) throws IOException {
		connection.send(obj);

	}

	/**
	 * This method is called by the connection to notify the controller
	 */
	@Override
	public void notify(Message message) {
		if (message instanceof PlayerDisconnectedMessage) {
			notifyDisconnection();
		} else {
			try {
				notifyMessage(message);
			} catch (IllegalArgumentException e) {
				try {
					connection.send("Error!");
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
	}

	/**
	 * closes the socket view
	 */
	@Override
	protected void close() throws IOException {
		connection.closeConnection();

	}

}
