package it.polimi.ingsw.ps04.view.rmi;

import java.rmi.RemoteException;

import javax.security.auth.login.LoginException;

import it.polimi.ingsw.ps04.client.view.connection.rmi.ClientRMIReceiverInterface;
import it.polimi.ingsw.ps04.controller.GamesManager;
import it.polimi.ingsw.ps04.utils.login.LoginManager;
import it.polimi.ingsw.ps04.utils.message.login.CredentialsMessage;
import it.polimi.ingsw.ps04.utils.message.login.LoginMessage;

/**
 * This class represent the RMI view to called in order to subscribe, login and
 * join a match Once players start to play they end to use this view and start
 * to use RMI View
 */
public class RMIGame implements RMIGameInterface {

	/**
	 * Once two or more players connect to server a match started. This method
	 * create a proper RMIView and send it to player's client
	 * 
	 * @param username
	 *            Username of the owner of the view
	 * @param clientStub
	 *            Reference to the RMIView in the server
	 */
	private void setClient(String username, ClientRMIReceiverInterface clientStub) throws RemoteException {
		Thread registerRMIConnectionThread = new Thread(new Runnable() {
			public void run() {
				GamesManager gamesManager = GamesManager.getInstance();
				gamesManager.registerRMIConnection(username, clientStub);
			}
		});
		registerRMIConnectionThread.start();
	}

	/**
	 * This method execute the login
	 */
	@Override
	public boolean registerClient(CredentialsMessage credentialsMessage, ClientRMIReceiverInterface clientStub)
			throws RemoteException, LoginException {
		Boolean operationSuccessful = LoginManager.getInstance().executeCredentialsMessage(credentialsMessage);

		if (operationSuccessful && credentialsMessage instanceof LoginMessage) {
			setClient(credentialsMessage.getUsername(), clientStub);
		}
		return operationSuccessful;

	}
}
