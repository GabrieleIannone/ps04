package it.polimi.ingsw.ps04.view.socket;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.util.NoSuchElementException;

import javax.security.auth.login.LoginException;

import it.polimi.ingsw.ps04.controller.GamesManager;
import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.utils.login.LoginManager;
import it.polimi.ingsw.ps04.utils.message.Message;
import it.polimi.ingsw.ps04.utils.message.PlayerDisconnectedMessage;
import it.polimi.ingsw.ps04.utils.message.login.CredentialsMessage;
import it.polimi.ingsw.ps04.utils.message.login.LoginMessage;
import it.polimi.ingsw.ps04.utils.message.login.SuccessMessage;
import it.polimi.ingsw.ps04.utils.observ.Observable;

/**
 * this class implements the connection with the socket
 */
public class SocketConnection extends Observable<Message> implements Runnable {

	private Socket socket;
	private String name;
	private boolean active = true;
	private ObjectOutputStream objectOutputStream;
	private ObjectInputStream objectInputStream;

	/**
	 * Constructs a socket connection with the given socket
	 * 
	 * @param socket
	 */
	public SocketConnection(Socket socket) {
		this.socket = socket;
	}

	/**
	 * checks if the connection is active
	 * 
	 * @return true if the connection is active
	 */
	private synchronized boolean isActive() {
		return active;
	}

	/**
	 * Reads a message from the other side of the connection
	 * 
	 * @return the message that was received
	 * @throws IOException
	 *             if there were problems in the connection
	 */
	private Message readMessage() throws IOException {
		Message message = null;
		try {
			message = (Message) objectInputStream.readObject();
		} catch (ClassNotFoundException | SocketException e) {
			closeConnection();
			notify(new PlayerDisconnectedMessage(new Player(name)));
		}
		return message;
	}

	/**
	 * Starts the thread. Reads message while it is active.
	 */
	@Override
	public void run() {
		try {
			objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
			objectInputStream = new ObjectInputStream(socket.getInputStream());
			CredentialsMessage credentialsMessage;
			Boolean operationSuccessful;
			do {
				credentialsMessage = (CredentialsMessage) readMessage();
				name = credentialsMessage.getUsername();
				try {
					operationSuccessful = LoginManager.getInstance().executeCredentialsMessage(credentialsMessage);
					if (operationSuccessful) {
						send(new SuccessMessage(operationSuccessful));
					}
				} catch (LoginException e) {
					System.out.println("login exception: " + e.getMessage());
					send(e);
					operationSuccessful = false;
				}
			} while (!operationSuccessful || !(credentialsMessage instanceof LoginMessage));
			GamesManager.getInstance().registerSocketConnection(name, this);
			while (isActive()) {
				Message message = readMessage();
				notify(message);
			}
		} catch (IOException | NoSuchElementException e) {
			e.printStackTrace();
			System.err.println("Error!");
		} finally {
			close();
		}
	}

	/**
	 * Sends an object to the other side of the connection
	 * 
	 * @param object
	 *            the object that you want to send
	 * @throws IOException
	 *             if there were problems in the connection
	 */
	public void send(Object object) throws IOException {
		if (objectOutputStream != null)
			objectOutputStream.writeObject(object);
	}

	/**
	 * closes the connection in a safe way
	 */
	public synchronized void closeConnection() {
		if (isActive()) {
			try {
				send("End of connection!");
			} catch (IOException e1) {
				// I do nothing, the connection was closed because of a
				// disconnection
			}
			try {
				if (socket != null) {
					socket.close();
					LoginManager.getInstance().disconnectUser(name);
				}
			} catch (IOException e) {
			}
			active = false;
			System.out.println("socket connection closed");
		}
	}

	/**
	 * close the connection
	 */
	private void close() {
		closeConnection();
	}

}
