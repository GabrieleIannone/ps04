package it.polimi.ingsw.ps04.view.rmi;

import java.io.IOException;
import java.rmi.RemoteException;

import it.polimi.ingsw.ps04.client.view.connection.rmi.ClientRMIReceiverInterface;
import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.utils.login.LoginManager;
import it.polimi.ingsw.ps04.utils.message.Message;
import it.polimi.ingsw.ps04.view.View;

/**
 * This class represent the concrete RMI View
 */
public class RMIView extends View implements RMIViewInterface {

	private ClientRMIReceiverInterface clientStub;

	/**
	 * Constructor of the RMI View
	 * 
	 * @param player
	 *            The player who belongs the view
	 * @param clientStub
	 *            The reference to the player's RMI View in the client
	 */
	public RMIView(Player player, ClientRMIReceiverInterface clientStub) {
		super(player);
		this.clientStub = clientStub;
	}

	/**
	 * This method is called by the client to send a message to server
	 * 
	 * @param message
	 *            The message sent by the client
	 */
	@Override
	public void receiveMessage(Message message) throws RemoteException {
		notifyMessage(message);
	}

	/**
	 * This method sends an object from the server to the client
	 * 
	 * @param obj
	 *            The object sent by the server
	 */
	@Override
	protected void sendObject(Object obj) throws RemoteException {
		clientStub.receiveObject(obj);

	}

	/**
	 * Close the connection between server and player's client
	 */
	@Override
	protected void close() throws IOException {
		clientStub = null;
		LoginManager.getInstance().disconnectUser(getPlayer().getName());
	}

}