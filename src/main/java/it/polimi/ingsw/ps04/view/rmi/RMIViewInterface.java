package it.polimi.ingsw.ps04.view.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

import it.polimi.ingsw.ps04.utils.message.Message;

/**
 * This class represent the interface of the RMI View
 * 
 * @see RMIView
 */
public interface RMIViewInterface extends Remote {

	/**
	 * Receives a message
	 * 
	 * @param message
	 *            the message that was sent
	 * @throws RemoteException
	 *             if there is a problem with the network
	 */
	void receiveMessage(Message message) throws RemoteException;

}
