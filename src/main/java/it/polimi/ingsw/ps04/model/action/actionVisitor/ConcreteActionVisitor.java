package it.polimi.ingsw.ps04.model.action.actionVisitor;

import it.polimi.ingsw.ps04.model.Model;
import it.polimi.ingsw.ps04.model.action.PassTurn;
import it.polimi.ingsw.ps04.model.action.main.AcquireBusinessPermitTile;
import it.polimi.ingsw.ps04.model.action.main.BuildEmporium;
import it.polimi.ingsw.ps04.model.action.main.BuildEmporiumWithKing;
import it.polimi.ingsw.ps04.model.action.main.ElectCouncillor;
import it.polimi.ingsw.ps04.model.action.quick.ChangeBusinessPermitTiles;
import it.polimi.ingsw.ps04.model.action.quick.EngageAssistant;
import it.polimi.ingsw.ps04.model.action.quick.GetAnotherPrimaryAction;
import it.polimi.ingsw.ps04.model.action.quick.SendAssistantToElectCouncillor;
import it.polimi.ingsw.ps04.model.board.City;
import it.polimi.ingsw.ps04.model.board.Council;
import it.polimi.ingsw.ps04.model.board.Region;
import it.polimi.ingsw.ps04.model.deck.business.BusinessPermitTile;

/**
 * this class implements the interface of the action visitor. It implements all
 * the visit methods in order to update the reference to the ones from the local
 * model
 */
public class ConcreteActionVisitor implements ActionVisitor {

	private Model model;

	/**
	 * Constructs a ConcreteActionVisitor from a given model
	 * 
	 * @param model
	 *            the model that you want to assign to the concrete action
	 *            visitor
	 */
	public ConcreteActionVisitor(Model model) {
		this.model = model;
	}

	/**
	 * @see ActionVisitor#visit(AcquireBusinessPermitTile)
	 */
	@Override
	public void visit(AcquireBusinessPermitTile action) throws IllegalArgumentException {
		Region clientRegion = action.getRegion();
		Region serverRegion = model.getMap().getRegionReference(clientRegion);
		action.setRegion(serverRegion);
	}

	/**
	 * @see ActionVisitor#visit(BuildEmporium)
	 */
	@Override
	public void visit(BuildEmporium action) throws IllegalArgumentException {
		City clientCity = action.getCity();
		City serverCity = model.getMap().getCityReference(clientCity);
		action.setCity(serverCity);
		BusinessPermitTile clientTile = action.getTile();
		BusinessPermitTile serverTile = model.getPlayersManager().getActivePlayer().getBusinessPermitTilePool()
				.getBusinessPermitTileReference(clientTile);
		action.setTile(serverTile);
		action.setMap(model.getMap());
	}

	/**
	 * @see ActionVisitor#visit(BuildEmporiumWithKing)
	 */
	@Override
	public void visit(BuildEmporiumWithKing action) throws IllegalArgumentException {
		City clientCity = action.getCity();
		City serverCity = model.getMap().getCityReference(clientCity);
		action.setCity(serverCity);
		action.setMap(model.getMap());
	}

	/**
	 * @see ActionVisitor#visit(ElectCouncillor)
	 */
	@Override
	public void visit(ElectCouncillor action) throws IllegalArgumentException {
		Region clientRegion = action.getRegion();
		if (clientRegion == null) {
			action.setCouncil(model.getMap().getKing().getCouncil());
		} else {
			Region serverRegion = model.getMap().getRegionReference(clientRegion);
			Council serverCouncil = serverRegion.getCouncil();

			action.setRegion(serverRegion);
			action.setCouncil(serverCouncil);
		}
	}

	/**
	 * @see ActionVisitor#visit(ChangeBusinessPermitTiles)
	 */
	@Override
	public void visit(ChangeBusinessPermitTiles action) throws IllegalArgumentException {
		Region clientRegion = action.getRegion();
		Region serverRegion = model.getMap().getRegionReference(clientRegion);
		action.setRegion(serverRegion);
	}

	/**
	 * @see ActionVisitor#visit(EngageAssistant)
	 */
	@Override
	public void visit(EngageAssistant action) {
		// do nothing
	}

	/**
	 * @see ActionVisitor#visit(GetAnotherPrimaryAction)
	 */
	@Override
	public void visit(GetAnotherPrimaryAction action) {
		// do nothing
	}

	/**
	 * @see ActionVisitor#visit(SendAssistantToElectCouncillor)
	 */
	@Override
	public void visit(SendAssistantToElectCouncillor action) throws IllegalArgumentException {
		Region clientRegion = action.getRegion();
		if (clientRegion == null) {
			action.setCouncil(model.getMap().getKing().getCouncil());
		} else {
			Region serverRegion = model.getMap().getRegionReference(clientRegion);
			Council serverCouncil = serverRegion.getCouncil();

			action.setRegion(serverRegion);
			action.setCouncil(serverCouncil);
		}
	}

	/**
	 * @see ActionVisitor#visit(PassTurn)
	 */
	@Override
	public void visit(PassTurn action) {
		model.getAvailableBonusesPool().clearAvailableBonus();
	}

}
