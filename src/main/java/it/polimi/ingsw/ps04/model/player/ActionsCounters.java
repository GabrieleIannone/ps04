package it.polimi.ingsw.ps04.model.player;

import java.io.Serializable;

import it.polimi.ingsw.ps04.utils.exception.ActionNotAvailableException;

/**
 * implements the counters of the actions of a player's turn, in order to
 * understand if an action is available or not.
 */
public class ActionsCounters implements Serializable {
	private static final long serialVersionUID = -2634288741044719319L;
	private int mainActionCounter;
	private boolean quickActionAvailable;

	/**
	 * Constructs an actions counters with the initial state
	 */
	public ActionsCounters() {
		resetActionsCounters();
	}

	/**
	 * Resets the actions counters of the player
	 */
	public void resetActionsCounters() {
		mainActionCounter = 1;
		quickActionAvailable = true;
	}

	/**
	 * Consumes all the counters.
	 */
	public void useAllActions() {
		mainActionCounter = 0;
		quickActionAvailable = false;
	}

	/**
	 * consumes a quick action
	 * 
	 * @throws ActionNotAvailableException
	 *             if that action counter is already consumed
	 */
	public void doQuickAction() throws ActionNotAvailableException {
		if (!quickActionAvailable) {
			throw new ActionNotAvailableException("Quick Action not available");
		}
		quickActionAvailable = false;
	}

	/**
	 * the quick action becomes available again
	 */
	public void undoQuickAction() {
		quickActionAvailable = true;
	}

	/**
	 * decreases the main action counter
	 * 
	 * @throws ActionNotAvailableException
	 *             if the main action is not available
	 */
	public void doMainAction() throws ActionNotAvailableException {
		if (mainActionCounter <= 0) {
			throw new ActionNotAvailableException("Main Action not available");
		}
		mainActionCounter--;
	}

	/**
	 * increases the main action counter
	 */
	public void additionalMainAction() {
		mainActionCounter++;
	}

	/**
	 * Checks if player can do an action of any type
	 * 
	 * @return false if he or she cannot, true if he or she can
	 */
	public boolean checkifnoActionAvalaible() {
		if ((mainActionCounter == 0) && (quickActionAvailable == false)) {
			return true;
		} else
			return false;
	}

}
