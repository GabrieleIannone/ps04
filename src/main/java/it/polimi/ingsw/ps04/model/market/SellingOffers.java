package it.polimi.ingsw.ps04.model.market;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.ps04.model.deck.business.BusinessPermitTile;
import it.polimi.ingsw.ps04.model.deck.politic.PoliticCard;
import it.polimi.ingsw.ps04.model.player.AssistantsCrew;
import it.polimi.ingsw.ps04.model.player.Player;

/**
 * Represents all the selling offers made by the same player
 */
public class SellingOffers implements Serializable {

	private static final long serialVersionUID = -3447108175454362027L;
	private AssistantsonSale assistantsonSale;
	private List<BusinessPermitTileonSale> tilesonSale = new ArrayList<>();
	private List<PoliticCardonSale> cardsonSale = new ArrayList<>();

	/**
	 * Create an empty selling offer to fill
	 */
	public SellingOffers() {
		assistantsonSale = new AssistantsonSale(new AssistantsCrew(), 0);
	}

	/**
	 * Check if all order object are on sale
	 * 
	 * @param order
	 *            the order to check
	 * @return false if the number of assistants on sale are lower than the
	 *         number in the order or if at least one business permit tile or
	 *         politic card is not on sale. True otherwise
	 */
	protected boolean isOnSale(BuyingOrder order) {

		boolean check = true;

		if (order.getAssistants().getMembersNumber() > assistantsonSale.getAssistantCrew().getMembersNumber()) {
			check = false;
		}

		for (BusinessPermitTile orderedTileIterator : order.getTiles()) {
			if (!isTileonSale(orderedTileIterator))
				check = false;
		}

		for (PoliticCard politicCardIterator : order.getCards()) {
			if (!isPoliticCardonSale(politicCardIterator))
				check = false;
		}

		return check;
	}

	/**
	 * Check if a specific tile is on sale.
	 * 
	 * @param tile
	 *            The tile to check.
	 * @return true if the tile is on sale. False otherwise.
	 */
	private boolean isTileonSale(BusinessPermitTile tile) {
		boolean check = false;
		for (BusinessPermitTileonSale tileonSaleIterator : tilesonSale) {
			if (tile.equals(tileonSaleIterator.getTile()))
				check = true;
		}
		return check;
	}

	/**
	 * Check if a specific politic card is on sale.
	 * 
	 * @param card
	 *            The politic card to check.
	 * @return true if the card is on sale. False otherwise.
	 */
	private boolean isPoliticCardonSale(PoliticCard card) {
		boolean check = false;
		for (PoliticCardonSale cardonSaleIterator : cardsonSale) {
			if (card.equals(cardonSaleIterator.getCard()))
				check = true;
		}
		return check;
	}

	/**
	 * Execute an order, moving all the object in the order from the seller to
	 * the buyer. For each object moved in this way the buyer pays its cost to
	 * the seller
	 * 
	 * @param seller
	 *            The seller of this transaction
	 * @param order
	 *            The object purchased by the buyer from the seller
	 * @param buyer
	 *            The buyer of this transaction
	 */
	protected void sell(Player seller, BuyingOrder order, Player buyer) {

		/**
		 * The seller gives to the buyer the number of assistants written into
		 * the order. Then the buyer pays the seller an amount of coins equal to
		 * the cost per assistant for each assistant bought.
		 * 
		 * Then I update the number of assistants on sale
		 */
		seller.getCoins().addCoins((order.getAssistants().getMembersNumber()) * (assistantsonSale.getCost()));
		seller.getAssistantCrew().sendAssistants(order.getAssistants().getMembersNumber());
		buyer.getCoins().payFor((order.getAssistants().getMembersNumber()) * (assistantsonSale.getCost()));
		buyer.getAssistantCrew().engageAssistants(order.getAssistants().getMembersNumber());
		assistantsonSale.getAssistantCrew().sendAssistants(order.getAssistants().getMembersNumber());

		/**
		 * For each tile into the order, the seller gives it to the buyer and
		 * the buyer pays an amount of coins to the seller equal to its cost
		 * 
		 * Then I remove the tile sold from the offers on sale
		 */
		for (BusinessPermitTile tileIterator : order.getTiles()) {
			BusinessPermitTileonSale tileonSale = getReferenceTileonSale(tileIterator);
			seller.getCoins().addCoins(tileonSale.cost);
			BusinessPermitTile acquiredTile = seller.getBusinessPermitTilePool()
					.getBusinessPermitTileReference(tileIterator);
			seller.getBusinessPermitTilePool().removePermitTile(acquiredTile);
			buyer.getCoins().payFor(tileonSale.cost);
			buyer.getBusinessPermitTilePool().addPermitTile(acquiredTile);
			tilesonSale.remove(tileonSale);
		}

		/**
		 * For each politic card into the order, the seller gives it to the
		 * buyer and the buyer pays an amount of coins to the seller equal to
		 * its cost
		 * 
		 * Then I remove the card sold from the offers on sale
		 */
		for (PoliticCard cardIterator : order.getCards()) {
			PoliticCardonSale cardonSale = getReferenceCardonSale(cardIterator);
			seller.getCoins().addCoins(cardonSale.cost);
			seller.getPoliticCardsHand().useCard(cardIterator);
			buyer.getCoins().payFor(cardonSale.cost);
			buyer.getPoliticCardsHand().drawCard(cardIterator);
			cardsonSale.remove(cardonSale);
		}
	}

	/**
	 * Check the cost of an order from a single player
	 * 
	 * @param buyingOrder
	 *            The order of which I want to calculate the cost
	 * @return cost of the order
	 */
	protected int calculateCost(BuyingOrder buyingOrder) {
		int cost = 0;
		cost += buyingOrder.getAssistants().getMembersNumber() * assistantsonSale.cost;
		for (BusinessPermitTile tileIterator : buyingOrder.getTiles()) {
			cost += getReferenceTileonSale(tileIterator).getCost();
		}
		for (PoliticCard cardIterator : buyingOrder.getCards()) {
			cost += getReferenceCardonSale(cardIterator).getCost();
		}
		return cost;
	}

	/**
	 * Add a new politic card to sell
	 * 
	 * @param card
	 *            the card to sell
	 */
	public void addCardOnSale(PoliticCardonSale card) {
		cardsonSale.add(card);
	}

	/**
	 * Add a new business permit tile to sell
	 * 
	 * @param tile
	 *            the business permit tile to sell
	 */
	public void addTileOnSale(BusinessPermitTileonSale tile) {
		tilesonSale.add(tile);
	}

	/**
	 * Returns the reference of a business permit tile equals to the one passed
	 * as parameter
	 * 
	 * @param tile
	 *            the tile equals to the tile that you want to get the reference
	 * @return the reference of the tile that was found
	 */
	private BusinessPermitTileonSale getReferenceTileonSale(BusinessPermitTile tile) {
		for (BusinessPermitTileonSale tileIterator : tilesonSale) {
			if (tileIterator.getTile().equals(tile))
				return tileIterator;
		}
		return null;
	}

	/**
	 * Returns the reference of a politic card equals to the one passed as
	 * parameter
	 * 
	 * @param tile
	 *            the tile equals to the politic card that you want to get the
	 *            reference
	 * @return the reference of the politic card that was found
	 */
	private PoliticCardonSale getReferenceCardonSale(PoliticCard card) {
		for (PoliticCardonSale cardIterator : cardsonSale) {
			if (cardIterator.getCard().equals(card))
				return cardIterator;
		}
		return null;
	}

	// Getters and Setters
	/**
	 * Returns the assistants on sale
	 * 
	 * @return the assistants on sale
	 */
	public AssistantsonSale getAssistantsonSale() {
		return assistantsonSale;
	}

	/**
	 * Changes the assistants on sale to be equal to the argument
	 * assistantsonSale
	 * 
	 * @param assistantsonSale
	 *            the assistants on sale that you want to set
	 */
	public void setAssistantsonSale(AssistantsonSale assistantsonSale) {
		this.assistantsonSale = assistantsonSale;
	}

	/**
	 * Returns the tiles on sale
	 * 
	 * @return the tiles on sale
	 */
	public List<BusinessPermitTileonSale> getTilesonSale() {
		return tilesonSale;
	}

	/**
	 * Returns the cards on sale
	 * 
	 * @return the cards on sale
	 */
	public List<PoliticCardonSale> getCardsonSale() {
		return cardsonSale;
	}

	/**
	 * Converts this <code>SellingOffers</code> object to a <code>String</code>
	 */
	@Override
	public String toString() {
		return "SellingOffers [assistantsonSale=" + assistantsonSale + ", tilesonSale=" + tilesonSale + ", cardsonSale="
				+ cardsonSale + "]";
	}

	/**
	 * Returns a hash code value for this object. It considers assistants on
	 * sale, cards on sale and tiles on sale.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((assistantsonSale == null) ? 0 : assistantsonSale.hashCode());
		result = prime * result + ((cardsonSale == null) ? 0 : cardsonSale.hashCode());
		result = prime * result + ((tilesonSale == null) ? 0 : tilesonSale.hashCode());
		return result;
	}

	/**
	 * Compares two selling offers for equality. The result is <code>true</code>
	 * if and only if the argument is not <code>null</code> and is a
	 * <code>SellingOffers</code> object that has the same assistants on sale,
	 * cards on sale and tiles on sale.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SellingOffers other = (SellingOffers) obj;
		if (assistantsonSale == null) {
			if (other.assistantsonSale != null)
				return false;
		} else if (!assistantsonSale.equals(other.assistantsonSale))
			return false;
		if (cardsonSale == null) {
			if (other.cardsonSale != null)
				return false;
		} else if (!cardsonSale.equals(other.cardsonSale))
			return false;
		if (tilesonSale == null) {
			if (other.tilesonSale != null)
				return false;
		} else if (!tilesonSale.equals(other.tilesonSale))
			return false;
		return true;
	}
}
