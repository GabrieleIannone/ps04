package it.polimi.ingsw.ps04.model;

import java.io.Serializable;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.common.base.MoreObjects;

import it.polimi.ingsw.ps04.controller.ServerController;
import it.polimi.ingsw.ps04.utils.log.Handlers;
import it.polimi.ingsw.ps04.utils.message.Message;
import it.polimi.ingsw.ps04.utils.message.PlayerDisconnectedMessage;

/**
 * This class works as a security level between the view and the model. The view
 * gets notifies by this object and not by the real model, so if it modifies the
 * model it only changes this copy.
 */
public class GameModelView extends Observable implements Observer, Serializable {
	private static final Logger log = Logger.getLogger(ServerController.class.getName());
	private static final long serialVersionUID = 1450592784959615435L;
	private Model model;

	/**
	 * Constructs an empty game model view
	 */
	public GameModelView() {
		log.addHandler(Handlers.getInstance().SERVER_HANDLER);
	}

	/**
	 * Returns the model of the game model view
	 * 
	 * @return the model of the game model view
	 */
	public Model getModel() {
		return model;
	}

	/**
	 * Changes the state of this object because it replaces the model to be
	 * equal to the argument model
	 * 
	 * @param model
	 *            the model that you want to set
	 */
	public void setState(Model model) {
		this.model = model;
	}

	/**
	 * updates the messages that come from the observed object
	 */
	@Override
	public void update(java.util.Observable o, Object arg) {
		if (!(o instanceof Model)) {
			throw new IllegalArgumentException();
		}
		log.log(Level.FINE, "GMV arg: " + arg);
		if (arg == null) {
			log.log(Level.FINE, "GMV: model changed");
			setState(((Model) o).clone());
			log.log(Level.FINE, "GMV: TIME OUT: " + model.getTurnTimeout().getSeconds());
			setChanged();
			notifyObservers();
		} else if (arg instanceof PlayerDisconnectedMessage) {
			log.log(Level.FINE, "GMV: number of observers after: " + this.countObservers());
			PlayerDisconnectedMessage disconnessionMessage = (PlayerDisconnectedMessage) arg;
			this.deleteObserver(disconnessionMessage.getView());
			log.log(Level.FINE, "GMV: number of observers before: " + this.countObservers());
			disconnessionMessage.setView(null);
			setChanged();
			notifyObservers(disconnessionMessage);
		} else if (arg instanceof Message) {
			log.log(Level.FINE, "arg instance of message");
			setChanged();
			notifyObservers(arg);
		} else {
			log.log(Level.FINE, "GMV does nothing");
		}

	}

	/**
	 * Converts this <code>GameModelView</code> object to a <code>String</code>
	 */
	@Override
	public String toString() {
		return MoreObjects.toStringHelper(getClass()).addValue(model).toString();

	}

}
