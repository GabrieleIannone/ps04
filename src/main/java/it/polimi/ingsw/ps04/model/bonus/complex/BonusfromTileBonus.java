package it.polimi.ingsw.ps04.model.bonus.complex;

import java.util.HashSet;
import java.util.Set;

import it.polimi.ingsw.ps04.client.view.userinterface.UserInterface;
import it.polimi.ingsw.ps04.model.Model;
import it.polimi.ingsw.ps04.model.bonus.Bonus;
import it.polimi.ingsw.ps04.model.bonus.bonusVisitor.BonusVisitor;
import it.polimi.ingsw.ps04.model.deck.business.BusinessPermitTile;
import it.polimi.ingsw.ps04.utils.exception.NoPermitTileException;

/**
 * This bonus is a complex bonus. It activates all the bonus from a number of
 * tiles equals to the bonus increment, owned and chosen by the player who
 * activates this.
 */
public class BonusfromTileBonus extends ComplexBonus {

	private static final long serialVersionUID = -2701499839259299955L;
	private Set<BusinessPermitTile> businessTiles = new HashSet<BusinessPermitTile>();

	/**
	 * Constructor. Set the bonus increment and the model.
	 * 
	 * @param bonusIncrementer
	 *            The bonusIncrement of this bonus.
	 * @param model
	 *            The local model. Used to reach the map.
	 */
	public BonusfromTileBonus(int bonusIncrementer, Model model) {
		super.bonusIncrement = bonusIncrementer;
		super.model = model;
	}

	/**
	 * @return The set of the tiles chosen by the player.
	 */
	public Set<BusinessPermitTile> getBusinessTiles() {
		return businessTiles;
	}

	/**
	 * Set the field businessTiles of this bonus.
	 * 
	 * @param cities
	 *            The set of the tiles chosen by the player who activated the
	 *            bonus.
	 */
	public void setBusinessTiles(Set<BusinessPermitTile> businessTiles) {
		this.businessTiles = businessTiles;
	}

	/**
	 * Called in the client. It is used to collect the choices of the player.
	 * 
	 * @see it.polimi.ingsw.ps04.client.view.userinterface.UserInterface#insertMyTile()
	 * 
	 * @param ui
	 *            The user interface used by the player who activated the bonus.
	 */
	@Override
	public void compileBonus(UserInterface ui) {
		try {
			while (businessTiles.size() < bonusIncrement) {
				BusinessPermitTile tile = ui.insertMyTile();
				businessTiles.add(tile);
			}
			ui.getMessageSender().sendCompiledComplexBonus(this);
		} catch (NoPermitTileException e) {
			if (businessTiles.size() > 0) {
				ui.getMessageSender().sendCompiledComplexBonus(this);
			}
		}
	}

	/**
	 * Redefine the reference of tiles and model.
	 * 
	 * @see BonusVisitor#redefine(BonusfromTileBonus, Model)
	 * 
	 * @param visitor
	 *            The local bonus visitor.
	 * 
	 * @param model
	 *            The local model.
	 */
	@Override
	public void redefine(BonusVisitor visitor, Model model) {
		visitor.redefine(this, model);
	}

	/**
	 * Execute the bonus. For each tile chosen by the player it activated all
	 * the bonus of that tile.
	 * 
	 * @param visitor
	 *            The local bonus visitor.
	 */
	@Override
	public void execute(BonusVisitor visitor) {
		for (BusinessPermitTile tileIterator : businessTiles) {
			Set<Bonus> activatedBonuses = tileIterator.getBonuses();
			for (Bonus bonusIterator : activatedBonuses) {
				bonusIterator.accept(visitor, model.getPlayersManager().getActivePlayer());
			}
		}
	}

}
