package it.polimi.ingsw.ps04.model.board;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import it.polimi.ingsw.ps04.model.bonus.Bonus;
import it.polimi.ingsw.ps04.model.deck.business.BusinessDeck;
import it.polimi.ingsw.ps04.utils.exception.BadConfigFileException;

/**
 * this class implements the regions of the game
 */
public class Region implements Serializable {

	private static final long serialVersionUID = 2876006294024588800L;
	private String name;
	private BusinessDeck businessDeck;
	private Council council;
	private Set<Bonus> bonuses;
	private Set<City> cities = new HashSet<City>();
	private boolean bonusTaken = false;

	/**
	 * Constructs a region with the given name
	 * 
	 * @param name
	 *            the name that you want to assign to the region
	 */
	public Region(String name) {
		this.name = name;
	}

	/**
	 * Constructs a region with the given parameters
	 * 
	 * @param name
	 *            the name that you want to assign to the region
	 * @param council
	 *            the council that you want to assign to the region
	 * @param bonuses
	 *            the bonuses that you want to assign to the region
	 */
	public Region(String name, Council council, Set<Bonus> bonuses) {
		this(name);
		this.council = council;
		this.bonuses = bonuses;
	}

	/**
	 * Returns the name of this region
	 * 
	 * @return the name of this region
	 */
	public String getName() {
		return name;
	}

	/**
	 * Returns the business deck of this region
	 * 
	 * @return the business deck of this region
	 */
	public BusinessDeck getBusinessDeck() {
		return businessDeck;
	}

	/**
	 * Changes the business deck of the region to be equal to the argument
	 * businessDeck
	 * 
	 * @param businessDeck
	 *            the business deck that you want to assign to this region
	 */
	public void setBusinessDeck(BusinessDeck businessDeck) {
		this.businessDeck = businessDeck;
	}

	/**
	 * Adds a city to this region
	 * 
	 * @param city
	 *            the city that you want to add to this region
	 */
	public void addCity(City city) {
		cities.add(city);
	}

	/**
	 * Returns a city reference to the city contained in this region that has
	 * the name passed in the argument cityName
	 * 
	 * @param cityName
	 *            the name of the city that you want to obtain the reference
	 * @return a city reference to the city contained in this region that has
	 *         the name passed in the argument cityName
	 * @throws BadConfigFileException
	 *             if the configuration files contains errors
	 */
	public City findCity(String cityName) throws BadConfigFileException {
		for (City city : cities) {
			if (cityName.equalsIgnoreCase(city.getName()))
				return city;
		}
		throw new BadConfigFileException("City " + cityName + " not found");
	}

	/**
	 * says if the bonus is taken
	 * 
	 * @return true if the bonus is taken
	 */
	public boolean isBonusTaken() {
		return bonusTaken;
	}

	/**
	 * set if the bonus is taken or not
	 * 
	 * @param bonusTaken
	 *            the bonusTaken
	 */
	public void setBonusTaken(boolean bonusTaken) {
		this.bonusTaken = bonusTaken;
	}

	/**
	 * Returns the council of the region
	 * 
	 * @return the council of the region
	 */
	public Council getCouncil() {
		return council;
	}

	/**
	 * Returns the bonuses of the region
	 * 
	 * @return the bonuses of the region
	 */
	public Set<Bonus> getBonuses() {
		return bonuses;
	}

	/**
	 * Changes the bonuses of the region to be equal to the argument bonuses
	 * 
	 * @param bonuses
	 *            the bonuses that you want to set
	 */
	public void setBonuses(Set<Bonus> bonuses) {
		this.bonuses = bonuses;
	}

	/**
	 * Returns a hash code value for this object. It considers if the bonus is
	 * taken, the bonuses, the business deck, the cities and the name.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bonuses == null) ? 0 : bonuses.hashCode());
		result = prime * result + ((businessDeck == null) ? 0 : businessDeck.hashCode());
		result = prime * result + ((cities == null) ? 0 : cities.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/**
	 * Compares two regions for equality. The result is <code>true</code> if and
	 * only if the argument is not <code>null</code> and is a
	 * <code>Region</code> object that represents the same bonusTaken, bonuses,
	 * name, business deck and cities.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Region other = (Region) obj;
		if (bonuses == null) {
			if (other.bonuses != null)
				return false;
		} else if (!bonuses.equals(other.bonuses))
			return false;
		if (businessDeck == null) {
			if (other.businessDeck != null)
				return false;
		} else if (!businessDeck.equals(other.businessDeck))
			return false;
		if (cities == null) {
			if (other.cities != null)
				return false;
		} else if (!cities.equals(other.cities))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	/**
	 * Converts this <code>Region</code> object to a <code>String</code>
	 */
	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.JSON_STYLE).append("name", name).append("bonus", bonuses)
				.append("cities", cities).append("council", council.getCouncillors())
				.append("business deck", businessDeck.getRevealedTiles()).toString();
	}

}
