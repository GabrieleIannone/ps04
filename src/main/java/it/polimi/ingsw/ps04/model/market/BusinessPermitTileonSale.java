package it.polimi.ingsw.ps04.model.market;

import it.polimi.ingsw.ps04.model.deck.business.BusinessPermitTile;

/**
 * Represents a single business permit tile that a player wants to sell
 */
public class BusinessPermitTileonSale extends ObjectonSale {

	private static final long serialVersionUID = 8107782249848515866L;
	private BusinessPermitTile tile;

	/**
	 * Create a new tile to sell
	 * 
	 * @param tile
	 *            The tile to sell
	 * @param cost
	 *            The tile cost
	 */
	public BusinessPermitTileonSale(BusinessPermitTile tile, int cost) {
		super(cost);
		this.tile = tile;
	}

	/**
	 * @return the tile, without his cost
	 */
	public BusinessPermitTile getTile() {
		return tile;
	}

	/**
	 * Converts this <code>BusinessPermitTileonSale</code> object to a
	 * <code>String</code>
	 */
	@Override
	public String toString() {
		return "BusinessPermitTileonSale [tile=" + tile + ", cost=" + cost + "]";
	}

	/**
	 * Returns a hash code value for this object. It considers the tile.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((tile == null) ? 0 : tile.hashCode());
		return result;
	}

	/**
	 * Compares two tiles on sale for equality. The result is <code>true</code>
	 * if and only if the argument is not <code>null</code> and is a
	 * <code>BusinessPermitTileonSale</code> object that represents the same
	 * tile.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		BusinessPermitTileonSale other = (BusinessPermitTileonSale) obj;
		if (tile == null) {
			if (other.tile != null)
				return false;
		} else if (!tile.equals(other.tile))
			return false;
		return true;
	}

}
