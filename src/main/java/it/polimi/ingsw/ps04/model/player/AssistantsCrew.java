package it.polimi.ingsw.ps04.model.player;

import java.io.Serializable;

/**
 * this class represents a group of assistants of the game
 */
public class AssistantsCrew implements Serializable {

	private static final long serialVersionUID = 5669155371310515437L;
	private int membersNumber;

	/**
	 * Constructs an empty assistants crew
	 */
	public AssistantsCrew() {
		membersNumber = 0;
	}

	/**
	 * Constructs an assistants crew with the right number of assistants, that
	 * is calculated from the player turn
	 * 
	 * @param playerTurnNumber
	 *            the turn of the player that owns the assistants crew
	 */
	public AssistantsCrew(int playerTurnNumber) {
		membersNumber = playerTurnNumber;
	}

	/**
	 * engages a given number of assistants
	 * 
	 * @param assistantsEngagedNumber
	 *            the number of assistants that you want to engage
	 */
	public void engageAssistants(int assistantsEngagedNumber) {
		if (assistantsEngagedNumber >= 0) {
			membersNumber += assistantsEngagedNumber;
		} else {
			throw new IllegalArgumentException();
		}
	}

	/**
	 * sends away from this crew a given number of assistants
	 * 
	 * @param assistantsSentNumber
	 *            the number of assistants that you want to send away
	 * @throws IllegalArgumentException
	 *             if the size of this crew is minor to the assistantsSentNumber
	 */
	public void sendAssistants(int assistantsSentNumber) throws IllegalArgumentException {
		if (assistantsSentNumber >= 0 && assistantsSentNumber <= membersNumber) {
			membersNumber -= assistantsSentNumber;
		} else {
			throw new IllegalArgumentException("You don't have enough assistants");
		}
	}

	/**
	 * Returns the number of assistants of this crew
	 * 
	 * @return the number of assistants of this crew
	 */
	public int getMembersNumber() {
		return membersNumber;
	}

	/**
	 * Converts this <code>AssistantsCrew</code> object to a <code>String</code>
	 */
	@Override
	public String toString() {
		return String.valueOf(membersNumber);
	}

	/**
	 * Returns a hash code value for this object. It considers the members
	 * number.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + membersNumber;
		return result;
	}

	/**
	 * Compares two assistants crew for equality. The result is
	 * <code>true</code> if and only if the argument is not <code>null</code>
	 * and is a <code>AssistantsCrew</code> object that contains the same number
	 * of members.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AssistantsCrew other = (AssistantsCrew) obj;
		if (membersNumber != other.membersNumber)
			return false;
		return true;
	}

}