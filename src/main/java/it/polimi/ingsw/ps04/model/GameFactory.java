package it.polimi.ingsw.ps04.model;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;

import it.polimi.ingsw.ps04.model.board.map.Map;
import it.polimi.ingsw.ps04.model.board.map.MapFactory;
import it.polimi.ingsw.ps04.model.bonus.BonusFactory;
import it.polimi.ingsw.ps04.model.deck.business.BusinessDeckFactory;
import it.polimi.ingsw.ps04.model.deck.politic.PoliticCard;
import it.polimi.ingsw.ps04.model.deck.politic.PoliticDeck;
import it.polimi.ingsw.ps04.model.nobilitytrack.NobilityTrack;
import it.polimi.ingsw.ps04.model.nobilitytrack.NobilityTrackFactory;
import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.utils.ConfigHelper;
import it.polimi.ingsw.ps04.utils.color.ColorsManager;
import it.polimi.ingsw.ps04.utils.color.ColorsManagerFactory;
import it.polimi.ingsw.ps04.utils.db.DBConnector;
import it.polimi.ingsw.ps04.utils.exception.BadConfigFileException;

/**
 * Builds the game model from a json configuration file
 */
public class GameFactory {

	/**
	 * creates a model from a given list of player names
	 * 
	 * @param playerNames
	 *            the names of the players that will play the game
	 * @return a model from a given list of player names
	 * @throws BadConfigFileException
	 *             if the configuration file contains error
	 * @throws JsonProcessingException
	 *             if there were problems encountered when processing JSON
	 *             content that are not pure I/O problems
	 * @throws IOException
	 *             if a problem with the I/O has occurred
	 */
	public static Model createGame(List<String> playerNames)
			throws JsonProcessingException, IOException, BadConfigFileException {
		ConfigHelper configHelper = new ConfigHelper();
		return createGame(playerNames, configHelper);
	}

	/**
	 * creates a model from a given list of player names and a configuration
	 * helper
	 * 
	 * @param playerNames
	 *            the names of the players that will play the game
	 * @param configHelper
	 *            the configuration helper that was initialized with the
	 *            configuration file that contains the game characteristics that
	 *            you want
	 * @return a model from a given list of player names and a configuration
	 *         helper
	 * @throws BadConfigFileException
	 *             if the configuration file contains error
	 * @throws JsonProcessingException
	 *             if there were problems encountered when processing JSON
	 *             content that are not pure I/O problems
	 * @throws IOException
	 *             if a problem with the I/O has occurred
	 */
	public static Model createGame(List<String> playerNames, ConfigHelper configHelper)
			throws JsonProcessingException, IOException, BadConfigFileException {
		try {
			DBConnector.getInstance().incrementPlayedGames(playerNames);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		List<Player> players = new ArrayList<>();
		// I copy the name of the players in order to shuffle them and to
		// calculate the turns
		Collections.shuffle(playerNames);

		Model gameModel = new Model();
		BonusFactory bonusFactory = new BonusFactory(gameModel, configHelper);
		ColorsManager colorsManager = new ColorsManagerFactory(configHelper, bonusFactory).factoryMethod();
		PoliticDeck politicCardsDeck = new PoliticDeck(colorsManager.getCorruptionColors());
		gameModel.setPoliticCardsDeck(politicCardsDeck);

		politicCardsDeck = gameModel.getPoliticDeck();
		NobilityTrackFactory nobilityTrackFactory = new NobilityTrackFactory(configHelper, bonusFactory, 0);
		NobilityTrack nobilityTrack = nobilityTrackFactory.factoryMethod();
		gameModel.setNobilityTrack(nobilityTrack);

		BusinessDeckFactory businessDeckFactory = new BusinessDeckFactory(bonusFactory);
		Map map = new MapFactory(colorsManager, configHelper, bonusFactory, businessDeckFactory, politicCardsDeck)
				.importMap("map_name_1");
		int playerTurn;
		int citiesNumber = map.getCitiesNumber();
		for (int i = 0; i < playerNames.size(); i++) {
			String playerName = playerNames.get(i);
			playerTurn = i + 1;
			List<PoliticCard> initialCards = politicCardsDeck.drawInitalCards();
			players.add(new Player(playerName, citiesNumber, playerTurn, initialCards));
		}
		gameModel.setMap(map);
		gameModel.setPlayers(players);
		gameModel.setTurnTimeout(configHelper.getTurnSeconds());
		return gameModel;
	}
}
