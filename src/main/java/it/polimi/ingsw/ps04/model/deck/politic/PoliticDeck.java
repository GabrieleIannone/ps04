package it.polimi.ingsw.ps04.model.deck.politic;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

import com.google.common.base.MoreObjects;

import it.polimi.ingsw.ps04.utils.color.ColorsManager;
import it.polimi.ingsw.ps04.utils.color.NamedColor;

/**
 * Represents politic deck, in which are stored all the politic card not drawn
 * yet.
 */
public class PoliticDeck implements Serializable {

	private static final long serialVersionUID = -1385651280790183544L;

	private static final int CARDS_FOR_EACH_COLOR = 13;

	private static final int JOKERS_FOR_EACH_COLOR = 2;

	private static final int INITIAL_PLAYER_POLITIC_CARDS_NUMBER = 6;

	private Set<NamedColor> corruptionColors = new HashSet<NamedColor>();
	private List<PoliticCard> deck = new ArrayList<>();

	/**
	 * Creates an empty politic deck
	 * 
	 * Called by the model to initialized an empty model
	 */
	public PoliticDeck() {
	}

	/**
	 * Creates a new Politic deck
	 * 
	 * @param corruptionColors
	 *            The set of colors used during the match
	 */
	public PoliticDeck(Set<NamedColor> corruptionColors) {
		this.corruptionColors = corruptionColors;
		fill();
	}

	/**
	 * Create and return a list of politic card whith size equals to the initial
	 * hand size.
	 * 
	 * Called by the game factory for each player when a new game starts
	 * 
	 * @see it.polimi.ingsw.ps04.model.GameFactory#createGame(List,
	 *      it.polimi.ingsw.ps04.utils.ConfigHelper)
	 * @return An initial politic card hand
	 */
	public List<PoliticCard> drawInitalCards() {
		List<PoliticCard> initialCards = new ArrayList<>();
		for (int i = 0; i < INITIAL_PLAYER_POLITIC_CARDS_NUMBER; i++) {
			initialCards.add(drawCard());
		}
		return initialCards;
	}

	/**
	 * Overwrites the corruptionColors field with the one taken by the politic
	 * deck passed by parameter. Than fills the deck.
	 * 
	 * @see PoliticDeck#fill()
	 * 
	 * @param politicCardsDeck
	 *            The deck with an equal set of corruption colors
	 */
	public void set(PoliticDeck politicCardsDeck) {
		this.corruptionColors = politicCardsDeck.getCorruptionColors();
		fill();
	}

	/**
	 * Takes the first card of the deck and return it. If the deck is empty,
	 * refills the deck and than calls this method again
	 * 
	 * @see PoliticDeck#fill()
	 * 
	 * @return The politic card drawn
	 */
	public PoliticCard drawCard() {
		Iterator<PoliticCard> iterator = deck.iterator();
		PoliticCard card = null;
		if (iterator.hasNext()) {
			card = iterator.next();
			iterator.remove();
		} else {
			fill();
			return drawCard();
		}
		return card;
	}

	/**
	 * Fills an empty deck. For each color in corruptionColors it adds a number
	 * of card of that color equals to the final field CARDS_FOR_EACH_COLOR.
	 * Than it adds a number of Joker equals to the final field
	 * JOKERS_FOR_EACH_COLOR multiplied by the number of colors. Finally it
	 * shuffles the deck
	 */
	private void fill() {
		for (NamedColor color : corruptionColors) {
			for (int i = 0; i < CARDS_FOR_EACH_COLOR; i++) {
				deck.add(new PoliticCard(color));
			}
			for (int i = 0; i < JOKERS_FOR_EACH_COLOR; i++) {
				deck.add(new PoliticCard(ColorsManager.JOKER));
			}
		}
		Collections.shuffle(deck);
	}

	/**
	 * Return a random corruption color. Used by the Council constructor
	 * 
	 * @see it.polimi.ingsw.ps04.model.board.Council
	 * @return a random corruption color
	 */
	public NamedColor getRandomCorruptionColor() {
		Random generator = new Random();
		Object[] colors = corruptionColors.toArray();
		NamedColor randomColor = (NamedColor) colors[generator.nextInt(colors.length)];
		return randomColor;
	}

	// Getters and Setters
	/**
	 * Returns the corruption colors of the cards of this deck
	 * 
	 * @return the corruption colors of the cards of this deck
	 */
	public Set<NamedColor> getCorruptionColors() {
		return corruptionColors;
	}

	/**
	 * Finds the corruption color with the given name
	 * 
	 * @param name
	 *            the name of the corruption color that you want to find
	 * @return the corruption color with the given name
	 */
	public NamedColor getCorruptionColor(String name) {
		for (NamedColor color : corruptionColors) {
			if (color.getName().equalsIgnoreCase(name)) {
				return color;
			}
		}
		throw new IllegalArgumentException();
	}

	/**
	 * Returns the politic deck
	 * 
	 * @return the politic deck
	 */
	public List<PoliticCard> getDeck() {
		return deck;
	}

	/**
	 * Returns a hash code value for this object. It considers the deck.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((deck == null) ? 0 : deck.hashCode());
		return result;
	}

	/**
	 * Compares two politic decks for equality. The result is <code>true</code>
	 * if and only if the argument is not <code>null</code> and is a
	 * <code>PoliticDeck</code> object that represents the same deck.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PoliticDeck other = (PoliticDeck) obj;
		if (deck == null) {
			if (other.deck != null)
				return false;
		} else if (!deck.equals(other.deck))
			return false;
		return true;
	}

	/**
	 * Converts this <code>PoliticDeck</code> object to a <code>String</code>
	 */
	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).addValue(corruptionColors).toString();
	}

}
