package it.polimi.ingsw.ps04.model.bonus.simple;

import it.polimi.ingsw.ps04.model.bonus.Bonus;
import it.polimi.ingsw.ps04.model.bonus.bonusVisitor.BonusVisitor;
import it.polimi.ingsw.ps04.model.nobilitytrack.NobilityTrack;
import it.polimi.ingsw.ps04.model.player.Player;

/**
 * This bonus gives a number of nobility points to the player who activates this
 * equals to the bonus increment.
 */
public class NobilityBonus extends Bonus {

	private static final long serialVersionUID = 4132196193970842465L;
	private NobilityTrack nobilityTrack;

	/**
	 * Constructor. Set the bonus increment.
	 * 
	 * @param bonusIncrement
	 *            The bonusIncrement of this bonus.
	 * @param nobilityTrack
	 *            The nobility track of the model.
	 */
	public NobilityBonus(int bonusIncrementer, NobilityTrack nobilityTrack) {
		super.bonusIncrement = bonusIncrementer;
		this.nobilityTrack = nobilityTrack;
	}

	/**
	 * Set the nobility track.
	 * 
	 * @param nobilityTrack
	 *            The nobility track of the model.
	 */
	public void setNobilityTrack(NobilityTrack nobilityTrack) {
		this.nobilityTrack = nobilityTrack;
	}

	/**
	 * Execute the bonus. Gives to the player a number of nobility points equals
	 * to the bonus increment. Than activates any bonus activated for the new
	 * nobility level.
	 * 
	 * @see it.polimi.ingsw.ps04.model.nobilitytrack.NobilityTrack#giveBonus(Player)
	 * 
	 * @param player
	 *            The player who activated the bonus.
	 */
	public void execute(Player player) {
		for (int i = 0; i < bonusIncrement; i++) {
			player.getNobilityLevel().increaseLevel();
			nobilityTrack.giveBonus(player);
		}
	}

	/**
	 * @see Bonus#accept(BonusVisitor, Player)
	 */
	@Override
	public void accept(BonusVisitor visitor, Player player) {
		visitor.visit(this, player);
	}
}
