package it.polimi.ingsw.ps04.model.market;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.model.player.PlayersManager;
import it.polimi.ingsw.ps04.utils.exception.ActionNotCompletedException;
import it.polimi.ingsw.ps04.utils.message.market.BuyingOrdersMessage;
import it.polimi.ingsw.ps04.utils.message.market.SellingOffersMessage;

/**
 * This class represent the market, in which model saved and manage all the
 * offers of each player
 */
public class Market implements Serializable {

	private static final long serialVersionUID = -5778339183068752660L;
	private Map<String, SellingOffers> onSale = new HashMap<>();
	private PlayersManager playersManager;
	private boolean marketIsOpen;
	private boolean sellingPhase = true;

	/**
	 * Creates a new Market and closes it immediately. Called during the
	 * initialization of a match
	 */
	public Market() {
		marketIsOpen = false;
	}

	/**
	 * Each time market phase starts, a new market is created by the model,
	 * passing his playerManager
	 */
	public Market(PlayersManager playersManager) {
		this.playersManager = new PlayersManager(playersManager);
		marketIsOpen = true;
	}

	/**
	 * Saves the received selling offer, matched which the player who made the
	 * offer
	 * 
	 * @param message
	 *            The message which contains the selling offer
	 */
	public void receiveSellingOffer(SellingOffersMessage message) {
		SellingOffers newOffer = message.getSellingOffers();
		onSale.put(playersManager.getActivePlayer().getName(), newOffer);
	}

	/**
	 * Check if ordered object are on sale and if the buyer has enough coins to
	 * afford the transaction
	 * 
	 * @param message
	 *            The message which contains the order
	 * @throws ActionNotCompletedException
	 *             if at least one object is missing or buyer does not have
	 *             enough coins
	 */
	public void checkOrder(BuyingOrdersMessage message) throws ActionNotCompletedException {
		Map<String, BuyingOrder> orders = message.getOrders();
		Set<String> sellersNames = orders.keySet();

		for (String playerNameIterator : sellersNames) {
			try {
				playersManager.getPlayerReference(new Player(playerNameIterator));
			} catch (IllegalArgumentException e) {
				throw new ActionNotCompletedException("Player " + playerNameIterator + " does not exist!");
			}
			if (!(onSale.get(playerNameIterator).isOnSale(orders.get(playerNameIterator)))) {
				throw new ActionNotCompletedException("You cannot buy something not in sale!");
			}
		}

		int orderCost = calculateCost(orders);

		if (orderCost > playersManager.getActivePlayer().getCoins().getCoinsNumber()) {
			throw new ActionNotCompletedException("You don't have enough coins!");
		}
	}

	/**
	 * Process the order. For each seller invokes sell method
	 * 
	 * @see SellingOffers#sell
	 * @param message
	 *            The message which contains the order
	 */
	public void processOrder(BuyingOrdersMessage message) {
		Map<String, BuyingOrder> orders = message.getOrders();

		Set<String> sellersNames = orders.keySet();
		for (String playerNameIterator : sellersNames) {
			Player seller = playersManager.getPlayerReference(new Player(playerNameIterator));
			onSale.get(playerNameIterator).sell(seller, orders.get(playerNameIterator),
					playersManager.getActivePlayer());
		}
	}

	/**
	 * Calculate the cost of an order
	 * 
	 * For each seller I pick his selling offer and I call the calculate cost
	 * method
	 * 
	 * @see SellingOffers#calculateCost(BuyingOrder)
	 * 
	 * @param orders
	 *            The order of which I want to calculate the cost
	 * @return cost of orders
	 */
	private int calculateCost(Map<String, BuyingOrder> orders) {
		int cost = 0;
		Set<String> sellersNames = orders.keySet();
		for (String playerNameIterator : sellersNames) {
			cost += onSale.get(playerNameIterator).calculateCost(orders.get(playerNameIterator));
		}
		return cost;
	}

	/**
	 * Start buying phase, shuffling players' turns order
	 */
	public void startBuyingPhase() {
		Collections.shuffle(playersManager.getActivePlayers());
		playersManager.startRound();
		sellingPhase = false;
	}

	/**
	 * Start selling phase
	 */
	public void startSellingPhase() {
		sellingPhase = true;
	}

	/**
	 * Close the market, once both selling and buying phases are ended
	 */
	public void closeMarket() {
		onSale.clear();
		marketIsOpen = false;
	}

	// Getters and Setters

	/**
	 * @return if the market is open
	 */
	public boolean isMarketisOpen() {
		return marketIsOpen;
	}

	/**
	 * @return each selling offer, matched to the name of the player who made it
	 */
	public Map<String, SellingOffers> getOffers() {
		return onSale;
	}

	/**
	 * Check in which phase is the market, if selling or buying
	 * 
	 * @return true if it is selling phase
	 */
	public boolean isSellingPhase() {
		return sellingPhase;
	}

	/**
	 * @return market players Manager
	 */
	public PlayersManager getPlayersManager() {
		return playersManager;
	}

	/**
	 * Returns a hash code value for this object. It considers the selling
	 * offers and the state of the market.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (marketIsOpen ? 1231 : 1237);
		result = prime * result + ((onSale == null) ? 0 : onSale.hashCode());
		result = prime * result + (sellingPhase ? 1231 : 1237);
		return result;
	}

	/**
	 * Compares two markets for equality. The result is <code>true</code> if and
	 * only if the argument is not <code>null</code> and is a
	 * <code>Market</code> object that represents the same selling offers and
	 * that is in the same state.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Market other = (Market) obj;
		if (marketIsOpen != other.marketIsOpen)
			return false;
		if (onSale == null) {
			if (other.onSale != null)
				return false;
		} else if (!onSale.equals(other.onSale))
			return false;
		if (sellingPhase != other.sellingPhase)
			return false;
		return true;
	}

}
