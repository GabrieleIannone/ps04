package it.polimi.ingsw.ps04.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.ps04.model.bonus.complex.ComplexBonus;

/**
 * This class is used by the model in order to save complex bonuses activated
 * whose need a choice by the active player in order to be execute
 */
public class WaitingBonuses implements Serializable {

	private static final long serialVersionUID = 387712742611651935L;
	private List<ComplexBonus> availableBonuses = new ArrayList<ComplexBonus>();

	public List<ComplexBonus> getAvailableBonus() {
		return new ArrayList<>(availableBonuses);
	}

	/**
	 * This method checks if there is a certain type of complex bonus waiting to
	 * be executed
	 * 
	 * @param bonus
	 *            The bonus sent by the client
	 * @return true if in availableBonus there is a bonus equals to bonus
	 */
	public boolean bonusIsWaiting(ComplexBonus bonus) {
		for (ComplexBonus bonusIterator : availableBonuses) {
			if (bonus.equals(bonusIterator)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Called by an activated ComplexBonus Once a complex bonus is activated,
	 * bonusVisitor saved that bonus in WaitingBonus and than notify to the
	 * active player to make a choice
	 * 
	 * @see ComplexBonus
	 * @param bonus
	 *            The activated Bonus
	 */
	public void addBonus(ComplexBonus bonus) {
		availableBonuses.add(bonus);
	}

	public void removeBonus(ComplexBonus bonus) {
		int index = -1;

		for (ComplexBonus bonusIterator : availableBonuses) {
			if (bonusIterator.equals(bonus))
				index = availableBonuses.indexOf(bonusIterator);
		}

		if (index >= 0)
			availableBonuses.remove(index);
	}

	/**
	 * Clear availableBonus in order to pass the turn
	 */
	public void clearAvailableBonus() {
		availableBonuses.clear();
	}

	/**
	 * Check if there are bonus still waiting to be execute Called to decide if
	 * the turn is over
	 * 
	 * @return true if there is no bonus to be execute
	 */
	public boolean areWaitingBonus() {
		return availableBonuses.isEmpty();
	}

	/**
	 * Returns a hash code value for this object. It considers the
	 * availableBonuses.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((availableBonuses == null) ? 0 : availableBonuses.hashCode());
		return result;
	}

	/**
	 * Compares two waiting bonuses objects for equality. The result is
	 * <code>true</code> if and only if the argument is not <code>null</code>
	 * and is a <code>WaitingBonuses</code> object that contains the same
	 * available bonuses
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WaitingBonuses other = (WaitingBonuses) obj;
		if (availableBonuses == null) {
			if (other.availableBonuses != null)
				return false;
		} else if (!availableBonuses.equals(other.availableBonuses))
			return false;
		return true;
	}

}
