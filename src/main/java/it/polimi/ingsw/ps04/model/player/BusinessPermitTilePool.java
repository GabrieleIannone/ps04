package it.polimi.ingsw.ps04.model.player;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import it.polimi.ingsw.ps04.model.deck.business.BusinessPermitTile;
import it.polimi.ingsw.ps04.utils.exception.NoPermitTileException;

/**
 * Represents the permit tile pool of the player
 */
public class BusinessPermitTilePool implements Serializable {

	private static final long serialVersionUID = 4764486973023149143L;
	private List<BusinessPermitTile> permitTiles = new ArrayList<>();

	/**
	 * Constructs an empty business permit tile pool
	 */
	public BusinessPermitTilePool() {
	}

	/**
	 * Returns a business permit tile reference
	 * 
	 * @param tile
	 *            the tile that you want to find the reference
	 * @return a business permit tile reference
	 */
	public BusinessPermitTile getBusinessPermitTileReference(BusinessPermitTile tile) {
		for (BusinessPermitTile iterableTile : permitTiles) {
			if (iterableTile.equals(tile)) {
				return iterableTile;
			}
		}
		throw new IllegalArgumentException("Tile not found!");
	}

	/**
	 * add a permit tile to this pool
	 * 
	 * @param permitTile
	 *            the permit tile that you want to add to this pool
	 */
	public void addPermitTile(BusinessPermitTile permitTile) {
		permitTiles.add(permitTile);
	}

	/**
	 * remove a permit tile from this pool
	 * 
	 * @param permitTile
	 *            the permit tile that you want to remove from this pool
	 */
	public void removePermitTile(BusinessPermitTile permitTile) {
		permitTiles.remove(permitTile);
	}

	/**
	 * Returns the number of permit tiles of this pool
	 * 
	 * @return the number of permit tiles of this pool
	 */
	public int getPermitTilesNumber() {
		return permitTiles.size();
	}

	/**
	 * Returns the permit tiles of this pool
	 * 
	 * @return the permit tiles of this pool
	 */
	public List<BusinessPermitTile> getPermitTiles() {
		if (permitTiles.size() == 0) {
			throw new NoPermitTileException("player has not permit tiles yet");
		}
		return permitTiles;
	}

	/**
	 * Returns the permit tiles at a given index of this pool
	 * 
	 * @param index
	 *            the index of the permit tiles that you want to retrieve
	 * @return the permit tiles at a given index of this pool
	 */
	public BusinessPermitTile getBusinessPermitTileAt(int index) {
		return permitTiles.get(index);
	}

	/**
	 * Converts this <code>BusinessPermitTilePool</code> object to a
	 * <code>String</code>
	 */
	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.JSON_STYLE).append("permit tiles", permitTiles).toString();
	}

	/**
	 * Returns a hash code value for this object. It considers the permit tiles.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((permitTiles == null) ? 0 : permitTiles.hashCode());
		return result;
	}

	/**
	 * Compares two business permit tile pools for equality. The result is
	 * <code>true</code> if and only if the argument is not <code>null</code>
	 * and is a <code>BusinessPermitTilePool<</code> object that has the same
	 * permit tiles.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BusinessPermitTilePool other = (BusinessPermitTilePool) obj;
		if (permitTiles == null) {
			if (other.permitTiles != null)
				return false;
		} else if (!permitTiles.equals(other.permitTiles))
			return false;
		return true;
	}

}