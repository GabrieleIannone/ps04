package it.polimi.ingsw.ps04.model.bonus.simple;

import it.polimi.ingsw.ps04.model.player.Player;

/**
 * This bonus is a simple bonus. It gives a number of assistants to the player
 * who activates this equals to the bonus increment.
 */
public class AssistantsBonus extends SimpleBonus {

	private static final long serialVersionUID = -7599723051010026741L;

	/**
	 * Constructor. Set the bonus increment.
	 * 
	 * @param bonusIncrement
	 *            The bonusIncrement of this bonus.
	 */
	public AssistantsBonus(int bonusIncrement) {
		if (bonusIncrement <= 0) {
			throw new IllegalArgumentException("the assistant increment must be > 0");
		}
		super.bonusIncrement = bonusIncrement;
	}

	/**
	 * Execute the bonus. Gives to the player a number of assistants equals to
	 * the bonus increment.
	 * 
	 * @param player
	 *            The player who activated the bonus.
	 */
	@Override
	public void execute(Player player) {
		player.getAssistantCrew().engageAssistants(bonusIncrement);
	}
}
