package it.polimi.ingsw.ps04.model.board;

import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import com.google.common.base.MoreObjects;

import it.polimi.ingsw.ps04.model.deck.politic.PoliticCard;
import it.polimi.ingsw.ps04.model.deck.politic.PoliticDeck;
import it.polimi.ingsw.ps04.utils.color.ColorsManager;

/**
 * this class implements the councils of the game
 */
public class Council implements Serializable {

	private static final long serialVersionUID = 6886952676460912645L;
	// the number of councillors in a council
	public static final int COUNCILLORS_NUMBER = 4;
	private Queue<Councillor> councillors;

	/**
	 * Constructs a council with councillors that have a random corruption color
	 * taken from the ones of the given politicCardsDeck
	 * 
	 * @param politicCardsDeck
	 *            the deck that contains the corruption colors that you want to
	 *            use for this council
	 */
	public Council(PoliticDeck politicCardsDeck) {
		councillors = new LinkedList<Councillor>();
		for (int i = 0; i < COUNCILLORS_NUMBER; i++) {
			councillors.add(new Councillor(politicCardsDeck.getRandomCorruptionColor()));
		}
	}

	/**
	 * Constructs a council with the given councillors
	 * 
	 * @param councillors
	 *            the councillors that you want to assign to this council
	 */
	public Council(Queue<Councillor> councillors) {
		this.councillors = councillors;
	}

	/**
	 * elects a councillor in this council
	 * 
	 * @param councillor
	 *            the councillor that you want to elect
	 */
	public void electCouncillour(Councillor councillor) {
		councillors.add(councillor);
		councillors.remove();
	}

	/**
	 * get the cost associated to the action "ElectCouncillor" given the number
	 * of pure color correspondence and the number of jokers
	 * 
	 * @param colorCorrespondencesCounter
	 *            the number of pure color correspondence
	 * @param jokerNumbers
	 *            the number of jokers
	 * @return the cost associated to the given parameter
	 */
	private int getCost(int colorCorrespondencesCounter, int jokerNumbers) {
		if (colorCorrespondencesCounter + jokerNumbers == 4) {
			return jokerNumbers;
		} else {
			return 1 + (4 - colorCorrespondencesCounter + jokerNumbers) * 3 + jokerNumbers;
		}
	}

	/**
	 * get the cost associated to the action "ElectCouncillor" given certain
	 * politic cards
	 * 
	 * @param politicCards
	 *            the politic cards that you want to use to elect the councillor
	 * @return the cost associated to the given politic cards
	 * @throws IllegalArgumentException
	 *             if there are more cards then the councillors of this council
	 *             or if no card where match and there are no jokers
	 */
	public int getCorruptionCost(List<PoliticCard> politicCards) throws IllegalArgumentException {
		if (politicCards.size() > 4) {
			throw new IllegalArgumentException("too many cards");
		}
		int colourCorrispondencesCounter = 0;
		int jokerNumbers = 0; // number of joker cards
		// I do a copy of councillors in order to delete the matched ones
		List<Councillor> councillorsCopy = new LinkedList<Councillor>(councillors);
		Iterator<Councillor> iterator;
		for (PoliticCard card : politicCards) {
			if (ColorsManager.isJoker(card.getColour())) {
				jokerNumbers++;
			} else {
				iterator = councillorsCopy.iterator();
				while (iterator.hasNext()) {
					if (card.getColour().equals(iterator.next().getColor())) {
						colourCorrispondencesCounter++;
						iterator.remove();
						break;
					}
				}
			}
		}
		if (colourCorrispondencesCounter + jokerNumbers == 0) {
			throw new IllegalArgumentException("no matches found");
		}
		return getCost(colourCorrispondencesCounter, jokerNumbers);
	}

	/**
	 * Returns the councillors of this council
	 * 
	 * @return the councillors of this council
	 */
	public Queue<Councillor> getCouncillors() {
		return councillors;
	}

	/**
	 * Converts this <code>Council</code> object to a <code>String</code>
	 */
	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).addValue(councillors).toString();
	}

}
