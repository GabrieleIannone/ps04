package it.polimi.ingsw.ps04.model.action.quick;

import it.polimi.ingsw.ps04.model.action.Action;
import it.polimi.ingsw.ps04.model.action.actionVisitor.ActionVisitor;
import it.polimi.ingsw.ps04.model.action.actionVisitor.VisitableAction;
import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.utils.exception.ActionNotCompletedException;

/**
 * this class implements the "GetAnotherPrimaryAction" action of the game.
 */
public class GetAnotherPrimaryAction extends QuickAction {

	private static final long serialVersionUID = 98438739277239241L;
	public static final int ASSISTANTS_TO_SEND = 3;
	public static final String DESCRIPTION = "get another primary action by sending 3 of your assistants";

	/**
	 * @see Action#execute(Player)
	 */
	@Override
	public void execute(Player player) throws ActionNotCompletedException {
		useQuickAction(player);

		try {
			player.getAssistantCrew().sendAssistants(ASSISTANTS_TO_SEND);
		} catch (IllegalArgumentException e) {
			player.getActionsCounters().undoQuickAction();
			throw new ActionNotCompletedException("Not enough assistants!");
		}
		player.getActionsCounters().additionalMainAction();
	}

	/**
	 * @see VisitableAction#accept(ActionVisitor)
	 */
	@Override
	public void accept(ActionVisitor visitor) {
		visitor.visit(this);
	}

	/**
	 * Converts this object to a <code>String</code>
	 */
	@Override
	public String toString() {
		return "got another primary action";
	}
}
