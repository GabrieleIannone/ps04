package it.polimi.ingsw.ps04.model.market;

import it.polimi.ingsw.ps04.model.player.AssistantsCrew;

/**
 * Represents the assistants that a player wants to sell
 */
public class AssistantsonSale extends ObjectonSale {

	private static final long serialVersionUID = 1664487982318844797L;
	private AssistantsCrew assistantsCrew;

	/**
	 * Creates a new set of assistants to sell
	 * 
	 * @param assistantCrew
	 *            Saves and manages the number of assistants to sell
	 * @param cost
	 *            the cost per assistant
	 */
	public AssistantsonSale(AssistantsCrew assistantCrew, int cost) {
		super(cost);
		this.assistantsCrew = assistantCrew;
	}

	/**
	 * @return the assistants without the cost
	 */
	public AssistantsCrew getAssistantCrew() {
		return assistantsCrew;
	}

	/**
	 * Converts this <code>AssistantsonSale</code> object to a
	 * <code>String</code>
	 */
	@Override
	public String toString() {
		return "AssistantsonSale [assistantCrew=" + assistantsCrew + ", cost=" + cost + "]";
	}

	/**
	 * Returns a hash code value for this object. It considers the assistants
	 * crew.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((assistantsCrew == null) ? 0 : assistantsCrew.hashCode());
		return result;
	}

	/**
	 * Compares two assistants for equality. The result is <code>true</code> if
	 * and only if the argument is not <code>null</code> and is a
	 * <code>AssistantsonSale</code> object that represents the same assistants
	 * crew.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AssistantsonSale other = (AssistantsonSale) obj;
		if (assistantsCrew == null) {
			if (other.assistantsCrew != null)
				return false;
		} else if (!assistantsCrew.equals(other.assistantsCrew))
			return false;
		return true;
	}

}
