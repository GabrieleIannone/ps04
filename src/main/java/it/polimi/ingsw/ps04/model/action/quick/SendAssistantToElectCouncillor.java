package it.polimi.ingsw.ps04.model.action.quick;

import it.polimi.ingsw.ps04.model.action.Action;
import it.polimi.ingsw.ps04.model.action.actionVisitor.ActionVisitor;
import it.polimi.ingsw.ps04.model.action.actionVisitor.VisitableAction;
import it.polimi.ingsw.ps04.model.board.Council;
import it.polimi.ingsw.ps04.model.board.Councillor;
import it.polimi.ingsw.ps04.model.board.Region;
import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.utils.exception.ActionNotCompletedException;

/**
 * this class implements the "SendAssistantToElectCouncillor" action of the
 * game.
 */
public class SendAssistantToElectCouncillor extends QuickAction {

	private static final long serialVersionUID = -8944031715500863777L;

	public static final String DESCRIPTION = "send an assistant to elect a councillor";

	private Council council;

	private Councillor councillor;

	// if region == null the client wants to corrupt the king's council
	Region region;

	/**
	 * constructs the action "SendAssistantToElectCouncillor"
	 * 
	 * @param region
	 *            the region that contains the council or null if it is the
	 *            council of the king
	 * @param councillor
	 *            the councillor that you want to elect
	 */
	public SendAssistantToElectCouncillor(Region region, Councillor councillor) {
		this.region = region;
		this.councillor = councillor;
	}

	/**
	 * @see Action#execute(Player)
	 */
	@Override
	public void execute(Player player) throws ActionNotCompletedException {

		useQuickAction(player);

		try {
			player.getAssistantCrew().sendAssistants(1); // sub 1 assistant
		} catch (IllegalArgumentException e) {
			player.getActionsCounters().undoQuickAction();
			throw new ActionNotCompletedException(e.getMessage());
		}
		council.electCouncillour(councillor);
	}

	// Getters and setters
	/**
	 * Returns the region of the council or null if the client wants to corrupt
	 * the king's council
	 * 
	 * @return the region of the council or null if the client wants to corrupt
	 *         the king's council
	 */
	public Region getRegion() {
		return region;
	}

	/**
	 * Changes the attribute region
	 * 
	 * @param region
	 *            the attribute that you want to set
	 */
	public void setRegion(Region region) {
		this.region = region;
	}

	/**
	 * Changes the council to be equal to the argument council
	 * 
	 * @param council
	 *            the council that you want to set
	 */
	public void setCouncil(Council council) {
		this.council = council;
	}

	/**
	 * @see VisitableAction#accept(ActionVisitor)
	 */
	@Override
	public void accept(ActionVisitor visitor) {
		visitor.visit(this);
	}

	/**
	 * Converts this object to a <code>String</code>
	 */
	@Override
	public String toString() {
		String result = "sent an assistant to elect in ";
		if (region == null) {
			result += "king's council";
		} else {
			result += region.getName() + " council";
		}
		return result;
	}

}
