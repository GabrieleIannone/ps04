package it.polimi.ingsw.ps04.model.board;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import it.polimi.ingsw.ps04.model.bonus.Bonus;
import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.utils.color.CityColor;
import it.polimi.ingsw.ps04.utils.exception.PlayerHasAlreadyBuiltException;

public class City implements Serializable {

	private static final long serialVersionUID = 657254263925780928L;
	private String name;
	private Set<Bonus> bonuses;
	private Set<Emporium> emporiums = new HashSet<Emporium>();
	private CityColor color;
	private Region region;

	/**
	 * constructs a city with the given name
	 * 
	 * @param name
	 *            the name that you want to assign to the city
	 */
	public City(String name) {
		this.name = name;
	}

	/**
	 * constructs a city with the given parameters
	 * 
	 * @param name
	 *            the name that you want to assign to the city
	 * @param region
	 *            the region that you want to assign to the city
	 * @param color
	 *            the city color that you want to assign to the city
	 */
	public City(String name, Region region, CityColor color) {
		this(name);
		this.region = region;
		this.color = color;
	}

	/**
	 * places the emporium of the given player in this city
	 * 
	 * @param player
	 *            the player that wants to build in this city
	 * @throws PlayerHasAlreadyBuiltException
	 *             if the given player has already built in this city. If this
	 *             exception is thrown, then the player will not build his
	 *             emporium in the city
	 * 
	 */
	public void build(Player player) throws PlayerHasAlreadyBuiltException {
		if (playerHasBuilt(player)) {
			throw new PlayerHasAlreadyBuiltException();
		} else {
			emporiums.add(new Emporium(player));
		}
	}

	/**
	 * checks if the player has already built in this city
	 * 
	 * @param player
	 *            the owner of the emporium that you want to check if it is
	 *            present
	 * @return true if the player has built in this city
	 */
	public boolean playerHasBuilt(Player player) {
		for (Emporium emp : emporiums) {
			if (emp.getOwner().getName().equals(player.getName()))
				return true;
		}
		return false;
	}

	/**
	 * Returns the number of emporiums that were built in this city
	 * 
	 * @return the number of emporiums that were built in this city
	 */
	public int getEmporiumsNumber() {
		return emporiums.size();
	}

	/**
	 * Returns the name of the city
	 * 
	 * @return the name of the city
	 */
	public String getName() {
		return name;
	}

	/**
	 * Returns the bonus of the city
	 * 
	 * @return the bonus of the city
	 */
	public Set<Bonus> getBonus() {
		return bonuses;
	}

	/**
	 * Changes the bonuses of the map to be equal to the argument bonuses
	 * 
	 * @param bonuses
	 *            the bonuses that you want to set
	 */
	public void setBonuses(Set<Bonus> bonuses) {
		this.bonuses = bonuses;
	}

	/**
	 * Returns the city color of the city
	 * 
	 * @return the city color of the city
	 */
	public CityColor getColour() {
		return color;
	}

	/**
	 * Returns the region of the city
	 * 
	 * @return the region of the city
	 */
	public Region getRegion() {
		return region;
	}

	/**
	 * Converts this <code>City</code> object to a <code>String</code>
	 */
	@Override
	public String toString() {
		ToStringBuilder stringBuilder = new ToStringBuilder(this, ToStringStyle.JSON_STYLE).append("name", name)
				.append("color", color).append("bonus", bonuses);
		if (!emporiums.isEmpty()) {
			stringBuilder.append("emporiums", emporiums);
		}
		return stringBuilder.toString();
	}

	/**
	 * Returns a hash code value for this object. It considers only the name of
	 * the city
	 */
	@Override
	public int hashCode() {
		return Objects.hash(name);
	}

	/**
	 * Compares two cities for equality. The result is <code>true</code> if and
	 * only if the argument is not <code>null</code> and is a <code>City</code>
	 * object with the same name of this city.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		City other = (City) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}
