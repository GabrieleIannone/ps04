package it.polimi.ingsw.ps04.model.player;

import java.io.Serializable;

/**
 * Implements the victory points of a player
 */
public class VictoryPoints implements Serializable {

	private static final long serialVersionUID = -6544586018663368869L;
	private int victoryPoints = 0;

	/**
	 * Returns the victory points of the player
	 * 
	 * @return the victory points of the player
	 */
	public int getVictoryPoints() {
		return victoryPoints;
	}

	/**
	 * Add a number of victory points to the player given by the value of the
	 * attribute pointsIncrement
	 * 
	 * @param pointsIncrement
	 *            the number of victory points that you want to add to the
	 *            player
	 */
	public void addPoints(int pointsIncrement) {
		if (pointsIncrement < 1) {
			throw new IllegalArgumentException("the victory points increments must be at least > 0");
		}
		victoryPoints += pointsIncrement;
	}

	/**
	 * Converts this <code>VictoryPoints</code> object to a <code>String</code>
	 */
	@Override
	public String toString() {
		return String.valueOf(victoryPoints);
	}

	/**
	 * Returns a hash code value for this object. It considers the numbers of
	 * victory points.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + victoryPoints;
		return result;
	}

	/**
	 * Compares two victory points objects for equality. The result is
	 * <code>true</code> if and only if the argument is not <code>null</code>
	 * and is a <code>VictoryPoints</code> object that has the same victory
	 * points.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VictoryPoints other = (VictoryPoints) obj;
		if (victoryPoints != other.victoryPoints)
			return false;
		return true;
	}

}
