package it.polimi.ingsw.ps04.model.action;

import it.polimi.ingsw.ps04.model.action.actionVisitor.ActionVisitor;
import it.polimi.ingsw.ps04.model.action.actionVisitor.VisitableAction;
import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.utils.exception.ActionNotCompletedException;

/**
 * it allows the player to pass the turn. It is also automatically done if the
 * player cannot do other actions
 */
public class PassTurn extends Action {

	private static final long serialVersionUID = 6601544813808068537L;
	public static final String DESCRIPTION = "pass turn";

	/**
	 * @see Action#execute(Player)
	 */
	@Override
	public void execute(Player player) throws ActionNotCompletedException {
		player.getActionsCounters().useAllActions();
	}

	/**
	 * @see VisitableAction#accept(ActionVisitor)
	 */
	@Override
	public void accept(ActionVisitor visitor) {
		visitor.visit(this);
	}

	/**
	 * Converts this object to a <code>String</code>
	 */
	@Override
	public String toString() {
		return "passed turn";
	}

}
