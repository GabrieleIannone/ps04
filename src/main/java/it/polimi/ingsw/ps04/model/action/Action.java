package it.polimi.ingsw.ps04.model.action;

import java.io.Serializable;

import it.polimi.ingsw.ps04.model.action.actionVisitor.VisitableAction;
import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.utils.exception.ActionNotCompletedException;

/**
 * this class is the representation of the concept of "Action" of the game.
 */
public abstract class Action implements VisitableAction, Serializable {

	private static final long serialVersionUID = 688591378557410291L;

	/**
	 * executes the action
	 * 
	 * @param player
	 *            the player that is doing the action
	 * @throws ActionNotCompletedException
	 *             if the action is not completed
	 */
	public abstract void execute(Player player) throws ActionNotCompletedException;

}
