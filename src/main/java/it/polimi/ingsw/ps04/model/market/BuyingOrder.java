package it.polimi.ingsw.ps04.model.market;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.ps04.model.deck.business.BusinessPermitTile;
import it.polimi.ingsw.ps04.model.deck.politic.PoliticCard;
import it.polimi.ingsw.ps04.model.player.AssistantsCrew;

/**
 * Represents an order made by a player during the buying phase
 */
public class BuyingOrder implements Serializable {

	private static final long serialVersionUID = 5362921737765460137L;
	private AssistantsCrew assistants = new AssistantsCrew();
	private List<BusinessPermitTile> tiles = new ArrayList<>();
	private List<PoliticCard> cards = new ArrayList<>();

	/**
	 * Add a new politic card to buy
	 * 
	 * @param card
	 *            the card to buy
	 */
	public void addCard(PoliticCard card) {
		cards.add(card);
	}

	/**
	 * Add a new business permit tile card to buy
	 * 
	 * @param tile
	 *            the business permit tile to buy
	 */
	public void addTile(BusinessPermitTile tile) {
		tiles.add(tile);
	}

	// Getters and Setters
	/**
	 * Returns the assistants of this buying order
	 * 
	 * @return the assistants of this buying order
	 */
	public AssistantsCrew getAssistants() {
		return assistants;
	}

	/**
	 * Returns the tiles of this buying order
	 * 
	 * @return the tiles of this buying order
	 */
	public List<BusinessPermitTile> getTiles() {
		return tiles;
	}

	/**
	 * Returns the politic cards of this buying order
	 * 
	 * @return the politic cards of this buying order
	 */
	public List<PoliticCard> getCards() {
		return cards;
	}

	/**
	 * Changes the assistants of this order to be equal to the argument
	 * assistants
	 * 
	 * @param assistants
	 *            the assistants that you want to set
	 */
	public void setAssistants(AssistantsCrew assistants) {
		this.assistants = assistants;
	}

	/**
	 * Converts this <code>BuyingOrder</code> object to a <code>String</code>
	 */
	@Override
	public String toString() {
		return "BuyingOrder [assistants=" + assistants + ", tiles=" + tiles + ", cards=" + cards + "]";
	}

}
