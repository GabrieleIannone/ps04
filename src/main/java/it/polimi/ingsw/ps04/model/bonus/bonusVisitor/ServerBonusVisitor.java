package it.polimi.ingsw.ps04.model.bonus.bonusVisitor;

import java.io.Serializable;

import it.polimi.ingsw.ps04.model.WaitingBonuses;
import it.polimi.ingsw.ps04.model.bonus.complex.ComplexBonus;
import it.polimi.ingsw.ps04.model.bonus.simple.NobilityBonus;
import it.polimi.ingsw.ps04.model.bonus.simple.PoliticCardsBonus;
import it.polimi.ingsw.ps04.model.player.Player;

/**
 * This class is the visitor used into the client. It implements BonusVisitor.
 * 
 * @see BonusVisitor
 */
public class ServerBonusVisitor implements BonusVisitor, Serializable {

	private static final long serialVersionUID = 3165125827776820571L;

	/**
	 * Once a complex bonus is visited, the visitor saves it in model waiting
	 * bonus.
	 * 
	 * @see ComplexBonus#redefine(BonusVisitor,
	 *      it.polimi.ingsw.ps04.model.Model)
	 * @see WaitingBonuses
	 * 
	 * @param bonus
	 *            The activated complex bonus.
	 * 
	 * @param player
	 *            The player who activated the bonus.
	 */
	@Override
	public void visit(ComplexBonus bonus, Player player) {
		bonus.saveWaitingBonus();
	}

	/**
	 * Once a PoliticCardsBonus is visited, the visitor executes the bonus.
	 * 
	 * @param bonus
	 *            The activated PoliticCardsBonus.
	 * 
	 * @param player
	 *            The player who activated the bonus.
	 */
	@Override
	public void visit(PoliticCardsBonus bonus, Player player) {
		bonus.execute(player);
	}

	/**
	 * Once a NobilityBonus is visited, the visitor executes the bonus.
	 * 
	 * @param bonus
	 *            The activated NobilityBonus.
	 * 
	 * @param player
	 *            The player who activated the bonus.
	 */
	@Override
	public void visit(NobilityBonus bonus, Player player) {
		bonus.execute(player);
	}

}
