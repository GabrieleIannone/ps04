package it.polimi.ingsw.ps04.model.bonus;

import java.io.Serializable;
import java.util.Objects;

import com.google.common.base.MoreObjects;

import it.polimi.ingsw.ps04.model.bonus.bonusVisitor.BonusVisitor;
import it.polimi.ingsw.ps04.model.bonus.bonusVisitor.VisitableBonus;
import it.polimi.ingsw.ps04.model.bonus.complex.ComplexBonus;
import it.polimi.ingsw.ps04.model.bonus.simple.SimpleBonus;
import it.polimi.ingsw.ps04.model.player.Player;

/**
 * This class represents the abstract bonus. It will be extended by ComplexBonus
 * and SimpleBonus.
 * 
 * Each bonus has a bonus increment, that says how many time that bonus has to
 * be execute.
 * 
 * @see SimpleBonus
 * 
 * @see ComplexBonus
 */
public abstract class Bonus implements VisitableBonus, Serializable {

	private static final long serialVersionUID = -90222140566559691L;
	protected int bonusIncrement;

	public int getBonusIncrementer() {
		return bonusIncrement;
	}

	/**
	 * Each time a bonus is activated this method will be called.
	 */
	public abstract void accept(BonusVisitor visitor, Player player);

	/**
	 * Converts this <code>Bonus</code> object to a <code>String</code>
	 */
	@Override
	public String toString() {
		return "\"" + MoreObjects.toStringHelper(this).addValue(bonusIncrement).toString().replace("{", "(")
				.replace("}", ")") + "\"";
	}

	/**
	 * Compares two bonus for equality. The result is <code>true</code> if and
	 * only if the argument is not <code>null</code> and is a <code>Bonus</code>
	 * object of the same type that represents the same bonus increment.
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		final Bonus other = (Bonus) obj;
		return Objects.equals(this.bonusIncrement, other.bonusIncrement);
	}

	/**
	 * Returns a hash code value for this object. It considers the bonus
	 * increment
	 */
	@Override
	public int hashCode() {
		return Objects.hash(bonusIncrement);
	}
}
