package it.polimi.ingsw.ps04.model.player;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import groovy.json.JsonOutput;
import it.polimi.ingsw.ps04.model.deck.politic.PoliticCard;

/**
 * this class represents the players of the game
 */
public class Player implements Serializable {

	private static final long serialVersionUID = -5866991002629188076L;
	private String name;
	private NobilityLevel nobilityLevel = new NobilityLevel();
	private VictoryPoints points = new VictoryPoints();
	private UnusedEmporiumPool unusedEmporiums;
	private PoliticCardsHand politicCardsHand;
	private AssistantsCrew assistantsCrew;
	private Coins coins;
	private ActionsCounters actionsCounters = new ActionsCounters();
	private BusinessPermitTilePool businessPermitTilePool; // used to build

	/**
	 * Constructs a player with the given name
	 * 
	 * @param name
	 *            the name that you want to assign to the player
	 */
	public Player(String name) {
		this.name = name;
	}

	/**
	 * Constructs a player with the given parameters
	 * 
	 * @param Name
	 *            the name that you want to assign to the player
	 * @param citiesNumber
	 *            the cities number of the map
	 * @param turn
	 *            the turn that you want to assign to the player
	 * @param politicCards
	 *            the initial politic cards that you want to assign to the
	 *            player
	 */
	public Player(String Name, int citiesNumber, int turn, List<PoliticCard> politicCards) {
		this.name = Name;
		this.politicCardsHand = new PoliticCardsHand(politicCards);
		this.unusedEmporiums = new UnusedEmporiumPool(citiesNumber);
		this.coins = new Coins(turn);
		this.assistantsCrew = new AssistantsCrew(turn);
		this.businessPermitTilePool = new BusinessPermitTilePool();
	}

	/**
	 * Adds a politic card to the player hand
	 * 
	 * @param politicCard
	 *            the politic card that you want to add to the player hand
	 */
	public void addPoliticCard(PoliticCard politicCard) {
		this.politicCardsHand.drawCard(politicCard);
	}

	// Get Metods

	/**
	 * Returns the unused emporiums of the player
	 * 
	 * @return the unused emporiums of the player
	 */
	public UnusedEmporiumPool getUnusedEmporiums() {
		return unusedEmporiums;
	}

	/**
	 * Returns the name of the player
	 * 
	 * @return the name of the player
	 */
	public String getName() {
		return name;
	}

	/**
	 * Returns the victory points of the player
	 * 
	 * @return the victory points of the player
	 */
	public VictoryPoints getPoints() {
		return points;
	}

	/**
	 * Returns the assistants crew of the player
	 * 
	 * @return the assistants crew of the player
	 */
	public AssistantsCrew getAssistantCrew() {
		return assistantsCrew;
	}

	/**
	 * Returns the coins of the player
	 * 
	 * @return the coins of the player
	 */
	public Coins getCoins() {
		return coins;
	}

	/**
	 * Returns the politic cards of the player
	 * 
	 * @return the politic cards of the player
	 */
	public PoliticCardsHand getPoliticCardsHand() {
		return politicCardsHand;
	}

	/**
	 * Returns the nobility level of the player
	 * 
	 * @return the nobility level of the player
	 */
	public NobilityLevel getNobilityLevel() {
		return nobilityLevel;
	}

	/**
	 * Returns the actions counters of the player
	 * 
	 * @return the actions counters of the player
	 */
	public ActionsCounters getActionsCounters() {
		return actionsCounters;
	}

	/**
	 * Returns the business permit tile pool of the player
	 * 
	 * @return the business permit tile pool of the player
	 */
	public BusinessPermitTilePool getBusinessPermitTilePool() {
		return businessPermitTilePool;
	}

	/**
	 * Returns a string that represents the public elements of the player
	 * 
	 * @return a string that represents the public elements of the player
	 */
	public String getPublicElementsString() {
		try {
			return JsonOutput.prettyPrint(getCommonElementsStringBuilder()
					.append("politic cards number", politicCardsHand.getHand().size()).toString());
		} catch (Exception e) {
			e.printStackTrace();
			return "Error in json";
		}
	}

	/**
	 * Returns a string that represents the private elements of the player
	 * 
	 * @return a string that represents the private elements of the player
	 */
	public String getPrivateElementsString() {
		return JsonOutput.prettyPrint(
				getCommonElementsStringBuilder().append("politic cards", politicCardsHand.getHand()).toString());
	}

	/**
	 * Returns a ToStringBuilder that represents the public elements of the
	 * player
	 * 
	 * @return a ToStringBuilder that represents the public elements of the
	 *         player
	 */
	private ToStringBuilder getCommonElementsStringBuilder() {
		return new ToStringBuilder(this, ToStringStyle.JSON_STYLE).append("name", name)
				.append("nobility level", nobilityLevel).append("victory points", points)
				.append("assistants", assistantsCrew).append("coins", coins).append("tiles", businessPermitTilePool)
				.append("unused emporiums", unusedEmporiums);
	}

	/**
	 * Converts this <code>Player</code> object to a <code>String</code>
	 */
	@Override
	public String toString() {
		return getPublicElementsString();
	}

	/**
	 * Returns a hash code value for this object. It considers assistantsCrew,
	 * businessPermitTilePool, name, nobilityLevel, victory points, politic
	 * cards handcoins and unused emporiums number.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((assistantsCrew == null) ? 0 : assistantsCrew.hashCode());
		result = prime * result + ((businessPermitTilePool == null) ? 0 : businessPermitTilePool.hashCode());
		result = prime * result + ((coins == null) ? 0 : coins.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((nobilityLevel == null) ? 0 : nobilityLevel.hashCode());
		result = prime * result + ((points == null) ? 0 : points.hashCode());
		result = prime * result + ((politicCardsHand == null) ? 0 : politicCardsHand.hashCode());
		result = prime * result + ((unusedEmporiums == null) ? 0 : unusedEmporiums.hashCode());
		return result;
	}

	/**
	 * Compares two players for equality. The result is <code>true</code> if and
	 * only if the argument is not <code>null</code> and is a
	 * <code>Player</code> object that represents the same assistantsCrew,
	 * businessPermitTilePool, name, nobilityLevel, victory points, politic
	 * cards handcoins and unused emporiums number.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Player other = (Player) obj;
		if (assistantsCrew == null) {
			if (other.assistantsCrew != null)
				return false;
		} else if (!assistantsCrew.equals(other.assistantsCrew))
			return false;
		if (businessPermitTilePool == null) {
			if (other.businessPermitTilePool != null)
				return false;
		} else if (!businessPermitTilePool.equals(other.businessPermitTilePool))
			return false;
		if (coins == null) {
			if (other.coins != null)
				return false;
		} else if (!coins.equals(other.coins))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (nobilityLevel == null) {
			if (other.nobilityLevel != null)
				return false;
		} else if (!nobilityLevel.equals(other.nobilityLevel))
			return false;
		if (points == null) {
			if (other.points != null)
				return false;
		} else if (!points.equals(other.points))
			return false;
		if (politicCardsHand == null) {
			if (other.politicCardsHand != null)
				return false;
		} else if (!(politicCardsHand.size() == other.politicCardsHand.size()))
			return false;
		if (unusedEmporiums == null) {
			if (other.unusedEmporiums != null)
				return false;
		} else if (!unusedEmporiums.equals(other.unusedEmporiums))
			return false;
		return true;
	}

}