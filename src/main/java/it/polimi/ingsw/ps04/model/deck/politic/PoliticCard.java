package it.polimi.ingsw.ps04.model.deck.politic;

import java.io.Serializable;

import it.polimi.ingsw.ps04.utils.color.NamedColor;

/**
 * Represents one politic card
 */
public class PoliticCard implements Serializable {

	private static final long serialVersionUID = 6938370780092347115L;
	private NamedColor color;

	/**
	 * Creates a new politic card, passing his color
	 * 
	 * @param color
	 *            Politic card corruption color
	 */
	public PoliticCard(NamedColor color) {
		this.color = color;
	}

	/**
	 * @return Politic Card corruption color
	 */
	public NamedColor getColour() {
		return color;
	}

	/**
	 * Converts this <code>PoliticCard</code> object to a <code>String</code>
	 */
	@Override
	public String toString() {
		return color.toString();
	}

	/**
	 * Returns a hash code value for this object. It considers the color.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((color == null) ? 0 : color.hashCode());
		return result;
	}

	/**
	 * Compares two cards for equality. The result is <code>true</code> if and
	 * only if the argument is not <code>null</code> and is a
	 * <code>PoliticCard</code> object that represents the same color.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PoliticCard other = (PoliticCard) obj;
		if (color == null) {
			if (other.color != null)
				return false;
		} else if (!color.equals(other.color))
			return false;
		return true;
	}

}
