package it.polimi.ingsw.ps04.model.action.main;

import it.polimi.ingsw.ps04.model.action.Action;
import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.utils.exception.ActionNotAvailableException;
import it.polimi.ingsw.ps04.utils.exception.ActionNotCompletedException;

/**
 * this class is the representation of the concept of "Main Action" of the game.
 */
public abstract class MainAction extends Action {

	private static final long serialVersionUID = 3769620857182585564L;

	/**
	 * Consume a main action to the player passed by parameter.
	 * 
	 * @param player
	 *            The player who activated an action.
	 * @throws ActionNotCompletedException
	 *             If the player has not main action available.
	 */
	public void useMainAction(Player player) throws ActionNotCompletedException {
		try {
			player.getActionsCounters().doMainAction();
		} catch (ActionNotAvailableException e) {
			throw new ActionNotCompletedException("Main action already done!");
		}
	}

}