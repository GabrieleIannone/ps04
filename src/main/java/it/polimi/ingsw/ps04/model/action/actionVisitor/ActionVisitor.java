package it.polimi.ingsw.ps04.model.action.actionVisitor;

import it.polimi.ingsw.ps04.model.action.PassTurn;
import it.polimi.ingsw.ps04.model.action.main.AcquireBusinessPermitTile;
import it.polimi.ingsw.ps04.model.action.main.BuildEmporium;
import it.polimi.ingsw.ps04.model.action.main.BuildEmporiumWithKing;
import it.polimi.ingsw.ps04.model.action.main.ElectCouncillor;
import it.polimi.ingsw.ps04.model.action.quick.ChangeBusinessPermitTiles;
import it.polimi.ingsw.ps04.model.action.quick.EngageAssistant;
import it.polimi.ingsw.ps04.model.action.quick.GetAnotherPrimaryAction;
import it.polimi.ingsw.ps04.model.action.quick.SendAssistantToElectCouncillor;

/**
 * this class is the interface of the action visitor. It declares all the visit
 * methods that must be implemented by the concrete visitor in order to update
 * the reference to the ones from the local model
 */
public interface ActionVisitor {

	/**
	 * visits an AcquireBusinessPermitTile action
	 * 
	 * @param action
	 *            the AcquireBusinessPermitTile action
	 */
	void visit(AcquireBusinessPermitTile action);

	/**
	 * visits an BuildEmporium action
	 * 
	 * @param action
	 *            the BuildEmporium action
	 */
	void visit(BuildEmporium action);

	/**
	 * visits an BuildEmporiumWithKing action
	 * 
	 * @param action
	 *            the BuildEmporiumWithKing action
	 */
	void visit(BuildEmporiumWithKing action);

	/**
	 * visits an ElectCouncillor action
	 * 
	 * @param action
	 *            the ElectCouncillor action
	 */
	void visit(ElectCouncillor action);

	/**
	 * visits an ChangeBusinessPermitTiles action
	 * 
	 * @param action
	 *            the ChangeBusinessPermitTiles action
	 */
	void visit(ChangeBusinessPermitTiles action);

	/**
	 * visits an EngageAssistant action
	 * 
	 * @param action
	 *            the EngageAssistant action
	 */
	void visit(EngageAssistant action);

	/**
	 * visits an GetAnotherPrimaryAction action
	 * 
	 * @param action
	 *            the GetAnotherPrimaryAction action
	 */
	void visit(GetAnotherPrimaryAction action);

	/**
	 * visits an SendAssistantToElectCouncillor action
	 * 
	 * @param action
	 *            the SendAssistantToElectCouncillor action
	 */
	void visit(SendAssistantToElectCouncillor action);

	/**
	 * visits an PassTurn action
	 * 
	 * @param action
	 *            the PassTurn action
	 */
	void visit(PassTurn action);
}
