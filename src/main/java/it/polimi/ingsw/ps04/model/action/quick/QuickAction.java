package it.polimi.ingsw.ps04.model.action.quick;

import it.polimi.ingsw.ps04.model.action.Action;
import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.utils.exception.ActionNotAvailableException;
import it.polimi.ingsw.ps04.utils.exception.ActionNotCompletedException;

/**
 * this class is the representation of the concept of "Quick Action" of the
 * game.
 */
public abstract class QuickAction extends Action {

	private static final long serialVersionUID = -1567096709688845294L;

	/**
	 * uses a quick action
	 * 
	 * @param player
	 *            the player that uses the quick action
	 * @throws ActionNotCompletedException
	 *             if the quick action was already done
	 */
	public void useQuickAction(Player player) throws ActionNotCompletedException {
		try {
			player.getActionsCounters().doQuickAction();
		} catch (ActionNotAvailableException e) {
			throw new ActionNotCompletedException("Quick action already done!");
		}
	}

}
