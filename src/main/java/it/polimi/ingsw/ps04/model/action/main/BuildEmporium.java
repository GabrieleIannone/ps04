package it.polimi.ingsw.ps04.model.action.main;

import it.polimi.ingsw.ps04.model.action.Action;
import it.polimi.ingsw.ps04.model.action.actionVisitor.ActionVisitor;
import it.polimi.ingsw.ps04.model.action.actionVisitor.VisitableAction;
import it.polimi.ingsw.ps04.model.board.City;
import it.polimi.ingsw.ps04.model.board.map.Map;
import it.polimi.ingsw.ps04.model.deck.business.BusinessPermitTile;
import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.utils.exception.ActionNotCompletedException;
import it.polimi.ingsw.ps04.utils.exception.AlreadyFaceDownException;
import it.polimi.ingsw.ps04.utils.exception.PlayerHasAlreadyBuiltException;

/**
 * this class implements the "BuildEmporium" action of the game. It is used when
 * someone wants to build in a city
 */
public class BuildEmporium extends MainAction {

	private static final long serialVersionUID = -1075923485413118127L;

	public static final String DESCRIPTION = "build an emporium in a city";

	private City city;
	private BusinessPermitTile tile;
	private Map map;

	/**
	 * constructs the action "BuildEmporium" with the given city and tile
	 * 
	 * @param city
	 *            the city where you want to build
	 * @param tile
	 *            the tile that you want to use to build
	 */
	public BuildEmporium(City city, BusinessPermitTile tile) {
		this.city = city;
		this.tile = tile;
	}

	/**
	 * constructs the action "BuildEmporium" with the given city and map
	 * 
	 * @param city
	 *            the city where you want to build
	 * @param map
	 *            the map of the current match
	 */
	protected BuildEmporium(City city, Map map) {
		this.city = city;
		this.map = map;
	}

	/**
	 * this method is used if someone want to build in a city.
	 *
	 * @see Action#execute(Player)
	 */
	@Override
	public void execute(Player player) throws ActionNotCompletedException {

		useMainAction(player);

		if (!(tile.isCityIncluded(city)))
			throw new ActionNotCompletedException("City is not included in this tile");

		try {
			tile.turnFaceDown();
		} catch (AlreadyFaceDownException e1) {
			throw new ActionNotCompletedException("Tile already used");
		}

		try {
			build(player);
		} catch (ActionNotCompletedException e) {
			tile.turnFaceUp();
			throw new ActionNotCompletedException(e.getMessage());
		}

	}

	/**
	 * Builds an emporium of the given player
	 * 
	 * @param player
	 *            the player that has to build
	 * @throws ActionNotCompletedException
	 *             if the action was not completed
	 */
	protected void build(Player player) throws ActionNotCompletedException {
		// player has to pay something to build
		int emporiumsBuilt = city.getEmporiumsNumber();
		// this if will check if someone else has built and in that case
		// will calculate tax
		try {
			player.getAssistantCrew().sendAssistants(emporiumsBuilt);
			city.build(player);
		} catch (PlayerHasAlreadyBuiltException e) {
			player.getActionsCounters().additionalMainAction();
			player.getAssistantCrew().engageAssistants(emporiumsBuilt);
			throw new ActionNotCompletedException("Player has already built!");
		} catch (IllegalArgumentException e) {
			// Not enough assistants
			player.getActionsCounters().additionalMainAction();
			throw new ActionNotCompletedException("Not enough assistants to send!");
		}

		try {
			player.getUnusedEmporiums().removeEmporium();
		} catch (ActionNotCompletedException e) {
			player.getActionsCounters().additionalMainAction();
			player.getAssistantCrew().engageAssistants(emporiumsBuilt);
			throw new ActionNotCompletedException(e.getMessage());
		}

		map.activateCitiesBonuses(player, city);
	}

	// Getters and setters
	/**
	 * Returns the city where you want to build
	 * 
	 * @return the city where you want to build
	 */
	public City getCity() {
		return city;
	}

	/**
	 * Returns the tile that the user wants to use to build
	 * 
	 * @return the tile that the user wants to use to build
	 */
	public BusinessPermitTile getTile() {
		return tile;
	}

	/**
	 * Changes the city where you want to build to be equal to the argument city
	 * 
	 * @param city
	 *            the city that you want to set
	 */
	public void setCity(City city) {
		this.city = city;
	}

	/**
	 * Changes the tile that the user wants to use to build to be equal to the
	 * argument tile
	 * 
	 * @param tile
	 *            the tile that you want to set
	 */
	public void setTile(BusinessPermitTile tile) {
		this.tile = tile;
	}

	/**
	 * Changes the map where you want to build to be equal to the argument tile
	 * 
	 * @param map
	 *            the map that you want to set
	 */
	public void setMap(Map map) {
		this.map = map;
	}

	/**
	 * @see VisitableAction#accept(ActionVisitor)
	 */
	@Override
	public void accept(ActionVisitor visitor) {
		visitor.visit(this);
	}

	/**
	 * Converts this object to a <code>String</code>
	 */
	@Override
	public String toString() {
		return "built in " + city.getName();
	}
}