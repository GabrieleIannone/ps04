package it.polimi.ingsw.ps04.model.bonus.simple;

import it.polimi.ingsw.ps04.model.player.Player;

/**
 * This bonus is a simple bonus. It gives a number of coins to the player who
 * activates this equals to the bonus increment.
 */
public class CoinsBonus extends SimpleBonus {

	/**
	 * Constructor. Set the bonus increment.
	 * 
	 * @param bonusIncrement
	 *            The bonusIncrement of this bonus.
	 */
	private static final long serialVersionUID = -1146370553228516267L;

	/**
	 * Constructs a coins bonus with the given bonus incrementer
	 * 
	 * @param bonusIncrementer
	 *            the bonus incrementer that you want to assign to this coins
	 *            bonus
	 */
	public CoinsBonus(int bonusIncrementer) {
		super.bonusIncrement = bonusIncrementer;
	}

	/**
	 * Execute the bonus. Gives to the player a number of coins equals to the
	 * bonus increment.
	 * 
	 * @param player
	 *            The player who activated the bonus.
	 */
	@Override
	public void execute(Player player) {
		player.getCoins().addCoins(bonusIncrement);
	}

}
