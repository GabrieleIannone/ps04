package it.polimi.ingsw.ps04.model.action.main;

import java.util.List;

import it.polimi.ingsw.ps04.model.action.Action;
import it.polimi.ingsw.ps04.model.action.actionVisitor.ActionVisitor;
import it.polimi.ingsw.ps04.model.action.actionVisitor.VisitableAction;
import it.polimi.ingsw.ps04.model.board.Region;
import it.polimi.ingsw.ps04.model.deck.politic.PoliticCard;
import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.utils.exception.ActionNotCompletedException;

/**
 * this class implements the "AcquireBusinessPermitTile" action of the game
 */
public class AcquireBusinessPermitTile extends MainAction {

	private static final long serialVersionUID = -2218809058323446170L;

	public static final String DESCRIPTION = "acquire a revealed business permite tile";

	private Region region;
	private List<PoliticCard> usedCards;
	private int businessTileNumber;

	/**
	 * constructs the action "AcquireBusinessPermitTile"
	 * 
	 * @param usedCards
	 *            the cards that the player wants to use
	 * @param region
	 *            the region of the business permit tile deck that contains the
	 *            tile that you want to obtain
	 * @param businessTileNumber
	 *            the number of the business tile
	 */
	public AcquireBusinessPermitTile(List<PoliticCard> usedCards, Region region, int businessTileNumber) {
		this.usedCards = usedCards;
		this.region = region;
		this.businessTileNumber = businessTileNumber;
	}

	/**
	 * @see Action#execute(Player)
	 */
	@Override
	public void execute(Player player) throws ActionNotCompletedException {

		useMainAction(player);

		if (!player.getPoliticCardsHand().containsCards(usedCards)) {
			player.getActionsCounters().additionalMainAction();
			throw new ActionNotCompletedException("Cards selected are not available!");
		}

		try {
			int cost = region.getCouncil().getCorruptionCost(usedCards);
			player.getCoins().payFor(cost);
		} catch (IllegalArgumentException e) {
			player.getActionsCounters().additionalMainAction();
			throw new ActionNotCompletedException("Not enough money!");
		}

		player.getPoliticCardsHand().useCards(usedCards);

		region.getBusinessDeck().takeTile(businessTileNumber, player);
	}

	/**
	 * Returns the region that contains the tile that you want to acquire
	 * 
	 * @return the region that contains the tile that you want to acquire
	 */
	public Region getRegion() {
		return region;
	}

	/**
	 * Changes the region of the action to be equal to the argument region
	 * 
	 * @param region
	 *            the region that you want to set
	 */
	public void setRegion(Region region) {
		this.region = region;
	}

	/**
	 * @see VisitableAction#accept(ActionVisitor)
	 */
	@Override
	public void accept(ActionVisitor visitor) {
		visitor.visit(this);
	}

	/**
	 * Converts this object to a <code>String</code>
	 */
	@Override
	public String toString() {
		return "acquired a tile from " + region.getName() + " using " + usedCards;
	}
}