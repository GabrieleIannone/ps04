package it.polimi.ingsw.ps04.model.market;

import java.io.Serializable;

/**
 * This class represents the all the objects that could be sold
 */
public abstract class ObjectonSale implements Serializable {

	private static final long serialVersionUID = 1043373093241107010L;
	protected int cost;

	/**
	 * Creates a new object and sets his cost. Called by the concrete classes
	 */
	protected ObjectonSale(int cost) {
		this.cost = cost;
	}

	/**
	 * @return cost of the object
	 */
	public int getCost() {
		return cost;
	}

	/**
	 * Returns a hash code value for this object. It considers the cost.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + cost;
		return result;
	}

	/**
	 * Compares two objects on sale for equality. The result is
	 * <code>true</code> if and only if the argument is not <code>null</code>
	 * and is a <code>ObjectonSale</code> object that has the same cost.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ObjectonSale other = (ObjectonSale) obj;
		if (cost != other.cost)
			return false;
		return true;
	}

}
