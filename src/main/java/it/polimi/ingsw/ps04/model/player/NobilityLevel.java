package it.polimi.ingsw.ps04.model.player;

import java.io.Serializable;

/**
 * This class implements the position of a player into the nobility track
 */
public class NobilityLevel implements Serializable {

	private static final long serialVersionUID = 5566882833965746710L;
	private int level = 0;

	/**
	 * Returns the nobility level of the player
	 * 
	 * @return the nobility level of the player
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * Increases the nobility level of the player
	 */
	public void increaseLevel() {
		level += 1;
	}

	/**
	 * Converts this <code>NobilityLevel</code> object to a <code>String</code>
	 */
	@Override
	public String toString() {
		return String.valueOf(level);
	}

	/**
	 * Returns a hash code value for this object. It considers the nobility
	 * level.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + level;
		return result;
	}

	/**
	 * Compares two nobility levels for equality. The result is
	 * <code>true</code> if and only if the argument is not <code>null</code>
	 * and is a <code>NobilityLevel</code> object that has the same level.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NobilityLevel other = (NobilityLevel) obj;
		if (level != other.level)
			return false;
		return true;
	}

}
