package it.polimi.ingsw.ps04.model.bonus.simple;

import it.polimi.ingsw.ps04.model.bonus.Bonus;
import it.polimi.ingsw.ps04.model.bonus.bonusVisitor.BonusVisitor;
import it.polimi.ingsw.ps04.model.player.Player;

/**
 * This class represents the abstract simple bonus. A simple bonus is a bonus
 * that can be execute without any choice from the player and without any other
 * class, except the player who activated it.
 * 
 * @see Bonus
 */
public abstract class SimpleBonus extends Bonus {

	private static final long serialVersionUID = -7501419875673418917L;

	/**
	 * This method executes the bonus activated. It will be implemented by each
	 * simple bonus.
	 * 
	 * @param player
	 *            The player who activated the bonus.
	 */
	public abstract void execute(Player player);

	/**
	 * @see Bonus#accept(BonusVisitor, Player)
	 */
	@Override
	public void accept(BonusVisitor visitor, Player player) {
		visitor.visit(this, player);
	}

}
