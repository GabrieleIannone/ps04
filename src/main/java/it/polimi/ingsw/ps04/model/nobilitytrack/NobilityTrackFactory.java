package it.polimi.ingsw.ps04.model.nobilitytrack;

import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.ThreadLocalRandom;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;

import it.polimi.ingsw.ps04.model.bonus.Bonus;
import it.polimi.ingsw.ps04.model.bonus.BonusFactory;
import it.polimi.ingsw.ps04.utils.ConfigHelper;
import it.polimi.ingsw.ps04.utils.exception.BadConfigFileException;

/**
 * this class builds the <code>NobilityTrack</code> objects from a json
 * configuration file.
 */
public class NobilityTrackFactory {

	private static final String NOBILITY_TRACKS = "nobilityTracks";
	private static final String STEP = "step";
	ConfigHelper configHelper;
	BonusFactory bonusFactory;
	private Map<Integer, Set<Bonus>> nobilityTrack = new TreeMap<>();

	/**
	 * constructs a <code>NobilityTrackFactory</code> object with the given
	 * parameters
	 * 
	 * @param configHelper
	 *            the configuration helper
	 * @param bonusFactory
	 *            the bonus factory
	 * @throws BadConfigFileException
	 *             if the configuration file contains error
	 * @throws JsonProcessingException
	 *             if there were problems encountered when processing JSON
	 *             content that are not pure I/O problems
	 * @throws IOException
	 *             if a problem with the I/O has occurred
	 */
	public NobilityTrackFactory(ConfigHelper configHelper, BonusFactory bonusFactory)
			throws JsonProcessingException, IOException, BadConfigFileException {
		setNobilityTrackFactory(configHelper, bonusFactory);
		buildNobilityTrack();
	}

	/**
	 * constructs the <code>NobilityTrackFactory</code> object with the assigned
	 * parameters and that corresponds to the given nobility number in the
	 * configuration file
	 * 
	 * @param configHelper
	 *            the configuration helper
	 * @param bonusFactory
	 *            the bonus factory
	 * @param nobilityNumber
	 *            the nobility number of the nobility track that you want to
	 *            retrieve
	 * @throws BadConfigFileException
	 *             if the configuration file contains error
	 * @throws JsonProcessingException
	 *             if there were problems encountered when processing JSON
	 *             content that are not pure I/O problems
	 * @throws IOException
	 *             if a problem with the I/O has occurred
	 */
	public NobilityTrackFactory(ConfigHelper configHelper, BonusFactory bonusFactory, int nobilityNumber)
			throws JsonProcessingException, IOException, BadConfigFileException {
		setNobilityTrackFactory(configHelper, bonusFactory);
		buildNobilityTrack(nobilityNumber);
	}

	/**
	 * Changes the configuration helper and the bonus factory of the nobility
	 * track factory to be equal to the ones passed as arguments
	 * 
	 * @param configHelper
	 *            the configuration helper
	 * @param bonusFactory
	 *            the bonus factory
	 */
	private void setNobilityTrackFactory(ConfigHelper configHelper, BonusFactory bonusFactory) {
		this.configHelper = configHelper;
		this.bonusFactory = bonusFactory;
	}

	/**
	 * builds the nobility track that corresponds to the given nobility number
	 * in the configuration file
	 * 
	 * @param nobilityNumber
	 * @throws BadConfigFileException
	 *             if the configuration file contains error
	 * @throws JsonProcessingException
	 *             if there were problems encountered when processing JSON
	 *             content that are not pure I/O problems
	 * @throws IOException
	 *             if a problem with the I/O has occurred
	 */
	private void buildNobilityTrack(int nobilityNumber)
			throws JsonProcessingException, IOException, BadConfigFileException {
		JsonNode tracksNode = configHelper.getRootNode().get(NOBILITY_TRACKS);
		JsonNode trackNode = tracksNode.get(nobilityNumber);
		for (JsonNode stepNode : trackNode) {
			int step = stepNode.get(STEP).asInt();
			Set<Bonus> bonuses = bonusFactory.getBonuses(stepNode.get(ConfigHelper.BONUSES));
			nobilityTrack.put(step, bonuses);
		}
	}

	/**
	 * builds a random nobility track from the one that are stored in the
	 * configuration file
	 * 
	 * @throws BadConfigFileException
	 *             if the configuration file contains error
	 * @throws JsonProcessingException
	 *             if there were problems encountered when processing JSON
	 *             content that are not pure I/O problems
	 * @throws IOException
	 *             if a problem with the I/O has occurred
	 */
	private void buildNobilityTrack() throws JsonProcessingException, IOException, BadConfigFileException {
		JsonNode tracksNode = configHelper.getRootNode().get(NOBILITY_TRACKS);
		int nobilityNumber = ThreadLocalRandom.current().nextInt(0, tracksNode.size());
		buildNobilityTrack(nobilityNumber);
	}

	/**
	 * Builds the nobility track
	 * 
	 * @return the nobility track that was built
	 * @throws JsonProcessingException
	 *             if there were problems encountered when processing JSON
	 *             content that are not pure I/O problems
	 * @throws IOException
	 *             if a problem with the I/O has occurred
	 */
	public NobilityTrack factoryMethod() throws JsonProcessingException, IOException {
		return new NobilityTrack(nobilityTrack);
	}

}
