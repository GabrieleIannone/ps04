package it.polimi.ingsw.ps04.model.player;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import groovy.json.JsonOutput;
import it.polimi.ingsw.ps04.model.deck.politic.PoliticCard;

/**
 * this class implements the politic cards that are in the hand of a player
 */
public class PoliticCardsHand implements Serializable {
	private static final long serialVersionUID = -1861123572474102419L;
	private List<PoliticCard> hand;

	/**
	 * Constructs a politic cards hand with the given politic cards
	 * 
	 * @param politicCards
	 *            the politic cards that you want to assign to this hand
	 */
	public PoliticCardsHand(List<PoliticCard> politicCards) {
		this.hand = politicCards;
	}

	/**
	 * Returns the number of politic cards of the hand
	 * 
	 * @return the number of politic cards of the hand
	 */
	public int size() {
		return hand.size();
	}

	/**
	 * add cards to this hand
	 * 
	 * @param politicCards
	 *            the cards that you want to add to this hand
	 */
	public void drawCards(List<PoliticCard> politicCards) {
		this.hand.addAll(politicCards);
	}

	/**
	 * add a card to this hand
	 * 
	 * @param card
	 *            the card that you want to add to this hand
	 */
	public void drawCard(PoliticCard card) {
		hand.add(card);
	}

	/**
	 * Checks if this hand contains the given cards
	 * 
	 * @param politicCards
	 *            the politic cards that you want to check if they are in this
	 *            hand or not
	 * @return true if the given politic cards are contained in this hand
	 */
	public boolean containsCards(List<PoliticCard> politicCards) {
		for (PoliticCard card : politicCards) {
			if (!(checkCard(card))) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Removes the given cards from this hand
	 * 
	 * @param usedCards
	 *            the cards that you want to remove from this hand
	 */
	public void useCards(List<PoliticCard> usedCards) {
		for (PoliticCard card : usedCards)
			useCard(card);
	}

	/**
	 * Checks if this hand contains the given card
	 * 
	 * @param card
	 *            the politic card that you want to check if it is in this hand
	 *            or not
	 * @return true if the given politic card is contained in this hand
	 */
	private boolean checkCard(PoliticCard card) {
		for (PoliticCard cardIterator : hand) {
			if (card.equals(cardIterator)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Removes the given card from this hand
	 * 
	 * @param usedCard
	 *            the card that you want to remove from this hand
	 */
	public void useCard(PoliticCard usedCard) {
		for (PoliticCard cardIterator : hand) {
			if (usedCard.equals(cardIterator)) {
				hand.remove(cardIterator);
				return;
			}
		}
	}

	/**
	 * Returns the politic cards at the specified index from this hand
	 * 
	 * @param index
	 *            the index of the politic cards that you want to retrieve from
	 *            this hand
	 * @return the politic cards at the specified index from this hand
	 */
	public PoliticCard cardAt(int index) {
		return hand.get(index);
	}

	/**
	 * Returns the politic cards of this hand
	 * 
	 * @return the politic cards of this hand
	 */
	public List<PoliticCard> getHand() {
		return hand;
	}

	/**
	 * Converts this <code>PoliticCardsHand</code> object to a
	 * <code>String</code>
	 */
	@Override
	public String toString() {
		return JsonOutput
				.prettyPrint(new ToStringBuilder(this, ToStringStyle.JSON_STYLE).append("hand", hand).toString());
	}

}
