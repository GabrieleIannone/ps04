package it.polimi.ingsw.ps04.model.deck.business;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.databind.JsonNode;

import it.polimi.ingsw.ps04.model.board.City;
import it.polimi.ingsw.ps04.model.board.Region;
import it.polimi.ingsw.ps04.model.bonus.Bonus;
import it.polimi.ingsw.ps04.model.bonus.BonusFactory;
import it.polimi.ingsw.ps04.utils.ConfigHelper;
import it.polimi.ingsw.ps04.utils.exception.BadConfigFileException;

/**
 * this class build the <code>BusinessDeck</code> objects from a json
 * configuration file.
 */
public class BusinessDeckFactory {

	private BonusFactory bonusFactory;

	/**
	 * Constructs a business deck factory with a given bonus factory
	 * 
	 * @param bonusFactory
	 *            the bonus factory that you want to assign to the business deck
	 *            factory
	 */
	public BusinessDeckFactory(BonusFactory bonusFactory) {
		this.bonusFactory = bonusFactory;
	}

	/**
	 * adds a business deck to a region
	 * 
	 * @param businessDeckNode
	 *            the json node that contains the business deck that you want to
	 *            assign
	 * @param region
	 *            the region that you want to assign the business deck
	 * @throws BadConfigFileException
	 *             if the configuration file contains error
	 */
	public void addBusinessDecktoRegion(JsonNode businessDeckNode, Region region) throws BadConfigFileException {
		List<BusinessPermitTile> tiles = new ArrayList<>();
		Set<City> cities;
		Set<Bonus> bonuses = null;
		for (JsonNode businessPermitTileNode : businessDeckNode) {
			cities = new HashSet<>();
			for (JsonNode citiesNode : businessPermitTileNode.get(ConfigHelper.CITIES)) {
				String cityName = citiesNode.textValue();
				cities.add(new City(cityName));
			}
			bonuses = bonusFactory.getBonuses(businessPermitTileNode.get(ConfigHelper.BONUSES));

			tiles.add(new BusinessPermitTile(cities, bonuses));
		}
		BusinessDeck businessDeck = new BusinessDeck(tiles);
		region.setBusinessDeck(businessDeck);
	}
}
