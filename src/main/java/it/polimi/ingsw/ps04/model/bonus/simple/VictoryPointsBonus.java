package it.polimi.ingsw.ps04.model.bonus.simple;

import it.polimi.ingsw.ps04.model.player.Player;

/**
 * This bonus is a simple bonus. It gives a number of victory points to the
 * player who activates this equals to the bonus increment.
 */
public class VictoryPointsBonus extends SimpleBonus {

	private static final long serialVersionUID = -8519717834403888481L;

	/**
	 * Constructor. Set the bonus increment.
	 * 
	 * @param bonusIncrement
	 *            The bonusIncrement of this bonus.
	 */
	public VictoryPointsBonus(int bonusIncrementer) {
		if (bonusIncrementer < 1) {
			throw new IllegalArgumentException("The points increments must be at least > 0");
		}
		super.bonusIncrement = bonusIncrementer;
	}

	/**
	 * Execute the bonus. Gives to the player a number of victory points equals
	 * to the bonus increment.
	 * 
	 * @param player
	 *            The player who activated the bonus.
	 */
	@Override
	public void execute(Player player) {
		player.getPoints().addPoints(bonusIncrement);
	}

}
