package it.polimi.ingsw.ps04.model.market;

import it.polimi.ingsw.ps04.model.deck.politic.PoliticCard;

/**
 * Represents a single politic card that a player wants to sell
 */
public class PoliticCardonSale extends ObjectonSale {

	private static final long serialVersionUID = 3371750797627966120L;
	private PoliticCard card;

	/**
	 * Create a new politic card to sell
	 * 
	 * @param card
	 *            The politic card to sell
	 * @param cost
	 *            The politic card cost
	 */
	public PoliticCardonSale(PoliticCard card, int cost) {
		super(cost);
		this.card = card;
	}

	/**
	 * @return the politic card, without his cost
	 */
	public PoliticCard getCard() {
		return card;
	}

	/**
	 * Converts this <code>PoliticCardonSale</code> object to a
	 * <code>String</code>
	 */
	@Override
	public String toString() {
		return "PoliticCardonSale [card=" + card + ", cost=" + cost + "]";
	}

	/**
	 * Returns a hash code value for this object. It considers the card.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((card == null) ? 0 : card.hashCode());
		return result;
	}

	/**
	 * Compares two cards on sale for equality. The result is <code>true</code>
	 * if and only if the argument is not <code>null</code> and is a
	 * <code>PoliticCardonSale</code> object that has the same card.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		PoliticCardonSale other = (PoliticCardonSale) obj;
		if (card == null) {
			if (other.card != null)
				return false;
		} else if (!card.equals(other.card))
			return false;
		return true;
	}

}
