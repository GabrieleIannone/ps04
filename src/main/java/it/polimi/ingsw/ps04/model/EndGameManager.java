package it.polimi.ingsw.ps04.model;

import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.ps04.model.player.Player;

/**
 * This class manage the ending of the game, giving the final points to players
 * and finding the winner(s).
 */
public class EndGameManager {
	private static final int VICTORY_POINTS = 3;
	private static final int FIRST_IN_NOBILITY_POINTS = 5;
	private static final int SECOND_IN_NOBILITY_POINTS = 2;
	private static final int FIRST_IN_BUSINESS_TILES_POINTS = 3;

	/**
	 * It gives the final points to players. It gives a number of points equals
	 * to the VICTORY_POINTS field to the player who built all his emporiums
	 * first, it gives a number of points equals to the FIRST_IN_NOBILITY_POINTS
	 * fields to the player(s) with the highest nobility level. If there is no
	 * tie it gives a number of points equals to the SECOND_IN_NOBILITY_POINTS
	 * field to the player(s) with the second highest nobility level. Finally it
	 * gives a number of points equals to the FIRST_IN_BUSINESS_TILES_POINTS
	 * field to the player(s) with the most business permit tiles.
	 * 
	 * @return The winner(s).
	 */
	public static List<Player> endGame(Model model) {
		model.gameFinished = true;
		model.getPlayersManager().getActivePlayer().getPoints().addPoints(VICTORY_POINTS);
		List<Player> noblestPlayers = new ArrayList<Player>();
		for (Player playerIterator : model.getPlayersManager().getActivePlayers()) {
			if (noblestPlayers.size() == 0) {
				noblestPlayers.add(playerIterator);
			} else if (playerIterator.getNobilityLevel().getLevel() > noblestPlayers.get(0).getNobilityLevel()
					.getLevel()) {
				noblestPlayers.clear();
				noblestPlayers.add(playerIterator);
			} else if (playerIterator.getNobilityLevel().getLevel() == noblestPlayers.get(0).getNobilityLevel()
					.getLevel()) {
				noblestPlayers.add(playerIterator);
			}
		}
		for (Player noblestPlayerIterator : noblestPlayers) {
			noblestPlayerIterator.getPoints().addPoints(FIRST_IN_NOBILITY_POINTS);
		}

		if (noblestPlayers.size() == 1) {
			Player noblestPlayer = noblestPlayers.get(0);
			noblestPlayers.clear();
			for (Player playerIterator : model.getPlayersManager().getActivePlayers()) {
				if (playerIterator != noblestPlayer) {
					if (noblestPlayers.size() == 0) {
						noblestPlayers.add(playerIterator);
					} else if (playerIterator.getNobilityLevel().getLevel() > noblestPlayers.get(0).getNobilityLevel()
							.getLevel()) {
						noblestPlayers.clear();
						noblestPlayers.add(playerIterator);
					} else if (playerIterator.getNobilityLevel().getLevel() == noblestPlayers.get(0).getNobilityLevel()
							.getLevel()) {
						noblestPlayers.add(playerIterator);
					}
				}
			}
			for (Player noblestPlayerIterator : noblestPlayers) {
				noblestPlayerIterator.getPoints().addPoints(SECOND_IN_NOBILITY_POINTS);
			}
		}

		List<Player> playersWiththeMostPermitTiles = new ArrayList<Player>();
		for (Player playerIterator : model.getPlayersManager().getActivePlayers()) {
			if (playersWiththeMostPermitTiles.size() == 0) {
				playersWiththeMostPermitTiles.add(playerIterator);
			} else if (playerIterator.getBusinessPermitTilePool().getPermitTilesNumber() > playersWiththeMostPermitTiles
					.get(0).getBusinessPermitTilePool().getPermitTilesNumber()) {
				playersWiththeMostPermitTiles.clear();
				playersWiththeMostPermitTiles.add(playerIterator);
			} else if (playerIterator.getBusinessPermitTilePool()
					.getPermitTilesNumber() == playersWiththeMostPermitTiles.get(0).getBusinessPermitTilePool()
							.getPermitTilesNumber()) {
				playersWiththeMostPermitTiles.add(playerIterator);
			}
		}
		for (Player playerWiththeMostPermitTilesIterator : playersWiththeMostPermitTiles) {
			playerWiththeMostPermitTilesIterator.getPoints().addPoints(FIRST_IN_BUSINESS_TILES_POINTS);
		}
		return findWinners(model);
	}

	/**
	 * Called by endGame. It returns the player with the most victory points. If
	 * there is a tie the player with the most assistant and politic cards is
	 * the winner. If there is still a tie, there are more than a winner and it
	 * returns all of them.
	 * 
	 * @param model
	 * @return The winner(s).
	 */
	private static List<Player> findWinners(Model model) {
		List<Player> winningPlayers = new ArrayList<Player>();
		for (Player playerIterator : model.getPlayersManager().getActivePlayers()) {
			if (winningPlayers.size() == 0) {
				winningPlayers.add(playerIterator);
			} else if (playerIterator.getPoints().getVictoryPoints() > winningPlayers.get(0).getPoints()
					.getVictoryPoints()) {
				winningPlayers.clear();
				winningPlayers.add(playerIterator);
			} else if (playerIterator.getPoints().getVictoryPoints() == winningPlayers.get(0).getPoints()
					.getVictoryPoints()) {
				winningPlayers.add(playerIterator);
			}

			if (winningPlayers.size() != 1) {
				List<Player> tiePlayers = new ArrayList<Player>(winningPlayers);
				winningPlayers.clear();
				for (Player tiePlayerIterator : tiePlayers) {
					if (winningPlayers.size() == 0) {
						winningPlayers.add(tiePlayerIterator);
					} else if (tiePlayerIterator.getAssistantCrew().getMembersNumber() + tiePlayerIterator
							.getPoliticCardsHand().size() > winningPlayers.get(0).getAssistantCrew().getMembersNumber()
									+ winningPlayers.get(0).getPoliticCardsHand().size()) {
						winningPlayers.clear();
						winningPlayers.add(tiePlayerIterator);
					} else if (tiePlayerIterator.getAssistantCrew().getMembersNumber() + tiePlayerIterator
							.getPoliticCardsHand().size() == winningPlayers.get(0).getAssistantCrew().getMembersNumber()
									+ winningPlayers.get(0).getPoliticCardsHand().size()) {
						winningPlayers.add(tiePlayerIterator);
					}
				}
			}
		}
		return winningPlayers;
	}
}
