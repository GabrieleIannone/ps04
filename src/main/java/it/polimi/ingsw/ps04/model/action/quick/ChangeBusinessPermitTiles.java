package it.polimi.ingsw.ps04.model.action.quick;

import it.polimi.ingsw.ps04.model.action.Action;
import it.polimi.ingsw.ps04.model.action.actionVisitor.ActionVisitor;
import it.polimi.ingsw.ps04.model.action.actionVisitor.VisitableAction;
import it.polimi.ingsw.ps04.model.board.Region;
import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.utils.exception.ActionNotCompletedException;

/**
 * this class implements the "ChangeBusinessPermitTiles" action of the game.
 */
public class ChangeBusinessPermitTiles extends QuickAction {

	private static final long serialVersionUID = -512938607064405229L;
	public static final String DESCRIPTION = "change revealed business permit tiles of a region";
	private Region region;

	/**
	 * constructs the action "ChangeBusinessPermitTiles"
	 * 
	 * @param region
	 *            the region of the business permit tile deck that contains the
	 *            tile that you want to change
	 */
	public ChangeBusinessPermitTiles(Region region) {
		this.region = region;
	}

	/**
	 * @see Action#execute(Player)
	 */
	@Override
	public void execute(Player player) throws ActionNotCompletedException {
		useQuickAction(player);

		try {
			player.getAssistantCrew().sendAssistants(1);
		} catch (IllegalArgumentException e) {
			player.getActionsCounters().undoQuickAction();
			throw new ActionNotCompletedException("You don't have enought assistants");
		}
		region.getBusinessDeck().changeRevealedTiles();
		// Region will call the method

	}

	// Getters and setters
	/**
	 * Returns the region that contains the tile that you want to change
	 * 
	 * @return the region that contains the tile that you want to change
	 */
	public Region getRegion() {
		return region;
	}

	/**
	 * Changes the region of the action to be equal to the argument region
	 * 
	 * @param region
	 *            the region that you want to set
	 */
	public void setRegion(Region region) {
		this.region = region;
	}

	/**
	 * @see VisitableAction#accept(ActionVisitor)
	 */
	@Override
	public void accept(ActionVisitor visitor) {
		visitor.visit(this);
	}

	/**
	 * Converts this object to a <code>String</code>
	 */
	@Override
	public String toString() {
		return "changed business tile in " + region.getName();
	}
}
