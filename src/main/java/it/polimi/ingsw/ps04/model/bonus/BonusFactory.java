package it.polimi.ingsw.ps04.model.bonus;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;

import it.polimi.ingsw.ps04.model.Model;
import it.polimi.ingsw.ps04.model.board.City;
import it.polimi.ingsw.ps04.model.bonus.Bonus;
import it.polimi.ingsw.ps04.model.deck.politic.PoliticDeck;
import it.polimi.ingsw.ps04.model.nobilitytrack.NobilityTrack;
import it.polimi.ingsw.ps04.utils.ConfigHelper;
import it.polimi.ingsw.ps04.utils.exception.BadConfigFileException;

/**
 * this class builds the <code>NobilityTrack</code> objects from a json
 * configuration file.
 */
public class BonusFactory {

	private Model model;
	private static final String CITY_BONUSES = "cityBonuses";
	private static final String BONUS_PATH = BonusFactory.class.getPackage().getName() + ".";
	private static final String SIMPLE_BONUS_PATH = BONUS_PATH + "simple" + ".";
	private static final String COMPLEX_BONUS_PATH = BONUS_PATH + "complex" + ".";
	public static final String BONUSES = "bonuses";
	private ConfigHelper configHelper;

	/**
	 * Constructs a <code>BonusFactory</code> from a given model and
	 * configHelper
	 * 
	 * @param model
	 * @param configHelper
	 */
	public BonusFactory(Model model, ConfigHelper configHelper) {
		this.model = model;
		this.configHelper = configHelper;
	}

	/**
	 * finds the cities bonuses in the config file
	 * 
	 * @return the cities bonuses
	 * @throws BadConfigFileException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	private List<Set<Bonus>> getCitiesBonuses() throws BadConfigFileException, JsonProcessingException, IOException {
		JsonNode citiesBonusesArrayNode = configHelper.getRootNode().get(CITY_BONUSES);
		JsonNode citiesBonusesNode = citiesBonusesArrayNode.get(0);
		List<Set<Bonus>> citiesBonuses = new ArrayList<>();
		Set<Bonus> cityBonuses;
		for (JsonNode cityBonusesNode : citiesBonusesNode) {
			cityBonuses = getBonuses(cityBonusesNode);
			citiesBonuses.add(cityBonuses);
		}
		return citiesBonuses;
	}

	/**
	 * adds the bonuses to the cities indicated in the attribute cities expect
	 * in the one indicated in excludedCity.
	 * 
	 * @param cities
	 *            the cities that you want to add the bonuses
	 * @param excludedCity
	 *            the city of the cities attribute that you don't want to add
	 *            the city
	 * @throws JsonProcessingException
	 *             if there were problems encountered when processing JSON
	 *             content that are not pure I/O problems
	 * @throws IOException
	 *             if a problem with the I/O has occurred
	 * @throws BadConfigFileException
	 *             if the configuration file contains error
	 */
	public void addBonuses(Set<City> cities, City excludedCity)
			throws BadConfigFileException, JsonProcessingException, IOException {
		List<Set<Bonus>> citiesBonuses = getCitiesBonuses();
		if (citiesBonuses.isEmpty()) {
			throw new BadConfigFileException("there are not cities bonuses");
		}
		List<Set<Bonus>> citiesBonusesCopy = new ArrayList<>(citiesBonuses);
		Collections.shuffle(citiesBonusesCopy);
		Iterator<Set<Bonus>> iterator = citiesBonusesCopy.iterator();
		Set<Bonus> bonuses;
		for (City city : cities) {
			if (city.equals(excludedCity)) {
				continue;
			}
			if (!iterator.hasNext()) {
				citiesBonusesCopy = new ArrayList<>(citiesBonuses);
				iterator = citiesBonusesCopy.iterator();
			}
			bonuses = iterator.next();
			iterator.remove();
			city.setBonuses(bonuses);
		}
	}

	/**
	 * Generates a bonus from a string
	 * 
	 * @param bonusName
	 *            the name of the bonus
	 * @param bonusValue
	 *            the value of the bonus
	 * @return the generated bonus
	 * @throws BadConfigFileException
	 *             if the configuration file contains error
	 */
	public Bonus generateBonusfromString(String bonusName, int bonusValue) throws BadConfigFileException {
		Class<?> bonusClass = null;
		try {
			bonusClass = Class.forName(SIMPLE_BONUS_PATH + bonusName);
		} catch (ClassNotFoundException e1) {
			try {
				// I try to see if it is a complex bonus

				bonusClass = Class.forName(COMPLEX_BONUS_PATH + bonusName);
			} catch (ClassNotFoundException e) {
				throw new BadConfigFileException("bonus does not exists");
			}
		}
		try {
			Constructor<?>[] constructors = bonusClass.getDeclaredConstructors();
			for (Constructor<?> constructor : constructors) {
				Class<?>[] parameterTypes = constructor.getParameterTypes();
				switch (parameterTypes.length) {
				case 1:
					if (parameterTypes[0].equals(int.class))
						return (Bonus) constructor.newInstance(bonusValue);
				case 2:
					if (parameterTypes[0].equals(int.class) && parameterTypes[1].equals(PoliticDeck.class)) {
						return (Bonus) constructor.newInstance(bonusValue, model.getPoliticDeck());
					} else if (parameterTypes[0].equals(int.class) && parameterTypes[1].equals(NobilityTrack.class)) {
						return (Bonus) constructor.newInstance(bonusValue, model.getNobilityTrack());
					} else if (parameterTypes[0].equals(int.class) && parameterTypes[1].equals(Model.class)) {
						return (Bonus) constructor.newInstance(bonusValue, model);
					}
				}
			}
			throw new BadConfigFileException("bonus does not exists");
		} catch (SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			throw new BadConfigFileException("bonus does not exists");
		}

	}

	/**
	 * Returns the bonuses from a json node
	 * 
	 * @param bonusesNode
	 *            the json node that contains the bonuses
	 * @return the bonuses of the json node
	 * @throws BadConfigFileException
	 *             if the configuration file contains error
	 */
	public Set<Bonus> getBonuses(JsonNode bonusesNode) throws BadConfigFileException {
		Set<Bonus> bonuses = new HashSet<>();
		Iterator<Entry<String, JsonNode>> nodeIterator = bonusesNode.fields();
		while (nodeIterator.hasNext()) {
			Entry<String, JsonNode> entry = nodeIterator.next();
			bonuses.add(generateBonusfromString(entry.getKey(), entry.getValue().asInt()));
		}
		return bonuses;
	}
}
