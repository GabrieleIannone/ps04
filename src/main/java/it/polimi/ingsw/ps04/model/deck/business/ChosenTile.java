package it.polimi.ingsw.ps04.model.deck.business;

import it.polimi.ingsw.ps04.model.board.Region;

/**
 * This class is used to save the region and the index of a tile that a player
 * wants take. It is used by BusinessPermitTileBonus
 * 
 * @see it.polimi.ingsw.ps04.model.bonus.complex.BusinessPermitTileBonus
 */
public class ChosenTile {
	private Region region;
	private int indexTile;

	/**
	 * Constructor.
	 * 
	 * @param region
	 *            The region which contains the tile to take.
	 * @param indexTile
	 *            The index of the chosen tile into the RevealedTiles set.
	 */
	public ChosenTile(Region region, int indexTile) {
		this.region = region;
		this.indexTile = indexTile;
	}

	// Getters

	/**
	 * Returns the index of the chosen tile into the RevealedTiles set
	 * 
	 * @return the index of the chosen tile into the RevealedTiles set
	 */
	public int getIndexTile() {
		return indexTile;
	}

	/**
	 * Returns the region which contains the tile to take.
	 * 
	 * @return the region which contains the tile to take.
	 */
	public Region getRegion() {
		return region;
	}

}
