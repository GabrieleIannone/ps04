package it.polimi.ingsw.ps04.model.player;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.google.common.base.MoreObjects;

import it.polimi.ingsw.ps04.utils.exception.ActivePlayerDisconnectedException;
import it.polimi.ingsw.ps04.utils.exception.NoActivePlayersException;
import it.polimi.ingsw.ps04.utils.exception.RoundFinishedException;

/**
 * Implements method to manage the players. For example it manages the order of
 * the turns and keep track of the active and the disconnected players.
 */
public class PlayersManager implements Serializable {

	private static final long serialVersionUID = -2563875220833239340L;
	// all the players of the game including the ones that disconnected
	private List<Player> allPlayers;
	private List<Player> activePlayers; // players that didn't disconnect
	private Player activePlayer;

	/**
	 * Constructs a players manager with a given list of players
	 * 
	 * @param players
	 *            the players that you want to manage
	 */
	public PlayersManager(List<Player> players) {
		this.allPlayers = players;
		this.activePlayers = players;
		activePlayer = players.get(0);
	}

	/**
	 * Constructs a players manager with the same characteristics of another
	 * players manager
	 * 
	 * @param playersManager
	 *            the players manager with the characteristics that you want to
	 *            assign to this players manager
	 */
	public PlayersManager(PlayersManager playersManager) {
		this.allPlayers = new ArrayList<Player>(playersManager.getAllPlayers());
		this.activePlayers = new ArrayList<Player>(playersManager.getActivePlayers());
		this.activePlayer = playersManager.getActivePlayer();
	}

	/**
	 * Returns all the active players
	 * 
	 * @return all the active players
	 */
	public List<Player> getActivePlayers() {
		return activePlayers;
	}

	/**
	 * Returns all the players, including the ones that disconnected
	 * 
	 * @return all the players, including the ones that disconnected
	 */
	public List<Player> getAllPlayers() {
		return allPlayers;
	}

	/**
	 * Returns the reference to the player that has the same name of the one
	 * passed as parameter
	 * 
	 * @param player
	 *            the player that contains the name of the player that you want
	 *            to retrieve
	 * @return
	 */
	public Player getPlayerReference(Player player) {
		for (Player iterablePlayer : allPlayers) {
			if (iterablePlayer.getName().equals(player.getName())) {
				return iterablePlayer;
			}
		}
		throw new IllegalArgumentException();
	}

	/**
	 * Updates the active player. The active player becomes the next player of
	 * the current active player in the order of the turns
	 * 
	 * @throws RoundFinishedException
	 *             if the round is finished
	 */
	public void updateActivePlayer() throws RoundFinishedException {
		Iterator<Player> it = activePlayers.iterator();
		while (it.hasNext()) {
			if (it.next().getName().equals(activePlayer.getName())) {
				if (it.hasNext()) {
					activePlayer = it.next();
				} else {
					activePlayer = activePlayers.get(0);
					throw new RoundFinishedException();
				}
			}
		}
	}

	/**
	 * Starts the round. The active player becomes the first player in the order
	 * of the turns
	 */
	public void startRound() {
		activePlayer = getFirstActivePlayer();
	}

	/**
	 * Returns the active player
	 * 
	 * @return the active player
	 */
	public Player getActivePlayer() {
		return activePlayer;
	}

	/**
	 * Returns the first active player in the order of the turns
	 * 
	 * @return the first active player in the order of the turns
	 */
	public Player getFirstActivePlayer() {
		return activePlayers.get(0);
	}

	/**
	 * Returns a hash code value for this object. It considers the active
	 * players and the disconnected players.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((activePlayers == null) ? 0 : activePlayers.hashCode());
		result = prime * result + ((allPlayers == null) ? 0 : allPlayers.hashCode());
		return result;
	}

	/**
	 * Compares two players managers for equality. The result is
	 * <code>true</code> if and only if the argument is not <code>null</code>
	 * and is a <code>PlayersManager</code> object that represents the same
	 * active players and the same disconnected players.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlayersManager other = (PlayersManager) obj;
		if (activePlayers == null) {
			if (other.activePlayers != null)
				return false;
		} else if (!activePlayers.equals(other.activePlayers))
			return false;
		if (allPlayers == null) {
			if (other.allPlayers != null)
				return false;
		} else if (!allPlayers.equals(other.allPlayers))
			return false;
		return true;
	}

	/**
	 * Converts this <code>PlayersManager</code> object to a <code>String</code>
	 */
	@Override
	public String toString() {
		return MoreObjects.toStringHelper(getClass()).add("Players", activePlayers).add("Active Player", activePlayer)
				.toString();
	}

	/**
	 * Removes a player passed as parameter from the active players
	 * 
	 * @param player
	 *            the player that you want to disconnect
	 * @throws ActivePlayerDisconnectedException
	 *             if the player that was disconnected was the active player
	 * @throws NoActivePlayersException
	 *             if no active players remained
	 */
	public void disconnectPlayer(Player player) throws ActivePlayerDisconnectedException, NoActivePlayersException {
		boolean activePlayerDisconnected = false;
		if (player.getName().equals(activePlayer.getName())) {
			if (activePlayers.size() == 1) {
				activePlayers.clear();
				throw new NoActivePlayersException();
			} else {
				try {
					updateActivePlayer();
				} catch (RoundFinishedException e) {
				}
				activePlayerDisconnected = true;
			}
		}
		player = getPlayerReference(player);
		activePlayers.remove(player);
		System.out.println("number of active players: " + activePlayers.size());
		if (activePlayerDisconnected)
			throw new ActivePlayerDisconnectedException();
	}

}
