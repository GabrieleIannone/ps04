package it.polimi.ingsw.ps04.model.bonus.bonusVisitor;

import it.polimi.ingsw.ps04.client.view.userinterface.UserInterface;
import it.polimi.ingsw.ps04.model.WaitingBonuses;
import it.polimi.ingsw.ps04.model.bonus.complex.ComplexBonus;
import it.polimi.ingsw.ps04.model.bonus.simple.NobilityBonus;
import it.polimi.ingsw.ps04.model.bonus.simple.PoliticCardsBonus;
import it.polimi.ingsw.ps04.model.player.Player;

/**
 * This class is the visitor used into the client. It implements BonusVisitor.
 * 
 * @see BonusVisitor
 */
public class ClientBonusVisitor implements BonusVisitor {

	private UserInterface ui;

	/**
	 * Constructor.
	 * 
	 * @param ui
	 *            The user interface used by the local client.
	 */
	public ClientBonusVisitor(UserInterface ui) {
		this.ui = ui;
	}

	/**
	 * Once a complex bonus is visited, the visitor redefines it, saves it in
	 * model waiting bonus and prints a message.
	 * 
	 * @see ComplexBonus#redefine(BonusVisitor,
	 *      it.polimi.ingsw.ps04.model.Model)
	 * @see WaitingBonuses
	 * 
	 * @param bonus
	 *            The activated complex bonus.
	 * 
	 * @param player
	 *            The player who activated the bonus.
	 */
	@Override
	public void visit(ComplexBonus bonus, Player player) {
		bonus.redefine(this, ui.getModel());
		bonus.saveWaitingBonus();
		System.out.println("New bonus waiting to be execute!");
	}

	/**
	 * Once a PoliticCardsBonus is visited, the visitor changes its politic deck
	 * with the client one and than executes the bonus.
	 * 
	 * @param bonus
	 *            The activated PoliticCardsBonus.
	 * 
	 * @param player
	 *            The player who activated the bonus.
	 */
	@Override
	public void visit(PoliticCardsBonus bonus, Player player) {
		bonus.setPoliticCardsDeck(ui.getModel().getPoliticDeck());
		bonus.execute(player);
	}

	/**
	 * Once a NobilityBonus is visited, the visitor changes its nobility track
	 * with the client one and than executes the bonus.
	 * 
	 * @param bonus
	 *            The activated NobilityBonus.
	 * 
	 * @param player
	 *            The player who activated the bonus.
	 */
	@Override
	public void visit(NobilityBonus bonus, Player player) {
		bonus.setNobilityTrack(ui.getModel().getNobilityTrack());
		bonus.execute(player);
	}

}
