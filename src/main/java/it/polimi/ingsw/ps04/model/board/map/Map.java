package it.polimi.ingsw.ps04.model.board.map;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.jgrapht.UndirectedGraph;
import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;

import groovy.json.JsonOutput;
import it.polimi.ingsw.ps04.model.board.City;
import it.polimi.ingsw.ps04.model.board.King;
import it.polimi.ingsw.ps04.model.board.Region;
import it.polimi.ingsw.ps04.model.bonus.Bonus;
import it.polimi.ingsw.ps04.model.bonus.bonusVisitor.BonusVisitor;
import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.utils.color.CityColor;

/**
 * this class represents the map of the game and manages the connections of the
 * cities
 */
public class Map implements Serializable {

	private static final long serialVersionUID = 7368755846294466107L;
	private String name;
	private King king;
	private List<Region> regions;
	private transient BonusVisitor bonusvisitor;

	/*
	 * I choose SimpleGraph implementation because our map is an undirected
	 * graph for which at most one edge connects any two vertices, and loops are
	 * not permitted.
	 */
	private UndirectedGraph<City, DefaultEdge> graph = new SimpleGraph<>(DefaultEdge.class);

	/**
	 * constructs a map with the given parameters
	 * 
	 * @param graph
	 *            the graph of the map
	 * @param king
	 *            the king of the map
	 * @param name
	 *            the name of the map
	 * @param regions
	 *            the regions of the map
	 */
	protected Map(UndirectedGraph<City, DefaultEdge> graph, King king, String name, List<Region> regions) {
		this.graph = graph;
		this.king = king;
		this.name = name;
		this.regions = regions;
	}

	/**
	 * Returns the name of the map
	 * 
	 * @return the name of the map
	 */
	public String getName() {
		return name;
	}

	/**
	 * Returns the regions of the map
	 * 
	 * @return the regions of the map
	 */
	public List<Region> getRegions() {
		return regions;
	}

	/**
	 * Returns the king of the map
	 * 
	 * @return the king of the map
	 */
	public King getKing() {
		return king;
	}

	/**
	 * Returns the graph of the map
	 * 
	 * @return the graph of the map
	 */
	public UndirectedGraph<City, DefaultEdge> getGraph() {
		return graph;
	}

	/**
	 * Returns the reference of the region that has the same name of the one
	 * passed as parameter
	 * 
	 * @param region
	 *            the region that has the same name of the region that you want
	 *            to get the reference
	 * @return the reference of the region that was found
	 */
	public Region getRegionReference(Region region) {
		for (Region iterableRegion : regions) {
			if (iterableRegion.getName().equalsIgnoreCase(region.getName())) {
				return iterableRegion;
			}
		}
		throw new IllegalArgumentException("Region not found!");
	}

	/**
	 * This is the recursive method that assigns the bonuses to the player who
	 * has built in the city specified in the attribute city
	 * 
	 * @param player
	 *            the player that you have to give the bonus
	 * @param city
	 *            the city where the player has built
	 * @param visitedcity
	 *            the cities that were visited previously
	 */
	private void activateCitiesBonuses(Player player, City city, Set<City> visitedcity) {
		if (city.playerHasBuilt(player)) {
			Set<Bonus> cityBonuses = city.getBonus();
			if (cityBonuses != null) {
				for (Bonus bonus : cityBonuses) {
					bonus.accept(bonusvisitor, player);
				}
			}
			visitedcity.add(city);
			for (City neighbor : getNeighbors(city)) {
				if (!(visitedcity.contains(neighbor))) {
					activateCitiesBonuses(player, neighbor, visitedcity);
				}
			}
		}
	}

	/**
	 * This method is called after a player built in a city. It activates the
	 * bonuses in the city in which player has just built and all the cities in
	 * which he has already built connected with that city
	 * 
	 * @param player
	 *            player the player that you have to give the bonus
	 * @param city
	 *            city the city where the player has built
	 */
	public void activateCitiesBonuses(Player player, City city) {
		Set<City> visitedcity = new HashSet<City>();
		activateCitiesBonuses(player, city, visitedcity);
		assignCityColorBonus(player, city.getColour());
		assignRegionBonus(player, city.getRegion());
	}

	/**
	 * Checks if the player has built in the given cities
	 * 
	 * @param cities
	 *            the cities that you want to check if the player has built
	 * @param player
	 *            the player that you want to check if he has built
	 * @return true if the player has built in all of this cities
	 */
	private boolean playerHasBuilt(Set<City> cities, Player player) {
		for (City city : cities) {
			if (!city.playerHasBuilt(player)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Assigns the region bonus if the player can have it
	 * 
	 * @param player
	 *            the player that you want to assign the region bonus
	 * @param region
	 *            the region that contains the bonus
	 */
	private void assignRegionBonus(Player player, Region region) {
		Set<City> cityOfSameRegion = getCities(region);
		if (playerHasBuilt(cityOfSameRegion, player) && (!region.isBonusTaken())) {
			for (Bonus bonus : region.getBonuses()) {
				bonus.accept(bonusvisitor, player);
			}
			region.setBonusTaken(true);
		}
	}

	/**
	 * Assigns the city color bonus if the player can have it
	 * 
	 * @param player
	 *            the player that you want to assign the city color bonus
	 * @param color
	 *            the color that contains the bonus
	 */
	private void assignCityColorBonus(Player player, CityColor color) {
		Set<City> cityOfSameColor = getCities(color);
		if (playerHasBuilt(cityOfSameColor, player)) {
			for (Bonus bonus : color.getBonus()) {
				bonus.accept(bonusvisitor, player);
			}
		}
	}

	/**
	 * Returns the cities that have the given color
	 * 
	 * @param color
	 *            the color of the cities that you want to retrieve
	 * @return the cities that have the given color
	 */
	private Set<City> getCities(CityColor color) {
		Set<City> cities = new HashSet<>();
		for (City city : getCities()) {
			if (city.getColour() != null && city.getColour().equals(color)) {
				cities.add(city);
			}
		}
		return cities;
	}

	/**
	 * Returns the number of the cities of the map
	 * 
	 * @return the number of the cities of the map
	 */
	public int getCitiesNumber() {
		return graph.vertexSet().size();
	}

	/**
	 * Returns the neighbors of the city passed in the attribute source
	 * 
	 * @param source
	 *            the city that you want to find the neighbors
	 * @return the neighbors of the city passed in the attribute source
	 */
	public Set<City> getNeighbors(City source) {
		Set<City> neighbors = new HashSet<City>();
		for (DefaultEdge edge : graph.edgesOf(source)) {
			if (!graph.getEdgeTarget(edge).equals(source))
				neighbors.add(graph.getEdgeTarget(edge));
			else
				neighbors.add(graph.getEdgeSource(edge));
		}
		return neighbors;
	}

	/**
	 * Returns the cities of the region passed in the attribute region
	 * 
	 * @param region
	 *            the region that contains the cities that you want
	 * @return the cities of the region passed in the attribute region
	 */
	public Set<City> getCities(Region region) {
		Set<City> citiesCopy = new HashSet<>(getCities());
		Iterator<City> it = citiesCopy.iterator();
		while (it.hasNext()) {
			if (!it.next().getRegion().getName().equals(region.getName())) {
				it.remove();
			}
		}
		return citiesCopy;
	}

	/**
	 * Returns the edges of the graph
	 * 
	 * @return the edges of the graph
	 */
	public Set<DefaultEdge> getEdges() {
		return graph.edgeSet();
	}

	/**
	 * Returns the cities of the map
	 * 
	 * @return the cities of the map
	 */
	public Set<City> getCities() {
		return graph.vertexSet();
	}

	/**
	 * Returns the king position
	 * 
	 * @return the king position
	 */
	public City getKingPosition() {
		return king.getActualCity();
	}

	/**
	 * moves the king in another city
	 * 
	 * @param destination
	 *            the city where you want to build
	 * @return number of steps the king has made
	 */
	public int moveKing(City destination) {
		int steps = minDistance(king.getActualCity(), destination);
		king.setActualCity(destination);
		return steps;
	}

	/**
	 * calculates the minimum distance from given cities
	 * 
	 * @param departure
	 *            the departure city
	 * @param destination
	 *            the destination city
	 * @return how many city there are in the minimum path between the departure
	 *         and the destination city
	 */
	public int minDistance(City departure, City destination) {
		DijkstraShortestPath<City, DefaultEdge> shortestPathsFinder = new DijkstraShortestPath<>(graph, departure,
				destination);
		return (int) shortestPathsFinder.getPathLength();
	}

	/**
	 * Returns the reference of the city that has the same name of the one
	 * passed as parameter
	 * 
	 * @param city
	 *            the city that has the same name of the city that you want to
	 *            get the reference
	 * @return the reference of the city that was found
	 */
	public City getCityReference(City city) {
		for (City iterableCity : getCities()) {
			if (iterableCity.getName().equals(city.getName()))
				return iterableCity;
		}
		throw new IllegalArgumentException("City not found!");
	}

	/**
	 * get the number of cities of a region
	 * 
	 * @param region
	 *            the region of which you want to know the number of cities
	 * @return the number of cities of the region
	 */
	public int getCitiesNumber(Region region) {
		int citiesNumber = 0;
		for (City city : graph.vertexSet()) {
			if (city.getRegion().getName().equals(region.getName())) {
				citiesNumber++;
			}
		}
		return citiesNumber;
	}

	/**
	 * Returns a list of strings that represents the edges
	 * 
	 * @return a list of strings that represents the edges
	 */
	protected List<String> getEdgesStrings() {
		List<String> edges = new ArrayList<>();
		for (DefaultEdge e : getEdges()) {
			edges.add("\"" + graph.getEdgeSource(e).getName() + " <-> " + graph.getEdgeTarget(e).getName() + "\"");
		}
		return edges;
	}

	/**
	 * Changes the bonus visitor of the map to be equal to the argument
	 * bonusVisitor
	 * 
	 * @param bonusVisitor
	 *            the bonus visitor that you want to set
	 */
	public void setBonusVisitor(BonusVisitor bonusVisitor) {
		this.bonusvisitor = bonusVisitor;
		for (Region regioniterator : regions) {
			regioniterator.getBusinessDeck().setBonusVisitor(bonusVisitor);
		}
	}

	/**
	 * Returns a hash code value for this object. It considers the king, the
	 * graph, the name and the regions
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((king == null) ? 0 : king.hashCode());
		result = prime * result + ((graph == null) ? 0 : graph.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((regions == null) ? 0 : regions.hashCode());
		return result;
	}

	/**
	 * Says if two edges are equals
	 * 
	 * @param firstGraph
	 *            the first graph that you want to compare
	 * @param secondGraph
	 *            the second graph that you want to compare
	 * @return <code>true</code> if the edges are equals
	 */
	private static boolean edgesAreEquals(UndirectedGraph<City, DefaultEdge> firstGraph,
			UndirectedGraph<City, DefaultEdge> secondGraph) {
		boolean flag;
		if (firstGraph.edgeSet().size() != secondGraph.edgeSet().size()) {
			return false;
		}
		for (DefaultEdge firstIterableEdge : firstGraph.edgeSet()) {
			flag = false;
			City source = firstGraph.getEdgeSource(firstIterableEdge);
			City target = firstGraph.getEdgeTarget(firstIterableEdge);
			for (DefaultEdge secondIterableEdge : secondGraph.edgeSet()) {
				if (secondGraph.getEdgeSource(secondIterableEdge).equals(source)
						&& secondGraph.getEdgeTarget(secondIterableEdge).equals(target)) {
					flag = true;
					break;
				}
			}
			if (flag == false) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Compares two maps for equality. The result is <code>true</code> if and
	 * only if the argument is not <code>null</code> and is a <code>Map</code>
	 * object that represents the same king, graph, name and regions.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Map other = (Map) obj;
		if (king == null) {
			if (other.king != null)
				return false;
		} else if (!king.equals(other.king))
			return false;
		if (graph == null) {
			if (other.graph != null)
				return false;
		} else if (!edgesAreEquals(graph, other.graph))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (regions == null) {
			if (other.regions != null)
				return false;
		} else {
			// if regions are equals
			if (regions.size() != other.regions.size())
				return false;
			boolean foundEqualRegion = false;
			for (Region region1 : this.getRegions()) {
				for (Region region2 : other.getRegions()) {
					if (region1.equals(region2)) {
						foundEqualRegion = true;
						break;
					}
				}
				if (foundEqualRegion == false)
					return false;
				foundEqualRegion = false;
			}
		}
		return true;
	}

	/**
	 * Converts this <code>Map</code> object to a <code>String</code>
	 */
	@Override
	public String toString() {
		return JsonOutput.prettyPrint(new ToStringBuilder(this, ToStringStyle.JSON_STYLE).append("king", king)
				.append("regions", regions).append("edges", getEdgesStrings()).toString());
	}
}
