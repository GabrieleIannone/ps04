package it.polimi.ingsw.ps04.model.bonus.simple;

import it.polimi.ingsw.ps04.model.bonus.Bonus;
import it.polimi.ingsw.ps04.model.bonus.bonusVisitor.BonusVisitor;
import it.polimi.ingsw.ps04.model.deck.politic.PoliticDeck;
import it.polimi.ingsw.ps04.model.player.Player;

/**
 * This bonus gives a number of politic cards to the player who activates this
 * equals to the bonus increment.
 */
public class PoliticCardsBonus extends Bonus {

	private static final long serialVersionUID = -3535139486026795636L;
	private PoliticDeck politicCardsDeck;

	/**
	 * Constructor. Set the bonus increment.
	 * 
	 * @param bonusIncrement
	 *            The bonusIncrement of this bonus.
	 * @param nobilityTrack
	 *            The politic deck of the model.
	 */
	public PoliticCardsBonus(int bonusIncrementer, PoliticDeck politicCardsDeck) {
		super.bonusIncrement = bonusIncrementer;
		this.politicCardsDeck = politicCardsDeck;
	}

	/**
	 * Set the nobility track.
	 * 
	 * @param nobilityTrack
	 *            The nobility track of the model.
	 */
	public void setPoliticCardsDeck(PoliticDeck politicCardsDeck) {
		this.politicCardsDeck = politicCardsDeck;
	}

	/**
	 * Execute the bonus. Gives to the player a number of politic cards equals
	 * to the bonus increment.
	 * 
	 * @see it.polimi.ingsw.ps04.model.deck.politic.PoliticDeck#drawCard()
	 * 
	 * @param player
	 *            The player who activated the bonus.
	 */
	public void execute(Player player) {
		for (int i = 0; i < bonusIncrement; i++) {
			player.addPoliticCard(politicCardsDeck.drawCard());
		}

	}

	/**
	 * @see Bonus#accept(BonusVisitor, Player)
	 */
	@Override
	public void accept(BonusVisitor visitor, Player player) {
		visitor.visit(this, player);
	}
}