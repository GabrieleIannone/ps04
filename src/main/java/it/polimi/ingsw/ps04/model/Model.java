package it.polimi.ingsw.ps04.model;

import java.io.Serializable;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Observable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import groovy.json.JsonOutput;
import it.polimi.ingsw.ps04.model.action.Action;
import it.polimi.ingsw.ps04.model.board.City;
import it.polimi.ingsw.ps04.model.board.map.Map;
import it.polimi.ingsw.ps04.model.bonus.bonusVisitor.BonusVisitor;
import it.polimi.ingsw.ps04.model.bonus.bonusVisitor.ServerBonusVisitor;
import it.polimi.ingsw.ps04.model.bonus.complex.ComplexBonus;
import it.polimi.ingsw.ps04.model.deck.politic.PoliticDeck;
import it.polimi.ingsw.ps04.model.market.BusinessPermitTileonSale;
import it.polimi.ingsw.ps04.model.market.Market;
import it.polimi.ingsw.ps04.model.market.PoliticCardonSale;
import it.polimi.ingsw.ps04.model.market.SellingOffers;
import it.polimi.ingsw.ps04.model.nobilitytrack.NobilityTrack;
import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.model.player.PlayersManager;
import it.polimi.ingsw.ps04.utils.message.RequestforBonus;
import it.polimi.ingsw.ps04.utils.message.market.BuyingOrdersMessage;
import it.polimi.ingsw.ps04.utils.message.market.MarketMessage;
import it.polimi.ingsw.ps04.utils.message.market.SellingOffersMessage;
import it.polimi.ingsw.ps04.utils.message.market.SkipBuyMessage;
import it.polimi.ingsw.ps04.utils.color.CityColor;
import it.polimi.ingsw.ps04.utils.exception.ActionNotCompletedException;
import it.polimi.ingsw.ps04.utils.message.ActionMessage;
import it.polimi.ingsw.ps04.utils.message.GameFinishedMessage;
import it.polimi.ingsw.ps04.utils.message.Message;

/**
 * Implements the model of the MVC pattern. It contains the state of a match.
 */
public class Model extends Observable implements Cloneable, Serializable {

	private static final long serialVersionUID = 6928398704832051090L;

	private static final long DEFAULT_TURN_SECONDS = 200;

	protected Map map;
	protected NobilityTrack nobilityTrack = new NobilityTrack();
	private PoliticDeck politicCardsDeck = new PoliticDeck();
	private PlayersManager playersManager;
	private WaitingBonuses waitingBonusesPool = new WaitingBonuses();
	private transient BonusVisitor bonusVisitor = new ServerBonusVisitor();
	private Duration turnTimeout = Duration.ofSeconds(DEFAULT_TURN_SECONDS);
	protected boolean gameFinished = false;
	private Market market;

	/**
	 * Constructs a model with the given parameters
	 * 
	 * @param players
	 *            the players of the match
	 * @param map
	 *            the map of the match
	 * @param politicCardsDeck
	 *            the politic cards deck of the match
	 * @param nobilityTrack
	 *            the nobility track of the match
	 * @param turnTimeout
	 *            the turn timeout of the match
	 */
	public Model(List<Player> players, Map map, PoliticDeck politicCardsDeck, NobilityTrack nobilityTrack,
			Duration turnTimeout) {
		this.politicCardsDeck = politicCardsDeck;
		setMap(map);
		setNobilityTrack(nobilityTrack);
		setPlayersManager(new PlayersManager(players));
		setTurnTimeout(turnTimeout);
		market = new Market(playersManager);
		market.closeMarket();
		waitingBonusesPool = new WaitingBonuses();
		bonusVisitor = new ServerBonusVisitor();
	}

	/**
	 * Constructs an empty model
	 */
	public Model() {
		market = new Market();
		waitingBonusesPool = new WaitingBonuses();
		bonusVisitor = new ServerBonusVisitor();
	}

	/**
	 * Sets the properties of this model with the ones of another model
	 * 
	 * @param model
	 *            the model that contains the properties that you want to assign
	 *            to this model
	 */
	public void setModel(Model model) {
		setMap(model.getMap());
		setNobilityTrack(model.nobilityTrack);
		setPoliticCardsDeck(model.getPoliticDeck());
		setPlayersManager(model.getPlayersManager());
		setTurnTimeout(model.getTurnTimeout());
	}

	/**
	 * Returns the city colors of this match
	 * 
	 * @return the city colors of this match
	 */
	public List<CityColor> getCityColors() {
		List<CityColor> colors = new ArrayList<>();
		CityColor color;
		for (City city : getMap().getCities()) {
			color = city.getColour();
			if (!colors.contains(color) && color != null) {
				colors.add(color);
			}
		}
		return colors;
	}

	/**
	 * Returns the current ranking of the game
	 * 
	 * @return the current ranking of the game
	 */
	public List<Player> getMatchRanking() {
		List<Player> winningPlayers = new ArrayList<>();
		List<Player> players = new ArrayList<>(playersManager.getAllPlayers());
		Comparator<Player> byNobilityPoints = (p1, p2) -> Integer.compare(p2.getNobilityLevel().getLevel(),
				p1.getNobilityLevel().getLevel());
		Comparator<Player> byAssistantsAndCards = (p1, p2) -> Integer.compare(
				p2.getPoliticCardsHand().size() + p2.getAssistantCrew().getMembersNumber(),
				p1.getPoliticCardsHand().size() + p1.getAssistantCrew().getMembersNumber());

		players.stream().sorted(byNobilityPoints.thenComparing(byAssistantsAndCards))
				.forEachOrdered(winningPlayers::add);
		return winningPlayers;
	}

	/**
	 * Executes an actions with the active player. This method will be called by
	 * the controller once received an action message.
	 * 
	 * @param action
	 *            the action that the active player have to execute
	 * @return true if the player cannot do other actions
	 */
	public boolean doAction(Action action) {

		action.execute(playersManager.getActivePlayer());
		notifyMessage(new ActionMessage(action));

		return playersManager.getActivePlayer().getActionsCounters().checkifnoActionAvalaible()
				&& waitingBonusesPool.areWaitingBonus();
	}

	/**
	 * Gives a bonus to the active player. This method will be called by the
	 * controller once received a request for bonus message
	 * 
	 * @param message
	 *            the message that contains the bonus that you have to give to
	 *            the active player
	 * @return true if the player cannot do other actions
	 * @See BonusVisitor
	 */
	public boolean executeBonus(RequestforBonus message) {
		ComplexBonus bonus = message.getBonus();
		if (waitingBonusesPool.bonusIsWaiting(bonus)) {
			bonus.redefine(bonusVisitor, this);
			bonus.execute(bonusVisitor);
			waitingBonusesPool.removeBonus(bonus);
		}

		notifyMessage(message);
		return playersManager.getActivePlayer().getActionsCounters().checkifnoActionAvalaible()
				&& waitingBonusesPool.areWaitingBonus();
	}

	/**
	 * Starts the market phase
	 */
	public void startMarketPhase() {
		market = new Market(playersManager);
		market.getPlayersManager().startRound();
	}

	/**
	 * Manages a market message. This method will be called by the controller
	 * once received a market message
	 * 
	 * @param message
	 *            the market message to manage
	 */
	public void manageMarket(MarketMessage message) {
		if (message instanceof SellingOffersMessage) {
			manageSellingOffer((SellingOffersMessage) message);
		}

		if (message instanceof BuyingOrdersMessage) {
			try {
				manageBuyingOrder((BuyingOrdersMessage) message);
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}
		}
		if (message instanceof SkipBuyMessage) {
			notifyMessage(message);
		}
	}

	/**
	 * Manages a selling offer message
	 * 
	 * @param message
	 *            the selling offers message to manage
	 */
	private void manageSellingOffer(SellingOffersMessage message) {
		if (!market.isSellingPhase()) {
			throw new ActionNotCompletedException("It's buying phase!");
		}

		if (checkSellingOffer(message, market.getPlayersManager().getActivePlayer())) {
			market.receiveSellingOffer(message);
			notifyMessage(message);
		} else
			throw new ActionNotCompletedException("You cannot sell objects you don't have!");
	}

	/**
	 * Manages a buying orders message
	 * 
	 * @param message
	 *            the buying orders message to manage
	 */
	private void manageBuyingOrder(BuyingOrdersMessage message) {
		if (market.isSellingPhase()) {
			throw new ActionNotCompletedException("It's selling phase!");
		}
		market.checkOrder(message);
		market.processOrder(message);
		notifyMessage(message);

	}

	/**
	 * ends the game
	 */
	public void endGame() {
		List<Player> winningPlayers = EndGameManager.endGame(this);
		notifyMessage(new GameFinishedMessage(winningPlayers));
	}

	/**
	 * clones this object
	 */
	@Override
	public Model clone() {
		return new Model(playersManager.getActivePlayers(), map, politicCardsDeck, nobilityTrack, turnTimeout);

	}

	/**
	 * notifies the given message to the observers
	 * 
	 * @param message
	 *            the message that that you want to notify
	 */
	public void notifyMessage(Message message) {
		setChanged();
		notifyObservers(message);
	}

	/**
	 * notifies the given exception to the observers
	 * 
	 * @param exception
	 *            the exception that you want to notify
	 */
	public void notifyException(Exception exception) {
		setChanged();
		notifyObservers(exception);
	}

	/**
	 * notify this object to the observers
	 */
	public void notifyModel() {
		setChanged();
		notifyObservers();
	}

	// Gets and setters

	/**
	 * Returns the politic cards deck of this model
	 * 
	 * @return the politic cards deck of this model
	 */
	public PoliticDeck getPoliticDeck() {
		return politicCardsDeck;
	}

	/**
	 * Returns the map of this model
	 * 
	 * @return the map of this model
	 */
	public Map getMap() {
		return map;
	}

	/**
	 * Returns the nobility track of this model
	 * 
	 * @return the nobility track of this model
	 */
	public NobilityTrack getNobilityTrack() {
		return nobilityTrack;
	}

	/**
	 * Checks if the game is finished
	 * 
	 * @return true if the game is finished
	 */
	public boolean isGameFinished() {
		return gameFinished;
	}

	/**
	 * Returns the players manager of this model
	 * 
	 * @return the players manager of this model
	 */
	public PlayersManager getPlayersManager() {
		return playersManager;
	}

	/**
	 * Returns the turn timeout of this model
	 * 
	 * @return the turn timeout of this model
	 */
	public Duration getTurnTimeout() {
		return turnTimeout;
	}

	/**
	 * Returns the available bonus that the player have to compile and activate
	 * 
	 * @return the available bonus that the player have to compile and activate
	 */
	public WaitingBonuses getAvailableBonusesPool() {
		return waitingBonusesPool;
	}

	/**
	 * Returns the market of this model
	 * 
	 * @return the market of this model
	 */
	public Market getMarket() {
		return market;
	}

	/**
	 * Changes the nobility track of this model to be equal to the argument
	 * nobilityTrack
	 * 
	 * @param nobilityTrack
	 *            the nobility track that you want to set
	 */
	public void setNobilityTrack(NobilityTrack nobilityTrack) {
		this.nobilityTrack.set(nobilityTrack);
		this.nobilityTrack.setBonusvisitor(bonusVisitor);
	}

	/**
	 * Changes the bonus visitor of this model to be equal to the argument
	 * bonusVisitor
	 * 
	 * @param bonusVisitor
	 *            the bonus visitor that you want to set
	 */
	public void setBonusVisitor(BonusVisitor bonusVisitor) {
		this.bonusVisitor = bonusVisitor;
	}

	/**
	 * Changes the map of this model to be equal to the argument map
	 * 
	 * @param map
	 *            the map that you want to set
	 */
	public void setMap(Map map) {
		this.map = map;
		this.map.setBonusVisitor(bonusVisitor);
	}

	/**
	 * Sets the boolean that says if the game is finished or not
	 * 
	 * @param gameFinished
	 *            true if the game is finished
	 */
	public void setGameFinished(boolean gameFinished) {
		this.gameFinished = gameFinished;
	}

	/**
	 * Changes the players of this model to be equal to the argument players
	 * 
	 * @param players
	 *            the players that you want to set
	 */
	public void setPlayers(List<Player> players) {
		playersManager = new PlayersManager(players);
		market = new Market(playersManager);
		market.closeMarket();
	}

	/**
	 * Changes the politic cards deck of this model to be equal to the argument
	 * politicCardsDeck
	 * 
	 * @param politicCardsDeck
	 *            the politic cards deck that you want to set
	 */
	public void setPoliticCardsDeck(PoliticDeck politicCardsDeck) {
		this.politicCardsDeck.set(politicCardsDeck);
	}

	/**
	 * Changes the players manager of this model to be equal to the argument
	 * playersManager
	 * 
	 * @param playersManager
	 *            the players manager that you want to set
	 */
	public void setPlayersManager(PlayersManager playersManager) {
		this.playersManager = playersManager;
	}

	/**
	 * Changes the turn timeout of this model to be equal to the argument
	 * turnTimeout
	 * 
	 * @param turnTimeout
	 *            the turn timeout that you want to set
	 */
	public void setTurnTimeout(Duration turnTimeout) {
		this.turnTimeout = turnTimeout;
	}

	/**
	 * Converts this <code>Model</code> object to a <code>String</code>
	 */
	@Override
	public String toString() {
		return JsonOutput.prettyPrint(new ToStringBuilder(this, ToStringStyle.JSON_STYLE).append("map", map)
				.append("players", playersManager.getActivePlayers()).append("nobility track", nobilityTrack)
				.append("corruption colors", politicCardsDeck.getCorruptionColors()).toString());
	}

	/**
	 * Called after model received a selling offer message Chek if the seller
	 * owns which has chosen to sell
	 * 
	 * @param message
	 *            the message which contains the selling offer
	 * @param player
	 *            the seller
	 * @return true if player has a number of assistants greater than or equals
	 *         to the number in message and if he has all the tiles and the
	 *         politics card saved in message, false otherwise
	 */
	public boolean checkSellingOffer(SellingOffersMessage message, Player player) {
		SellingOffers sellingOffer = message.getSellingOffers();
		if (sellingOffer.getAssistantsonSale().getAssistantCrew().getMembersNumber() > player.getAssistantCrew()
				.getMembersNumber()) {
			return false;
		}

		for (BusinessPermitTileonSale tileIterator : sellingOffer.getTilesonSale()) {
			if (!player.getBusinessPermitTilePool().getPermitTiles().contains(tileIterator.getTile())) {
				return false;
			}
		}

		for (PoliticCardonSale cardIterator : sellingOffer.getCardsonSale()) {
			if (!player.getPoliticCardsHand().getHand().contains(cardIterator.getCard())) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Returns a hash code value for this object. It considers map, nobility
	 * track, waiting bonuses, players manager and politic cards deck.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((map == null) ? 0 : map.hashCode());
		result = prime * result + ((nobilityTrack == null) ? 0 : nobilityTrack.hashCode());
		result = prime * result + ((playersManager == null) ? 0 : playersManager.hashCode());
		result = prime * result + ((politicCardsDeck == null) ? 0 : politicCardsDeck.hashCode());
		result = prime * result + ((waitingBonusesPool == null) ? 0 : waitingBonusesPool.hashCode());
		return result;
	}

	/**
	 * Compares two model for equality. The result is <code>true</code> if and
	 * only if the argument is not <code>null</code> and is a <code>Model</code>
	 * object that represents the same map, nobility track, waiting bonuses,
	 * players manager and politic cards deck.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Model other = (Model) obj;
		if (map == null) {
			if (other.map != null)
				return false;
		} else if (!map.equals(other.map))
			return false;
		if (nobilityTrack == null) {
			if (other.nobilityTrack != null)
				return false;
		} else if (!nobilityTrack.equals(other.nobilityTrack))
			return false;
		if (waitingBonusesPool == null) {
			if (other.waitingBonusesPool != null)
				return false;
		} else if (!waitingBonusesPool.equals(other.waitingBonusesPool))
			return false;
		if (playersManager == null) {
			if (other.playersManager != null)
				return false;
		} else if (!playersManager.equals(other.playersManager))
			return false;

		if (politicCardsDeck == null) {
			if (other.politicCardsDeck != null)
				return false;
		} else {
			// if decks contains the same cards (also in different order)
			if (politicCardsDeck.getDeck().size() != other.getPoliticDeck().getDeck().size())
				return false;
			if (!politicCardsDeck.getDeck().containsAll(other.getPoliticDeck().getDeck())) {

				return false;
			} else if (!other.getPoliticDeck().getDeck().containsAll(politicCardsDeck.getDeck())) {
				return false;
			}
		}
		return true;
	}

}
