package it.polimi.ingsw.ps04.model.action.quick;

import it.polimi.ingsw.ps04.model.action.Action;
import it.polimi.ingsw.ps04.model.action.actionVisitor.ActionVisitor;
import it.polimi.ingsw.ps04.model.action.actionVisitor.VisitableAction;
import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.utils.exception.ActionNotCompletedException;

/**
 * this class implements the "EngageAssistant" action of the game.
 */
public class EngageAssistant extends QuickAction {

	private static final long serialVersionUID = -6938003202521889318L;
	public static final int COINS_TO_PAY = 3;
	public static final String DESCRIPTION = "engage an assistant";

	/**
	 * @see Action#execute(Player)
	 */
	@Override
	public void execute(Player player) throws ActionNotCompletedException {
		useQuickAction(player);

		try {
			player.getCoins().payFor(COINS_TO_PAY);
		} catch (IllegalArgumentException e) {
			player.getActionsCounters().undoQuickAction();
			throw new ActionNotCompletedException("You don't have enought coins");
		}
		player.getAssistantCrew().engageAssistants(1);

	}

	/**
	 * @see VisitableAction#accept(ActionVisitor)
	 */
	@Override
	public void accept(ActionVisitor visitor) {
		visitor.visit(this);
	}

	/**
	 * Converts this object to a <code>String</code>
	 */
	@Override
	public String toString() {
		return "engaged 1 assistant";
	}
}
