package it.polimi.ingsw.ps04.model.bonus.bonusVisitor;

import java.util.HashSet;
import java.util.Set;

import it.polimi.ingsw.ps04.model.Model;
import it.polimi.ingsw.ps04.model.board.City;
import it.polimi.ingsw.ps04.model.board.Region;
import it.polimi.ingsw.ps04.model.bonus.complex.BonusfromCityBonus;
import it.polimi.ingsw.ps04.model.bonus.complex.BonusfromTileBonus;
import it.polimi.ingsw.ps04.model.bonus.complex.BusinessPermitTileBonus;
import it.polimi.ingsw.ps04.model.bonus.complex.ComplexBonus;
import it.polimi.ingsw.ps04.model.bonus.simple.NobilityBonus;
import it.polimi.ingsw.ps04.model.bonus.simple.PoliticCardsBonus;
import it.polimi.ingsw.ps04.model.bonus.simple.SimpleBonus;
import it.polimi.ingsw.ps04.model.deck.business.BusinessPermitTile;
import it.polimi.ingsw.ps04.model.deck.business.ChosenTile;
import it.polimi.ingsw.ps04.model.player.Player;

/**
 * The bonus visitor interface. It visits all the activated bonus, managing
 * their behavior.
 */
public interface BonusVisitor {

	/**
	 * By default simple bonus are executed.
	 * 
	 * @param bonus
	 *            The activated simple bonus.
	 * @param player
	 *            The player who activated the bonus.
	 */
	default void visit(SimpleBonus bonus, Player player) {
		bonus.execute(player);
	}

	void visit(ComplexBonus bonus, Player player);

	void visit(PoliticCardsBonus bonus, Player player);

	void visit(NobilityBonus bonus, Player player);

	/**
	 * This method update the cities and model references into a
	 * BonusfromCityBonus.
	 * 
	 * @param bonus
	 *            The bonus to redefine.
	 * @param model
	 *            The local model.
	 */
	default void redefine(BonusfromCityBonus bonus, Model model) {
		bonus.setModel(model);
		Set<City> cities = bonus.getCities();
		Set<City> newCities = new HashSet<City>();
		for (City clientCity : cities) {
			City serverCity = model.getMap().getCityReference(clientCity);
			newCities.add(serverCity);
		}
		bonus.setCities(newCities);
	}

	/**
	 * This method update the tiles and model references into a
	 * BonusfromTileBonus.
	 * 
	 * @param bonus
	 *            The bonus to redefine.
	 * @param model
	 *            The local model.
	 */
	default void redefine(BonusfromTileBonus bonus, Model model) {
		bonus.setModel(model);
		Set<BusinessPermitTile> tiles = bonus.getBusinessTiles();
		Set<BusinessPermitTile> newTiles = new HashSet<BusinessPermitTile>();
		for (BusinessPermitTile clientTile : tiles) {
			BusinessPermitTile serverTile = model.getPlayersManager().getActivePlayer().getBusinessPermitTilePool()
					.getBusinessPermitTileReference(clientTile);
			newTiles.add(serverTile);
		}
		bonus.setBusinessTiles(newTiles);
	}

	/**
	 * This method update the regions and model references into a
	 * BusinessPermitTileBonus.
	 * 
	 * @param bonus
	 *            The bonus to redefine.
	 * @param model
	 *            The local model.
	 */
	default void redefine(BusinessPermitTileBonus bonus, Model model) {
		bonus.setModel(model);
		Set<ChosenTile> chosenTiles = bonus.getChosenTiles();
		Set<ChosenTile> newChosenTiles = new HashSet<>();
		for (ChosenTile tileIterator : chosenTiles) {
			Region serverRegion = model.getMap().getRegionReference(tileIterator.getRegion());
			int numTile = tileIterator.getIndexTile();
			ChosenTile serverTile = new ChosenTile(serverRegion, numTile);
			newChosenTiles.add(serverTile);
		}
		bonus.setChosenTiles(newChosenTiles);
	}

}
