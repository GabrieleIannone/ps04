package it.polimi.ingsw.ps04.model.deck.business;

import java.io.Serializable;
import java.util.Set;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import it.polimi.ingsw.ps04.model.board.City;
import it.polimi.ingsw.ps04.model.bonus.Bonus;
import it.polimi.ingsw.ps04.utils.exception.AlreadyFaceDownException;
import it.polimi.ingsw.ps04.utils.string.StringHelper;

/**
 * This class represents a single business permit tile.
 */
public class BusinessPermitTile implements Serializable {

	private static final long serialVersionUID = -3213581599735160338L;
	private Set<City> cities;
	private Set<Bonus> bonuses;
	private boolean faceUp = true;

	/**
	 * Creates a new business permit tile.
	 * 
	 * @param cities
	 *            The new tile will permit to build in this cities.
	 * @param bonuses
	 *            This bonuses will be activated when a player acquire the tile.
	 */
	public BusinessPermitTile(Set<City> cities, Set<Bonus> bonuses) {
		this.cities = cities;
		this.bonuses = bonuses;
	}

	/**
	 * Turn the tile face down. It is called when the tile is used to build in a
	 * city.
	 * 
	 * @throws AlreadyFaceDownException
	 *             if the tile is already face down.
	 */
	public void turnFaceDown() throws AlreadyFaceDownException {
		if (faceUp)
			faceUp = false;
		else
			throw new AlreadyFaceDownException();
	}

	/**
	 * Turn the tile face up.
	 */
	public void turnFaceUp() {
		faceUp = true;
	}

	/**
	 * Check if there is a city in the set of the tile with a name equals to the
	 * name of the city passed by parameter.
	 * 
	 * @param city
	 *            The city to check.
	 * @return true if the city is found. False otherwise.
	 */
	public boolean isCityIncluded(City city) {
		for (City cityComparable : cities) {
			if (cityComparable.getName().equals(city.getName()))
				return true;
		}
		return false;
	}

	// Getters and Setters
	/**
	 * Returns the bonuses of this tile
	 * 
	 * @return the bonuses of this tile
	 */
	public Set<Bonus> getBonuses() {
		return bonuses;
	}

	/**
	 * says if this tile is face up
	 * 
	 * @return true if the tile is face up
	 */
	public boolean isFaceUp() {
		return faceUp;
	}

	/**
	 * Returns the cities where you can build with this tile
	 * 
	 * @return the cities where you can build with this tile
	 */
	public Set<City> getCities() {
		return cities;
	}

	/**
	 * Returns a hash code value for this object. It considers the bonuses and
	 * the cities.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bonuses == null) ? 0 : bonuses.hashCode());
		result = prime * result + ((cities == null) ? 0 : cities.hashCode());
		return result;
	}

	/**
	 * Compares two tiles for equality. The result is <code>true</code> if and
	 * only if the argument is not <code>null</code> and is a
	 * <code>BusinessPermitTile</code> object that represents the same bonuses
	 * and cities.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BusinessPermitTile other = (BusinessPermitTile) obj;
		if (bonuses == null) {
			if (other.bonuses != null)
				return false;
		} else if (!bonuses.equals(other.bonuses))
			return false;
		if (cities == null) {
			if (other.cities != null)
				return false;
		} else if (cities.size() != other.getCities().size()) {
			return false;
		}
		for (City cityIterator : other.getCities()) {
			if (!isCityIncluded(cityIterator)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Converts this <code>BusinessPermitTile</code> object to a
	 * <code>String</code>
	 */
	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.JSON_STYLE)
				.append("cities", StringHelper.getCitiesNameArray(cities)).append("bonus", bonuses)
				.append("face up", faceUp).toString();
	}

}
