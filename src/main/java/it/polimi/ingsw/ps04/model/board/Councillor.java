package it.polimi.ingsw.ps04.model.board;

import java.io.Serializable;

import it.polimi.ingsw.ps04.utils.color.NamedColor;

/**
 * this class implements the councillors of the game
 */
public class Councillor implements Serializable {
	private static final long serialVersionUID = 8515363070348838250L;
	private NamedColor color;

	/**
	 * constructs a councillor with the given color
	 * 
	 * @param color
	 *            the color that you want to assign to this councillor
	 */
	public Councillor(NamedColor color) {
		this.color = color;
	}

	/**
	 * Returns the color of this councillor
	 * 
	 * @return the color of this councillor
	 */
	public NamedColor getColor() {
		return color;
	}

	/**
	 * Converts this <code>Councillor</code> object to a <code>String</code>
	 */
	@Override
	public String toString() {
		return color.toString();
	}
}
