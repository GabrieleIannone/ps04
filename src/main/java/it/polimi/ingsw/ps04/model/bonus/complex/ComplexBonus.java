package it.polimi.ingsw.ps04.model.bonus.complex;

/**
 * This class represents the abstract simple bonus. A complex bonus is a bonus
 * that needs a choice from the player in order to be be execute.
 * 
 * @see Bonus
 */
import it.polimi.ingsw.ps04.client.view.userinterface.UserInterface;
import it.polimi.ingsw.ps04.model.Model;
import it.polimi.ingsw.ps04.model.bonus.Bonus;
import it.polimi.ingsw.ps04.model.bonus.bonusVisitor.BonusVisitor;
import it.polimi.ingsw.ps04.model.player.Player;

public abstract class ComplexBonus extends Bonus {

	private static final long serialVersionUID = -2932250607635702207L;
	protected Model model;

	/**
	 * Save this bonus in the WaitingBonuses.
	 */
	public void saveWaitingBonus() {
		model.getAvailableBonusesPool().addBonus(this);
	}

	/**
	 * This method executes the bonus activated. It will be implemented by each
	 * complex bonus.
	 */
	public abstract void execute(BonusVisitor visitor);

	public void setModel(Model model) {
		this.model = model;
	}

	/**
	 * @see Bonus#accept(BonusVisitor, Player)
	 */
	@Override
	public void accept(BonusVisitor visitor, Player player) {
		visitor.visit(this, player);
	}

	/**
	 * This method redefines the references of the bonus field. It will be
	 * implemented by each complex bonus.
	 */
	public abstract void redefine(BonusVisitor visitor, Model model);

	/**
	 * Called in the client. It collects the choices of the player who activated
	 * the bonus. It will be implemented by each complex bonus.
	 * 
	 * @param ui
	 *            The user interface chose by the player.
	 */
	public abstract void compileBonus(UserInterface ui);

}
