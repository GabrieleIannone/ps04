package it.polimi.ingsw.ps04.model.board;

import java.io.Serializable;

import it.polimi.ingsw.ps04.model.player.Player;

/**
 * this class implements the emporiums of the game
 */
public class Emporium implements Serializable {
	private static final long serialVersionUID = -8896737676235797405L;
	private Player owner;

	/**
	 * Counstructs an emporium with the given owner
	 * 
	 * @param owner
	 *            the owner of the emporium
	 */
	public Emporium(Player owner) {
		this.owner = owner;
	}

	/**
	 * Returns the owner of the emporium
	 * 
	 * @return the owner of the emporium
	 */
	public Player getOwner() {
		return owner;
	}

	/**
	 * Converts this <code>Emporium</code> object to a <code>String</code>
	 */
	@Override
	public String toString() {
		return "\"" + owner.getName() + "\"";
	}

}
