package it.polimi.ingsw.ps04.model.bonus.bonusVisitor;

import it.polimi.ingsw.ps04.model.bonus.Bonus;
import it.polimi.ingsw.ps04.model.player.Player;

/**
 * Interface for a visitable bonus. Bonus implements this interface.
 * 
 * @see Bonus
 */
public interface VisitableBonus {

	void accept(BonusVisitor visitor, Player player);

}
