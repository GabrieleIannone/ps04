package it.polimi.ingsw.ps04.model.bonus.simple;

import it.polimi.ingsw.ps04.model.player.Player;

/**
 * This bonus is a simple bonus. It gives a number of addictional main action to
 * the player who activates this equals to the bonus increment.
 */
public class AdditionalMainActionBonus extends SimpleBonus {

	private static final long serialVersionUID = 1465459835953063738L;

	/**
	 * Constructor. Set the bonus increment.
	 * 
	 * @param bonusIncrement
	 *            The bonusIncrement of this bonus.
	 */
	public AdditionalMainActionBonus(int bonusIncrement) {
		super.bonusIncrement = bonusIncrement;
	}

	/**
	 * Execute the bonus. Gives to the player a number of addictional main
	 * action equals to the bonus increment.
	 * 
	 * @param player
	 *            The player who activated the bonus.
	 */
	@Override
	public void execute(Player player) {
		int i;
		for (i = 0; i < bonusIncrement; i++) {
			player.getActionsCounters().additionalMainAction();
		}
	}

}
