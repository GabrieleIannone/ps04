package it.polimi.ingsw.ps04.model.nobilitytrack;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import it.polimi.ingsw.ps04.model.bonus.Bonus;
import it.polimi.ingsw.ps04.model.bonus.bonusVisitor.BonusVisitor;
import it.polimi.ingsw.ps04.model.player.Player;

/**
 * this class implements the nobility track of the game
 */
public class NobilityTrack implements Serializable {

	private static final long serialVersionUID = 7539203927747249316L;
	private Map<Integer, Set<Bonus>> track = new HashMap<Integer, Set<Bonus>>();
	private transient BonusVisitor bonusVisitor;

	/**
	 * constructs an empty nobility track
	 */
	public NobilityTrack() {
	}

	/**
	 * constructs a nobility track with a given track
	 * 
	 * @param track
	 *            the track that you want to assign to the nobility track
	 */
	protected NobilityTrack(Map<Integer, Set<Bonus>> track) {
		this.track = track;
	}

	/**
	 * Returns the bonuses of the step passed as parameter
	 * 
	 * @param step
	 *            the step of the nobility track that you want to know the
	 *            bonuses
	 * @return the bonuses of that step
	 */
	private Set<Bonus> getBonuses(int step) {
		return track.get(step);
	}

	/**
	 * Returns the track of the nobility track
	 * 
	 * @return the track of the nobility track
	 */
	public Map<Integer, Set<Bonus>> getTrack() {
		return track;
	}

	/**
	 * gives the bonus to the player specified as parameter
	 * 
	 * @param player
	 *            the player that you want to assign the bonus
	 */
	public void giveBonus(Player player) {
		int step = player.getNobilityLevel().getLevel();
		Set<Bonus> bonuses;
		if (track.containsKey(step)) {
			bonuses = getBonuses(step);
			for (Bonus bonus : bonuses) {
				bonus.accept(bonusVisitor, player);
			}
		}
	}

	/**
	 * Compares two nobility tracks for equality. The result is
	 * <code>true</code> if and only if the argument is not <code>null</code>
	 * and is a <code>NobilityTrack</code> object that has the same track.
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		final NobilityTrack other = (NobilityTrack) obj;
		if (track == null) {
			return false;
		}
		if (!Objects.equals(this.track.keySet(), other.track.keySet())) {
			return false;
		}
		Set<Integer> steps = this.track.keySet();
		for (int step : steps) {
			Set<Bonus> thisBonuses = this.track.get(step);
			Set<Bonus> otherBonuses = other.track.get(step);
			if (!thisBonuses.equals(otherBonuses)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Changes the track of this nobility track with a track of the nobility
	 * track passed as parameter
	 * 
	 * @param nobilityTrack
	 *            the nobility track that you want to take the track
	 */
	public void set(NobilityTrack nobilityTrack) {
		this.track = nobilityTrack.getTrack();
	}

	/**
	 * Changes the bonus visitor of the nobility track to be equal to the
	 * argument bonusvisitor
	 * 
	 * @param bonusVisitor
	 *            the bonus visitor that you want to set
	 */
	public void setBonusvisitor(BonusVisitor bonusVisitor) {
		this.bonusVisitor = bonusVisitor;
	}

	/**
	 * Returns a hash code value for this object. It considers the track.
	 */
	@Override
	public int hashCode() {
		return Objects.hash(track);
	}

	/**
	 * Converts this <code>NobilityTrack</code> object to a <code>String</code>
	 */
	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		result.append("{");
		for (Integer integer : track.keySet()) {
			result.append("\"" + integer + "\":");
			result.append(track.get(integer) + ",");
		}
		result.deleteCharAt(result.length() - 1);
		result.append("}");
		return result.toString();
	}
}
