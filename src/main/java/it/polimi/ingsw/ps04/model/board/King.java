package it.polimi.ingsw.ps04.model.board;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * this class implements the king of the game
 */
public class King implements Serializable {
	private static final long serialVersionUID = -1918209074932860965L;
	private City actualCity;
	private Council council;

	/**
	 * Constructs a king with a given council and that is placed in the city
	 * passed in the argoument capital
	 * 
	 * @param capital
	 *            the city where you want to place the king
	 * @param council
	 *            the council that you want to assign to the king
	 */
	public King(City capital, Council council) {
		setActualCity(capital);
		this.council = council;
	}

	/**
	 * Returns the city where the king is located
	 * 
	 * @return the city where the king is located
	 */
	public City getActualCity() {
		return actualCity;
	}

	/**
	 * Returns the council of the king
	 * 
	 * @return the council of the king
	 */
	public Council getCouncil() {
		return council;
	}

	/**
	 * Moves the king to the city passed as parameter
	 * 
	 * @param actualCity
	 *            the city where you want to place the king
	 */
	public void setActualCity(City actualCity) {
		this.actualCity = actualCity;
	}

	/**
	 * Returns a hash code value for this object. It considers the actual city
	 * of the king.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((actualCity == null) ? 0 : actualCity.hashCode());
		return result;
	}

	/**
	 * Compares two kings for equality. The result is <code>true</code> if and
	 * only if the argument is not <code>null</code> and is a <code>King</code>
	 * object that represents the is placed in the same city.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		King other = (King) obj;
		if (actualCity == null) {
			if (other.actualCity != null)
				return false;
		} else if (!actualCity.equals(other.actualCity))
			return false;
		return true;
	}

	/**
	 * Converts this <code>King</code> object to a <code>String</code>
	 */
	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.JSON_STYLE).append("city", actualCity.getName())
				.append("council", council.getCouncillors()).toString();
	}

}
