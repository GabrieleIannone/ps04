package it.polimi.ingsw.ps04.model.deck.business;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import com.google.common.base.MoreObjects;

import it.polimi.ingsw.ps04.model.bonus.Bonus;
import it.polimi.ingsw.ps04.model.bonus.bonusVisitor.BonusVisitor;
import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.utils.exception.ActionNotCompletedException;

/**
 * This class represents the deck of business permit tiles of each region.
 */
public class BusinessDeck implements Serializable {

	private static final long serialVersionUID = -212850380245568100L;
	private List<BusinessPermitTile> deck;
	private List<BusinessPermitTile> revealedTiles = new ArrayList<>();
	private transient BonusVisitor bonusVisitor;
	private static final int REVEALED_TILES_NUMBER = 2;

	/**
	 * Creates a new business permit tile, passing a list of business permit
	 * tile. Once set the deck, this method shuffles it and calls the method
	 * revealAllTiles()
	 * 
	 * @see BusinessDeck#revealAllTiles()
	 * 
	 * @param deck
	 *            The list of business permit tiles that will be the new dek
	 */
	protected BusinessDeck(List<BusinessPermitTile> deck) {
		this.deck = deck;
		shuffleDeck();
		revealAllTiles();
	}

	/**
	 * Return the revealed tiles of this deck. The revealed tiles are the ones
	 * that a player can acquire with the proper actions or bonus
	 * 
	 * @return the revealed tiles
	 */
	public List<BusinessPermitTile> getRevealedTiles() {
		return revealedTiles;
	}

	/**
	 * Remove the first tile of the deck and adds it to the revealed tiles. If
	 * the deck is empty the exception is catch here doing nothing. In this way
	 * once the deck is finished no more tiles will be revealed.
	 */
	private void revealTile() {
		try {
			BusinessPermitTile tileToReveal = deck.remove(0);
			revealedTiles.add(tileToReveal);
		} catch (IndexOutOfBoundsException e) {
		}
	}

	/**
	 * Called by the constructor in order to reveal a number of tiles equals to
	 * the field REVEALED_TILES_NUMBER.
	 * 
	 * @see BusinessDeck#REVEALED_TILES_NUMBER
	 */
	private void revealAllTiles() {
		int tilesToRevail = deck.size() < REVEALED_TILES_NUMBER ? deck.size() : REVEALED_TILES_NUMBER;
		for (int i = 0; i < tilesToRevail; i++) {
			revealTile();
		}
	}

	/**
	 * Takes a tile and adds it to the player BusinessPermitTilePool. Than
	 * activated each bonus written on the taken tile. *
	 * 
	 * @see it.polimi.ingsw.ps04.model.player.BusinessPermitTilePool#addPermitTile(
	 *      BusinessPermitTile)
	 * 
	 * @see it.polimi.ingsw.ps04.model.bonus.Bonus#accept(BonusVisitor, Player)
	 * 
	 * @param i
	 *            The position of the tile into the revealedTiles list. It will
	 *            decreased by 1, because list index starts with 0.
	 * @param player
	 *            The player who take the tile.
	 * @throws ActionNotCompletedException
	 *             if does not exist a tile in revealedTiles at position i-1
	 */
	public void takeTile(int i, Player player) throws ActionNotCompletedException {
		BusinessPermitTile tileTaken = null;
		try {
			tileTaken = revealedTiles.remove(i - 1);
		} catch (Exception e) {
			throw new ActionNotCompletedException("Business Tile unavailable");
		}
		player.getBusinessPermitTilePool().addPermitTile(tileTaken);
		revealTile();
		Set<Bonus> activatedBonuses = tileTaken.getBonuses();
		for (Bonus bonusIterator : activatedBonuses) {
			bonusIterator.accept(bonusVisitor, player);
		}
	}

	/**
	 * Changes the revealed tiles. Used by the quick action
	 * ChangeBusinessPermitTiles.
	 * 
	 * @see it.polimi.ingsw.ps04.model.action.quick.ChangeBusinessPermitTiles
	 */
	public void changeRevealedTiles() {
		deck.addAll(revealedTiles);
		revealedTiles.clear();
		revealAllTiles();
	}

	/**
	 * Shuffles the deck.
	 */
	private void shuffleDeck() {
		Collections.shuffle(deck);
	}

	// Getters and Setters

	/**
	 * Changes the bonus visitor of the map to be equal to the argument
	 * bonusVisitor
	 * 
	 * @param bonusVisitor
	 *            the bonus visitor that you want to set
	 */
	public void setBonusVisitor(BonusVisitor bonusVisitor) {
		this.bonusVisitor = bonusVisitor;
	}

	/**
	 * Returns a hash code value for this object. It considers the deck and the
	 * revealed tiles.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((deck == null) ? 0 : deck.hashCode());
		result = prime * result + ((revealedTiles == null) ? 0 : revealedTiles.hashCode());
		return result;
	}

	/**
	 * Compares two business decks for equality. The result is <code>true</code>
	 * if and only if the argument is not <code>null</code> and is a
	 * <code>BusinessDeck</code> object that represents the same deck and
	 * revealed tiles.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BusinessDeck other = (BusinessDeck) obj;
		if (deck == null) {
			if (other.deck != null)
				return false;
		} else if (!deck.equals(other.deck))
			return false;
		if (revealedTiles == null) {
			if (other.revealedTiles != null)
				return false;
		} else if (!revealedTiles.equals(other.revealedTiles))
			return false;
		return true;
	}

	/**
	 * Converts this <code>BusinessDeck</code> object to a <code>String</code>
	 */
	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).addValue(revealedTiles).toString();
	}
}
