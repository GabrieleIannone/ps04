package it.polimi.ingsw.ps04.model.player;

import java.io.Serializable;

import it.polimi.ingsw.ps04.utils.exception.ActionNotCompletedException;

/**
 * Represents the pool of unused emporiums of the player
 */
public class UnusedEmporiumPool implements Serializable {

	private static final long serialVersionUID = 4617821028487675324L;
	// if at the beginning of the turn is 0 then the game ends
	private int unusedEmporiums;
	private static final float CITIES_EMPORIUMS_RATIO = (float) 2 / 3;

	/**
	 * Constructs an unused emporium pool from a given number of cities number
	 * 
	 * @param citiesNumber
	 *            the cities number of the map
	 */
	public UnusedEmporiumPool(int citiesNumber) {
		this.unusedEmporiums = Math.round(CITIES_EMPORIUMS_RATIO * citiesNumber);
	}

	/**
	 * Checks if this unused emporium pool is not empty
	 * 
	 * @return true if this unused emporium pool is not empty
	 */
	public boolean hasEmporium() {
		return unusedEmporiums > 0;
	}

	/**
	 * Removes an emporium from this pool
	 * 
	 * @throws ActionNotCompletedException
	 *             if this pool has no emporium
	 */
	public void removeEmporium() throws ActionNotCompletedException {
		if (unusedEmporiums > 0) {
			unusedEmporiums--;
		} else
			throw new ActionNotCompletedException("No emporium left to build");
	}

	/**
	 * Converts this <code>UnusedEmporiumPool</code> object to a
	 * <code>String</code>
	 */
	@Override
	public String toString() {
		return String.valueOf(unusedEmporiums);
	}

	/**
	 * Returns a hash code value for this object. It considers the numbers of
	 * unused emporiums.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + unusedEmporiums;
		return result;
	}

	/**
	 * Compares two unused emporiums pools for equality. The result is
	 * <code>true</code> if and only if the argument is not <code>null</code>
	 * and is a <code>UnusedEmporiumPool</code> object that has the same
	 * emporiums number.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UnusedEmporiumPool other = (UnusedEmporiumPool) obj;
		if (unusedEmporiums != other.unusedEmporiums)
			return false;
		return true;
	}

}
