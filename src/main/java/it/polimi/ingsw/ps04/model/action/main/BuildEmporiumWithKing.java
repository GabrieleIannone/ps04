package it.polimi.ingsw.ps04.model.action.main;

import java.util.List;

import it.polimi.ingsw.ps04.model.action.Action;
import it.polimi.ingsw.ps04.model.action.actionVisitor.ActionVisitor;
import it.polimi.ingsw.ps04.model.action.actionVisitor.VisitableAction;
import it.polimi.ingsw.ps04.model.board.City;
import it.polimi.ingsw.ps04.model.board.map.Map;
import it.polimi.ingsw.ps04.model.deck.politic.PoliticCard;
import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.utils.exception.ActionNotCompletedException;

/**
 * this class implements the "BuildEmporiumWithKing" action of the game. It is
 * used when someone wants to build in a city with the help of the king
 */
public class BuildEmporiumWithKing extends MainAction {

	private static final long serialVersionUID = -6126648635475410487L;
	public static final String DESCRIPTION = "build an emporium in a city with the help of the king";
	private City city;
	private Map map;
	private List<PoliticCard> usedCards;

	/**
	 * constructs the action "BuildEmporiumWithKing"
	 * 
	 * @param city
	 *            the city where you want to build
	 * @param usedCards
	 *            the cards that the player wants to use
	 * 
	 */
	public BuildEmporiumWithKing(City city, List<PoliticCard> usedCards) {
		this.city = city;
		this.usedCards = usedCards;
	}

	/**
	 * @see Action#execute(Player)
	 */
	@Override
	public void execute(Player player) throws ActionNotCompletedException {
		useMainAction(player);
		int cost = 0;
		cost += map.getKing().getCouncil().getCorruptionCost(usedCards);

		if (player.getPoliticCardsHand().containsCards(usedCards)) {
			player.getPoliticCardsHand().useCards(usedCards);
		} else {
			player.getActionsCounters().additionalMainAction();
			throw new ActionNotCompletedException();
		}

		cost += 2 * map.minDistance(map.getKingPosition(), city);
		try {
			player.getCoins().payFor(cost);
		} catch (IllegalArgumentException e) {
			player.getActionsCounters().additionalMainAction();
			player.getPoliticCardsHand().drawCards(usedCards);
			e.printStackTrace();
			throw new ActionNotCompletedException("Not enough coins!");
		}

		try {
			new BuildEmporium(city, map).build(player);
		} catch (ActionNotCompletedException e) {
			player.getCoins().addCoins(cost);
			player.getActionsCounters().additionalMainAction();
			player.getPoliticCardsHand().drawCards(usedCards);
			throw new ActionNotCompletedException(e.getMessage());
		}
		map.moveKing(city);
	}

	// Getters and setters
	/**
	 * Returns the city where you want to build
	 * 
	 * @return the city where you want to build
	 */
	public City getCity() {
		return city;
	}

	/**
	 * Changes the city where you want to build to be equal to the argument city
	 * 
	 * @param city
	 *            the city that you want to set
	 */
	public void setCity(City city) {
		this.city = city;
	}

	/**
	 * Changes the map where you want to build to be equal to the argument tile
	 * 
	 * @param map
	 *            the map that you want to set
	 */
	public void setMap(Map map) {
		this.map = map;
	}

	/**
	 * @see VisitableAction#accept(ActionVisitor)
	 */
	@Override
	public void accept(ActionVisitor visitor) {
		visitor.visit(this);
	}

	/**
	 * Converts this object to a <code>String</code>
	 */
	@Override
	public String toString() {
		return "built in " + city.getName() + " using king";
	}
}
