package it.polimi.ingsw.ps04.model.action.actionVisitor;

/**
 * the interface that must be implemented from an action that has to be visited
 * by the ActionVisitor
 */
public interface VisitableAction {

	/**
	 * accepts the action visitor
	 * 
	 * @param visitor
	 *            the action visitor that you want to accept
	 */
	void accept(ActionVisitor visitor);

}