package it.polimi.ingsw.ps04.model.bonus.complex;

import java.util.HashSet;
import java.util.Set;

import it.polimi.ingsw.ps04.client.view.userinterface.UserInterface;
import it.polimi.ingsw.ps04.model.Model;
import it.polimi.ingsw.ps04.model.board.Region;
import it.polimi.ingsw.ps04.model.bonus.bonusVisitor.BonusVisitor;
import it.polimi.ingsw.ps04.model.deck.business.ChosenTile;

/**
 * This bonus is a complex bonus. It gives a number of politic cards to the
 * player who activates this equals to the bonus increment. chosen by the player
 * who activates this.
 */
public class BusinessPermitTileBonus extends ComplexBonus {

	private static final long serialVersionUID = -984880837331732999L;
	private Set<ChosenTile> chosenTiles = new HashSet<>();

	/**
	 * Constructor. Set the bonus increment and the model.
	 * 
	 * @param bonusIncrementer
	 *            The bonusIncrement of this bonus.
	 * @param model
	 *            The local model. Used to reach the map.
	 */
	public BusinessPermitTileBonus(int businessPermitTileIncrement, Model model) {
		if (businessPermitTileIncrement <= 0) {
			throw new IllegalArgumentException();
		}
		super.bonusIncrement = bonusIncrement;
		super.model = model;
	}

	/**
	 * @return A set of ChosenTile.
	 * 
	 * @see it.polimi.ingsw.ps04.model.deck.business.ChosenTile
	 */
	public Set<ChosenTile> getChosenTiles() {
		return chosenTiles;
	}

	/**
	 * Set the field tilesMap of this bonus.
	 * 
	 * @param chosenTiles
	 *            A set of ChosenTile, that represents the choice made by the
	 *            player.
	 */
	public void setChosenTiles(Set<ChosenTile> chosenTiles) {
		this.chosenTiles = chosenTiles;
	}

	/**
	 * Called in the client. It is used to collect the choices of the player.
	 * 
	 * @see it.polimi.ingsw.ps04.client.view.userinterface.UserInterface#insertRegion()
	 * @see it.polimi.ingsw.ps04.client.view.userinterface.UserInterface#chooseTile()
	 * 
	 * @param ui
	 *            The user interface used by the player who activated the bonus.
	 */
	@Override
	public void compileBonus(UserInterface ui) {
		while (chosenTiles.size() < bonusIncrement) {
			Region region = ui.insertRegion();
			int numTile = ui.chooseTile(region);
			ChosenTile tile = new ChosenTile(region, numTile);
			chosenTiles.add(tile);
		}
		ui.getMessageSender().sendCompiledComplexBonus(this);
	}

	/**
	 * Redefine the reference of regions and model.
	 * 
	 * @see BonusVisitor#redefine(BusinessPermitTileBonus, Model)
	 * 
	 * @param visitor
	 *            The local bonus visitor.
	 * 
	 * @param model
	 *            The local model.
	 */
	@Override
	public void redefine(BonusVisitor visitor, Model model) {
		visitor.redefine(this, model);
	}

	/**
	 * Execute the bonus. For each ChosenTile
	 * 
	 * @param visitor
	 *            The local bonus visitor.
	 */
	@Override
	public void execute(BonusVisitor visitor) {
		for (ChosenTile tileIterator : chosenTiles) {
			tileIterator.getRegion().getBusinessDeck().takeTile(tileIterator.getIndexTile(),
					model.getPlayersManager().getActivePlayer());
		}
	}

}
