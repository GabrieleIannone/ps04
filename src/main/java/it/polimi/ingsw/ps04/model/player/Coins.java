package it.polimi.ingsw.ps04.model.player;

import java.io.Serializable;

/**
 * this class implements the coins of the game
 */
public class Coins implements Serializable {

	private static final long serialVersionUID = -3739490016308628819L;
	private int coinsNumber;

	/**
	 * Constructs a group of coins with a number of coins that is calculated
	 * from the turn of the player
	 * 
	 * @param playerTurnNumber
	 *            the turn of the player
	 */
	public Coins(int playerTurnNumber) {
		// second player gets an extra coin, third gets 2 extra money and so on
		coinsNumber = 6 + (playerTurnNumber - 1);
	}

	/**
	 * Returns the coins number of this group of coins
	 * 
	 * @return the coins number of this group of coins
	 */
	public int getCoinsNumber() {
		return coinsNumber;
	}

	/**
	 * Pays for an number of coins. This number will be subtracted to the
	 * previous number of coins of this group of coins
	 * 
	 * @param coins
	 *            the number of coins that you need to pay
	 * @throws IllegalArgumentException
	 *             if the number of coins of this group of coins is minor to the
	 *             number of coins that you have to pay
	 */
	public void payFor(int coins) throws IllegalArgumentException {
		if (coins >= 0 && coins <= coinsNumber) {
			coinsNumber -= coins;
		} else {
			throw new IllegalArgumentException("not enough coins");
		}
	}

	/**
	 * Adds coins to this group of coins
	 * 
	 * @param coins
	 *            the coins that you want to add to this group of coins
	 */
	public void addCoins(int coins) {
		coinsNumber += coins;
	}

	/**
	 * Converts this <code>Coins</code> object to a <code>String</code>
	 */
	@Override
	public String toString() {
		return String.valueOf(coinsNumber);
	}

	/**
	 * Returns a hash code value for this object. It considers the coins number.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + coinsNumber;
		return result;
	}

	/**
	 * Compares two coins group for equality. The result is <code>true</code> if
	 * and only if the argument is not <code>null</code> and is a
	 * <code>Coins</code> object that contains the same number of coins.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Coins other = (Coins) obj;
		if (coinsNumber != other.coinsNumber)
			return false;
		return true;
	}

}
