package it.polimi.ingsw.ps04.model.board.map;

import org.jgrapht.UndirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;

import it.polimi.ingsw.ps04.model.board.City;
import it.polimi.ingsw.ps04.model.board.Council;
import it.polimi.ingsw.ps04.model.board.King;
import it.polimi.ingsw.ps04.model.board.Region;
import it.polimi.ingsw.ps04.model.bonus.Bonus;
import it.polimi.ingsw.ps04.model.bonus.BonusFactory;
import it.polimi.ingsw.ps04.model.deck.business.BusinessDeckFactory;
import it.polimi.ingsw.ps04.model.deck.politic.PoliticDeck;
import it.polimi.ingsw.ps04.utils.ConfigHelper;
import it.polimi.ingsw.ps04.utils.color.CityColor;
import it.polimi.ingsw.ps04.utils.color.ColorsManager;
import it.polimi.ingsw.ps04.utils.exception.BadConfigFileException;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;
import java.io.IOException;

/**
 * this class build the <code>Map</code> objects from a json configuration file.
 */
public class MapFactory {

	private static final String COLOR = "color";
	private static final String REGION = "region";
	private static final String EDGES = "edges";
	private static final String SOURCE = "source";
	private static final String TARGET = "target";
	private static final String MAPS = "maps";
	private static final String CITY_COLORS = "cityColors";
	private static final String BUSINESS_DECK = "businessDeck";

	private ColorsManager colorsManager;
	private BonusFactory bonusFactory;
	private ConfigHelper configHelper;
	private BusinessDeckFactory businessDeckFactory;
	private PoliticDeck politicCardsDeck;

	/**
	 * constructs a <code>MapFactory</code> object with the given parameters
	 * 
	 * @param colorsManager
	 *            the colors manager that you want to assign to the map factory
	 * @param configHelper
	 *            the configuration helper that you want to assign to the map
	 *            factory
	 * @param bonusFactory
	 *            the bonus factory that you want to assign to the map factory
	 * @param businessDeckFactory
	 *            the business deck factory that you want to assign to the map
	 *            factory
	 * @param politicCardsDeck
	 *            the politic cards deck that you want to assign to the map
	 *            factory
	 */
	public MapFactory(ColorsManager colorsManager, ConfigHelper configHelper, BonusFactory bonusFactory,
			BusinessDeckFactory businessDeckFactory, PoliticDeck politicCardsDeck) {
		this.colorsManager = colorsManager;
		this.configHelper = configHelper;
		this.bonusFactory = bonusFactory;
		this.businessDeckFactory = businessDeckFactory;
		this.politicCardsDeck = politicCardsDeck;
	}

	/**
	 * builds a map from a given <code>JsonNode</code>
	 * 
	 * @param mapNode
	 *            the <code>JsonNode</code> that contains the map that you want
	 *            to build
	 * @return the Map that was contained in the JsonNode with all the bonuses
	 *         already assigned
	 * @throws BadConfigFileException
	 *             if the configuration file contains error
	 * @throws JsonProcessingException
	 *             if there were problems encountered when processing the
	 *             <code>JsonNode</code> that are not pure I/O problems
	 * @throws IOException
	 *             if a problem with the I/O has occurred
	 */
	private Map buildMap(JsonNode mapNode) throws BadConfigFileException, JsonProcessingException, IOException {
		UndirectedGraph<City, DefaultEdge> graph = new SimpleGraph<>(DefaultEdge.class);
		City capital = null;
		List<Region> regions = new ArrayList<>();
		JsonNode citiesNode;
		Region region;
		JsonNode bonusesNode;
		Set<Bonus> regionBonuses;
		colorsManager.setCityColors(mapNode.get(CITY_COLORS));
		for (JsonNode regionNode : mapNode.get(REGION)) {
			String regionName = regionNode.get(ConfigHelper.NAME).textValue();
			bonusesNode = regionNode.get(ConfigHelper.BONUSES);
			regionBonuses = bonusFactory.getBonuses(bonusesNode);
			region = new Region(regionName, new Council(politicCardsDeck), regionBonuses);
			regions.add(region);
			citiesNode = regionNode.get(ConfigHelper.CITIES);
			City city;
			CityColor cityColor;
			for (JsonNode cityNode : citiesNode) {
				String cityName = cityNode.get(ConfigHelper.NAME).textValue();
				try {
					String cityColorName = cityNode.get(COLOR).textValue();
					cityColor = colorsManager.getCityColor(cityColorName);
					city = new City(cityName, region, cityColor);
					region.addCity(city);
					graph.addVertex(city);
				} catch (NullPointerException e) {
					// the city is the capital
					city = new City(cityName, region, null);
					capital = city;
					region.addCity(capital);
					graph.addVertex(capital);
				}
			}
			businessDeckFactory.addBusinessDecktoRegion(regionNode.get(BUSINESS_DECK), region);
		}
		bonusFactory.addBonuses(graph.vertexSet(), capital);

		// now I have to connect the cities
		JsonNode edgesNode = mapNode.get(EDGES);
		for (JsonNode edgeNode : edgesNode) {
			String source = edgeNode.get(SOURCE).asText();
			String target = edgeNode.get(TARGET).asText();
			graph.addEdge(new City(source), new City(target));
		}
		String mapName = mapNode.get(ConfigHelper.NAME).textValue();
		King king = new King(capital, new Council(politicCardsDeck));
		return new Map(graph, king, mapName, regions);
	}

	/**
	 * Returns the <code>JsonNode</code> that contains all the maps
	 * 
	 * @return the <code>JsonNode</code> that contains all the maps
	 * @throws JsonProcessingException
	 *             if there were problems encountered when processing the
	 *             <code>JsonNode</code> that are not pure I/O problems
	 * @throws IOException
	 *             if a problem with the I/O has occurred
	 */
	private JsonNode getMapsJsonNode() throws JsonProcessingException, IOException {
		JsonNode node = configHelper.getRootNode();
		return node.get(MAPS);
	}

	/**
	 * Returns the <code>JsonNode</code> of the map that has the same name of
	 * the one passed as parameter
	 * 
	 * @param mapName
	 *            the name of the map that you want to know the JsonNode
	 * @return the <code>JsonNode</code> of the map that has the same name of
	 *         the one passed as parameter
	 * @throws JsonProcessingException
	 *             if there were problems encountered when processing the
	 *             <code>JsonNode</code> that are not pure I/O problems
	 * @throws IOException
	 *             if a problem with the I/O has occurred
	 */
	private JsonNode getMapJsonNode(String mapName) throws JsonProcessingException, IOException {
		JsonNode maps = getMapsJsonNode();
		for (JsonNode map : maps) {
			if (map.get(ConfigHelper.NAME).asText().equals(mapName)) {
				return map;
			}
		}
		throw new IllegalArgumentException("map not found");
	}

	/**
	 * Imports the Map that has the same name of the one passed as parameter and
	 * that was build from the configuration file
	 * 
	 * @param mapName
	 *            the name of the map that you want to import from the
	 *            configuration file
	 * @return the Map that has the same name of the one passed as parameter and
	 *         that was build from the configuration file
	 * @throws JsonProcessingException
	 *             if there were problems encountered when processing JSON
	 *             content that are not pure I/O problems
	 * @throws IOException
	 *             if a problem with the I/O has occurred
	 * @throws BadConfigFileException
	 *             if the configuration file contains error
	 */
	public Map importMap(String mapName) throws JsonProcessingException, IOException, BadConfigFileException {
		JsonNode mapNode = getMapJsonNode(mapName);
		return buildMap(mapNode);
	}

	/**
	 * Returns the set of string that contains the names of the maps that are
	 * stored in the configuration file
	 * 
	 * @return the set of string that contains the names of the maps that are
	 *         stored in the configuration file
	 * @throws JsonProcessingException
	 *             if there were problems encountered when processing JSON
	 *             content that are not pure I/O problems
	 * @throws IOException
	 *             if a problem with the I/O has occurred
	 */
	public Set<String> getMapsNames() throws JsonProcessingException, IOException {
		JsonNode maps = getMapsJsonNode();
		Set<String> mapsNames = new HashSet<>();
		for (JsonNode map : maps) {
			mapsNames.add(map.get(ConfigHelper.NAME).asText());
		}
		return mapsNames;
	}

	/**
	 * Returns a random JsonNode of a map from the ones stored in the
	 * configuration file
	 * 
	 * @return a random JsonNode of a map from the ones stored in the
	 *         configuration file
	 * @throws JsonProcessingException
	 *             if there were problems encountered when processing JSON
	 *             content that are not pure I/O problems
	 * @throws IOException
	 *             if a problem with the I/O has occurred
	 */
	private JsonNode getRandomMapJsonNode() throws JsonProcessingException, IOException {
		JsonNode maps = getMapsJsonNode();
		int randomNumber = ThreadLocalRandom.current().nextInt(0, maps.size());
		return maps.get(randomNumber);
	}

	/**
	 * Imports a random Map that was build from the maps stored in the
	 * configuration file
	 * 
	 * @return a random Map that was build from the maps stored in the
	 *         configuration file
	 * @throws JsonProcessingException
	 *             if there were problems encountered when processing JSON
	 *             content that are not pure I/O problems
	 * @throws IOException
	 *             if a problem with the I/O has occurred
	 * @throws BadConfigFileException
	 *             if the configuration file contains error
	 */
	public Map importRandomMap() throws JsonProcessingException, IOException, BadConfigFileException {
		JsonNode mapNode = getRandomMapJsonNode();
		return buildMap(mapNode);
	}

}