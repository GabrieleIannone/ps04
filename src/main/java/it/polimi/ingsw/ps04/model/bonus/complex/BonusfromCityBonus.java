package it.polimi.ingsw.ps04.model.bonus.complex;

import java.util.HashSet;
import java.util.Set;

import it.polimi.ingsw.ps04.client.view.userinterface.UserInterface;
import it.polimi.ingsw.ps04.model.Model;
import it.polimi.ingsw.ps04.model.board.City;
import it.polimi.ingsw.ps04.model.bonus.Bonus;
import it.polimi.ingsw.ps04.model.bonus.bonusVisitor.BonusVisitor;
import it.polimi.ingsw.ps04.model.bonus.simple.NobilityBonus;

/**
 * This bonus is a complex bonus. It activates all the bonus from a number of
 * cities equals to the bonus increment, chosen by the player who activates
 * this.
 */
public class BonusfromCityBonus extends ComplexBonus {

	private static final long serialVersionUID = -5397663489634495177L;
	private Set<City> cities = new HashSet<City>();

	/**
	 * Constructor. Set the bonus increment and the model.
	 * 
	 * @param bonusIncrementer
	 *            The bonusIncrement of this bonus.
	 * @param model
	 *            The local model. Used to reach the map.
	 */
	public BonusfromCityBonus(int bonusIncrementer, Model model) {
		super.bonusIncrement = bonusIncrementer;
		super.model = model;
	}

	/**
	 * @return The set of the cities chosen by the player.
	 */
	public Set<City> getCities() {
		return cities;
	}

	/**
	 * Set the field cities of this bonus.
	 * 
	 * @param cities
	 *            The set of the cities chosen by the player who activated the
	 *            bonus.
	 */
	public void setCities(Set<City> cities) {
		this.cities = cities;
	}

	/**
	 * Called in the client. It is used to collect the choices of the player.
	 * 
	 * @see it.polimi.ingsw.ps04.client.view.userinterface.UserInterface#insertCity()
	 * 
	 * @param ui
	 *            The user interface used by the player who activated the bonus.
	 */
	@Override
	public void compileBonus(UserInterface ui) {
		while (cities.size() < bonusIncrement) {
			City city = ui.insertCity();
			cities.add(city);
		}
		ui.getMessageSender().sendCompiledComplexBonus(this);
	}

	/**
	 * Redefine the reference of cities and model.
	 * 
	 * @see BonusVisitor#redefine(BonusfromCityBonus, Model)
	 * 
	 * @param visitor
	 *            The local bonus visitor.
	 * 
	 * @param model
	 *            The local model.
	 */
	@Override
	public void redefine(BonusVisitor visitor, Model model) {
		visitor.redefine(this, model);
	}

	/**
	 * Execute the bonus. For each city chosen by the player it activated all
	 * the bonus of that city, except for the NobilityBonus ones.
	 * 
	 * @param visitor
	 *            The local bonus visitor.
	 */
	@Override
	public void execute(BonusVisitor visitor) {
		for (City cityIterator : cities) {
			Set<Bonus> activatedBonuses = cityIterator.getBonus();
			for (Bonus bonusIterator : activatedBonuses) {
				if (!(bonusIterator instanceof NobilityBonus)) {
					bonusIterator.accept(visitor, model.getPlayersManager().getActivePlayer());
				}
			}
		}
	}

}