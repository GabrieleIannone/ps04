package it.polimi.ingsw.ps04.utils.message.login;

/**
 * a credentials message that is used for the login of a user
 */
public class LoginMessage extends CredentialsMessage {

	private static final long serialVersionUID = -7639195898601649393L;

	/**
	 * Constructs a login message with the given username and password
	 * 
	 * @param username
	 *            the username of the user that wants to do the login
	 * @param password
	 *            the password of the user that wants to do the login
	 */
	public LoginMessage(String username, String password) {
		super(username, password);
	}

}
