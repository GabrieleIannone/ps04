package it.polimi.ingsw.ps04.utils.statistics;

import java.io.Serializable;
import java.time.Duration;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import groovy.json.JsonOutput;

/**
 * this class implements the statistics of the player
 */
public class PlayerStatistics implements Serializable {

	private static final long serialVersionUID = 6970764579991742476L;
	private String player;
	private Integer wonGames;
	private Integer totalGames;
	private Integer gameMinutes;

	/**
	 * Constructs the statistics of the player with the given parameters
	 * 
	 * @param player
	 *            the name of the player
	 * @param wonGames
	 *            the number of games that the player has won
	 * @param totalGames
	 *            the number of games that the player has played
	 * @param gameMinutes
	 *            the number of minutes that the player has played
	 */
	public PlayerStatistics(String player, int wonGames, int totalGames, int gameMinutes) {
		this.player = player;
		this.wonGames = wonGames;
		this.totalGames = totalGames;
		this.gameMinutes = gameMinutes;
	}

	/**
	 * Returns the name of the player
	 * 
	 * @return the name of the player
	 */
	public String getPlayerName() {
		return player;
	}

	/**
	 * Returns the number of games that the player has won
	 * 
	 * @return the number of games that the player has won
	 */
	public Integer getWonGames() {
		return wonGames;
	}

	/**
	 * Returns the number of games that the player has played
	 * 
	 * @return the number of games that the player has played
	 */
	public Integer getPlayedGames() {
		return totalGames;
	}

	/**
	 * Returns the number of minutes that the player has played
	 * 
	 * @return the number of minutes that the player has played
	 */
	public Integer getGameMinutes() {
		return gameMinutes;
	}

	/**
	 * Returns a hash code value for this object. It considers player name, game
	 * minutes, played games and won games.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((gameMinutes == null) ? 0 : gameMinutes.hashCode());
		result = prime * result + ((player == null) ? 0 : player.hashCode());
		result = prime * result + ((totalGames == null) ? 0 : totalGames.hashCode());
		result = prime * result + ((wonGames == null) ? 0 : wonGames.hashCode());
		return result;
	}

	/**
	 * Compares two player statistics for equality. The result is
	 * <code>true</code> if and only if the argument is not <code>null</code>
	 * and is a <code>PlayerStatistics</code> object that represents the same
	 * player name, game minutes, played games and won games.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlayerStatistics other = (PlayerStatistics) obj;
		if (gameMinutes == null) {
			if (other.gameMinutes != null)
				return false;
		} else if (!gameMinutes.equals(other.gameMinutes))
			return false;
		if (player == null) {
			if (other.player != null)
				return false;
		} else if (!player.equals(other.player))
			return false;
		if (totalGames == null) {
			if (other.totalGames != null)
				return false;
		} else if (!totalGames.equals(other.totalGames))
			return false;
		if (wonGames == null) {
			if (other.wonGames != null)
				return false;
		} else if (!wonGames.equals(other.wonGames))
			return false;
		return true;
	}

	/**
	 * Converts this <code>PlayerStatistics</code> object to a
	 * <code>String</code>
	 */
	@Override
	public String toString() {
		long hours = Duration.ofMinutes(gameMinutes).toHours();
		long minutes = Duration.ofMinutes(gameMinutes).toMinutes();
		return JsonOutput.prettyPrint(new ToStringBuilder(this, ToStringStyle.JSON_STYLE).append("Player", player)
				.append("Won games", wonGames).append("total games", totalGames)
				.append("game time", "" + hours + ":" + (minutes - hours * 60)).toString());
	}

}
