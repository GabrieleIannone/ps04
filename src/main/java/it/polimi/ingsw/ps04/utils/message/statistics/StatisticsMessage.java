package it.polimi.ingsw.ps04.utils.message.statistics;

import it.polimi.ingsw.ps04.utils.message.Message;

/**
 * a generic statistics message
 */
public class StatisticsMessage extends Message {

	private static final long serialVersionUID = 8232220476462255839L;
	String requestOwner;

	/**
	 * Changes the request owner of the statistics message to be equal to the
	 * argument requestOwner
	 * 
	 * @param requestOwner
	 *            the owner of the request for the statistics message
	 */
	public void setRequestOwner(String requestOwner) {
		this.requestOwner = requestOwner;
	}

	/**
	 * Returns the owner of the request for the statistics message
	 * 
	 * @return the owner of the request for the statistics message
	 */
	public String getRequestOwner() {
		return requestOwner;
	}

}
