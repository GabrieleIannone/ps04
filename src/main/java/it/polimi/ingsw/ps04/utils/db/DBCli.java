package it.polimi.ingsw.ps04.utils.db;

import asg.cliche.*;

import java.io.IOException;
import java.sql.SQLException;

/**
 * this class let you interact with the db with a minimal cli
 */
public class DBCli {

	DBConnector db = DBConnector.getInstance();

	/**
	 * prints all the instances of the db
	 */
	@Command
	public void printAll() {
		System.out.println(db);
	}

	/**
	 * deletes a user from the db
	 * 
	 * @param nickname
	 *            the nickname of the user that you want to delete
	 * @param password
	 *            the password of the user that you want to delete
	 */
	@Command
	public void delete(String nickname, String password) {
		try {
			db.deleteUser(nickname, password);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * inserts the user into the db
	 * 
	 * @param nickname
	 *            the nickname of the user that you want to insert
	 * @param email
	 *            the email of the user that you want to insert
	 * @param password
	 *            the password of the user that you want to insert
	 */
	@Command
	public void insert(String nickname, String email, String password) {
		try {
			db.insertUser(nickname, email, password);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * checks if a user is contained into the db
	 * 
	 * @param nickname
	 *            the nickname of the user
	 * @param password
	 *            the nickname of the user
	 * @return true if the user exists
	 */
	@Command
	public boolean login(String nickname, String password) {
		return db.checkUser(nickname, password);
	}

	/**
	 * exits from the cli
	 */
	@Command
	public void exit() {
	}

	/**
	 * launches the DBcli
	 * 
	 * @param args
	 * @throws IOException
	 *             Signals that an I/O exception of some sort has occurred
	 */
	public static void main(String[] args) throws IOException {
		ShellFactory.createConsoleShell("DB", "Enter '?list' to list all commands", new DBCli()).commandLoop();
	}
}