package it.polimi.ingsw.ps04.utils.message.statistics;

import it.polimi.ingsw.ps04.utils.statistics.Ranking;

/**
 * the message that contains the ranking of all the games
 */
public class RankingMessage extends StatisticsMessage {

	private static final long serialVersionUID = 1739727356507914729L;
	Ranking ranking;

	/**
	 * Constructs an empty ranking message in order to request it.
	 */
	public RankingMessage() {
	}

	/**
	 * Constructs a ranking message with the given ranking
	 * 
	 * @param ranking
	 *            the ranking of all the games that you want to insert in this
	 *            message
	 */
	public RankingMessage(Ranking ranking) {
		this.ranking = ranking;
	}

	/**
	 * Returns the ranking of all the games contained in this message
	 * 
	 * @return the ranking of all the games contained in this message
	 */
	public Ranking getRanking() {
		return ranking;
	}

	/**
	 * Converts this <code>RankingMessage</code> object to a <code>String</code>
	 */
	@Override
	public String toString() {

		return ranking.toString();
	}
}
