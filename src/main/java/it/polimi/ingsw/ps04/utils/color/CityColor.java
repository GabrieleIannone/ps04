package it.polimi.ingsw.ps04.utils.color;

import java.awt.Color;
import java.util.Set;

import com.google.common.base.MoreObjects.ToStringHelper;

import it.polimi.ingsw.ps04.model.bonus.Bonus;

/**
 * this class represents a color of the city. So in addition to named color it
 * has also the bonus.
 */
public class CityColor extends NamedColor {

	private static final long serialVersionUID = -2762676973689985429L;
	private Set<Bonus> bonus;

	/**
	 * Constructs a city color with the given name, color and bonus
	 * 
	 * @param name
	 *            the name that you want to assign to the city color
	 * @param color
	 *            the color that you want to assign to the city color
	 * @param bonus
	 *            the bonus that you want to assign to the city color
	 */
	public CityColor(String name, Color color, Set<Bonus> bonus) {
		super(name, color);
		this.bonus = bonus;
	}

	/**
	 * Constructs a city color with the given named color and bonus
	 * 
	 * @param color
	 *            the named color that you want to assign to the city color
	 * @param bonus
	 *            the bonus that you want to assign to the city color
	 */
	public CityColor(NamedColor color, Set<Bonus> bonus) {
		super(color.getName(), color.getRed(), color.getGreen(), color.getBlue());
		this.bonus = bonus;
	}

	public Set<Bonus> getBonus() {
		return bonus;
	}

	/**
	 * Returns a hash code value for this object. It considers the bonus.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((bonus == null) ? 0 : bonus.hashCode());
		return result;
	}

	/**
	 * Compares two city colors for equality. The result is <code>true</code> if
	 * and only if the argument is not <code>null</code> and is a
	 * <code>CityColor</code> object that has the same bonus.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CityColor other = (CityColor) obj;
		if (bonus == null) {
			if (other.bonus != null)
				return false;
		} else if (!bonus.equals(other.bonus))
			return false;
		return true;
	}

	/**
	 * Converts this <code>CityColor</code> object to a
	 * <code>ToStringHelper</code>
	 */
	@Override
	protected ToStringHelper toStringHelper() {
		return super.toStringHelper().add("Bonus", bonus);
	}

}
