package it.polimi.ingsw.ps04.utils.exception;

/**
 * Signals that there are no active players
 */
public class NoActivePlayersException extends Exception {
	private static final long serialVersionUID = 1405267092762685678L;

	/**
	 * Constructs the exception with the default message
	 */
	public NoActivePlayersException() {
		super("there are no active players");
	}
}
