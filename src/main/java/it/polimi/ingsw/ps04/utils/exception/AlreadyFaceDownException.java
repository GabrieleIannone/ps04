package it.polimi.ingsw.ps04.utils.exception;

/**
 * Signals that a tile was already face down
 */
public class AlreadyFaceDownException extends Exception {

	private static final long serialVersionUID = 626374670890778031L;

}
