package it.polimi.ingsw.ps04.utils.color;

import java.awt.Color;

import com.google.common.base.MoreObjects;
import com.google.common.base.MoreObjects.ToStringHelper;

import it.polimi.ingsw.ps04.utils.string.StringHelper;

/**
 * this class represents a color with a name. So in addition to color it has
 * also the name.
 */
public class NamedColor extends Color {

	private static final long serialVersionUID = -6950094197621740494L;
	private String name;

	/**
	 * Constructs a named color with the given name and color
	 * 
	 * @param name
	 *            the name that you want to assign to this color
	 * @param color
	 *            the color that you want to assign to this color
	 */
	public NamedColor(String name, Color color) {
		super(color.getRed(), color.getGreen(), color.getBlue());
		this.name = name;
	}

	/**
	 * Constructs a named color with the given name and r,g,b values
	 * 
	 * @param name
	 *            the name that you want to assign to this color
	 * @param r
	 *            the value r of the color that you want to assign to this color
	 * @param g
	 *            the value g of the color that you want to assign to this color
	 * @param b
	 *            the value b of the color that you want to assign to this color
	 */
	public NamedColor(String name, int r, int g, int b) {
		super(r, g, b);
		this.name = name;
	}

	/**
	 * Returns the name of the color
	 * 
	 * @return the name of the color
	 */
	public String getName() {
		return name;
	}

	/**
	 * Returns a hash code value for this object. It considers the name.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/**
	 * Compares two named colors for equality. The result is <code>true</code>
	 * if and only if the argument is not <code>null</code> and is a
	 * <code>NamedColor</code> object that has the same name.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		NamedColor other = (NamedColor) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	/**
	 * Converts this <code>NamedColor</code> object to a
	 * <code>ToStringHelper</code>
	 * 
	 * @return the to string helper that was created
	 */
	protected ToStringHelper toStringHelper() {
		return MoreObjects.toStringHelper(this).addValue(name);
	}

	/**
	 * Converts this <code>NamedColor</code> object to a <code>String</code>
	 */
	@Override
	public String toString() {
		return StringHelper.putQuotes(name);
	}
}
