package it.polimi.ingsw.ps04.utils.exception;

/**
 * Signals that there are no permit tile
 */
public class NoPermitTileException extends RuntimeException {

	private static final long serialVersionUID = -8002418793077850865L;

	/**
	 * Constructs the exception with the default message
	 */
	public NoPermitTileException() {
		this("no permit tile");
	}

	/**
	 * Constructs the exception with the given message
	 * 
	 * @param message
	 *            the message that you want to assign to this exception
	 */
	public NoPermitTileException(String message) {
		super(message);
	}

}
