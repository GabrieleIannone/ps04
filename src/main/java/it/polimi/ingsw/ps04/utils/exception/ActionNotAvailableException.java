package it.polimi.ingsw.ps04.utils.exception;

/**
 * Signals that an action is not available
 */
public class ActionNotAvailableException extends Exception {

	private static final long serialVersionUID = -7888682379857334651L;

	/**
	 * Constructs the exception with the default message
	 */
	public ActionNotAvailableException() {
		super("action not available");
	}

	/**
	 * Constructs the exception with the given message
	 * 
	 * @param message
	 *            the message that you want to assign to this exception
	 */
	public ActionNotAvailableException(String message) {
		super(message);
	}
}
