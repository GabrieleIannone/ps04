package it.polimi.ingsw.ps04.utils.color;

import java.awt.Color;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.base.MoreObjects;

import it.polimi.ingsw.ps04.model.bonus.Bonus;
import it.polimi.ingsw.ps04.model.bonus.BonusFactory;
import it.polimi.ingsw.ps04.utils.ConfigHelper;
import it.polimi.ingsw.ps04.utils.exception.BadConfigFileException;

/**
 * this class implements some utilities to manage the colors
 */
public class ColorsManager {

	public static final String BONUSES = "bonuses";
	public static Color CAPITAL_COLOR = new Color(159, 0, 255); // it's violet
	// it's a green
	public static NamedColor JOKER = new NamedColor("joker", 11, 218, 81);
	private Map<String, Color> aviableColors;
	private Set<NamedColor> corruptionColors;
	private Set<CityColor> cityColors = new HashSet<>();
	private BonusFactory bonusFactory;

	/**
	 * Constructs a colors manager with the given parameters
	 * 
	 * @param aviableColors
	 *            the available colors that you want to assign to the colors
	 *            manager
	 * @param corruptionColors
	 *            the corruprion colors that you want to assign to the colors
	 *            manager
	 * @param bonusFactory
	 *            the bonus factory that you want to assign to the colors
	 *            manager
	 */
	public ColorsManager(Map<String, Color> aviableColors, Set<NamedColor> corruptionColors,
			BonusFactory bonusFactory) {
		this.aviableColors = aviableColors;
		this.corruptionColors = corruptionColors;
		this.bonusFactory = bonusFactory;
	}

	/**
	 * Returns the corruption colors of this colors manager
	 * 
	 * @return the corruption colors of this colors manager
	 */
	public Set<NamedColor> getCorruptionColors() {
		return corruptionColors;
	}

	/**
	 * Finds the color associated with the given name
	 * 
	 * @param name
	 *            the name of the color that you want to find
	 * @return the color associated with the given name
	 */
	public Color getColor(String name) {
		return aviableColors.get(name);
	}

	/**
	 * Finds the city color associated with the given name
	 * 
	 * @param name
	 *            the name of the city color that you want to find
	 * @return
	 */
	public CityColor getCityColor(String name) {
		for (CityColor iterableCityColor : cityColors) {
			if (iterableCityColor.getName().equals(name)) {
				return iterableCityColor;
			}
		}
		throw new IllegalArgumentException();
	}

	/**
	 * Takes the city colors of the given json node and sets them as the city
	 * colors of this colors manager
	 * 
	 * @param cityColorsNode
	 *            the json node that contains the city colors
	 * @throws BadConfigFileException
	 *             if the configuration file contains error
	 */
	public void setCityColors(JsonNode cityColorsNode) throws BadConfigFileException {
		for (JsonNode cityColorNode : cityColorsNode) {
			addCityColor(cityColorNode);
		}
	}

	/**
	 * Adds a city color that is contained in the given json node to this colors
	 * manager
	 * 
	 * @param cityColorNode
	 *            the json node that contains the city color that you want to
	 *            add to this colors manager
	 * @throws BadConfigFileException
	 *             if the configuration file contains error
	 */
	private void addCityColor(JsonNode cityColorNode) throws BadConfigFileException {
		String colorName = cityColorNode.get(ConfigHelper.NAME).textValue();
		Color color = getColor(colorName);
		Set<Bonus> bonuses = bonusFactory.getBonuses(cityColorNode.get(BONUSES));
		addCityColor(new CityColor(colorName, color, bonuses));
	}

	/**
	 * add the given city color to the city colors of this colors manager
	 * 
	 * @param cityColor
	 *            the city color that you want to add to this colors manager
	 */
	public void addCityColor(CityColor cityColor) {
		cityColors.add(cityColor);
	}

	/**
	 * says if the named color in the attribute color is a joker
	 * 
	 * @param color
	 *            the named color that you want to check to see if it is a joker
	 * @return true if the given color is a joker
	 */
	public static boolean isJoker(NamedColor color) {
		return color.equals(JOKER);
	}

	/**
	 * Converts this <code>ColorsManager</code> object to a <code>String</code>
	 */
	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).omitNullValues().add("Aviable Colors", aviableColors)
				.add("Corruption Colors", corruptionColors).add("City Colors", cityColors).toString();
	}

	/**
	 * Returns a hash code value for this object. It considers the available
	 * colors, the city colors and the corruption colors.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((aviableColors == null) ? 0 : aviableColors.hashCode());
		result = prime * result + ((cityColors == null) ? 0 : cityColors.hashCode());
		result = prime * result + ((corruptionColors == null) ? 0 : corruptionColors.hashCode());
		return result;
	}

	/**
	 * Compares two colors managers for equality. The result is
	 * <code>true</code> if and only if the argument is not <code>null</code>
	 * and is a <code>ColorsManager</code> object that has the same available
	 * colors, city colors and corruption colors.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ColorsManager other = (ColorsManager) obj;
		if (aviableColors == null) {
			if (other.aviableColors != null)
				return false;
		} else if (!aviableColors.equals(other.aviableColors))
			return false;
		if (cityColors == null) {
			if (other.cityColors != null)
				return false;
		} else if (!cityColors.equals(other.cityColors))
			return false;
		if (corruptionColors == null) {
			if (other.corruptionColors != null)
				return false;
		} else if (!corruptionColors.equals(other.corruptionColors))
			return false;
		return true;
	}

}