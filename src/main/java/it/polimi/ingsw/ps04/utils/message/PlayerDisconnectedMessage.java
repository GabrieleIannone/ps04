package it.polimi.ingsw.ps04.utils.message;

import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.view.View;

/**
 * the message that is sent when a player was disconnected
 */
public class PlayerDisconnectedMessage extends Message {

	private static final long serialVersionUID = -8160713924624780310L;
	private Player player;
	private View view;

	/**
	 * Constructs a player disconnected message with the player that went
	 * offline
	 * 
	 * @param player
	 *            the player that went offline
	 */
	public PlayerDisconnectedMessage(Player player) {
		this.player = player;
	}

	/**
	 * Returns the player that went offline
	 * 
	 * @return the player that went offline
	 */
	public Player getPlayer() {
		return player;
	}

	/**
	 * Changes the view of this message to be equal to the argument view
	 * 
	 * @param view
	 *            the view of the player that went offline
	 */
	public void setView(View view) {
		this.view = view;
	}

	/**
	 * Returns the view of the player that went offline
	 * 
	 * @return the view of the player that went offline
	 */
	public View getView() {
		return view;
	}

	/**
	 * Converts this <code>PlayerDisconnectedMessage</code> object to a
	 * <code>String</code>
	 */
	@Override
	public String toString() {
		return player.getName() + " disconnected";
	}
}
