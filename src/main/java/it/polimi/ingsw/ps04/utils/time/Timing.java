package it.polimi.ingsw.ps04.utils.time;

/**
 * this interface is implemented by the classes that wants to use a timeout
 */
public interface Timing {

	/**
	 * this method will be called when the time is up
	 */
	void onTimesUp();
}
