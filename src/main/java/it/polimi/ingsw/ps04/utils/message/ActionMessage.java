package it.polimi.ingsw.ps04.utils.message;

import com.google.common.base.MoreObjects;

import it.polimi.ingsw.ps04.model.action.Action;

/**
 * a message that contains an action
 */
public class ActionMessage extends Message {
	private static final long serialVersionUID = -198026608176462590L;
	private Action action;

	/**
	 * Constructs an action message that contains the given action
	 * 
	 * @param action
	 *            the action that you want to insert in this message
	 */
	public ActionMessage(Action action) {
		this.action = action;
	}

	/**
	 * Returns the action that is contained in this action message
	 * 
	 * @return the action that is contained in this action message
	 */
	public Action getAction() {
		return action;
	}

	/**
	 * Converts this <code>ActionMessage</code> object to a <code>String</code>
	 */
	@Override
	public String toString() {
		return MoreObjects.toStringHelper(getClass()).addValue(action).toString();
	}
}
