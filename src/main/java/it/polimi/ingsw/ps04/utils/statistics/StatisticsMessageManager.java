package it.polimi.ingsw.ps04.utils.statistics;

import java.sql.SQLException;

import it.polimi.ingsw.ps04.utils.db.DBConnector;
import it.polimi.ingsw.ps04.utils.message.statistics.PlayerStatisticsMessage;
import it.polimi.ingsw.ps04.utils.message.statistics.RankingMessage;
import it.polimi.ingsw.ps04.utils.message.statistics.StatisticsMessage;

/**
 * this class manages the statistics message.
 */
public class StatisticsMessageManager {

	private StatisticsMessage responseMessage;

	/**
	 * Constructs a statistics message manager with the given parameters and
	 * calculates the response message
	 * 
	 * @param message
	 *            the statistics message that you want to manage
	 * @param requestOwner
	 *            the user who sent the message
	 */
	public StatisticsMessageManager(StatisticsMessage message, String requestOwner) {
		if (message instanceof RankingMessage) {
			try {
				Ranking ranking = new Ranking(DBConnector.getInstance().getUnorderedStatistics());
				responseMessage = new RankingMessage(ranking);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else if (message instanceof PlayerStatisticsMessage) {
			PlayerStatisticsMessage playerStatisticsMessage = (PlayerStatisticsMessage) message;
			try {
				PlayerStatistics statistics = DBConnector.getInstance()
						.getPlayerStatistics(playerStatisticsMessage.getPlayerName());
				responseMessage = new PlayerStatisticsMessage(statistics);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		responseMessage.setRequestOwner(requestOwner);
	}

	/**
	 * Returns the calculated response message
	 * 
	 * @return the calculated response message
	 */
	public StatisticsMessage getResponseMessage() {
		return responseMessage;
	}

}
