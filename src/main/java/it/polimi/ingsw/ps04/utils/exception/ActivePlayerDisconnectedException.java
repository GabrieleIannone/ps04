package it.polimi.ingsw.ps04.utils.exception;

/**
 * Signals that the active player was disconnected
 */
public class ActivePlayerDisconnectedException extends Exception {
	private static final long serialVersionUID = -4202643311059571277L;

	/**
	 * Constructs the exception with the default message
	 */
	public ActivePlayerDisconnectedException() {
		super("active player disconnected");
	}
}
