package it.polimi.ingsw.ps04.utils.message;

import java.util.List;

import it.polimi.ingsw.ps04.model.player.Player;

/**
 * the message that is sent when the game is finished
 */
public class GameFinishedMessage extends Message {

	private static final long serialVersionUID = -1236764284857088820L;
	List<Player> winningPlayers;

	/**
	 * Constructs a game finished message with the given players
	 * 
	 * @param winningPlayers
	 *            the players that won the game
	 */
	public GameFinishedMessage(List<Player> winningPlayers) {
		this.winningPlayers = winningPlayers;
	}

}
