package it.polimi.ingsw.ps04.utils.message.login;

import com.google.common.base.MoreObjects;
import com.google.common.base.MoreObjects.ToStringHelper;

import it.polimi.ingsw.ps04.utils.message.Message;

/**
 * a message that is used for the login functions
 */
public class CredentialsMessage extends Message {

	private static final long serialVersionUID = 5587151077113570603L;
	private String username;
	private String password;

	/**
	 * Constructs a credentials message with the given username and password
	 * 
	 * @param username
	 *            the username of the user
	 * @param password
	 *            the password of the user
	 */
	public CredentialsMessage(String username, String password) {
		this.username = username;
		this.password = password;
	}

	/**
	 * Returns the username of the user
	 * 
	 * @return the username of the user
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Returns the password of the user
	 * 
	 * @return the password of the user
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Converts this <code>CredentialsMessage</code> object to a
	 * <code>ToStringHelper</code>
	 * 
	 * @return the to string helper that was created
	 */
	public ToStringHelper getToStringHelper() {
		return MoreObjects.toStringHelper(this).addValue(username).addValue(password);
	}

	/**
	 * Converts this <code>CredentialsMessage</code> object to a
	 * <code>String</code>
	 */
	@Override
	public String toString() {
		return getToStringHelper().toString();
	}
}
