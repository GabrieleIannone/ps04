package it.polimi.ingsw.ps04.utils.message.market;

import it.polimi.ingsw.ps04.model.market.Market;
import it.polimi.ingsw.ps04.utils.message.Message;

/**
 * A message that sends the entire market.
 */
public class UpdateMarketMessage extends Message {

	private static final long serialVersionUID = -7230079010857095187L;
	private Market market;

	/**
	 * Constructor.
	 * 
	 * @param market
	 *            The market to send.
	 */
	public UpdateMarketMessage(Market market) {
		this.market = market;
	}

	/**
	 * @return The market sent.
	 */
	public Market getMarket() {
		return market;
	}

}
