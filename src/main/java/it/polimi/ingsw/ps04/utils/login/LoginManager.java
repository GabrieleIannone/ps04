package it.polimi.ingsw.ps04.utils.login;

import java.sql.SQLException;
import java.util.Collection;

import javax.security.auth.login.FailedLoginException;
import javax.security.auth.login.LoginException;

import com.google.common.collect.HashMultimap;

import it.polimi.ingsw.ps04.utils.db.DBConnector;
import it.polimi.ingsw.ps04.utils.message.login.CredentialsMessage;
import it.polimi.ingsw.ps04.utils.message.login.DeleteUserMessage;
import it.polimi.ingsw.ps04.utils.message.login.LoginMessage;
import it.polimi.ingsw.ps04.utils.message.login.SignupMessage;

/**
 * this class implements some utilities to manage the login of the user
 */
public class LoginManager {

	private static LoginManager instance = null;
	private DBConnector dbc = DBConnector.getInstance();
	protected HashMultimap<Integer, String> onlineUsers = HashMultimap.create();

	/**
	 * Constructs the login manager. It is private because of the singleton
	 * pattern.
	 */
	private LoginManager() {
	}

	/**
	 * Returns the instance of the login manager. If it does not exist it will
	 * be created.
	 * 
	 * @return the instance of the login manager.
	 */
	public static LoginManager getInstance() {
		if (instance == null) {
			instance = new LoginManager();

		}
		return instance;
	}

	/**
	 * Returns the users that logged in
	 * 
	 * @return the users that logged in
	 */
	public Collection<String> getOnlineUsers() {
		return onlineUsers.values();
	}

	/**
	 * Executes a login message
	 * 
	 * @param loginMessage
	 *            the login message that you want to execute
	 * @return true if the user logged in
	 * @throws FailedLoginException
	 *             if the login was not successful
	 */
	private boolean executeLoginMessage(LoginMessage loginMessage) throws FailedLoginException {
		return login(loginMessage.getUsername(), loginMessage.getPassword());
	}

	/**
	 * Executes a login
	 * 
	 * @param nickname
	 *            the nickname of the player
	 * @param password
	 *            the password of the player
	 * @return true if the user logged in
	 * @throws FailedLoginException
	 *             if the login was not successful
	 */
	private boolean login(String nickname, String password) throws FailedLoginException {
		Boolean result = false;
		Boolean credentialsCorrect = dbc.checkUser(nickname, password);
		if (credentialsCorrect) { // the user exists in DB
			synchronized (onlineUsers) {
				if (!onlineUsers.containsEntry(nickname.hashCode(), nickname)) {
					result = onlineUsers.put(nickname.hashCode(), nickname);
					return result;
				} else {
					throw new FailedLoginException("you are already logged in");
				}
			}
		} else {
			throw new FailedLoginException("wrong nickname and password"); // wrong
																			// nick-password
		}

	}

	/**
	 * Executes a logout
	 * 
	 * @param nickname
	 *            the nickname of the player
	 * @param password
	 *            the password of the player
	 * @throws LoginException
	 *             if the credentials were wrong
	 */
	protected void logout(String nickname, String password) throws LoginException {
		if (dbc.checkUser(nickname, password)) {
			synchronized (onlineUsers) {
				onlineUsers.remove(nickname.hashCode(), nickname);
			}
			System.out.println(nickname + " logged out");
		} else
			throw new LoginException("logout failed, bad credentials");
	}

	/**
	 * Executes a delete user message
	 * 
	 * @param deleteUserMessage
	 *            the delete user message that you want to execute
	 * @return true if the user was correctly deleted from the database
	 * @throws LoginException
	 *             if the credentials were wrong
	 * @throws SQLException
	 *             if there was a problem with the database
	 */
	private boolean executeDeleteMessage(DeleteUserMessage deleteUserMessage) throws LoginException, SQLException {
		return deleteUser(deleteUserMessage.getUsername(), deleteUserMessage.getPassword());
	}

	/**
	 * Deletes a user
	 * 
	 * @param nickname
	 *            the nickname of the player
	 * @param password
	 *            the password of the player
	 * @return true if the user was correctly deleted from the database
	 * @throws LoginException
	 *             if the credentials were wrong
	 * @throws SQLException
	 *             if there was a problem with the database
	 */
	private boolean deleteUser(String nickname, String password) throws LoginException, SQLException {
		if (dbc.deleteUser(nickname, password)) {
			synchronized (onlineUsers) {
				onlineUsers.remove(nickname.hashCode(), nickname);
			}
			System.out.println(nickname + " deleted");
			return true;
		} else
			return false;
	}

	/**
	 * Executes a sign up message
	 * 
	 * @param signupMessage
	 *            the sign up message that you want to execute
	 * @return true if the sign up was successful
	 * @throws LoginException
	 *             if the sign up was not successful
	 * @throws SQLException
	 *             if there was a problem with the database
	 */
	private boolean executeSignupMessage(SignupMessage signupMessage) throws LoginException, SQLException {
		return signUp(signupMessage.getUsername(), signupMessage.getPassword(), signupMessage.getEmail());
	}

	/**
	 * registers a user in the DB
	 * 
	 * @param nickname
	 *            the nickname of the player
	 * @param password
	 *            the password of the player
	 * @param email
	 *            the email of the player
	 * @return true if the user was correctly saved in the DB
	 * @throws LoginException
	 *             if the sign up was not successful
	 * @throws SQLException
	 *             if there was a problem with the database
	 */
	private boolean signUp(String nickname, String password, String email) throws LoginException, SQLException {
		if (dbc.insertUser(nickname, password, email)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * executes a credentials message
	 * 
	 * @param credentialsMessage
	 *            the credentials message that you want to execute
	 * @return true if the operation was successful
	 * @throws LoginException
	 *             if something in the operation went wrong
	 */
	public boolean executeCredentialsMessage(CredentialsMessage credentialsMessage) throws LoginException {
		if (credentialsMessage instanceof LoginMessage) {
			return executeLoginMessage((LoginMessage) credentialsMessage);
		} else if (credentialsMessage instanceof SignupMessage) {
			try {
				return executeSignupMessage((SignupMessage) credentialsMessage);
			} catch (SQLException e) {
				throw new LoginException(e.getMessage());
			}
		} else if (credentialsMessage instanceof DeleteUserMessage) {
			try {
				return executeDeleteMessage((DeleteUserMessage) credentialsMessage);
			} catch (SQLException e) {
				throw new LoginException(e.getMessage());
			}
		}
		return false;
	}

	/**
	 * every user is disconnected
	 */
	public void logoutAll() {
		onlineUsers.clear();
	}

	/**
	 * eliminate a user from the online users
	 * 
	 * @param username
	 *            the username of the user that you want to disconnect
	 */
	public void disconnectUser(String username) {
		onlineUsers.remove(username.hashCode(), username);
	}
}