package it.polimi.ingsw.ps04.utils.message.market;

/**
 * This message is used to inform the user interface that the selling phase
 * started.
 */
public class SellingPhaseMessage extends MarketMessage {

	private static final long serialVersionUID = -1209975866875322650L;

}
