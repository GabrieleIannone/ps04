package it.polimi.ingsw.ps04.utils.message;

/**
 * The message that is sent when you want to receive the model
 */
public class ModelRequestMessage extends Message {

	private static final long serialVersionUID = -578123726571724744L;

}
