package it.polimi.ingsw.ps04.utils.exception;

/**
 * Signals that a player had already built in that city
 */
public class PlayerHasAlreadyBuiltException extends Exception {

	private static final long serialVersionUID = 3710164011371668805L;

	/**
	 * Constructs the exception with the default message
	 */
	public PlayerHasAlreadyBuiltException() {
		super("player has already built in this city");
	}

	/**
	 * Constructs the exception with the given message
	 * 
	 * @param message
	 *            the message that you want to assign to this exception
	 */
	public PlayerHasAlreadyBuiltException(String message) {
		super(message);
	}
}
