package it.polimi.ingsw.ps04.utils.log;

import java.io.File;
import java.io.IOException;
import java.util.logging.FileHandler;

/**
 * this class contains all the handlers of the loggers of the system.
 */
public class Handlers {

	private static Handlers instance = null;
	public FileHandler SERVER_HANDLER;

	/**
	 * Constructs the Handlers object with all the handlers.
	 */
	private Handlers() {
		try {
			SERVER_HANDLER = new FileHandler(
					"src" + File.separator + "main" + File.separator + "log" + File.separator + "Server.log", false);
		} catch (SecurityException | IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Returns the instance of the Handlers class
	 * 
	 * @return the instance of the Handlers class
	 */
	public static Handlers getInstance() {
		if (instance == null) {
			instance = new Handlers();
		}
		return instance;
	}
}
