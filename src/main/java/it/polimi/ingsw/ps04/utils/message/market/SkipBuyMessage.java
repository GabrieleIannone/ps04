package it.polimi.ingsw.ps04.utils.message.market;

/**
 * An empty message. This message is sent if a player want buy nothing.
 */
public class SkipBuyMessage extends MarketMessage {

	private static final long serialVersionUID = 4482816036559425822L;
	
	/**
	 * Constructor.
	 */
	public SkipBuyMessage() {
	}

}
