package it.polimi.ingsw.ps04.utils.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.ps04.utils.statistics.PlayerStatistics;

/**
 * this class implements some methods to easily access to the database
 */
public class DBConnector {

	private static final String USERS_TABLE = "users";
	private static final String NICKNAME = "nickname";
	private static final String PASSWORD = "password";
	private static final String EMAIL = "email";
	private static final String PLAYED_GAMES = "played_games";
	private static final String WON_GAMES = "won_games";
	private static final String GAME_MINUTES = "game_minutes";
	private static final String DATABASE_FILEPATH = "src/main/db/database";
	private Connection conn;

	private static final String CREATE_TABLE_QUERY = "CREATE TABLE users ( nickname varchar(30) NOT NULL, email varchar(30) NOT NULL , password varchar(30) NOT NULL, played_games INTEGER DEFAULT '0',won_games INTEGER DEFAULT '0', game_minutes INTEGER DEFAULT '0')";
	private static final String PRIMARY_KEY_QUERY = "ALTER TABLE users ADD PRIMARY KEY (nickname)";
	private static final String UNIQUE_QUERY = "ALTER TABLE users ADD UNIQUE (email)";

	private static DBConnector instance = null;

	/**
	 * Constructs a DBConnector. It loads the jdbcDriver, does the connection to
	 * the database. If it does not exists it creates it.
	 */
	private DBConnector() {
		loadDriver("org.hsqldb.jdbcDriver");
		try {
			conn = DriverManager.getConnection("jdbc:hsqldb:file:" + DATABASE_FILEPATH + ";shutdown=true", "SA", "");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			update(CREATE_TABLE_QUERY);
			update(PRIMARY_KEY_QUERY);
			update(UNIQUE_QUERY);
		} catch (SQLException e) {
			// e.printStackTrace();
		}

	}

	/**
	 * undo the changes that were made before the last commit
	 * 
	 * @throws SQLException
	 *             if there were problem with the sql
	 */
	public void rollback() throws SQLException {
		conn.rollback();
	}

	/**
	 * set autocommit to the value of the attribute bool
	 * 
	 * @param bool
	 *            true if you want to enable autocommit
	 * @throws SQLException
	 *             if there were problem with the sql
	 */
	public void setAutocommit(boolean bool) throws SQLException {
		conn.setAutoCommit(bool);
	}

	/**
	 * execute an update in the database
	 * 
	 * @param expression
	 *            the expression that you want to execute
	 * @throws SQLException
	 *             if there were problem with the sql
	 */
	public synchronized void update(String expression) throws SQLException {
		Statement st = null;
		st = conn.createStatement(); // statements
		int i = st.executeUpdate(expression); // run the query
		if (i == -1) {
			System.err.println("db error : " + expression);
		}
		st.close();
	}

	/**
	 * db writes out to files and performs clean shuts down otherwise there will
	 * be an unclean shutdown when program ends
	 * 
	 * @throws SQLException
	 *             if there were problem with the sql
	 */
	public void shutdown() throws SQLException {
		Statement st = conn.createStatement();
		st.execute("SHUTDOWN");
		st.close();
		conn.close();
	}

	/**
	 * Returns the instance of the DBConnectior
	 * 
	 * @return the instance of the DBConnectior
	 */
	public static DBConnector getInstance() {
		if (instance == null) {
			instance = new DBConnector();
		}
		return instance;
	}

	/**
	 * loads the driver specified in the driver attribute
	 * 
	 * @param driver
	 *            the driver that you want to load
	 */
	private static void loadDriver(String driver) {
		try {
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			System.err.println("MySQL JDBC Driver abstent");
			e.printStackTrace();
			return;
		}
	}

	/**
	 * adapts the string in the attribute s in order to be stored correctly in
	 * the database
	 * 
	 * @param s
	 *            the string that you want to adapt
	 * @return the adapted string
	 */
	private static String adaptString(String s) {
		return s.replace("\\", "\\\\").replaceAll("\'", "\'\'");
	}

	/**
	 * tells if this user exists in the database
	 * 
	 * @param nickname
	 *            the nickname of the user
	 * @param password
	 *            the password of the user
	 * @return true if the user is registered
	 */
	public boolean checkUser(String nickname, String password) {
		nickname = adaptString(nickname);
		password = adaptString(password);
		Statement stmt;
		Boolean successful = false;
		try {
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM " + USERS_TABLE + " WHERE " + NICKNAME + " LIKE '"
					+ nickname + "' AND " + PASSWORD + " LIKE '" + password + "'");
			if (rs.next()) { // if a correspondence was found
				successful = true;
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return successful;
	}

	/**
	 * inserts a user in the database
	 * 
	 * @param nickname
	 *            the nickname of the user
	 * @param password
	 *            the password of the user
	 * @param email
	 *            the email of the user
	 * @return true if the user was inserted correctly
	 * @throws SQLException
	 *             if there were problem with the sql
	 */
	public boolean insertUser(String nickname, String password, String email) throws SQLException {
		nickname = adaptString(nickname);
		password = adaptString(password);
		email = adaptString(email);

		// Statement stmt = connection.createStatement();
		String query = "INSERT INTO " + USERS_TABLE + " (" + NICKNAME + ", " + EMAIL + ", " + PASSWORD + ") VALUES ('"
				+ nickname + "', '" + email + "', ?)";
		PreparedStatement pstmt = conn.prepareStatement(query);
		pstmt.setString(1, password);
		int successful = pstmt.executeUpdate();
		pstmt.close();
		if (successful == 1) {
			return true;
		} else
			return false;
	}

	/**
	 * deletes a user from the database
	 * 
	 * @param nickname
	 *            the nickname of the user
	 * @param password
	 *            the password of the user
	 * @return true if the user was correctly eliminated from the database
	 * @throws SQLException
	 *             if there were problem with the sql
	 */
	public boolean deleteUser(String nickname, String password) throws SQLException {
		nickname = adaptString(nickname);
		password = adaptString(password);

		Statement stmt = conn.createStatement();
		int successful = stmt.executeUpdate("DELETE FROM " + USERS_TABLE + " WHERE " + NICKNAME + " = '" + nickname
				+ "' AND  " + PASSWORD + " = '" + password + "'");
		stmt.close();
		if (successful == 1)
			return true;
		else
			return false;
	}

	public void deleteAllUsers() throws SQLException {
		Statement stmt = conn.createStatement();
		stmt.executeUpdate("DELETE FROM " + USERS_TABLE);
		stmt.close();
	}

	/**
	 * increments a field in the user table. The row is identified by the
	 * nickname, while the field that you want to increase is the one specified
	 * in the field attribute.
	 * 
	 * @param nickname
	 *            the nickname of the user that you want to increment a field
	 * @param field
	 *            the field that you want to increment
	 * @param increment
	 *            the increment of the field
	 * @throws SQLException
	 *             if there were problem with the sql
	 */
	public void incrementField(String nickname, String field, int increment) throws SQLException {
		nickname = adaptString(nickname);
		Statement stmt = conn.createStatement();
		stmt.executeUpdate("UPDATE " + USERS_TABLE + " SET " + field + " = " + field + " + " + increment + " WHERE "
				+ NICKNAME + " = '" + nickname + "'");
		stmt.close();
	}

	/**
	 * increments of '1' a field in the user table. The row is identified by the
	 * nickname, while the field that you want to increase is the one specified
	 * in the field attribute.
	 * 
	 * @param nickname
	 *            the nickname of the user that you want to increment a field
	 * @param field
	 *            the field that you want to increment
	 * @throws SQLException
	 *             if there were problem with the sql
	 */
	private void incrementField(String nickname, String field) throws SQLException {
		incrementField(nickname, field, 1);
	}

	/**
	 * increments the played games of a player
	 * 
	 * @param nickname
	 *            the player that you want to increment the played games
	 * @throws SQLException
	 *             if there were problem with the sql
	 */
	public void incrementPlayedGames(String nickname) throws SQLException {
		incrementField(nickname, PLAYED_GAMES);
	}

	/**
	 * increments played games of a list of players
	 * 
	 * @param nicknames
	 *            the nicknames of the player that you want to increment the
	 *            played games
	 * @throws SQLException
	 *             if there were problem with the sql
	 */
	public void incrementPlayedGames(List<String> nicknames) throws SQLException {
		for (String name : nicknames) {
			incrementPlayedGames(name);
		}
	}

	/**
	 * increments won games of a player
	 * 
	 * @param nickname
	 *            the player that you want to increment the won games
	 * @throws SQLException
	 *             if there were problem with the sql
	 */
	public void incrementWonGames(String nickname) throws SQLException {
		incrementField(nickname, WON_GAMES);
	}

	/**
	 * increment game minutes of a player
	 * 
	 * @param nickname
	 *            the player that you want to increment the game minutes
	 * @param minutes
	 *            the minutes that you want to add
	 * @throws SQLException
	 *             if there were problem with the sql
	 */
	public void incrementGameMinutes(String nickname, int minutes) throws SQLException {
		incrementField(nickname, GAME_MINUTES, minutes);
	}

	/**
	 * Returns the statistics of a player
	 * 
	 * @param nickname
	 *            the nickname of the player that you want to get the statistics
	 * @return the statistics of a player
	 * @throws SQLException
	 *             if there were problem with the sql
	 */
	public PlayerStatistics getPlayerStatistics(String nickname) throws SQLException {
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery("SELECT " + NICKNAME + ",played_games,won_games,game_minutes FROM users WHERE "
				+ NICKNAME + " = '" + nickname + "'");
		PlayerStatistics stats;
		if (rs.next()) {
			stats = new PlayerStatistics(rs.getString(NICKNAME), rs.getInt("won_games"), rs.getInt("played_games"),
					rs.getInt("game_minutes"));
		} else
			stats = null;
		rs.close();
		stmt.close();
		return stats;
	}

	/**
	 * Returns the statistics of the players without a particular order
	 * 
	 * @return the statistics of the players without a particular order
	 * @throws SQLException
	 *             if there were problem with the sql
	 */
	public List<PlayerStatistics> getUnorderedStatistics() throws SQLException {
		List<PlayerStatistics> statistics = new ArrayList<>();
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery("SELECT " + NICKNAME + ",played_games,won_games,game_minutes FROM users");
		while (rs.next()) {
			statistics.add(new PlayerStatistics(rs.getString(NICKNAME), rs.getInt("won_games"),
					rs.getInt("played_games"), rs.getInt("game_minutes")));
		}
		rs.close();
		stmt.close();
		return statistics;
	}

	/**
	 * returns a string that represents to the entire dataset
	 */
	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		try (Statement stmt = conn.createStatement()) {
			try (ResultSet rs = stmt.executeQuery("SELECT * FROM users")) {
				ResultSetMetaData rsmd = rs.getMetaData();
				int columnsNumber = rsmd.getColumnCount();
				while (rs.next()) {
					for (int i = 1; i <= columnsNumber; i++) {
						if (i > 1)
							result.append(",  ");
						String columnValue = rs.getString(i);
						result.append(columnValue + " " + rsmd.getColumnName(i));
					}
					result.append(System.getProperty("line.separator"));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result.toString();
	}
}
