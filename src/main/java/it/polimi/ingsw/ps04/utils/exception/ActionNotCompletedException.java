package it.polimi.ingsw.ps04.utils.exception;

/**
 * Signals that an action was not completed
 */
public class ActionNotCompletedException extends RuntimeException {

	private static final long serialVersionUID = -8305626555278502079L;

	/**
	 * Constructs the exception with the default message
	 */
	public ActionNotCompletedException() {
		super("action not completed");
	}

	/**
	 * Constructs the exception with the given message
	 * 
	 * @param message
	 *            the message that you want to assign to this exception
	 */
	public ActionNotCompletedException(String message) {
		super(message);
	}
}
