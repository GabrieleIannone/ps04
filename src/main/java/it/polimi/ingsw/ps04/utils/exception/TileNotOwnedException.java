package it.polimi.ingsw.ps04.utils.exception;

/**
 * Signals that the player is trying to use a tile that is not his property
 */
public class TileNotOwnedException extends RuntimeException {

	private static final long serialVersionUID = 820338454725567350L;

}
