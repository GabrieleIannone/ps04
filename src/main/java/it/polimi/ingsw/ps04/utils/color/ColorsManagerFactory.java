package it.polimi.ingsw.ps04.utils.color;

import java.awt.Color;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;

import it.polimi.ingsw.ps04.model.bonus.BonusFactory;
import it.polimi.ingsw.ps04.utils.ConfigHelper;

/**
 * 
 * this class build the <code>ColorsManager</code> objects from a json
 * configuration file.
 */
public class ColorsManagerFactory {

	private static final String COLORS = "colors";
	private static final String R = "r";
	private static final String G = "g";
	private static final String B = "b";
	private static final String CORRUPTION_COLORS_CONFIGURATIONS = "corruptionColorsConfigurations";
	private static final String CORRUPTION_COLORS = "corruptionColors";

	ConfigHelper configHelper;
	BonusFactory bonusFactory;

	/*
	 * this structure is used to associate the name of the color with its r g b
	 * values
	 */
	private Map<String, Color> availableColors = new HashMap<>();
	private Set<NamedColor> corruptionColors = new HashSet<>();

	/**
	 * Constructs a colors manager factory with the given parameters
	 * 
	 * @param configHelper
	 *            the configuration helper that you want to assign to the colors
	 *            manager factory
	 * @param bonusFactory
	 *            the bonus factory that you want to assign to the colors
	 *            manager factory
	 * @throws JsonProcessingException
	 *             if there were problems encountered when processing JSON
	 *             content that are not pure I/O problems
	 * @throws IOException
	 *             if a problem with the I/O has occurred
	 */
	public ColorsManagerFactory(ConfigHelper configHelper, BonusFactory bonusFactory)
			throws JsonProcessingException, IOException {
		setColorsManager(configHelper, bonusFactory);
		importCorruptionColorsConfiguration();
	}

	/**
	 * Constructs a colors manager factory with the given parameters
	 * 
	 * @param configHelper
	 *            the configuration helper that you want to assign to the colors
	 *            manager factory
	 * @param bonusFactory
	 *            the bonus factory that you want to assign to the colors
	 *            manager factory
	 * @param corruptionColorsId
	 *            the id of the corruption colors configuration of the file
	 * @throws JsonProcessingException
	 *             if there were problems encountered when processing JSON
	 *             content that are not pure I/O problems
	 * @throws IOException
	 *             if a problem with the I/O has occurred
	 */
	public ColorsManagerFactory(ConfigHelper configHelper, BonusFactory bonusFactory, String corruptionColorsId)
			throws JsonProcessingException, IOException {
		setColorsManager(configHelper, bonusFactory);
		importCorruptionColorsConfiguration(corruptionColorsId);
	}

	/**
	 * sets some parameters of the colors manager
	 * 
	 * @param configHelper
	 *            the configuration helper that you want to assign to the colors
	 *            manager factory
	 * @param bonusFactory
	 *            the bonus factory that you want to assign to the colors
	 *            manager factory
	 * @throws JsonProcessingException
	 *             if there were problems encountered when processing JSON
	 *             content that are not pure I/O problems
	 * @throws IOException
	 *             if a problem with the I/O has occurred
	 */
	private void setColorsManager(ConfigHelper configHelper, BonusFactory bonusFactory)
			throws JsonProcessingException, IOException {
		this.configHelper = configHelper;
		importAvailableColors();
		this.bonusFactory = bonusFactory;
	}

	/**
	 * imports the available colors
	 * 
	 * @throws JsonProcessingException
	 *             if there were problems encountered when processing JSON
	 *             content that are not pure I/O problems
	 * @throws IOException
	 *             if a problem with the I/O has occurred
	 */
	private void importAvailableColors() throws JsonProcessingException, IOException {
		JsonNode root = configHelper.getRootNode();
		JsonNode colorsNode = root.get(COLORS);
		int r, g, b;
		for (JsonNode colorNode : colorsNode) {
			String colorName = colorNode.get(ConfigHelper.NAME).asText();
			r = colorNode.get(R).asInt();
			g = colorNode.get(G).asInt();
			b = colorNode.get(B).asInt();
			availableColors.put(colorName, new Color(r, g, b));
		}
	}

	/**
	 * Returns the corruption colors configurations node
	 * 
	 * @return the corruption colors configurations node
	 * @throws JsonProcessingException
	 *             if there were problems encountered when processing JSON
	 *             content that are not pure I/O problems
	 * @throws IOException
	 *             if a problem with the I/O has occurred
	 */
	private JsonNode getCorruptionColorsConfigurationsNode() throws JsonProcessingException, IOException {
		JsonNode root = configHelper.getRootNode();
		return root.get(CORRUPTION_COLORS_CONFIGURATIONS);
	}

	/**
	 * Returns the corruption colors configurations node with the given id
	 * 
	 * @param id
	 *            the id of the corruption colors configurations node that you
	 *            want to retrieve
	 * @return the corruption colors configurations node with the given id
	 * @throws JsonProcessingException
	 *             if there were problems encountered when processing JSON
	 *             content that are not pure I/O problems
	 * @throws IOException
	 *             if a problem with the I/O has occurred
	 */
	private JsonNode getCorruptionColorsConfigurationNode(String id) throws JsonProcessingException, IOException {
		JsonNode ConfigurationsNode = getCorruptionColorsConfigurationsNode();
		for (JsonNode corruptionColorsConfigurationNode : ConfigurationsNode) {
			if (corruptionColorsConfigurationNode.get(ConfigHelper.ID).asText().equals(id))
				return corruptionColorsConfigurationNode;
		}
		throw new IllegalArgumentException();
	}

	/**
	 * Returns a random corruption colors configuration node
	 * 
	 * @return a random corruption colors configuration node
	 * @throws JsonProcessingException
	 *             if there were problems encountered when processing JSON
	 *             content that are not pure I/O problems
	 * @throws IOException
	 *             if a problem with the I/O has occurred
	 */
	private JsonNode getRandomCorruptionColorsConfigurationNode() throws JsonProcessingException, IOException {
		JsonNode configurationsNode = getCorruptionColorsConfigurationsNode();
		int randomNumber = ThreadLocalRandom.current().nextInt(0, configurationsNode.size());
		return configurationsNode.get(randomNumber);
	}

	/**
	 * imports the corruption colors configuration with the given id
	 * 
	 * @param id
	 *            the id of the corruption colors configurations node that you
	 *            want to import
	 * @throws JsonProcessingException
	 *             if there were problems encountered when processing JSON
	 *             content that are not pure I/O problems
	 * @throws IOException
	 *             if a problem with the I/O has occurred
	 */
	private void importCorruptionColorsConfiguration(String id) throws JsonProcessingException, IOException {
		JsonNode corruptionColorsConfiguration = getCorruptionColorsConfigurationNode(id);
		addCorruptionColors(corruptionColorsConfiguration);
	}

	/**
	 * imports a random corruption colors configuration
	 * 
	 * @throws JsonProcessingException
	 *             if there were problems encountered when processing JSON
	 *             content that are not pure I/O problems
	 * @throws IOException
	 *             if a problem with the I/O has occurred
	 */
	private void importCorruptionColorsConfiguration() throws JsonProcessingException, IOException {
		JsonNode corruptionColorsConfiguration = getRandomCorruptionColorsConfigurationNode();
		addCorruptionColors(corruptionColorsConfiguration);
	}

	/**
	 * adds corruption colors that are contained in the given json node
	 * 
	 * @param corruptionColorsConfiguration
	 *            the json node that contains the corruption colors that you
	 *            want to add
	 */
	private void addCorruptionColors(JsonNode corruptionColorsConfiguration) {
		for (JsonNode corruptionColorNode : corruptionColorsConfiguration.get(CORRUPTION_COLORS)) {
			String corruptionColorName = corruptionColorNode.asText();
			Color corruptionColor = availableColors.get(corruptionColorName);
			corruptionColors.add(new NamedColor(corruptionColorName, corruptionColor));
		}
	}

	/**
	 * Returns the builded colors manager
	 * 
	 * @return the builded colors manager
	 * @throws JsonProcessingException
	 *             if there were problems encountered when processing JSON
	 *             content that are not pure I/O problems
	 * @throws IOException
	 *             if a problem with the I/O has occurred
	 */
	public ColorsManager factoryMethod() throws JsonProcessingException, IOException {
		return new ColorsManager(availableColors, corruptionColors, bonusFactory);
	}

}
