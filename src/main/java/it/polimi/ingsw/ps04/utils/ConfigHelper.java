package it.polimi.ingsw.ps04.utils;

import java.io.File;
import java.io.IOException;
import java.time.Duration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * this class implements some utilities to manage the configurations file
 */
public class ConfigHelper {

	private static final String SOURCES = "src" + File.separator + "main" + File.separator;
	private static final String CONFING_FILE = SOURCES + "config.json";
	public File configFile;
	public static final String GUI_PATH = SOURCES + "gui" + File.separator;
	public static final String NAME = "name";
	public static final String ID = "id";
	public static final String REGION = "region";
	public static final String CITIES = "cities";
	public static final String BONUSES = "bonuses";
	private static final String TURN_SECONDS = "turnSeconds";

	/**
	 * Constructs a configuration helper with a given configuration file
	 * 
	 * @param configFile
	 *            the configuration file that you want to assign to this
	 *            configuration helper
	 */
	public ConfigHelper(File configFile) {
		this.configFile = configFile;
	}

	/**
	 * Constructs a configuration helper with the default configuration file
	 */
	public ConfigHelper() {
		configFile = new File(CONFING_FILE);
	}

	/**
	 * Returns the seconds of a turn of the user that are written in the
	 * configuration file
	 * 
	 * @return the seconds of a turn of the user that are written in the
	 *         configuration file
	 * @throws JsonProcessingException
	 *             if there were problems encountered when processing JSON
	 *             content that are not pure I/O problems
	 * @throws IOException
	 *             if a problem with the I/O has occurred
	 */
	public Duration getTurnSeconds() throws JsonProcessingException, IOException {
		JsonNode root = getRootNode();
		long seconds = root.get(TURN_SECONDS).asInt();
		return Duration.ofSeconds(seconds);
	}

	/**
	 * Returns the root node of the configuration file
	 * 
	 * @return the root node of the configuration file
	 * @throws JsonProcessingException
	 *             if there were problems encountered when processing JSON
	 *             content that are not pure I/O problems
	 * @throws IOException
	 *             if a problem with the I/O has occurred
	 */
	public JsonNode getRootNode() throws JsonProcessingException, IOException {

		// create an ObjectMapper instance.
		ObjectMapper mapper = new ObjectMapper();
		// use the ObjectMapper to read the json string and create a tree
		return mapper.readTree(configFile);
	}
}
