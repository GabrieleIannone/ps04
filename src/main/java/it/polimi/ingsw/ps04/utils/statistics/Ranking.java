package it.polimi.ingsw.ps04.utils.statistics;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import groovy.json.JsonOutput;

/**
 * This class represents the ranking of the all games
 */
public class Ranking implements Serializable {

	private static final long serialVersionUID = 2010460548555331601L;
	private List<PlayerStatistics> ranking;

	/**
	 * Constructs a ranking from the given statistics
	 * 
	 * @param statistics
	 *            the statistics that you want to order to get a ranking
	 */
	public Ranking(List<PlayerStatistics> statistics) {
		ranking = sortPlayersStatistics(statistics);
	}

	/**
	 * Sorts the statistics to get a ranking
	 * 
	 * @param statistics
	 *            the statistics that you want to order to get the ranking
	 * @return the ranking from the given statistics
	 */
	private List<PlayerStatistics> sortPlayersStatistics(List<PlayerStatistics> statistics) {
		List<PlayerStatistics> ranking = new ArrayList<>();

		Comparator<PlayerStatistics> byWonGames = (s1, s2) -> s2.getWonGames().compareTo(s1.getWonGames());

		Comparator<PlayerStatistics> byTime = (s1, s2) -> Integer.compare(s1.getGameMinutes(), s2.getGameMinutes());

		statistics.stream().sorted(byWonGames.thenComparing(byTime)).forEachOrdered(ranking::add);
		return ranking;
	}

	/**
	 * Returns the calculated ranking
	 * 
	 * @return the calculated ranking
	 */
	public List<PlayerStatistics> getRanking() {
		return ranking;
	}

	/**
	 * Returns a hash code value for this object. It considers the ranking.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ranking == null) ? 0 : ranking.hashCode());
		return result;
	}

	/**
	 * Compares two rankings for equality. The result is <code>true</code> if
	 * and only if the argument is not <code>null</code> and is a
	 * <code>Ranking</code> object that represents the same ranking.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ranking other = (Ranking) obj;
		if (ranking == null) {
			if (other.ranking != null)
				return false;
		} else if (!ranking.equals(other.ranking))
			return false;
		return true;
	}

	/**
	 * Converts this <code>Ranking</code> object to a <code>String</code>
	 */
	@Override
	public String toString() {
		return JsonOutput
				.prettyPrint(new ToStringBuilder(this, ToStringStyle.JSON_STYLE).append("ranking", ranking).toString());
	}
}
