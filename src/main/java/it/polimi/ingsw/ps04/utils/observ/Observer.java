package it.polimi.ingsw.ps04.utils.observ;

/**
 * a version of Observer that accepts only the type <T>
 * 
 * @param <T>
 *            the type that you want to allow
 */
public interface Observer<T> {

	/**
	 * Notifies the observer
	 * 
	 * @param message
	 *            the message that you want to notify
	 */
	public void notify(T message);

}
