package it.polimi.ingsw.ps04.utils.message.market;

import it.polimi.ingsw.ps04.model.market.SellingOffers;

/**
 * This class represents the message used during the selling phase. It contains
 * all the selling offers made by a single player.
 */
public class SellingOffersMessage extends MarketMessage {

	private static final long serialVersionUID = -3207974055952325974L;
	private SellingOffers sellingOffers;

	/**
	 * An empty offers message. It is used if a player wants to sell nothing.
	 */
	public SellingOffersMessage() {
		sellingOffers = new SellingOffers();
	}

	/**
	 * Creates a new selling offers message.
	 * 
	 * @param sellingOffers
	 *            The offers made by the player.
	 */
	public SellingOffersMessage(SellingOffers sellingOffers) {
		this.sellingOffers = sellingOffers;
	}

	/**
	 * @return The offers saved into the message.
	 */
	public SellingOffers getSellingOffers() {
		return sellingOffers;
	}

	@Override
	public String toString() {
		return "SellingOffersMessage [sellingOffers=" + sellingOffers + "]";
	}

}
