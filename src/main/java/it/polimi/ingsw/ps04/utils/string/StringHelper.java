package it.polimi.ingsw.ps04.utils.string;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import it.polimi.ingsw.ps04.model.board.City;

/**
 * this class implements some utilities to manage the strings
 */
public class StringHelper {

	/**
	 * Returns an enumerated list of the objects passed as argument
	 * 
	 * @param objectsList
	 *            the objects that you want to get the enumerated list
	 * @return an enumerated list of the objects passed as argument
	 */
	public static String getEnumeratedObjectsString(List<?> objectsList) {
		String result = "";
		int order = 1;
		for (Object obj : objectsList) {
			result += order + ": " + obj + "\n";
			order++;
		}
		return result;
	}

	/**
	 * Puts quotes around the string passed in the argoument input
	 * 
	 * @param input
	 *            the string that you want to surround with quotes
	 * @return the input string surrounded by quotes
	 */
	public static String putQuotes(String input) {
		return "\"" + input + "\"";
	}

	/**
	 * Returns a list of string that contains only the names of the cities
	 * passed as parameter
	 * 
	 * @param cities
	 *            the cities that you want to get the array of names
	 * @return a list of string that contains only the names of the cities
	 *         passed as parameter
	 */
	public static List<String> getCitiesNameArray(Set<City> cities) {
		List<String> citiesNames = new ArrayList<>();
		for (City city : cities) {
			citiesNames.add(putQuotes(city.getName()));
		}
		return citiesNames;
	}
}
