package it.polimi.ingsw.ps04.utils.message;

import it.polimi.ingsw.ps04.model.bonus.complex.ComplexBonus;

public class RequestforBonus extends Message {
	private static final long serialVersionUID = 3806794239564994431L;
	ComplexBonus bonus;

	public RequestforBonus(ComplexBonus bonus) {
		this.bonus = bonus;
	}

	public ComplexBonus getBonus() {
		return bonus;
	}

}
