package it.polimi.ingsw.ps04.utils.message.login;

import it.polimi.ingsw.ps04.utils.message.Message;

/**
 * this message is sent to communicate a result of a login operation
 */
public class SuccessMessage extends Message {
	private static final long serialVersionUID = -9174502483610163231L;
	boolean result;

	/**
	 * Constructs a success message with a given result
	 * @param result the result of the operation
	 */
	public SuccessMessage(boolean result) {
		this.result = result;
	}

	/**
	 * Returns the result of the operation
	 * @return the result of the operation
	 */
	public boolean getResult() {
		return result;
	}
}
