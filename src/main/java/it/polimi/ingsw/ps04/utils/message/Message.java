package it.polimi.ingsw.ps04.utils.message;

import java.io.Serializable;

/**
 * The most important information unit of the system
 */
public abstract class Message implements Serializable {

	private static final long serialVersionUID = -1239689918922547843L;

}
