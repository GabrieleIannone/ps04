package it.polimi.ingsw.ps04.utils.observ;

import java.util.ArrayList;
import java.util.List;

/**
 * a version of observable that accepts only the type <T>
 * 
 * @param <T>
 *            the type that you want to allow
 */
public class Observable<T> {

	private List<Observer<T>> observers = new ArrayList<Observer<T>>();

	/**
	 * Adds an observer to the list of observers for this object.
	 * 
	 * @param observer
	 *            the observer that you want to register
	 */
	public void register(Observer<T> observer) {
		synchronized (observers) {
			observers.add(observer);
		}
	}

	/**
	 * Deletes an observer from the list of observers of this object.
	 * 
	 * @param observer
	 *            observer the observer that you want to delete
	 */
	public void deregister(Observer<T> observer) {
		synchronized (observers) {
			observers.remove(observer);
		}
	}

	/**
	 * Notify a message to the observers
	 * 
	 * @param message
	 *            the message that you want to notify
	 */
	protected void notify(T message) {
		synchronized (observers) {
			for (Observer<T> observer : observers) {
				observer.notify(message);
			}
		}
	}

}
