package it.polimi.ingsw.ps04.utils.message.statistics;

import it.polimi.ingsw.ps04.utils.statistics.PlayerStatistics;

/**
 * the message that contains the statistics of a player
 */
public class PlayerStatisticsMessage extends StatisticsMessage {
	private static final long serialVersionUID = -1556106086907179625L;
	PlayerStatistics statistics;
	String player;

	/**
	 * Constructs a player statistics message with the given statistics
	 * 
	 * @param statistics
	 */
	public PlayerStatisticsMessage(PlayerStatistics statistics) {
		this.statistics = statistics;
	}

	/**
	 * Constructs a player statistics message with the player that you want to
	 * know the statistics
	 * 
	 * @param player
	 *            the player that you want to know the statistics
	 */
	public PlayerStatisticsMessage(String player) {
		this.player = player;
	}

	/**
	 * Returns the statistics of this statistics message
	 * 
	 * @return the statistics of this statistics message
	 */
	public PlayerStatistics getStatistics() {
		return statistics;
	}

	/**
	 * Returns the player that you want to know the statistics
	 * 
	 * @return the player that you want to know the statistics
	 */
	public String getPlayerName() {
		return player;
	}

	/**
	 * Converts this <code>PlayerStatisticsMessage</code> object to a
	 * <code>String</code>
	 */
	@Override
	public String toString() {
		return statistics.toString();
	}
}
