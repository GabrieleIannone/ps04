package it.polimi.ingsw.ps04.utils.message.market;

import it.polimi.ingsw.ps04.utils.message.Message;

/**
 * This class represents the message used during the market phase.
 */
public abstract class MarketMessage extends Message {

	private static final long serialVersionUID = 1923190217971326622L;

}
