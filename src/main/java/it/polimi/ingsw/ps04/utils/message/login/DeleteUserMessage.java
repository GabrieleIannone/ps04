package it.polimi.ingsw.ps04.utils.message.login;

/**
 * a credentials message that is used for deleting a user
 */
public class DeleteUserMessage extends CredentialsMessage {

	private static final long serialVersionUID = 1048882389566879236L;

	/**
	 * Constructs a delete user message with the given username and password
	 * 
	 * @param username
	 *            the username of the user that you want to delete
	 * @param password
	 *            the password of the user that you want to delete
	 */
	public DeleteUserMessage(String username, String password) {
		super(username, password);
	}

}
