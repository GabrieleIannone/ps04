package it.polimi.ingsw.ps04.utils.message;

/**
 * This message is used to communicate a simple message (a string)
 */
public class SimpleMessage extends Message {

	private static final long serialVersionUID = 3977422507308821308L;
	String message;

	/**
	 * Constructs a simple message with a given message
	 * 
	 * @param message
	 *            the message that you want to communicate
	 */
	public SimpleMessage(String message) {
		this.message = message;
	}

	/**
	 * Returns the message contained in this object
	 * 
	 * @return the message contained in this object
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Returns the message contained in this object
	 */
	@Override
	public String toString() {
		return getMessage();
	}
}
