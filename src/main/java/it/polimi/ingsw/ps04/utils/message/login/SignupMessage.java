package it.polimi.ingsw.ps04.utils.message.login;

/**
 * a credentials message that is used for the sign up of a user
 */
public class SignupMessage extends CredentialsMessage {

	private static final long serialVersionUID = 5612597986361358699L;
	String email;

	/**
	 * Constructs a sign up message with the given username, email and password
	 * 
	 * @param username
	 *            the username of the user that wants to sign up
	 * @param email
	 *            the email of the user that wants to sign up
	 * @param password
	 *            the password of the user that wants to sign up
	 */
	public SignupMessage(String username, String email, String password) {
		super(username, password);
		this.email = email;
	}

	/**
	 * Returns the email of the user that wants to sign up
	 * @return the email of the user that wants to sign up
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Converts this <code>SignupMessage</code> object to a
	 * <code>String</code>
	 */
	@Override
	public String toString() {
		return super.getToStringHelper().addValue(email).toString();
	}

}
