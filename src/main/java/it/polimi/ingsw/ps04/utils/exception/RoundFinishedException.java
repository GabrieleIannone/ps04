package it.polimi.ingsw.ps04.utils.exception;

/**
 * Signals that the round is finished
 */
public class RoundFinishedException extends Exception {

	private static final long serialVersionUID = 6456907381889975271L;

}
