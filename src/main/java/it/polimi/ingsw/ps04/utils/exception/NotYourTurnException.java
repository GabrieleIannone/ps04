package it.polimi.ingsw.ps04.utils.exception;

/**
 * Signals that a player tried to do an action when it was not is turn
 */
public class NotYourTurnException extends RuntimeException {

	private static final long serialVersionUID = -4183333379824836019L;

	/**
	 * Constructs the exception with the default message
	 */
	public NotYourTurnException() {
		this("It is not your turn!");
	}

	/**
	 * Constructs the exception with the given message
	 * 
	 * @param message
	 *            the message that you want to assign to this exception
	 */
	public NotYourTurnException(String message) {
		super(message);
	}
}
