package it.polimi.ingsw.ps04.utils.exception;

/**
 * Signals that the configuration file contains error
 */
public class BadConfigFileException extends Exception {
	private static final long serialVersionUID = 1394935622564165463L;

	/**
	 * Constructs the exception with the default message
	 */
	public BadConfigFileException() {
		super("bad file configuration");
	}

	/**
	 * Constructs the exception with the given message
	 * 
	 * @param message
	 *            the message that you want to assign to this exception
	 */
	public BadConfigFileException(String message) {
		super(message);
	}
}
