package it.polimi.ingsw.ps04.utils.message.market;

import java.util.HashMap;
import java.util.Map;

import it.polimi.ingsw.ps04.model.market.BuyingOrder;

/**
 * This class represents the message used during the buying phase. It contains
 * all the orders of a single player.
 */
public class BuyingOrdersMessage extends MarketMessage {

	private static final long serialVersionUID = -6631455406836653815L;
	private Map<String, BuyingOrder> orders = new HashMap<>();

	/**
	 * Creates a new order message.
	 * 
	 * @param orders
	 *            All the orders, each linked to its seller.
	 */
	public BuyingOrdersMessage(Map<String, BuyingOrder> orders) {
		this.orders = orders;
	}

	/**
	 * @return A map with all the orders.
	 */
	public Map<String, BuyingOrder> getOrders() {
		return orders;
	}

	@Override
	public String toString() {
		return "BuyingOrdersMessage [orders=" + orders + "]";
	}

}
