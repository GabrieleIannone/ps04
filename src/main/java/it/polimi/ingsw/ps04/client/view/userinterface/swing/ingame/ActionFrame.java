package it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;

import it.polimi.ingsw.ps04.client.view.userinterface.UserInterface;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame.action.ChooseCouncillorFrame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame.action.ChooseRegionFrame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.FrameUtilities;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.StandardButtonListener;

public class ActionFrame extends FrameUtilities {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final String TITLE = "Action";
	private static final String GET_ANOTHER_MAIN_ACTION = "Get another main action";
	private static final String ENGAGE_ASSISTANT = "Engage an assistant";
	private static final String CHANGE_PERMIT_TILES = "Change region permit tiles";
	private static final String SEND_ASSISTANT_TO_ELECT_COUNCIL = "Send an assistant to elect a council";
	private static final String PASS_TURN = "pass turn";
	private static final String ELECT_COUNCILLOR = "Elect a councillor";
	private static final String ACQUIRE_BUSINESS_PERMIT_TILE = "Acquire Business Permit tile";
	private static final String SEND_ASSISTANT_TO_ELECT_KING_COUNCIL = "Send an assistant to elect king council";
	private static final String ELECT_COUNCILLOR_KING = "Elect a councillor for king coucil";
	private final static int X_SIZE = 700;
	private final static int Y_SIZE = 500;

	private JButton passTurn, ElectCouncillor, buyBusinessPermitTile, ElectCouncillorKing;
	private JButton getAnotherMainAction, engageAssistant, changePermitTiles, sendAssistantToElectCouncil, sendAssistantToElectCouncilKing;
	private JPanel quickActionPanel = new JPanel();
	private JPanel mainActionPanel = new JPanel();
	private JPanel mainPanel= new JPanel();
	private transient UserInterface ui;

	/**
	 * Builder for ActionFrame which has UserInterface in order to pass the reference to actions
	 * 
	 * @param ui
	 * 			which I use to pass the reference to the actions
	 */
	public ActionFrame(UserInterface ui) {
		this.ui = ui;
		createFrame();
		settingMethod();
	}


	/**
	 * @see FrameUtilities
	 */
	@Override
	public void settingMethod() {
		settingMethodStandard(TITLE);
		this.setSize(X_SIZE,Y_SIZE);
	}

	/**
	 * Here I create a frame which has to the right quick action Panel and to the right main
	 * action Panel
	 */
	@Override
	public void createFrame() {
		mainPanel.setLayout(new BorderLayout());
		addComponentQuickActionPanel();
		addComponentMainActionPanel();
		mainPanel.add(quickActionPanel, BorderLayout.EAST);
		mainPanel.add(mainActionPanel, BorderLayout.WEST);
		add(mainPanel);
	}
	
	
	/**
	 * Here I manage to add quick action buttons to quick action Panel
	 */
	private void addComponentQuickActionPanel() {
		quickActionPanel.setLayout(new BoxLayout (quickActionPanel, BoxLayout.Y_AXIS));
		addLabel ("Quick Actions", quickActionPanel);
		getAnotherMainAction = addButton (GET_ANOTHER_MAIN_ACTION, quickActionPanel);
		getAnotherMainAction.addActionListener(new Listener());
		engageAssistant = addButton (ENGAGE_ASSISTANT, quickActionPanel);
		engageAssistant.addActionListener(new Listener());
		changePermitTiles = addButton (CHANGE_PERMIT_TILES, quickActionPanel);
		changePermitTiles.addActionListener(new Listener());
		sendAssistantToElectCouncil = addButton (SEND_ASSISTANT_TO_ELECT_COUNCIL, quickActionPanel);
		sendAssistantToElectCouncil.addActionListener(new Listener());
		sendAssistantToElectCouncilKing = addButton (SEND_ASSISTANT_TO_ELECT_KING_COUNCIL, quickActionPanel);
		sendAssistantToElectCouncilKing.addActionListener(new Listener());
	}
	
	/**
	 * Here I manage to add main action buttons to main action Panel
	 */
	private void addComponentMainActionPanel( ) {
		mainActionPanel.setLayout(new BoxLayout (mainActionPanel, BoxLayout.Y_AXIS));
		addLabel ("Main Actions", mainActionPanel);
		buyBusinessPermitTile = addButton (ACQUIRE_BUSINESS_PERMIT_TILE, mainActionPanel);
		buyBusinessPermitTile.addActionListener(new Listener());
		ElectCouncillor = addButton (ELECT_COUNCILLOR, mainActionPanel);
		ElectCouncillor.addActionListener(new Listener());
		ElectCouncillorKing = addButton (ELECT_COUNCILLOR_KING, mainActionPanel);
		ElectCouncillorKing.addActionListener(new Listener());
		passTurn = addButton (PASS_TURN, mainActionPanel);
		passTurn.addActionListener(new Listener());
	}

	/**
	 * Here i manage an inner listener for action buttons
	 *
	 */
	private class Listener extends StandardButtonListener implements ActionListener {

		/**
		 * Here I manage what to do if you click on a button doing the respective action
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			
			if (e.getSource().equals(sendAssistantToElectCouncil)){
				sendAssistantToElectCouncil();
				exit();
			}
			else if (e.getSource().equals(changePermitTiles)) {
				changePermitTiles();
				exit();
			}
			else if (e.getSource().equals(passTurn)) {
				passTurn();
				exit();
			}
			else if (e.getSource().equals(getAnotherMainAction)) {
				getAnotherMainAction();
				exit();
			}
			else if (e.getSource().equals(engageAssistant)) {
				engageAssistant();
				exit();
			}
			else if (e.getSource().equals(ElectCouncillor)) {
				ElectCouncillor();
				exit();
			}
			else if (e.getSource().equals(buyBusinessPermitTile)) {
				buyBusinessPermitTile();
				exit();
			}
			else if (e.getSource().equals(sendAssistantToElectCouncilKing)) {
				sendAssistantToElectCouncilKing();
				exit();
			}
			else if (e.getSource().equals(ElectCouncillorKing)) {
				ElectCouncillorKing();
				exit();
			}	
		}

		/**
		 * Thoe methods are called if you wanted to do some action with the king
		 */
		private void sendAssistantToElectCouncilKing() {
			Boolean quickAction = false;
			Boolean kingAction = true;
			new ChooseCouncillorFrame (ui, null, quickAction, kingAction);
		}

		private void ElectCouncillorKing() {
			Boolean mainAction = true;
			Boolean kingAction = true;
			new ChooseCouncillorFrame (ui, null, mainAction, kingAction);
		}

		/**
		 * Those methods are called if you clicked on a action buttons and do the respectives 
		 * actions
		 */
		private void ElectCouncillor() {
			Boolean mainAction = true;
			new ChooseRegionFrame(ui, mainAction, ELECT_COUNCILLOR);
		}
		
		private void getAnotherMainAction(){
			ui.getMessageSender().getAnotherPrimaryAction();
		}
		
		private void engageAssistant() {
			ui.getMessageSender().engageAssistant();
		}
		
		private void changePermitTiles(){
			Boolean quickAction = false;
			new ChooseRegionFrame(ui, quickAction , CHANGE_PERMIT_TILES);
		}
		
		private void sendAssistantToElectCouncil() {
			Boolean quickAction = false;
			new ChooseRegionFrame(ui, quickAction, SEND_ASSISTANT_TO_ELECT_COUNCIL);
		}
		
		private void passTurn() {
			ui.getMessageSender().passTurn();
		}
		
		private void buyBusinessPermitTile() {
			Boolean mainAction = true;
			new ChooseRegionFrame(ui, mainAction, ACQUIRE_BUSINESS_PERMIT_TILE);
		}

		/**
		 * @see StandardButtonListener
		 */
		@Override
		public void exit() {
			dispose();	
		}
	}
}
