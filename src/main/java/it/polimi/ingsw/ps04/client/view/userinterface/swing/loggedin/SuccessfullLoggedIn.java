package it.polimi.ingsw.ps04.client.view.userinterface.swing.loggedin;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import it.polimi.ingsw.ps04.client.view.userinterface.swing.GUIGame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.FrameUtilities;

public class SuccessfullLoggedIn extends FrameUtilities {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;		
		
	private static final String TITLE = "Waiting room";
	
	/**
	 * Builder with GuiGame reference in order to use it when the game is ready
	 * 
	 * @param gg
	 * 			which I use when game is ready
	 */
	public SuccessfullLoggedIn (GUIGame gg) {
		createFrame();
		settingMethod();
		}
		
	/**
	 * @see FrameUtilities
	 */
	public void settingMethod() {
		settingMethodStandard(TITLE);
		this.pack();
	}
	
	/**
	 * Create a simple frame with a background image and in the center a label
	 */
	@Override
	public void createFrame() {
		setLayout(new BorderLayout());
		JLabel background=new JLabel(new ImageIcon(GUIGame.IMAGES_PATH +"councilOfFour2.jpg"));
		background.setLayout(new FlowLayout());
		JLabel label = new JLabel ("Waiting for game");
		label.setForeground(Color.YELLOW);
		background.add(label);		
		add(background);
	}
}
