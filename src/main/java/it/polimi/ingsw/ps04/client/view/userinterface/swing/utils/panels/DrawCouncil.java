package it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.panels;

import java.awt.GridLayout;
import java.io.File;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import it.polimi.ingsw.ps04.client.view.userinterface.swing.GUIGame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.ImageColorReplacer;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.frames.SomethingWentWrongFrame;
import it.polimi.ingsw.ps04.model.board.Council;
import it.polimi.ingsw.ps04.model.board.Councillor;

public class DrawCouncil extends JComponent {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8113207203356457949L;

	private static final String COUNCILLOR_IMAGE_PATH = GUIGame.IMAGES_PATH + "councillor.png";

	private static final int BUTTON_WIDTH = 30;
	private static final int BUTTON_HEIGTH = 30;

	private Council council;
	JPanel panel = new JPanel();

	/**
	 * Builder for draw council which draw the council
	 * 
	 * @param council
	 * 				 i wanted to draw
	 */
	public DrawCouncil(Council council) {
		this.council = council;
		createFrame();
		panel.setVisible(true);
	}

	/**
	 * Here i create frame with councillors
	 */
	public void createFrame() {
		panel.setLayout(new GridLayout());
		JLabel useless = new JLabel("");
		sets(useless);
		panel.add(useless);
		/*
		 * here i draw councillors
		 */
		for (Councillor councillor : council.getCouncillors()) {
			try {
				JLabel label;
				label = new JLabel(new ImageIcon(
						ImageColorReplacer.changeColor(new File(COUNCILLOR_IMAGE_PATH), councillor.getColor())));
				sets(label);
			} catch (IOException e) {
				new SomethingWentWrongFrame();
				e.printStackTrace();
			}
		}
		JLabel empty = new JLabel("");
		sets(empty);
		panel.add(empty);
	}
	
	/**
	 * Here i set label and add it to main panel
	 * 
	 * @param component
	 * 					to be set
	 */
	private void sets(JComponent component) {
		component.setSize(BUTTON_WIDTH, BUTTON_HEIGTH);
		component.setAlignmentX(CENTER_ALIGNMENT);
		component.setAlignmentY(CENTER_ALIGNMENT);
		component.setOpaque(false);
		panel.add(component);
	}
	
	/**
	 * Here i return this panel in order to be used by others frame/panels
	 * 
	 * @return panel
	 * 				which is used in other frames
	 */
	public JPanel getPanel() {
		return panel;
	}
}
