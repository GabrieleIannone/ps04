package it.polimi.ingsw.ps04.client.view.userinterface.cli.utils;

import java.util.ArrayList;
import java.util.List;

import java.util.Scanner;

import it.polimi.ingsw.ps04.model.deck.business.BusinessPermitTile;
import it.polimi.ingsw.ps04.model.deck.politic.PoliticCard;
import it.polimi.ingsw.ps04.model.market.AssistantsonSale;
import it.polimi.ingsw.ps04.model.market.BusinessPermitTileonSale;
import it.polimi.ingsw.ps04.model.market.PoliticCardonSale;
import it.polimi.ingsw.ps04.model.market.SellingOffers;
import it.polimi.ingsw.ps04.model.player.AssistantsCrew;
import it.polimi.ingsw.ps04.utils.string.StringHelper;

/**
 * it has same functions to help to take the input from the user for the sell
 * phase of the market
 *
 */
public class MarketSellHelper {

	List<BusinessPermitTile> userTiles;
	List<PoliticCard> userCards;
	SellingOffers offers = new SellingOffers();
	Scanner scanner;

	/**
	 * constructs a market sell helper with the tiles and the cards of the user
	 * and with a given scanner
	 * 
	 * @param userTiles
	 *            the business permit tiles of the user
	 * @param userCards
	 *            the cards of the user
	 * @param scanner
	 *            the scanner where the function reads the input
	 */
	public MarketSellHelper(List<BusinessPermitTile> userTiles, List<PoliticCard> userCards, Scanner scanner) {
		if (userTiles != null)
			this.userTiles = new ArrayList<>(userTiles);
		if (userCards != null)
			this.userCards = new ArrayList<>(userCards);
		this.scanner = scanner;
	}

	/**
	 * allows the user to sell assistants
	 */
	private void sellAssistants() {
		System.out.println("insert the number of assistants that you want to sell: ");
		AssistantsCrew assistants = new AssistantsCrew(scanner.nextInt());
		scanner.nextLine();
		if (assistants.getMembersNumber() > 0) {
			System.out.println("insert cost of an assistant: ");
			AssistantsonSale assistantsOnSale = new AssistantsonSale(assistants, scanner.nextInt());
			scanner.nextLine();
			offers.setAssistantsonSale(assistantsOnSale);
		}
	}

	/**
	 * allows the user to sell business permit tiles
	 */
	private void sellPermitTiles() {
		int userInput;
		if (userTiles != null && !userTiles.isEmpty()) {
			int tilesSold = 0;
			while (userTiles.size() > tilesSold) {
				System.out.println("your tiles:");
				System.out.println(StringHelper.getEnumeratedObjectsString(userTiles));
				System.out.println(
						"insert the number of the tile that you want to sell or -1 if you want to stop to sell tiles: ");
				userInput = scanner.nextInt();
				scanner.nextLine();
				if (userInput == -1) {
					break;
				}
				try {
					BusinessPermitTile tile = userTiles.get(userInput - 1);
					// I remove the tile from the copy of the real tiles
					userTiles.remove(userInput - 1);
					System.out.println("insert cost of the tile: ");
					offers.addTileOnSale(new BusinessPermitTileonSale(tile, scanner.nextInt()));
					scanner.nextLine();
					tilesSold++;
				} catch (IndexOutOfBoundsException e) {
					System.err.println("you don't have this tile");
					continue;
				}
			}
		} else {
			System.out.println("you don't have permit tiles");
		}
	}

	/**
	 * allows the user to sell politic cards
	 */
	private void sellPoliticCards() {
		int userInput;
		if (userCards != null && !userCards.isEmpty()) {
			int cardsSold = 0;
			while (userCards.size() > cardsSold) {
				System.out.println("your cards:");
				System.out.println(StringHelper.getEnumeratedObjectsString(userCards));
				System.out.println(
						"insert the number of the card that you want to sell or -1 if you want to stop to sell cards: ");
				userInput = scanner.nextInt();
				scanner.nextLine();
				if (userInput == -1) {
					break;
				}
				try {
					PoliticCard card = userCards.get(userInput - 1);
					userCards.remove(userInput - 1);
					System.out.println("insert cost of the card: ");
					offers.addCardOnSale(new PoliticCardonSale(card, scanner.nextInt()));
					scanner.nextLine();
					cardsSold++;
				} catch (IndexOutOfBoundsException e) {
					System.err.println("you don't have this card");
					continue;
				}
			}
		} else {
			System.out.println("you don't have cards");
		}
	}

	/**
	 * allows the user to sell
	 * 
	 * @return the selling offers of the user
	 */
	public SellingOffers sell() {
		int choice;
		while (true) {
			do {
				System.out.println("choose what to sell:");
				System.out.println("1. assistants\n2. business permit tile\n3. politic card\n4. nothing");
				choice = scanner.nextInt();
				scanner.nextLine();
			} while (choice != 1 && choice != 2 && choice != 3 && choice != 4);
			switch (choice) {
			case 1:
				sellAssistants();
				break;
			case 2:
				sellPermitTiles();
				break;
			case 3:
				sellPoliticCards();
				break;
			case 4:
				break;
			}
			if (!InputHelper.askYesNo(scanner, "do you want to sell more?")) {
				return offers;
			}
		}

	}
}
