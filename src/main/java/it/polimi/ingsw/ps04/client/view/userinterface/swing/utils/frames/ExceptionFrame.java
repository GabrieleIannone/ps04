package it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.frames;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;

import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.FrameUtilities;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.StandardButtonListener;

public class ExceptionFrame extends FrameUtilities {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4459774682363536816L;
	
	private final static int X_SIZE = 500;
	private final static int Y_SIZE = 300;

	private String info;
	private String title;
	
	/**
	 * Builder for exception frame which use title of the exception and the info about it
	 * 
	 * @param title
	 * 			   of the exception called
	 * @param info
	 * 			  about the exception itself
	 */
	public ExceptionFrame(String title, String info) {
		this.info = info;
		this.title = title;
		createFrame();
		settingMethod();
	}
	
	/**
	 * @see FrameUtilities
	 */
	@Override
	public void settingMethod() {
		settingMethodStandard(title);
		this.setSize(X_SIZE, Y_SIZE);
	}

	/**
	 * Here I create a simple frame with a label with the info in the center and an exit button in the south position
	 */
	@Override
	public void createFrame() {
		setLayout(new BorderLayout());
		exit = addButton (EXIT, getContentPane());
		exit.addActionListener(new Listener());
		JLabel actionNotComplete = addLabel (info, getContentPane());
		add(exit, BorderLayout.SOUTH);
		add(actionNotComplete, BorderLayout.CENTER);
	}
	
	/**
	 * Inner class where I manage listener
	 *
	 */
	private class Listener extends StandardButtonListener implements ActionListener {

		/**
		 * Here I manage what to do if you click on exit button
		 */
		@Override
		public void actionPerformed(ActionEvent e) {

			if (e.getSource().equals(exit)) {
				exit();
			}
		}
		
		/**
		 * @see StandardButtonListener
		 */
		@Override
		public void exit() {
			dispose();
		}
	}
}
