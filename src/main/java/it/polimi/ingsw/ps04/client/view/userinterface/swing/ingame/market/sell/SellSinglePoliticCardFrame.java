package it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame.market.sell;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import it.polimi.ingsw.ps04.client.view.userinterface.swing.GUIGame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.FrameUtilities;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.ImageColorReplacer;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.StandardButtonListener;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.frames.SomethingWentWrongFrame;
import it.polimi.ingsw.ps04.model.deck.politic.PoliticCard;
import it.polimi.ingsw.ps04.model.market.PoliticCardonSale;
import it.polimi.ingsw.ps04.model.market.SellingOffers;

public class SellSinglePoliticCardFrame extends FrameUtilities{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8597863188254180514L;
	
	private static final String TITLE = "Sell politic card";
	private static final String CARD_IMAGE_PATH = GUIGame.IMAGES_PATH + "politics_card.png";
	private static final int NUM_ROW = 1;
	private static final int NUM_COLUMNS = 1;
	
	private PoliticCard card;
	private JPanel cardPanel = new JPanel();
	private JPanel buttonPanel = new JPanel();
	private JTextField cardCost;
	private int cost = -1;
	private PoliticCardonSale politicCardonSale;
	private SellingOffers offers;

	/**
	 * Builder for Sell Single Politic Card Frame which take offers reference to update it and card you wanted to sell
	 * 
	 * @param card
	 * 			  you wanted to sell
	 * @param offers
	 * 				reference in order to update it
	 */
	public SellSinglePoliticCardFrame(PoliticCard card, SellingOffers offers) {
		this.offers = offers;
		this.card = card;
		createFrame();
		settingMethod();
	}

	/**
	 * @see FrameUtilities
	 */
	@Override
	public void settingMethod() {
		settingMethodStandard(TITLE);
		pack();
	}

	/**
	 * I create a simple frame with a button panel in the south and a card panel in the center
	 */
	@Override
	public void createFrame() {
		Container framePanel = getContentPane();
		addComponentsToButtonPanel();
		addComponentsToCardPanel();
		framePanel.add(buttonPanel, BorderLayout.SOUTH);
		framePanel.add(cardPanel, BorderLayout.CENTER);
	}

	/**
	 * Here I add a card label to show what are you wanting to sell and a TextField for the cost
	 */
	private void addComponentsToCardPanel() {
		cardPanel.setLayout(new GridLayout(NUM_ROW, NUM_COLUMNS));
		try {
			JLabel cardLabel = new JLabel (new ImageIcon(
					ImageColorReplacer.changeColor(new File(CARD_IMAGE_PATH), card.getColour())));
			cardPanel.add(cardLabel);
		} catch (IOException e) {
			new SomethingWentWrongFrame();
			e.printStackTrace();
		}
		cardCost = addTextField ("", cardPanel);
		cardCost.addActionListener(new Listener());
	}

	/**
	 * Here I add done and exit buttons to button Panel
	 */
	private void addComponentsToButtonPanel() {
		buttonPanel.setLayout(new FlowLayout());
		exit = addButton (EXIT, buttonPanel);
		exit.addActionListener(new Listener());
		done = addButton (DONE, buttonPanel);
		done.addActionListener(new Listener());
	}
	
	/**
	 * Inner class to manage this kind of listener
	 *
	 */
	private class Listener extends StandardButtonListener implements ActionListener {

		/**
		 * Here I manage what to do if you click on a button
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			/*
			 * What to do if you click on exit button
			 */
			if (e.getSource().equals(exit)) {
				exit();
			}
			else if (e.getSource().equals(cardCost)) {
				setCost(Integer.valueOf(cardCost.getText()));
			}
			/*
			 * If you click on done and the cost is not set this will generate a Something Went Wrong Frame
			 * else you add your card to offers
			 */
			else if (e.getSource().equals(done)) {
				if (cost==-1) {
					new SomethingWentWrongFrame();
				}
				else {
					politicCardonSale = new PoliticCardonSale (card, cost);
					offers.addCardOnSale(politicCardonSale);
					exit();
				}
			}
		}
		
		/**
		 * Here I set the cost for the card
		 * 
		 * @param cardCost
		 * 				 which is the cost of the card
		 */
		private void setCost (int cardCost) {
			cost = cardCost; 
		}

		/**
		 * @see StandardButtonListener
		 */
		@Override
		public void exit() {
			dispose();
		}
	}
}
