package it.polimi.ingsw.ps04.client.view.userinterface.cli;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import asg.cliche.Command;
import asg.cliche.Param;
import groovy.json.JsonOutput;
import it.polimi.ingsw.ps04.utils.color.CityColor;
import it.polimi.ingsw.ps04.utils.color.NamedColor;
import it.polimi.ingsw.ps04.utils.exception.NoPermitTileException;
import it.polimi.ingsw.ps04.utils.string.StringHelper;
import it.polimi.ingsw.ps04.client.view.userinterface.UserInterface;
import it.polimi.ingsw.ps04.client.view.userinterface.cli.utils.MarketBuyHelper;
import it.polimi.ingsw.ps04.client.view.userinterface.cli.utils.MarketSellHelper;
import it.polimi.ingsw.ps04.model.action.main.*;
import it.polimi.ingsw.ps04.model.action.quick.*;
import it.polimi.ingsw.ps04.model.action.PassTurn;
import it.polimi.ingsw.ps04.model.board.City;
import it.polimi.ingsw.ps04.model.board.Councillor;
import it.polimi.ingsw.ps04.model.board.Region;
import it.polimi.ingsw.ps04.model.bonus.complex.ComplexBonus;
import it.polimi.ingsw.ps04.model.deck.business.BusinessPermitTile;
import it.polimi.ingsw.ps04.model.deck.politic.PoliticCard;
import it.polimi.ingsw.ps04.model.market.SellingOffers;
import it.polimi.ingsw.ps04.model.player.Player;

/**
 * this class represents the shell that is opened after that the client did the
 * login
 */
public class LoggedCli {

	private static final String SEPARATOR = ",";
	private UserInterface ui;
	private Scanner scanner;

	/**
	 * constructs a logged cli with the given user interface
	 * 
	 * @param ui
	 *            the user interface
	 */
	public LoggedCli(UserInterface ui, Scanner scanner) {
		this.ui = ui;
		this.scanner = scanner;
	}

	/**
	 * allows the user to do the action "AcquireBusinessPermitTile"
	 * 
	 * @param cardsNumbers
	 *            the numbers of the card from your hand separated by ','
	 * @param region
	 *            the region of the business permit tile
	 * @param businessTileNumber
	 *            the number of the business permite tile
	 */
	@Command(description = AcquireBusinessPermitTile.DESCRIPTION)
	public void acquireBusinessPermitTile(
			@Param(name = "card numbers", description = "the numbers of the card from your hand separated by ',''") String cardsNumbers,
			@Param(name = "region", description = "the region of the business permit tile") Region region,
			@Param(name = "business tile number", description = "the number of the business permite tile") int businessTileNumber) {
		List<PoliticCard> cards = getCardsFromString(cardsNumbers);
		ui.getMessageSender().acquireBusinessPermitTile(cards, region, businessTileNumber);
	}

	/**
	 * allows the user to do the action "BuildEmporium"
	 * 
	 * @param city
	 *            the city where you want to build
	 * @param businessPermitTileNumber
	 *            the number of your business permite tile
	 */
	@Command(description = BuildEmporium.DESCRIPTION)
	public void buildEmporium(@Param(name = "city", description = "the city where you want to build") City city,
			@Param(name = "business tile number", description = "the number of your business permite tile") int businessPermitTileNumber) {
		BusinessPermitTile tile = ui.getClientPlayer().getBusinessPermitTilePool()
				.getBusinessPermitTileAt(businessPermitTileNumber - 1);
		ui.getMessageSender().buildEmporium(city, tile);
	}

	/**
	 * allows the user to do the action "BuildEmporiumWithKing"
	 * 
	 * @param cardsNumbers
	 *            the numbers of the card from your hand separated by ','
	 * @param city
	 *            the city where you want to build
	 */
	@Command(description = BuildEmporiumWithKing.DESCRIPTION)
	public void buildEmporiumWithKing(
			@Param(name = "card numbers", description = "the numbers of the card from your hand separated by ',''") String cardsNumbers,
			@Param(name = "city", description = "the city where you want to build") City city) {
		List<PoliticCard> cards = getCardsFromString(cardsNumbers);
		ui.getMessageSender().buildEmporiumWithKing(cards, city);
	}

	/**
	 * shows the current ranking of the game
	 * 
	 * @return a string that represents the current ranking of the game
	 */
	@Command(description = "show the current ranking of the game")
	public String getMatchRanking() {
		return ui.getModel().getMatchRanking().toString();
	}

	/**
	 * shows the corruption colors
	 * 
	 * @return a string that represents the corruption colors
	 */
	@Command(description = "show corruption colors")
	public String getCorruptionColors() {
		return ui.getModel().getPoliticDeck().getCorruptionColors().toString();
	}

	/**
	 * shows the councils
	 * 
	 * @return a string that represents the councils
	 */
	@Command(description = "show councils")
	public String getCouncils() {
		System.out.println("councils");
		StringBuilder councilsString = new StringBuilder();
		for (Region region : ui.getModel().getMap().getRegions()) {
			councilsString.append(region.getName() + ": " + region.getCouncil().toString() + "\n");
		}
		councilsString.append("king" + ": " + ui.getModel().getMap().getKing().getCouncil().toString() + "\n");
		return councilsString.toString();
	}

	/**
	 * shows the map
	 * 
	 * @return a string that represents the map
	 */
	@Command(description = "show map")
	public String getMap() {
		return ui.getModel().getMap().toString();
	}

	/**
	 * shows the waiting bonuses
	 * 
	 * @return a string that represents the waiting bonuses
	 */
	@Command(description = "show waiting bonuses")
	public String getWaitingBonuses() {
		return StringHelper.getEnumeratedObjectsString(ui.getModel().getAvailableBonusesPool().getAvailableBonus());
	}

	/**
	 * activates a waiting bonus
	 * 
	 * @param index
	 *            the index of the complex bonus that you have to activate
	 */
	@Command(description = "activate a waiting bonus")
	public void activateWaitingBonus(int index) {
		try {
			ComplexBonus bonus = ui.getModel().getAvailableBonusesPool().getAvailableBonus().get(index - 1);
			bonus.compileBonus(ui);
			ui.getMessageSender().sendCompiledComplexBonus(bonus);
		} catch (IndexOutOfBoundsException e) {
			System.err.println("wrong index.");
			return;
		}
	}

	/**
	 * shows the revealed permit tiles
	 * 
	 * @return a string that represents the permit tiles
	 */
	@Command(description = "show revealed permit tiles")
	public String getRevealedPermitTiles() {
		ToStringBuilder tilesBuilder = new ToStringBuilder(this, ToStringStyle.JSON_STYLE);
		for (Region region : ui.getModel().getMap().getRegions()) {
			tilesBuilder.append(region.getName(), region.getBusinessDeck().getRevealedTiles());
		}
		return JsonOutput.prettyPrint(tilesBuilder.toString());
	}

	/**
	 * shows the state of the player
	 * 
	 * @return a string that represents the state of the player
	 */
	@Command(description = "show my state")
	public String getMe() {
		return ui.getClientPlayer().getPrivateElementsString();
	}

	/**
	 * shows the nobility track
	 * 
	 * @return a string that represents the nobility track
	 */
	@Command(description = "show nobility track")
	public String getNobilityTrack() {
		return ui.getModel().getNobilityTrack().toString();
	}

	/**
	 * shows the permit tiles
	 * 
	 * @return a string that represents the permit tiles
	 */
	@Command(description = "show my permit tiles")
	public String getPermitTiles() {
		try {
			return StringHelper
					.getEnumeratedObjectsString(ui.getClientPlayer().getBusinessPermitTilePool().getPermitTiles());
		} catch (NoPermitTileException e) {
			return "you have no tiles";
		}
	}

	/**
	 * shows all the public elements of the players
	 * 
	 * @return a string that represents all the the public elements of the
	 *         players
	 */
	@Command(description = "show all the public elements of players")
	public String getPlayers() {
		String players = "";
		for (Player player : ui.getModel().getPlayersManager().getActivePlayers()) {
			players += player.getPublicElementsString();
		}
		return players;
	}

	/**
	 * shows the politic cards of the player
	 * 
	 * @return a string that represents the politic cards of the player
	 */
	@Command(description = "show my politic cards")
	public String getPoliticCards() {
		return StringHelper.getEnumeratedObjectsString(ui.getClientPlayer().getPoliticCardsHand().getHand());
	}

	/**
	 * allows the player to sell
	 */
	@Command(description = "sell")
	public void sell() {
		List<BusinessPermitTile> userTiles;
		Player clientPlayer = ui.getClientPlayer();
		try {
			// userTiles =
			// ui.getClientPlayer().getBusinessPermitTilePool().getPermitTiles();
			userTiles = ui.getModel().getPlayersManager().getPlayerReference(clientPlayer).getBusinessPermitTilePool()
					.getPermitTiles();
		} catch (NoPermitTileException e) {
			userTiles = null;
		}
		MarketSellHelper sellCli = new MarketSellHelper(userTiles, clientPlayer.getPoliticCardsHand().getHand(),
				scanner);
		SellingOffers offers = sellCli.sell();
		ui.getMessageSender().sell(offers);
	}

	/**
	 * takes a string of the number of the cards separated by commas and return
	 * the correspondent cards of the user
	 * 
	 * @param cardsString
	 *            the indexes of cards in player's hand separated by commas
	 * @return a list of the selected cards
	 */
	private List<PoliticCard> getCardsFromString(String cardsNumbers) {
		String[] cardsString = cardsNumbers.split(SEPARATOR);
		List<PoliticCard> cards = new ArrayList<>();
		for (String cardString : cardsString) {
			int cardIndex = Integer.parseInt(cardString);
			cards.add(ui.getClientPlayer().getPoliticCardsHand().cardAt(cardIndex - 1));
		}
		return cards;
	}

	/**
	 * requests the ranking to the server
	 */
	@Command(description = "get ranking")
	public void getRanking() {
		ui.getMessageSender().ranking();
	}

	/**
	 * requests your statistics to the server
	 */
	@Command(description = "get your statistics")
	public void getMyStatistics() {
		ui.getMessageSender().statistics(ui.getClientPlayer().getName());
	}

	/**
	 * requests the statistics of a given player to the server
	 * 
	 * @param player
	 *            the player that you want to know the statistics
	 */
	@Command(description = "get the statistics of a player")
	public void getStatistics(
			@Param(name = "player", description = "the player that you want to know the statistics") String player) {
		ui.getMessageSender().statistics(player);
	}

	/**
	 * shows the offers
	 * 
	 * @return a string that represents the offers
	 */
	@Command(description = "show offers")
	public String showOffers() {
		Map<String, SellingOffers> offers = ui.getModel().getMarket().getOffers();
		if (offers.size() > 0)
			return offers.toString();
		else
			return "no offers";
	}

	/**
	 * allows the user to buy from the other players
	 */
	@Command(description = "buy from other players")
	public void buy() {
		MarketBuyHelper buyHelper = new MarketBuyHelper(ui.getModel().getMarket().getOffers(), scanner);
		ui.getMessageSender().buy(buyHelper.compileOrders());
	}

	/**
	 * allows the user to select a region or the king
	 * 
	 * @return the selected region or null if the user selected the king
	 */
	private Region selectKingOrRegion() {
		int choice;
		Region region = null;
		do {
			System.out.println("choose:\n1. region\n2. king");
			choice = scanner.nextInt();
			scanner.nextLine();
		} while (choice != 1 && choice != 2);
		if (choice == 1) {
			System.out.println("insert region: ");
			String regionName = scanner.next();
			region = new Region(regionName);
		}
		return region;
	}

	/**
	 * allows the user to do the action "ElectCouncillor"
	 * 
	 * @param councillorColorName
	 *            the name of the color of the councillor
	 */
	@Command(description = ElectCouncillor.DESCRIPTION)
	public void electConcillor(
			@Param(name = "councillor color name", description = "the name of the councillor's color") String councillorColorName) {
		try {
			NamedColor corruptionColor = ui.getModel().getPoliticDeck().getCorruptionColor(councillorColorName);
			Councillor councillor = new Councillor(corruptionColor);
			Region region = selectKingOrRegion();
			ui.getMessageSender().electCouncillor(region, councillor);
		} catch (IllegalArgumentException e) {
			System.err.println("the color does not exist");
		}
	}

	/**
	 * allows the user to do the action "ChangeBusinessPermitTiles"
	 * 
	 * @param region
	 *            the region that contains the tiles to change
	 */
	@Command(description = ChangeBusinessPermitTiles.DESCRIPTION)
	public void changeBusinessPermitTiles(
			@Param(name = "region", description = "the region that contains the tiles to change") Region region) {
		ui.getMessageSender().changeBusinessPermitTiles(region);
	}

	/**
	 * allows the user to do the action "EngageAssistant"
	 */
	@Command(description = EngageAssistant.DESCRIPTION)
	public void engageAssistant() {
		ui.getMessageSender().engageAssistant();
	}

	/**
	 * allows the user to do the action "GetAnotherPrimaryAction"
	 */
	@Command(description = GetAnotherPrimaryAction.DESCRIPTION)
	public void getAnotherPrimaryAction() {
		ui.getMessageSender().getAnotherPrimaryAction();
	}

	/**
	 * allows the user to do the action "SendAssistantToElectCouncillor"
	 * 
	 * @param councillorColorName
	 *            the name of the councillor's color
	 */
	@Command(description = SendAssistantToElectCouncillor.DESCRIPTION)
	public void sendAssistantToElectCouncillor(
			@Param(name = "councillor color name", description = "the name of the councillor's color") String councillorColorName) {
		try {
			NamedColor corruptionColor = ui.getModel().getPoliticDeck().getCorruptionColor(councillorColorName);
			Councillor councillor = new Councillor(corruptionColor);
			Region region = selectKingOrRegion();
			ui.getMessageSender().sendAssistantToElectCouncillor(region, councillor);
		} catch (IllegalArgumentException e) {
			System.err.println("the color does not exist");
		}
	}

	/**
	 * allows the user to pass the turn
	 */
	@Command(description = PassTurn.DESCRIPTION)
	public void passTurn() {
		ui.getMessageSender().passTurn();
	}

	/**
	 * allows the user to skip the sell phase
	 */
	@Command(description = "skip sell phase")
	public void skipSell() {
		ui.getMessageSender().skipSell();
	}

	/**
	 * shows the city colors
	 * 
	 * @return
	 */
	@Command(description = "show city colors")
	public String getCityColors() {
		StringBuilder stringBuilder = new StringBuilder();
		for (CityColor cityColor : ui.getModel().getCityColors()) {
			stringBuilder.append(cityColor.getName());
			stringBuilder.append(": ");
			stringBuilder.append(cityColor.getBonus().toString());
			stringBuilder.append(System.lineSeparator());
		}
		return stringBuilder.toString();
	}

	/**
	 * allows the user to skip the buy phase
	 */
	@Command(description = "skip buy phase")
	public void skipBuy() {
		ui.getMessageSender().skipBuy();
	}

}
