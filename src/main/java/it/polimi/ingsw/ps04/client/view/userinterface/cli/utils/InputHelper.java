package it.polimi.ingsw.ps04.client.view.userinterface.cli.utils;

import java.util.Scanner;

/**
 * it has same functions to help to take the input from the user
 */
public class InputHelper {

	/**
	 * asks the user a question and returns a boolean that represents the answer
	 * 
	 * @param scanner
	 *            the scanner where the function reads the input
	 * @param question
	 *            the question that you have to ask to the user
	 * @return true if the answer was yes, false if the answer was no
	 */
	public static boolean askYesNo(Scanner scanner, String question) {
		boolean answer;
		while (true) {
			System.out.println(question + " [Y/n]");
			String choice = scanner.nextLine();
			if (choice.equalsIgnoreCase("y")) {
				answer = true;
				break;
			}
			if (choice.equalsIgnoreCase("n")) {
				answer = false;
				break;
			}
		}
		return answer;
	}
}
