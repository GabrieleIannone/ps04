package it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame.action;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JPanel;

import it.polimi.ingsw.ps04.client.view.userinterface.UserInterface;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.FrameUtilities;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.StandardButtonListener;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.component.TileButton;
import it.polimi.ingsw.ps04.model.board.Region;
import it.polimi.ingsw.ps04.model.deck.business.BusinessPermitTile;
import it.polimi.ingsw.ps04.model.deck.politic.PoliticCard;

public class ChooseTileFromRegionFrame extends FrameUtilities{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3361324779859330260L;
	
	private static final String TITLE = "Choose tile";
	private static final int X_SIZE = 300;
	private static final int Y_SIZE = 200;

	private Region region;
	private UserInterface ui;
	private BusinessPermitTile tile;
	private ArrayList<PoliticCard> cardUsed;
	private int tileNumber;
	
	/**
	 * Builder for ChooseTileFromRegionFrame which has UserInterface to send action, the region of
	 * which I want to draw revealed tiles and the cards used to buy it
	 * 
	 * @param ui
	 * 			which is used to send action
	 * @param region
	 * 				of which I want to draw council
	 * @param cardUsed
	 * 				  to acquire tile
	 */
	public ChooseTileFromRegionFrame(UserInterface ui, Region region, ArrayList<PoliticCard> cardUsed) {
		this.cardUsed = cardUsed;
		this.ui = ui;
		this.region = region;
		createFrame();
		settingMethod();
	}

	/**
	 * @see FrameUtilities
	 */
	@Override
	public void settingMethod() {
		settingMethodStandard(TITLE);
		this.setSize(X_SIZE, Y_SIZE);
	}

	/**
	 * Here I create a frame which has a button exit in south position and revealed tile from a 
	 * region drawn in the center 
	 */
	@Override
	public void createFrame() {
		Container framePanel = getContentPane();
		framePanel.setLayout(new BorderLayout());
		exit = addButton ("exit", framePanel);
		exit.addActionListener(new Listener());
		JPanel tilePanel = new JPanel();
		tilePanel.setLayout(new FlowLayout());
		for (BusinessPermitTile tile : region.getBusinessDeck().getRevealedTiles()) {
			TileButton button = new TileButton(tilePanel, tile);
			button.addActionListener(new Listener());
		}
		framePanel.add(exit, BorderLayout.SOUTH);
		framePanel.add(tilePanel, BorderLayout.CENTER);
	}
	
	/**
	 * This method will be usefull if you want to know if player wants to get first or second 
	 * tile revealed
	 * 
	 * @param tile
	 * 			  which is the tile I want
	 * @return tileNumber
	 * 					 the position number of the revealed tiles
	 */
	public int getTileNumber(BusinessPermitTile tile) {
		for (BusinessPermitTile tileIterator : region.getBusinessDeck().getRevealedTiles()){
			tileNumber = 1;
			if (tileIterator.equals(tile))
				return tileNumber;
			else
				tileNumber++;
		}
		return -1;
	}
	
	/**
	 * this method will return the listener in order to get right informations for the 
	 * complex bonus
	 * 
	 * @return listener
	 * 				   in order to get right informations for complex bonus	
	 */
	public Listener getTileListener() {
		return getTileListener();
	}
	
	/**
	 * Inner class to manage listeners
	 *
	 */
	public class Listener extends StandardButtonListener implements ActionListener {

		/**
		 * Here I manage what to do if you click on exit or tile Button
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			
			if (e.getSource().equals(exit)) {
				exit();
			}
			/*
			 * If you click on a tile you are acquiring it to 
			 */
			else {
				TileButton buttonPressed = (TileButton) e.getSource();
				tile = buttonPressed.getTile();
				ui.getMessageSender().acquireBusinessPermitTile(cardUsed, region, getTileNumber(tile));
				exit();
			}
		}
		/**
		 * @see StandardButtonListener
		 */
		@Override
		public void exit() {
			dispose();
		}
		
		/**
		 * this method is used to get Tile number for complex bonus
		 * 
		 * @return tileNumber
		 * 					which is choosen
		 */
		public int getTileNumberComplex() {
			return tileNumber;
		}
	}

}