package it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.frames;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;

import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.FrameUtilities;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.StandardButtonListener;

public class SomethingWentWrongFrame extends FrameUtilities {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final String TITLE = "Ooooops";
	private static final String INFO = "Something went wrong, try to exit and restart";

	/**
	 * Builder for a frame which is used if something went wrong
	 */
	public SomethingWentWrongFrame() {
		createFrame();
		settingMethod();
	}
	
	/**
	 * @see FrameUtilities
	 */
	@Override
	public void settingMethod() {
		settingMethodStandard(TITLE);
		this.pack();
	}

	/**
	 * Here I create a simple frame with a info in the middle and an exit button in the south position
	 */
	@Override
	public void createFrame() {
		Container framePanel = getContentPane();
		JLabel info = addLabel (INFO, framePanel);
		exit = addButton (EXIT, framePanel);
		exit.addActionListener(new Listener());
		framePanel.add(exit, BorderLayout.SOUTH);
		framePanel.add(info, BorderLayout.CENTER);
	}
	
	/**
	 * Inner class to manage listener
	 *
	 */
	private class Listener extends StandardButtonListener implements ActionListener {

		/**
		 * What to do if you click on exit button
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			
			if (e.getSource().equals(exit)) {
				exit();
			}
		}
		
		/**
		 * @see StandardButtonListener
		 */
		@Override
		public void exit() {
			dispose();
		}
	}
}
