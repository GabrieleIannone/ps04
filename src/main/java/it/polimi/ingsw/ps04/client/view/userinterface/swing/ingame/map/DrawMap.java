package it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame.map;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Random;
import java.util.Set;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import it.polimi.ingsw.ps04.client.view.userinterface.UserInterface;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.GUIGame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame.CityFrame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.component.CityButton;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.component.TileButton;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.frames.BusinessPermitTileCardFrame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.panels.DrawCouncil;
import it.polimi.ingsw.ps04.model.board.City;
import it.polimi.ingsw.ps04.model.board.Region;
import it.polimi.ingsw.ps04.model.deck.business.BusinessPermitTile;


public class DrawMap extends JComponent{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private final static int TILE_Y = 420;
	private final static int COUNCIL_Y = 360;
	
	private final static int COAST_MAX_BOUND = 315;
	private final static int COAST_MIN_BOUND = 50;
	
	private final static int HILL_MAX_BOUND = 725;
	private final static int HILL_MIN_BOUND = 450;
	
	private final static int MONTAINS_MAX_BOUND = 1150;
	private final static int MONTAINS_MIN_BOUND = 875;
	
	private final static int HEIGHT_MAX_BOUND = 275;
	private final static int HEIGHT_MIN_BOUND = 0;
	
	private final static int CITY_BUTTON_HEIGHT = 70;
	private final static int TILE_BUTTON_WIDTH = 70;
	private final static int COUNCILLOR_HEIGHT = 40;
	private final static int COUNCILLOR_WIDTH = 150;
	private final static int CITY_BUTTON_WITH_KING_WIDTH = 100;
	
	private String coast = "coast";
	private String hills = "hills";
	private String mountains = "mountains";
	private int sourceX;
	private int sourceY;
	private int targetX;
	private int targetY;
	private int x,y;
	private Random random = new Random();
	private JLabel background;
	
	private ArrayList<CityButton> cities = new ArrayList<CityButton>();
	
	private UserInterface ui;
	
	/**
	 * Map builder
	 * 
	 * @param ui 
	 * 			the UserInterface you used to build this map
	 */
	public DrawMap(UserInterface ui) {
		this.ui = ui;
		createPanel();
	}
	
	/**
	 * Here I create a panel with a background JLabel in order to separate cities in region and add background
	 * to the contentPanel
	 * 
	 */
	public void createPanel() {
		setLayout(new BorderLayout());
		background=new JLabel(new ImageIcon(GUIGame.IMAGES_PATH +"map.png"));
		background.setLayout(new BorderLayout());
		drawMap(background);
		add(background);
		background.setVisible(true);
	}
	
	/**
	 * Draw a map in three steps: first draw cities, secondary draws tiles for each region
	 * and last draw Council
	 * 
	 * @param background
	 * 					the JLabel on which you draw city and tile buttons and councils
	 */
	private void drawMap(Container background) {
		background.setLayout(null);
		for (Region region : ui.getModel().getMap().getRegions()) {
			drawCity(region);
			drawTile(region);
			drawCouncil(region);
		}
	}
	
	/**
	 * draw a council of a region and set right bounds
	 * 
	 * @param region 
	 * 				which is the council region
	 */
	private void drawCouncil(Region region) {
		DrawCouncil drawCouncil = new DrawCouncil(region.getCouncil());
		JPanel councilPanel = drawCouncil.getPanel();
		if (region.getName().equals(hills)) {
			councilPanel.setBounds(525, COUNCIL_Y, COUNCILLOR_WIDTH, COUNCILLOR_HEIGHT);
		}
		if (region.getName().equals(coast)) {
			councilPanel.setBounds(100, COUNCIL_Y, COUNCILLOR_WIDTH, COUNCILLOR_HEIGHT);
		}
		if (region.getName().equals(mountains)) {
			councilPanel.setBounds(950, COUNCIL_Y, COUNCILLOR_WIDTH, COUNCILLOR_HEIGHT);
		}
		background.add(councilPanel);
	}
	
	/**
	 * Draw Tile Button at the bottom of region for each visible tile
	 * 
	 * @param region
	 * 				which is the tile region
	 */
	private void drawTile(Region region) {
	      int tileCounter = 0;
	      for (BusinessPermitTile tile : region.getBusinessDeck().getRevealedTiles()) {
	    	  TileButton tileButton = new TileButton(background, tile);
		      if (region.getName().equals(hills)) {
				  tileButton.setBounds(525 + (100*tileCounter), TILE_Y, TILE_BUTTON_WIDTH, CITY_BUTTON_HEIGHT);
		      }
		      if (region.getName().equals(coast)) {
				  tileButton.setBounds(100 + (100*tileCounter), TILE_Y, TILE_BUTTON_WIDTH, CITY_BUTTON_HEIGHT);
		      }
		      if (region.getName().equals(mountains)) {
				  tileButton.setBounds(950 + (100*tileCounter), TILE_Y, TILE_BUTTON_WIDTH, CITY_BUTTON_HEIGHT);
		      }
			  tileCounter++;
			  tileButton.addActionListener(new TileButtonListener(tile));
			  background.add(tileButton);
	      }
	}

	/**
	 * Draw cities of a specific region
	 * 
	 * @param region
	 * 				which is the cities region
	 */
	private void drawCity(Region region) {
		for (City city :ui.getModel().getMap().getCities(region)) {
	    	  CityButton button = new CityButton (city.getName(), background, city);
	    	  button.addActionListener(new CityListener(city));
	    	  setupCoordinates(button, city.getRegion().getName());
	      	}
	}

	/**
	 * this method will set random positioning for x and y coordinates
	 * 
	 * @param button
	 * 				which is the city
	 * @param name
	 * 				of the region 
	 */
	private void setupCoordinates(CityButton button, String name) {
	  x = setRandomPositionX(name);
	  y = random.nextInt(HEIGHT_MAX_BOUND) + HEIGHT_MIN_BOUND;
	  button.setBounds(x, y, CITY_BUTTON_WITH_KING_WIDTH, CITY_BUTTON_HEIGHT);
	  while(checkCollisions(button)) {
		  setupCoordinates(button, name);
		  checkCollisions(button);
	  } 
	  cities.add(button);
	}

	/**
	 * this override paint method will draw links between cities
	 * 
	 */
	@Override
	public void paint (Graphics g) {
	/*
	 * Here i draw links between citybuttons
	 */
	super.paint(g);
		for (Region region : ui.getModel().getMap().getRegions()) { 
			drawEdges(region, g);
			}
	}
	
	/**
	 * I draw edges in this method using jgraph map edges
	 * 
	 * @param region
	 * 				 of cities I wanted to draw edges
	 * @param g
	 * 			which is the graphics I want to repaint
	 */
	private void drawEdges(Region region, Graphics g) {
		for (City city: ui.getModel().getMap().getCities(region)) {
			for (CityButton cityButton : cities) {
				/*
				 * Here i save citybutton x and y in order to link them with others city
				 */
				if (cityButton.getText().equals(city.getName())) {
					CityButton sourceCity = cityButton;
					sourceX = (int) sourceCity.getBounds().getCenterX();
					sourceY = (int) sourceCity.getBounds().getCenterY();
				}
			}
			Set<City> citiesEdge = ui.getModel().getMap().getNeighbors(city);
			for (City cityEdge: citiesEdge) {
				for (CityButton cityButton : cities) {
					/*
					 * Here i save targetCity x and y in order to link them with sourceCity
					 */
					if (cityButton.getText().equals(cityEdge.getName())) {
						targetX = (int) cityButton.getBounds().getCenterX();
						targetY = (int) cityButton.getBounds().getCenterY();
						g.setColor(Color.ORANGE);
						g.drawLine(sourceX, sourceY, targetX, targetY);
						}
					}
				}
			}
	}

	/**
	 * Set a random x for each different region
	 * 
	 * @param name
	 * 			  the region name
	 * @return x position
	 * 					 of the city
	 */
	private int setRandomPositionX(String name) {
	Random rand = new Random();
  	if (name.equals(hills)) {
  		  	x = rand.nextInt(HILL_MAX_BOUND - HILL_MIN_BOUND) + (HILL_MIN_BOUND);
	}
  	if (name.equals(coast)) {  
  		 	x = rand.nextInt(COAST_MAX_BOUND - COAST_MIN_BOUND) + (COAST_MIN_BOUND);
  	  		}
  	if (name.equals(mountains)) {
  		  	x = rand.nextInt(MONTAINS_MAX_BOUND - MONTAINS_MIN_BOUND) + (MONTAINS_MIN_BOUND );
  	}
	return x;
	}
	
	/**
	 * Will check if one button collides with others
	 * 
	 * @param button
	 * 				is the city button
	 * @return  true if there is a collision 
	 * 		 	false if there is not collision with others city buttons
	 */
	private boolean checkCollisions(CityButton button) {
		if (cities.isEmpty()) {
			return false;
		}
		for (JButton other : cities) {
				if (other.equals(button)){
					return false;
				}
				else if (button.getBounds().intersects(other.getBounds())) {
					return true;
				}
			else if (button.getBounds().intersects(other.getBounds())) {
				return true;
			}
		}
		return false;
	}
	
	public ArrayList<CityButton>  getCitiesMap () {
		return cities;
	}
	
	/**
	 * Inner class to manage city listeners
	 *
	 */
	private class CityListener implements ActionListener {

		private City city;

		public CityListener(City city) {
			this.city = city;
		}
		   
		/**
		 * Here i manage specific action performed
		 * if you click on a city button will be displayed the frame relative to it
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			new CityFrame (city, ui);	
		}
	}
		/**
		 * A second inner class to manage tile listeners
		 *
		 */
		private class TileButtonListener implements ActionListener {

			private BusinessPermitTile tile;

			/**
			 * Builder of tile listener 
			 * 
			 * @param tile
			 * 			  with is built the tile button
			 */
			public TileButtonListener(BusinessPermitTile tile) {
				this.tile = tile;
			}
			
			/**
			 * Here i manage specific action performed
			 * if you click on a tile button will be displayed the frame relative to it
			 */
			@Override
			public void actionPerformed(ActionEvent arg0) {
				new BusinessPermitTileCardFrame(tile);	
			}
		}
}
