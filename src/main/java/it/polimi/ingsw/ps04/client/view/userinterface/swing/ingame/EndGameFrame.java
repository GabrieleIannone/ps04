package it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame;

import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.FrameUtilities;
import it.polimi.ingsw.ps04.utils.message.GameFinishedMessage;

public class EndGameFrame extends FrameUtilities {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final String TITLE = "End Game";
	
	private GameFinishedMessage message;
	
	/**
	 * Builder of End game Frame which has the game end message
	 * 
	 * @param message
	 * 				 which I write in the center label
	 */
	public EndGameFrame(GameFinishedMessage message ) {
		this.message = message;
		createFrame();
		settingMethod();
	}

	/**
	 * @see FrameUtilities
	 */
	@Override
	public void settingMethod() {
		settingMethodStandard(TITLE);
		this.pack();
	}

	/**
	 * Here I create a simple frame with a label showing message content
	 */
	@Override
	public void createFrame() {		
		addLabel (message.toString(), getContentPane());
	}
	
}
