package it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import it.polimi.ingsw.ps04.client.view.userinterface.UserInterface;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.GUIGame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame.action.ChoosePoliticCardsToUseFrame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame.action.ChooseYourTileFrame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.FrameUtilities;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.StandardButtonListener;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.frames.SomethingWentWrongFrame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.panels.BonusesPanel;
import it.polimi.ingsw.ps04.model.board.City;
import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.utils.exception.NoPermitTileException;

public class CityFrame extends FrameUtilities{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final String BUILD_WITH_KING = "Build with king";
	private static final int X_SIZE = 600;
	private static final int Y_SIZE = 400;
	
	private JButton build, buildWithKing;
	private City city;
	private UserInterface ui;
	private JPanel playerCityPanel = new JPanel();
	private JPanel actionPanel = new JPanel();
	private JPanel bonusPanel = new JPanel();
	private JLabel background;

	
	/**
	 * Builder for City Frame which has reference of the city
	 * 
	 * @param city
	 * 			  reference in order to get the right information
	 * @param ui
	 * 			which I use to get information
	 */
	public CityFrame(City city, UserInterface ui) {
		this.city = city;
		this.ui = ui;
		createFrame();
		settingMethod();
	}
	
	/**
	 * @see FrameUtilities
	 */
	@Override
	public void settingMethod() {
		settingMethodStandard(city.getName());
		this.setSize(X_SIZE, Y_SIZE);
	}

	/**
	 * Here I create a frame which is a background image and to the right JLabels about the city and to the left
	 * the action you can do in this city
	 */
	@Override
	public void createFrame() {
		setLayout(new BorderLayout());
		background = new JLabel(new ImageIcon(GUIGame.IMAGES_PATH +"CityInside.jpg"));
		background.setLayout(new FlowLayout());
		addComponentsToPlayersCityPanel();
		addComponentsToActionPanel();
		background.add(actionPanel);
		background.add(playerCityPanel);
		add(background);
	}
	
	/**
	 * Here I put the action you can do like build, build with king and exit and if the king is in this city
	 */
	private void addComponentsToActionPanel() {
		actionPanel.setLayout(new BoxLayout (actionPanel, BoxLayout.Y_AXIS));
		if (city.equals(ui.getModel().getMap().getKingPosition())) {
			addLabel ("All Hail! King is here!", actionPanel);
		} 
		addLabel ("Actions: ", actionPanel);
		build = addButton ("Build emporium", actionPanel);
		build.addActionListener(new Listener());
		buildWithKing = addButton ("Build emporium with king", actionPanel);
		buildWithKing.addActionListener(new Listener());
		exit = addButton ("exit", actionPanel);
		exit.addActionListener(new Listener());
	}

	/**
	 * Here I write city name and who has built here using JLabels
	 */
	private void addComponentsToPlayersCityPanel() {
		playerCityPanel.setLayout(new BoxLayout (playerCityPanel, BoxLayout.Y_AXIS));
		addLabel ("Welcome in: " + city.getName(), playerCityPanel);
		/*
		 * Here i add bonuses of the city to city if it has
		 */
		if (city.getBonus() != null) {
			addLabel ("City bonuses: ", playerCityPanel);
			BonusesPanel bonus = new BonusesPanel(city.getBonus());
			bonusPanel = bonus.getPanel();
			playerCityPanel.add(bonusPanel);
		}
		addLabel ("Players who has build here are: ", playerCityPanel);
		for (Player playerHasBuilt : ui.getModel().getPlayersManager().getActivePlayers()) {
			if (ui.getModel().getMap().getCityReference(city).playerHasBuilt(playerHasBuilt))
				addLabel (playerHasBuilt.getName(), playerCityPanel);
		}
	}

	/**
	 * Inner class to manage actionlistener
	 *
	 */
	private class Listener extends StandardButtonListener implements ActionListener {
	
		/**
		 * Here I manage what to do when you click on build, build with king and exit button
		 */
	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource().equals(build)) {
			build();
		}

		else if (e.getSource().equals(buildWithKing)) {
			buildWithKing();
		}
		
		else if (e.getSource().equals(exit)) {
			exit();
		}
	}

	/**
	 * Here I manage what to do if you want to build
	 */
	private void build() {
		try {
			new ChooseYourTileFrame (ui, city);
		} catch (NoPermitTileException e) {
			new SomethingWentWrongFrame();
		}
		exit();
	}
	
	/**
	 * Here I manage what to do if you want to build with king
	 */
	private void buildWithKing() {
		new ChoosePoliticCardsToUseFrame(ui, null, BUILD_WITH_KING, city);
		exit();
	}
	
	/**
	 * @see StandardButtonListener
	 */
	@Override
	public void exit() {
		dispose();
	}
	}
}
