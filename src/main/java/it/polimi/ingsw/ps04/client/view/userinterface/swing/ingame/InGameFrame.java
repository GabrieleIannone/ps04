package it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.*;

import it.polimi.ingsw.ps04.client.view.userinterface.UserInterface;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.GUIGame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame.map.DrawMap;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame.map.PlayerKingPanel;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame.map.ResourcesPanel;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.FrameUtilities;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.StandardButtonListener;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.component.CityButton;

public class InGameFrame extends FrameUtilities {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final String TITLE = "Council of four";

	private GUIGame gg;
	private DrawMap centralPanelMap;
	private ResourcesPanel southPanelResources;
	private PlayerKingPanel centerPlayersPanel;
	private UserInterface ui;
	private JPanel southPanel;
	private JPanel playersPanel;
	private JMenuBar menu = new JMenuBar();
	private JMenuItem exitMenu;
	private Container framePanel = getContentPane();
	
	private ArrayList<CityButton> cities;

	/**
	 * Builder of the main frame in game that shows map, resources, king, buttons like action, statistics, 
	 * ranking
	 * 
	 * @param drawMap 
	 * 				 which I use to draw map in the center
	 * 
	 */
	public InGameFrame(GUIGame gg, DrawMap drawMap){
		this.gg = gg;
		this.ui = gg;
		this.centralPanelMap = drawMap;
		gg.setInGameFrame(this);
		buildModel();
	}
	
	/**
	 * I use this method to update GUI after an action
	 */
	public void buildModel(){
		createFrame();
		settingMethod();
	}
	
	/**
	 * @see FrameUtilities, added a size and a menu bar
	 */
	@Override
	public void settingMethod() {
		this.setSize(1550, 1000);
		this.setJMenuBar(menu);
		settingMethodStandard(TITLE);
	}
	
	/**
	 * Here I crete frame with an upper menu and set central map
	 */
	@Override
	public void createFrame() {
		framePanel.setLayout(new FlowLayout());
		exitMenu = new JMenuItem(EXIT);
		sets(exitMenu);
		exitMenu.addActionListener(new Listener());
		menu.add(exitMenu);
		cities = centralPanelMap.getCitiesMap();
		framePanel.add(centralPanelMap);
		addComponentsToMainPanel();
	}	

	/**
	 * Here I add infoPanel to frame. Info panel includes other panels that show player resources, buttons, 
	 * king council and where he is players button and your points. 
	 * 
	 */
	public void addComponentsToMainPanel() {
		JPanel infoPanel = new JPanel();
		infoPanel.setLayout(new BorderLayout());
		southPanelResources = new ResourcesPanel(ui);
		centerPlayersPanel = new PlayerKingPanel(gg);
		setIconToButtons();
		playersPanel = centerPlayersPanel.getPlayerKingPanel();
		southPanel = southPanelResources.getResourcesPanel();
		infoPanel.add(southPanel, BorderLayout.EAST);
		infoPanel.add(playersPanel, BorderLayout.CENTER);
		framePanel.add(infoPanel);
		southPanelResources.validate();
		centerPlayersPanel.validate();
	}
	
	/**
	 * Here I set city button Icon, if king moves then the icon will be updated
	 */
	public void setIconToButtons() {
		for (CityButton cityButton: cities) {
			if (cityButton.getText().equals(ui.getModel().getMap().getKingPosition().getName()))
				cityButton.setIcon(new ImageIcon(GUIGame.IMAGES_PATH + "city_small_with_King.png"));
			else 
				cityButton.setIcon(new ImageIcon(GUIGame.IMAGES_PATH + "city_small.png"));
		}
	}

	/**
	 * Here I set component font and foreground
	 */
	@Override
	public void sets(JComponent component) {
		component.setFont(new Font("Serif", Font.ITALIC, 20));
		component.setForeground(Color.RED);
	}

	/**
	 * Inner class to manage listeners
	 *
	 */
	private class Listener extends StandardButtonListener implements ActionListener {
		
		/**
		 * Manage to exit if you click on the upper menu 
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			
			if (e.getSource().equals(exitMenu)) {		
				exit();
			}
		}	
		
		/**
		 * @see StandardButtonListener
		 */
		@Override
		public void exit() {
			dispose();
		}
	}
}