package it.polimi.ingsw.ps04.client.view;

import java.util.List;
import java.util.Map;

import it.polimi.ingsw.ps04.client.view.connection.Connection;
import it.polimi.ingsw.ps04.model.action.Action;
import it.polimi.ingsw.ps04.model.action.PassTurn;
import it.polimi.ingsw.ps04.model.action.main.*;
import it.polimi.ingsw.ps04.model.action.quick.*;
import it.polimi.ingsw.ps04.model.board.City;
import it.polimi.ingsw.ps04.model.board.Councillor;
import it.polimi.ingsw.ps04.model.board.Region;
import it.polimi.ingsw.ps04.model.bonus.complex.ComplexBonus;
import it.polimi.ingsw.ps04.model.deck.business.BusinessPermitTile;
import it.polimi.ingsw.ps04.model.deck.politic.PoliticCard;
import it.polimi.ingsw.ps04.model.market.BuyingOrder;
import it.polimi.ingsw.ps04.model.market.SellingOffers;
import it.polimi.ingsw.ps04.utils.message.ActionMessage;
import it.polimi.ingsw.ps04.utils.message.ModelRequestMessage;
import it.polimi.ingsw.ps04.utils.message.RequestforBonus;
import it.polimi.ingsw.ps04.utils.message.login.DeleteUserMessage;
import it.polimi.ingsw.ps04.utils.message.login.LoginMessage;
import it.polimi.ingsw.ps04.utils.message.login.SignupMessage;
import it.polimi.ingsw.ps04.utils.message.market.BuyingOrdersMessage;
import it.polimi.ingsw.ps04.utils.message.market.SellingOffersMessage;
import it.polimi.ingsw.ps04.utils.message.market.SkipBuyMessage;
import it.polimi.ingsw.ps04.utils.message.statistics.RankingMessage;
import it.polimi.ingsw.ps04.utils.message.statistics.PlayerStatisticsMessage;

/**
 * this class sends the messages from the client to the server
 */
public class MessageSender {

	protected Connection connection;

	/**
	 * constructs a message sender with a given connection
	 * 
	 * @param connection
	 *            the connection that you want to send the messages
	 */
	public MessageSender(Connection connection) {
		this.connection = connection;
	}

	/**
	 * asks to the connection to send a login request
	 * 
	 * @param username
	 *            the username of the player
	 * @param password
	 *            the password of the player
	 * @return true if the user logged in
	 */
	public boolean login(String username, String password) {
		LoginMessage loginMessage = new LoginMessage(username, password);
		return connection.sendCredentialsMessage(loginMessage);
	}

	/**
	 * asks to the connection to send a sign up request
	 * 
	 * @param username
	 *            the username of the player
	 * @param email
	 *            the email of the player
	 * @param password
	 *            the password of the player
	 * @return true if the sign up was successful
	 */
	public boolean signup(String username, String email, String password) {
		SignupMessage signupMessage = new SignupMessage(username, email, password);
		return connection.sendCredentialsMessage(signupMessage);
	}

	/**
	 * asks to the connection to send a delete user request
	 * 
	 * @param username
	 *            the username of the player
	 * @param password
	 *            the password of the player
	 * @return true if the user was deleted
	 */
	public boolean deleteUser(String username, String password) {
		DeleteUserMessage deleteMessage = new DeleteUserMessage(username, password);
		return connection.sendCredentialsMessage(deleteMessage);
	}

	/**
	 * asks to the connection to send some selling offers
	 * 
	 * @param offers
	 *            the selling offers that you want to send
	 */
	public void sell(SellingOffers offers) {
		SellingOffersMessage offerMessage = new SellingOffersMessage(offers);
		connection.sendMessage(offerMessage);
	}

	/**
	 * asks to the connection to send an action
	 * 
	 * @param action
	 *            the action that you want to send
	 */
	private void sendAction(Action action) {
		ActionMessage actionMessage = new ActionMessage(action);
		connection.sendMessage(actionMessage);
	}

	/**
	 * asks to the connection to perform the action "AcquireBusinessPermitTile"
	 * 
	 * @param usedCards
	 *            the cards that the player wants to use
	 * @param region
	 *            the region of the business permit tile deck that contains the
	 *            tile that you want to obtain
	 * @param businessTileNumber
	 *            the number of the business tile
	 */
	public void acquireBusinessPermitTile(List<PoliticCard> usedCards, Region region, int businessTileNumber) {
		sendAction(new AcquireBusinessPermitTile(usedCards, region, businessTileNumber));
	}

	/**
	 * asks to the connection to perform the action "BuildEmporium"
	 * 
	 * @param city
	 *            the city where you want to build
	 * @param tile
	 *            the tile that you want to use to build
	 */
	public void buildEmporium(City city, BusinessPermitTile tile) {
		sendAction(new BuildEmporium(city, tile));
	}

	/**
	 * asks to the connection to perform the action "BuildEmporiumWithKing"
	 * 
	 * @param usedCards
	 *            the cards that the player wants to use
	 * @param city
	 *            the city where you want to build
	 */
	public void buildEmporiumWithKing(List<PoliticCard> usedCards, City city) {
		sendAction(new BuildEmporiumWithKing(city, usedCards));
	}

	/**
	 * asks to the connection to perform the action "ElectCouncillor"
	 * 
	 * @param region
	 *            the region that contains the council or null if it is the
	 *            council of the king
	 * @param councillor
	 *            the councillor that you want to elect
	 */
	public void electCouncillor(Region region, Councillor councillor) {
		sendAction(new ElectCouncillor(region, councillor));
	}

	/**
	 * asks to the connection to perform the action "ChangeBusinessPermitTiles"
	 * 
	 * @param region
	 *            the region of the business permit tile deck that contains the
	 *            tile that you want to change
	 */
	public void changeBusinessPermitTiles(Region region) {
		sendAction(new ChangeBusinessPermitTiles(region));
	}

	/**
	 * asks to the connection to perform the action "EngageAssistant"
	 */
	public void engageAssistant() {
		sendAction(new EngageAssistant());
	}

	/**
	 * asks to the connection to perform the action "GetAnotherPrimaryAction"
	 */
	public void getAnotherPrimaryAction() {
		sendAction(new GetAnotherPrimaryAction());
	}

	/**
	 * asks to the connection to perform the action
	 * SendAssistantToElectCouncillor
	 * 
	 * @param region
	 *            the region that contains the council or null if it is the
	 *            council of the king
	 * @param councillor
	 *            the councillor that you want to elect
	 */
	public void sendAssistantToElectCouncillor(Region region, Councillor councillor) {
		sendAction(new SendAssistantToElectCouncillor(region, councillor));
	}

	/**
	 * asks to the connection to pass the turn of the player
	 */
	public void passTurn() {
		sendAction(new PassTurn());
	}

	/**
	 * asks to the connection to send the compiled complex bonus
	 * 
	 * @param bonus
	 *            the compiled complex bonus
	 */
	public void sendCompiledComplexBonus(ComplexBonus bonus) {
		connection.sendMessage(new RequestforBonus(bonus));
	}

	/**
	 * asks to the connection to send the ranking request
	 */
	public void ranking() {
		connection.sendMessage(new RankingMessage());
	}

	/**
	 * asks to the connection to retrieve the statistics of a given user
	 * 
	 * @param player
	 *            the user that you want to know the statistics
	 */
	public void statistics(String player) {
		connection.sendMessage(new PlayerStatisticsMessage(player));

	}

	/**
	 * asks to the connection to retrieve the model
	 */
	public void requestModel() {
		connection.sendMessage(new ModelRequestMessage());
	}

	/**
	 * asks to the connection to send the buying orders of the player
	 * 
	 * @param orders
	 *            the buying orders of the player
	 */
	public void buy(Map<String, BuyingOrder> orders) {
		BuyingOrdersMessage ordersMessage = new BuyingOrdersMessage(orders);
		connection.sendMessage(ordersMessage);
	}

	/**
	 * asks to the connection to send a skip buy message
	 */
	public void skipBuy() {
		connection.sendMessage(new SkipBuyMessage());
	}

	/**
	 * asks to the connection to send an empty selling offers message
	 */
	public void skipSell() {
		connection.sendMessage(new SellingOffersMessage());
	}

}
