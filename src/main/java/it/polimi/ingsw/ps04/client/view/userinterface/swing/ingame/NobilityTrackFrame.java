package it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;
import java.util.Set;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import it.polimi.ingsw.ps04.client.view.userinterface.UserInterface;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.GUIGame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.FrameUtilities;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.StandardButtonListener;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.frames.ShowBonusNobilityTrackFrame;
import it.polimi.ingsw.ps04.model.bonus.Bonus;
import it.polimi.ingsw.ps04.model.nobilitytrack.NobilityTrack;

public class NobilityTrackFrame extends FrameUtilities {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8454682450662274172L;
	
	private static final String TITLE = "Nobility Path";
	private static final int NUM_ROW = 2;
	private static final int NUM_COLUMS = 10;
	private static final int NOBILITY_LEVEL_MAX = 20;
	
	private NobilityTrack nobility;
	private Map<Integer, Set<Bonus>> track;
	private JPanel nobilityTrackPanel = new JPanel();
	private UserInterface ui;
	
	/**
	 * Builder of the Nobility track frame with the nobility track from which I will get all informations about it
	 * and the ui that I need to display player nobility point
	 * 
	 * @param nobility
	 * 				  from which I will get all informations about nobility track
	 * @param ui
	 * 			which I use to display player nobility point
	 */
	public NobilityTrackFrame(NobilityTrack nobility, UserInterface ui) {
		this.nobility = nobility;
		this.ui = ui;
		createFrame();
		settingMethod();
	}
	
	/**
	 * @see FrameUtilities
	 */
	@Override
	public void settingMethod() {
		settingMethodStandard(TITLE);
		this.pack();
	}

	/**
	 * Here I create a frame with in south position exit button, information in north side and in the center the
	 * nobility track
	 */
	@Override
	public void createFrame() {
		Container framePanel = getContentPane();
		framePanel.setLayout(new BorderLayout());
		track = nobility.getTrack();
		JLabel info = addLabel ("This is nobility track, you are in the coloured step", framePanel);
		exit = addButton ("exit", framePanel);
		exit.addActionListener(new Listener());
		addComponentToNobilityPanel();
		framePanel.add(info, BorderLayout.NORTH);
		framePanel.add(nobilityTrackPanel, BorderLayout.CENTER);
		framePanel.add(exit, BorderLayout.SOUTH);	
	}
	
	/**
	 * Here I add component to Nobility Panel adding all buttons with, as text, the number of each step, if a step
	 * has some bonuses it will be shown with the nobility point image, your nobility level is displayed with
	 * the yellow button
	 */
	private void addComponentToNobilityPanel() {
		nobilityTrackPanel.setLayout(new GridLayout(NUM_ROW, NUM_COLUMS));
		for (int step = 0; step <=  NOBILITY_LEVEL_MAX; step++) {
			JButton stepButton = new JButton(String.valueOf(step));
			if (track.get(step) != null) {
				stepButton.setIcon(new ImageIcon(GUIGame.IMAGES_PATH +"nobilitypoints.png"));
				stepButton.addActionListener(new Listener());
			}
			if (step == ui.getClientPlayer().getNobilityLevel().getLevel()) {
				stepButton.setBackground(Color.YELLOW);
			}
			nobilityTrackPanel.add(stepButton);
		}
	}

	/**
	 * Inner class to manage listener
	 *
	 */
	private class Listener extends StandardButtonListener implements ActionListener {

		/**
		 * Here I manage what to do if you click a button
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
		
		int stepClicked;
	
		/**
		 * Here I manage what to do if you click exit button or a step in the nobility track
		 */
		if (e.getSource().equals(exit)) {
			exit();
		}
			else {
				JButton buttonPressed = (JButton) e.getSource();
				stepClicked = Integer.valueOf(buttonPressed.getText());
				new ShowBonusNobilityTrackFrame(stepClicked, track);
			}
		}	
		/**
		 * @see StandardButtonListener
		 */
		@Override
		public void exit() {
			dispose();
		}
	}
}
