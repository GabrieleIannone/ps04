package it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.frames;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

import it.polimi.ingsw.ps04.client.view.userinterface.UserInterface;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.FrameUtilities;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.StandardButtonListener;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.panels.BonusesPanel;
import it.polimi.ingsw.ps04.model.board.Region;
import it.polimi.ingsw.ps04.utils.color.CityColor;

public class BonusesFrame extends FrameUtilities {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8348758479177305117L;

	private static final String TITLE = "Available Bonuses";
	private static final int NUM_ROW = 10;
	private static final int NUM_COLUMNS = 2;
	
	private UserInterface ui;
	private JPanel bonusPanel = new JPanel();
	private JPanel buttonPanel = new JPanel();

	/**
	 * Builder for BonusesFrame which has UserInterface in order to get waiting bonuses
	 * 
	 * @param ui
	 * 			which I use to get information about waiting bonuses
	 */
	public BonusesFrame(UserInterface ui) {
		this.ui = ui;
		createFrame();
		settingMethod();
	}
	
	/**
	 * @see FrameUtilities
	 */
	@Override
	public void settingMethod() {
		settingMethodStandard(TITLE);
		this.pack();
	}

	/**
	 * Here I create a simple frame with a central panel showing bonuses from regions and exit button in the south 
	 * position
	 */
	@Override
	public void createFrame() {
		Container framePanel = getContentPane();
		framePanel.setLayout(new BoxLayout(framePanel, BoxLayout.Y_AXIS));
		drawBonuses();
		addButton();
		framePanel.add(bonusPanel, BorderLayout.CENTER);
		framePanel.add(buttonPanel, BorderLayout.SOUTH);
	}

	/**
	 * Here I add exit button 
	 */
	private void addButton() {
		exit = addButton (EXIT, buttonPanel);
		exit.addActionListener(new Listener());
	}

	/**
	 * Here I add bonuses to central panel
	 */
	private void drawBonuses() {
		bonusPanel.setLayout(new GridLayout (NUM_ROW, NUM_COLUMNS));
		addLabel ("Available bonuses from build in all city colored: ", bonusPanel);
		addLabel ("", bonusPanel);
		for (CityColor cityColor :ui.getModel().getCityColors()) {
			addLabel (cityColor.getName() ,bonusPanel);
			BonusesPanel bonuses = new BonusesPanel(cityColor.getBonus());
			JPanel newBonus = bonuses.getPanel();
			bonusPanel.add(newBonus);
		}
		addLabel ("Available bonuses from build in all region city: ", bonusPanel);
		addLabel ("", bonusPanel);
		for (Region region :ui.getModel().getMap().getRegions()) {
			addLabel (region.getName() ,bonusPanel);
			BonusesPanel bonuses = new BonusesPanel(region.getBonuses());
			JPanel newBonus = bonuses.getPanel();
			bonusPanel.add(newBonus);
		}
	}
	
	/**
	 * Inner class to manage listener
	 *
	 */
	private class Listener extends StandardButtonListener implements ActionListener {

		/**
		 * Manage what to do if you click on exit button
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			
			if (e.getSource().equals(exit)) {
				exit();
			}
		}
	
		/**
		 * @see StandardButtonListener
		 */
		@Override
		public void exit() {
			dispose();
		}
	}
}
