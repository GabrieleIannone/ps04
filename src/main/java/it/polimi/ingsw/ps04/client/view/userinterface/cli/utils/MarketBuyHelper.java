package it.polimi.ingsw.ps04.client.view.userinterface.cli.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import it.polimi.ingsw.ps04.model.deck.business.BusinessPermitTile;
import it.polimi.ingsw.ps04.model.deck.politic.PoliticCard;
import it.polimi.ingsw.ps04.model.market.BusinessPermitTileonSale;
import it.polimi.ingsw.ps04.model.market.BuyingOrder;
import it.polimi.ingsw.ps04.model.market.PoliticCardonSale;
import it.polimi.ingsw.ps04.model.market.SellingOffers;
import it.polimi.ingsw.ps04.model.player.AssistantsCrew;
import it.polimi.ingsw.ps04.utils.string.StringHelper;

/**
 * it has same functions to help to take the input from the user for the buy
 * phase of the market
 *
 */
public class MarketBuyHelper {

	Map<String, SellingOffers> offers;
	Map<String, BuyingOrder> orders = new HashMap<>();
	Scanner scanner;

	/**
	 * constructs a market buy helper with a given offers and scanner
	 * 
	 * @param offers
	 *            the offers of the sell phase
	 * @param scanner
	 *            the scanner where the function reads the input
	 */
	public MarketBuyHelper(Map<String, SellingOffers> offers, Scanner scanner) {
		this.offers = offers;
		this.scanner = scanner;
	}

	/**
	 * adds assistants to the order
	 * 
	 * @param player
	 *            the owner of the order
	 * @param order
	 *            the buying order
	 */
	private void addAssistantsToOrder(String player, BuyingOrder order) {
		System.out.println("number of assistants: ");
		int assistantsNumber = scanner.nextInt();
		scanner.nextLine();
		AssistantsCrew assistantsCrewToBuy = new AssistantsCrew(assistantsNumber);
		order.setAssistants(assistantsCrewToBuy);
	}

	/**
	 * adds business permit tiles to the order
	 * 
	 * @param player
	 *            the owner of the order
	 * @param order
	 *            the buying order
	 */
	private void addPermitTilesToOrder(String player, BuyingOrder order) {
		List<BusinessPermitTileonSale> tilesOnSale = offers.get(player).getTilesonSale();
		if (!tilesOnSale.isEmpty()) {
			int tileNumber;
			do {
				System.out.println("select a business permit tile:");
				System.out.println(StringHelper.getEnumeratedObjectsString(tilesOnSale));
				tileNumber = scanner.nextInt();
				scanner.nextLine();
			} while (tileNumber < 1 || tileNumber > tilesOnSale.size());
			BusinessPermitTile selectedTile = tilesOnSale.get(tileNumber - 1).getTile();
			order.addTile(selectedTile);
		} else {
			System.out.println("there are not tiles on sale");
		}
	}

	/**
	 * adds politic cards to the order
	 * 
	 * @param player
	 *            the owner of the order
	 * @param order
	 *            the buying order
	 */
	private void addPoliticCardsToOrder(String player, BuyingOrder order) {

		List<PoliticCardonSale> cardsOnSale = offers.get(player).getCardsonSale();
		if (!cardsOnSale.isEmpty()) {
			int cardNumber;
			do {
				System.out.println("select a cards:");
				System.out.println(StringHelper.getEnumeratedObjectsString(cardsOnSale));
				cardNumber = scanner.nextInt();
				scanner.nextLine();
			} while (cardNumber < 1 || cardNumber > cardsOnSale.size());
			PoliticCard selectedCard = cardsOnSale.get(cardNumber - 1).getCard();
			order.addCard(selectedCard);
		} else {
			System.out.println("there are not cards on sale");
		}

	}

	/**
	 * allows the user to compile the orders
	 * 
	 * @return the orders compiled by the user
	 */
	public Map<String, BuyingOrder> compileOrders() {
		int choice;
		while (true) {
			System.out.println("insert player: ");
			String player = scanner.nextLine();
			BuyingOrder order = new BuyingOrder();
			while (true) {
				do {
					System.out.println("choose what to buy from " + player + ":");
					System.out.println("1. assistants\n2. business permit tile\n3. politic card\n4. nothing");
					choice = scanner.nextInt();
					scanner.nextLine();
				} while (choice != 1 && choice != 2 && choice != 3 && choice != 4);
				switch (choice) {
				case 1:
					addAssistantsToOrder(player, order);
					break;
				case 2:
					addPermitTilesToOrder(player, order);
					break;
				case 3:
					addPoliticCardsToOrder(player, order);
					break;
				case 4:
					break;
				}
				orders.put(player, order);
				if (!InputHelper.askYesNo(scanner, "do you want to buy more from " + player + "?")) {
					break;
				}
			}
			if (!InputHelper.askYesNo(scanner, "do you want to buy more?")) {
				System.out.println("orders:" + orders);
				return orders;
			}
		}
	}
}
