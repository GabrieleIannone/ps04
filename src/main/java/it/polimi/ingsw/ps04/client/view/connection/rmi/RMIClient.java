package it.polimi.ingsw.ps04.client.view.connection.rmi;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import javax.security.auth.login.LoginException;

import it.polimi.ingsw.ps04.client.view.connection.Connection;
import it.polimi.ingsw.ps04.utils.message.Message;
import it.polimi.ingsw.ps04.utils.message.login.CredentialsMessage;
import it.polimi.ingsw.ps04.view.rmi.RMIGameInterface;
import it.polimi.ingsw.ps04.view.rmi.RMIViewInterface;

/**
 * This class represents the RMI connection used by the client. This class
 * manage only the sending of a message.
 * 
 * @see ClientRMIReceiver for the reception of a message.
 */
public class RMIClient extends Connection {

	private String serverAddress;
	private int port;
	private ClientRMIReceiver rmireceiver;
	private RMIViewInterface serverStub;
	private RMIGameInterface initialServerStub;

	/**
	 * Constructor. It creates a default connection using the local IP.
	 */
	public RMIClient() {
		this("127.0.01", 52365);
	}

	/**
	 * Constructor. Sets the server IP and port.
	 * 
	 * @param serverAddress
	 *            The server IP.
	 * @param port
	 *            The server port.
	 */
	public RMIClient(String serverAddress, int port) {
		this.serverAddress = serverAddress;
		this.port = port;
	}

	/**
	 * Sets the server stub with the one passed by parameter.
	 * 
	 * @param serverStub
	 *            The server stub.
	 */
	protected void setServerStub(RMIViewInterface serverStub) {
		this.serverStub = serverStub;
	}

	/**
	 * Sends a message to the server stub saved in this class.
	 * 
	 * @param message
	 *            The message to send to the server.
	 */
	@Override
	public void sendMessage(Message message) {
		Thread senderMessageThread = new Thread(new Runnable() {
			public void run() {
				try {
					serverStub.receiveMessage(message);
				} catch (RemoteException e) {
					e.printStackTrace();
				}
			}
		});
		senderMessageThread.start();
		
	}

	/**
	 * Starts a connection with server. Called at the beginning to start the
	 * connection with server looking for a RMIGameInterface with the default
	 * name game.
	 * 
	 * @see RMIGameInterface
	 */
	@Override
	public void startConnection() {
		Registry registry;
		try {
			registry = LocateRegistry.getRegistry(serverAddress, port);
			initialServerStub = (RMIGameInterface) registry.lookup("game");
			rmireceiver = new ClientRMIReceiver(this);
		} catch (RemoteException | NotBoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Sends a credential message. Used for login.
	 */
	@Override
	public boolean sendCredentialsMessage(CredentialsMessage credentialsMessage) {
		try {
			return initialServerStub.registerClient(credentialsMessage, rmireceiver);
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		} catch (LoginException e) {
			notify(e);
			return false;
		}
	}
}