package it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame.action;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

import it.polimi.ingsw.ps04.client.view.userinterface.UserInterface;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.GUIGame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.FrameUtilities;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.ImageColorReplacer;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.StandardButtonListener;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.frames.SomethingWentWrongFrame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.panels.DrawCouncil;
import it.polimi.ingsw.ps04.model.board.Councillor;
import it.polimi.ingsw.ps04.model.board.Region;
import it.polimi.ingsw.ps04.utils.color.NamedColor;

public class ChooseCouncillorFrame extends FrameUtilities {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3208593952288935128L;
	
	private static final String TITLE = "Choose Councillor";
	private static final String INFO_COUNCILLOR = "Choose councillor to add: ";
	private static final String COUNCILLOR_IMAGE_PATH = GUIGame.IMAGES_PATH + "councillor.png";
	private static final int X_SIZE = 600;
	private static final int Y_SIZE = 300;
	
	private Region region;
	private UserInterface ui;
	private JPanel buttonPanel= new JPanel();
	private JPanel councilPanel= new JPanel();
	private JPanel councillorPanel = new JPanel();
	private DrawCouncil drawCouncil;
	private Councillor councillorChoosen;
	private ArrayList <JButton> councillorArrayButton = new ArrayList<>();
	private JPanel councillorElegiblePanel = new JPanel();
	private Boolean mainAction;
	
	private JButton done;
	private Boolean kingAction;

	/**
	 * Builder for Choose Councillor Frame which has UserInterface to send action, the region of
	 * which I want to draw council, a boolean that means if the action I want to do is main or not
	 * another boolean which rapresents if the action is related to king
	 * 
	 * @param ui
	 * 			which is used to send action
	 * @param region
	 * 				of which I want to draw council
	 * @param mainAction
	 * 					which is a boolean if the action I want to do is main or quick
	 * @param kingAction
	 * 					which is a boolean if the action is for king council or not
	 */
	public ChooseCouncillorFrame(UserInterface ui, Region region, Boolean mainAction, Boolean kingAction) {
		this.kingAction = kingAction;
		this.mainAction = mainAction;
		this.ui = ui;
		this.region = region;
		createFrame();
		settingMethod();
	}
	
	/**
	 * @see FrameUtilities
	 */
	@Override
	public void settingMethod() {
		settingMethodStandard(TITLE);
		this.setSize(X_SIZE,Y_SIZE);
	}

	/**
	 * Here I create the frame which has an info label in top position, council drawn and the list
	 * of the all elegibles councillor in the middle and exit button in south position
	 */
	@Override
	public void createFrame() {
		/*
		 * If is a king action then draw king council else draw region council
		 */
		if (kingAction) {
			drawCouncil = new DrawCouncil(ui.getModel().getMap().getKing().getCouncil());
		}
		else {
			drawCouncil = new DrawCouncil(ui.getModel().getMap().getRegionReference(region).getCouncil());
		}
		councilPanel = drawCouncil.getPanel();
		councillorPanel.setLayout(new BoxLayout(councillorPanel, BoxLayout.Y_AXIS));
		addLabel (INFO_COUNCILLOR ,councillorPanel);
		addCouncillorsElegibles();
		addButtonPanel();
		councillorPanel.add(councillorElegiblePanel);
		add(buttonPanel, BorderLayout.SOUTH);
		add(councilPanel, BorderLayout.NORTH);
		add(councillorPanel, BorderLayout.CENTER);
	}
	
	/**
	 * Here i create exit button
	 */
	private void addButtonPanel() {
		buttonPanel = new JPanel();
		exit = addButton (EXIT, buttonPanel);
		exit.addActionListener(new Listener());
	}

	/**
	 * Here I draw a councillor button for each corruption color there is in configuration file
	 */
	private void addCouncillorsElegibles () {
		for (NamedColor color : ui.getModel().getPoliticDeck().getCorruptionColors()) {
			try {
				JButton button;
				button = new JButton(new ImageIcon(
						ImageColorReplacer.changeColor(new File(COUNCILLOR_IMAGE_PATH), color)));
				button.setText(color.getName());
				councillorArrayButton.add(button);
				button.addActionListener(new Listener());
				councillorElegiblePanel.add(button);
			} catch (IOException e) {
				new SomethingWentWrongFrame();
			}
		}
	}

	/**
	 * Inner class to manage listeners
	 * 
	 */
	private class Listener extends StandardButtonListener implements ActionListener {

		/**
		 * Here I manage what to do if you click on exit, done or a councillor button
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			
			if (e.getSource().equals(exit)) {
				exit();
			}
			else if (e.getSource().equals(done)) {
				done();
			}
			/*
			 * Here I elect the new councillor
			 */
			else {
				for (JButton button : councillorArrayButton) {
					if (e.getSource().equals(button)) {
						for (NamedColor color : ui.getModel().getPoliticDeck().getCorruptionColors()) {
							if (color.getName().equals(button.getText())) {
								councillorChoosen = new Councillor(color);
								done();		
							}
						}
					}
				}
			}
		}
		
		/**
		 * Here I manage what to do if you click done button: if you want to do a main action
		 * then will send elect councillor else will send a quick action
		 */
		private void done() {
			if (mainAction) {
				ui.getMessageSender().electCouncillor(region, councillorChoosen);
			}
			else  {
				ui.getMessageSender().sendAssistantToElectCouncillor(region, councillorChoosen);
			}
			exit();
		}
		
		/**
		 * @see StandardButtonListener
		 */
		@Override
		public void exit() {
			dispose();
		}		
	}
}
