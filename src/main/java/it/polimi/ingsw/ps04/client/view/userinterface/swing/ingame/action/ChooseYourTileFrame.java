package it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame.action;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import it.polimi.ingsw.ps04.client.view.userinterface.UserInterface;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.FrameUtilities;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.StandardButtonListener;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.component.TileButton;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.frames.WrongInformationFrame;
import it.polimi.ingsw.ps04.model.board.City;
import it.polimi.ingsw.ps04.model.deck.business.BusinessPermitTile;

public class ChooseYourTileFrame extends FrameUtilities {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1272979964237519730L;
	
	private static final String TITLE = "Choose tile for build";
	private static final int X_SIZE = 400;
	private static final int Y_SIZE = 300;
	
	private JPanel buttonPanel = new JPanel();
	private JPanel tilePanel = new JPanel();
	private UserInterface ui;
	private BusinessPermitTile tile;
	private City city;

	/**
	 * Builder for ChooseYourTileFrame which has UserInterface to send action and the city where I
	 * want to build
	 * 
	 * @param ui
	 * 			which is used to send action
	 * @param city
	 * 			  where I want to build
	 */
	public ChooseYourTileFrame(UserInterface ui, City city) {
		this.city = city;
		this.ui = ui;
		createFrame();
		settingMethod();
	}
	
	/**
	 * @see FrameUtilities
	 */
	@Override
	public void settingMethod() {
		settingMethodStandard(TITLE);
		setSize(X_SIZE, Y_SIZE);
	}

	/**
	 * Here I create frame which has button Panel in the south position and tile Panel in the 
	 * center
	 */
	@Override
	public void createFrame() {
		Container framePanel = getContentPane();
		framePanel.setLayout(new BorderLayout());
		addComponentToButtonPanel();
		addComponentToTilePanel();
		framePanel.add(buttonPanel, BorderLayout.SOUTH);
		framePanel.add(tilePanel, BorderLayout.CENTER);
	}
	
	/**
	 * Here I add tiles, as buttons, which I am able to use to tile Panel
	 */
	private void addComponentToTilePanel() {
		tilePanel.setLayout(new FlowLayout());
		for (BusinessPermitTile tile : ui.getClientPlayer().getBusinessPermitTilePool().getPermitTiles()) {
			TileButton button = new TileButton(tilePanel, tile);
			button.addActionListener(new Listener());
			/*
			 * If I used this tile then I don't see it
			 */
			if (tile.isFaceUp()) {
				button.setVisible(false);
			}
			if (tile.isFaceUp()) {
				button.setVisible(true);
			}
		}
	}

	/**
	 * Here I add done and exit buttons to Button Panel
	 */
	private void addComponentToButtonPanel() {
		buttonPanel.setLayout(new FlowLayout());
		done = addButton (DONE, buttonPanel);
		done.addActionListener(new Listener());
		exit = addButton (EXIT, buttonPanel);
		exit.addActionListener(new Listener());
	}

	/**
	 * I use this method for complex bonus to get listener related to tile
	 * 
	 * @return listener
	 * 				   which I use to get the right tile 
	 */
	public Listener getTileListener() {
		return getTileListener();
	}
	
	/**
	 * Inner class to manage listener
	 *
	 */
	public class Listener extends StandardButtonListener implements ActionListener {

		/**
		 * Here I manage what to do if you click on done, exit or tile buttons
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			
			/*
			 * If you want to build but you didn't select any tile new Wrong Information Frame 
			 * is called
			 */
			if (e.getSource().equals(done)) {
				if (tile==null) {
					new WrongInformationFrame();
				}
				else {
					ui.getMessageSender().buildEmporium(city, tile);
				}
				exit();
			}
			else if (e.getSource().equals(exit)) {
				exit();
			}
			/*
			 * If you click on tile button you select this tile
			 */
			else {
				TileButton buttonPressed = (TileButton) e.getSource();
				tile = buttonPressed.getTile();
			}
		}
		/**
		 * @see StandardButtonListener
		 */
		@Override
		public void exit() {
			dispose();
		}
		
		/**
		 * this method is used to get Tile for complex bonus
		 * 
		 * @return tile
		 * 			   choosen
		 */
		public BusinessPermitTile getTile() {
			return tile;
		}
	}
}
