package it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.frames;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;
import java.util.Set;

import javax.swing.JLabel;
import javax.swing.JPanel;

import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.FrameUtilities;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.StandardButtonListener;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.panels.BonusesPanel;
import it.polimi.ingsw.ps04.model.bonus.Bonus;

public class ShowBonusNobilityTrackFrame extends FrameUtilities {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2751011479547823944L;
	
	private static final String TITLE = "Nobility Bonus";
	
	private int stepClicked;
	private Map<Integer, Set<Bonus>> track;
	private JPanel bonusPanel = new JPanel();
	
	/**
	 * Builder of the nobility track frame which use track for getting information about the step you clicked
	 * 
	 * @param stepClicked
	 * 					 is the number of the step you clicked
	 * @param track
	 * 				nobility track from model
	 */
	public ShowBonusNobilityTrackFrame(int stepClicked, Map<Integer, Set<Bonus>> track) {
		this.stepClicked = stepClicked;
		this.track = track;
		createFrame();
		settingMethod();
	}

	/**
	 * @see FrameUtilities
	 */
	@Override
	public void settingMethod() {
		settingMethodStandard(TITLE);
		this.pack();
	}

	/**
	 * Here i create frame which is divided in three parts: 
	 * north info
	 * central what kind of bonus has this step 
	 * south exit button
	 */
	@Override
	public void createFrame() {
		Container framePanel = getContentPane();
		framePanel.setLayout(new BorderLayout());
		JLabel info = addLabel (String.valueOf(stepClicked) + "° step of Nobility Track has bonuses!", framePanel);
		addComponentToNobilityPanel();
		exit = addButton("exit", framePanel);
		exit.addActionListener(new Listener());
		framePanel.add(info, BorderLayout.NORTH);
		framePanel.add(bonusPanel, BorderLayout.CENTER);
		framePanel.add(exit, BorderLayout.SOUTH);
	}
	
	/**
	 * Here I figure out what kind of bonuses are for each stepclicked and show them
	 * 
	 */
	private void addComponentToNobilityPanel() {
		BonusesPanel bonus = new BonusesPanel(track.get(stepClicked));
		bonusPanel = bonus.getPanel();
	}
	
	/**
	 * Inner class to manage listener 
	 *
	 */
	private class Listener extends StandardButtonListener implements ActionListener {
		
		/**
		 * Here I manage what to do if you click exit button
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			
			if (e.getSource().equals(exit)) {
				exit();
			}
		}
		
		/**
		 * @see StandardButtonListener
		 */
		@Override
		public void exit() {
			dispose();
		}
		
	}
}
