package it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.frames;

import java.awt.Container;

import javax.swing.BoxLayout;

import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.FrameUtilities;
import it.polimi.ingsw.ps04.utils.statistics.PlayerStatistics;
import it.polimi.ingsw.ps04.utils.statistics.Ranking;

public class RankingFrame extends FrameUtilities {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7318701810112008321L;

	private static final String TITLE = "Ranking";

	private static final String GAME = " games";
	private static final String MINUTE = " minutes";
	
	private Ranking ranking;
	
	/**
	 * Builder which receive ranking
	 * 
	 * @param ranking
	 * 				 sorted by win number and less time spent in game
	 */
	public RankingFrame(Ranking ranking) {
		this.ranking = ranking;
		createFrame();
		settingMethod();
	}

	/**
	 * Here i use standard settings
	 */
	@Override
	public void settingMethod() {
		settingMethodStandard(TITLE);
		this.pack();
	}
	
	/**
	 * I create this frame with sorted statistics of all players 
	 */
	@Override
	public void createFrame() {
		Container framePanel = getContentPane();
		framePanel.setLayout(new BoxLayout (framePanel, BoxLayout.Y_AXIS));
		for (PlayerStatistics player : ranking.getRanking()) {
			addLabel ("Player: " + player.getPlayerName(), framePanel);
			addLabel ("Have won: " + String.valueOf(player.getWonGames()) + GAME, framePanel);
			addLabel ("Have played for: " + String.valueOf(player.getGameMinutes()) + MINUTE, framePanel);
			addLabel ("Have played: " + String.valueOf(player.getPlayedGames()) + GAME, framePanel);
			addLabel ("", framePanel);
		}
	}
}
