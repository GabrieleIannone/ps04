package it.polimi.ingsw.ps04.client.view.userinterface.swing.unlogged;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;

import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.FrameUtilities;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.StandardButtonListener;

public class SuccessfullSignedIn extends FrameUtilities{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final String TITLE = "Success Signed In";
	
	public SuccessfullSignedIn() {
		createFrame();
		settingMethod();
	}

	@Override
	public void settingMethod() {
		settingMethodStandard(TITLE);
		this.setSize(100,100);
	}

	@Override
	public void createFrame() {
		Container framePanel = this.getContentPane();
		framePanel.setLayout (new BoxLayout(framePanel, BoxLayout.Y_AXIS));
		addLabel ("You are now registered, please Login", framePanel);
		exit = addButton (EXIT, framePanel);
		exit.addActionListener(new Listener());
	}
	/**
	 * Inner class to manage listeners
	 *
	 */
	private class Listener extends StandardButtonListener implements ActionListener {

		JButton buttonPressed;
		
		@Override
		public void actionPerformed(ActionEvent e) {
			
			buttonPressed = (JButton) e.getSource();
			
			if (buttonPressed.getText().equals(EXIT)) {
				exit();
			}
		}
		@Override
		public void exit() {
			dispose();
		}
	}
}
