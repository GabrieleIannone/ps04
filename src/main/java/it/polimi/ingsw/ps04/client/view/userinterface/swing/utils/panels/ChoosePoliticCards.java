package it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.panels;

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import it.polimi.ingsw.ps04.client.view.userinterface.swing.GUIGame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.ComponentUtilities;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.ImageColorReplacer;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.frames.SomethingWentWrongFrame;
import it.polimi.ingsw.ps04.model.deck.politic.PoliticCard;
import it.polimi.ingsw.ps04.model.player.Player;

public class ChoosePoliticCards extends JComponent implements ComponentUtilities{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final String CARD_IMAGE_PATH = GUIGame.IMAGES_PATH + "politics_card.png";
	
	private ArrayList<PoliticCard> cardChoosen = new ArrayList<>();
	
	private JPanel mainPanel;
	private Player player;
	
	/**
	 * Builder for Choose Politic cards frame which use player to draw players card 
	 * 
	 * @param player
	 * 				who has to choose politic cards
	 */
	public ChoosePoliticCards(Player player) {
		this.player = player;
		createPanel();
		mainPanel.setVisible(true);
	}

	/**
	 * Here I create a panel in which for each politic card I draw a button with relative listener 
	 * If something go wrong will be displayed SomethingWentWrongFrame
	 */
	public void createPanel() {
		mainPanel = new JPanel();
		mainPanel.setLayout(new GridLayout());
		for (PoliticCard card : player.getPoliticCardsHand().getHand()) {
			try {
				JButton button;
				button = new JButton(new ImageIcon(
						ImageColorReplacer.changeColor(new File(CARD_IMAGE_PATH), card.getColour())));
				button.setText(card.getColour().getName());
				button.setHorizontalTextPosition(SwingConstants.CENTER);
				button.setVerticalTextPosition(SwingConstants.TOP);
				button.addActionListener(new Listener(card));
				sets(button);
				mainPanel.add(button);
			} catch (IOException e) {
				new SomethingWentWrongFrame();
			}
		}
	}
	
	/**
	 * Here i return this panel in order to be used by others frames/panels
	 * 
	 * @return panel
	 * 				which is used in other frames
	 */
	public JPanel getPanel() {
		return mainPanel;
	}

	/**
	 * Here I return the choosen cards in order to use them in other frames/panels
	 * 
	 * @return cardChoosen
	 * 					  which is  the arraylist of the card you choose 
	 */
	public ArrayList<PoliticCard> getCardsChoosen() {
		return cardChoosen;
	}

	/**
	 * Here i create button and add it to main panel
	 * 
	 * @param button
	 * 				in order to add actionlistener
	 */
	@Override
	public JButton addButton(String string, Container frame) {
		JButton button = new JButton(string);
		sets(button);
		frame.add(button);
		return button;
	}
 
	/**
	 * Here i create label and add it to main panel
	 * 
	 * @param label
	 * 			   itself
	 */
	@Override
	public JLabel addLabel(String string, Container panel) {
		JLabel label = new JLabel(string);
		sets (label);
		panel.add(label);
		return label;
	}

	/**
	 * Here i set component and add it to main panel
	 * 
	 * @param component
	 * 				   to be set
	 */
	@Override
	public void sets(JComponent component) {
		component.setFont(new Font("Serif", Font.ITALIC, 20));
		component.setForeground(Color.RED);
		component.setBackground(Color.WHITE);
		component.setAlignmentX(CENTER_ALIGNMENT);
		component.setAlignmentY(CENTER_ALIGNMENT);
	}


	/**
	 * Inner class to manage listener to cards
	 *
	 */
	private class Listener implements ActionListener {

		private PoliticCard card;

		/**
		 * Builder for this car listener which have Politic Card you are listening on
		 * 
		 * @param card
		 * 			  you are listening on
		 */
		public Listener(PoliticCard card) {
			this.card = card;
		}
		
		/**
		 * What to do if you on a card
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			cardChoosen.add(card);
			
		}
	}
}
