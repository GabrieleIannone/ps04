package it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame.action;

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;

import it.polimi.ingsw.ps04.client.view.userinterface.UserInterface;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.FrameUtilities;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.StandardButtonListener;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.panels.DrawCouncil;
import it.polimi.ingsw.ps04.model.board.Region;

public class ChooseRegionFrame extends FrameUtilities {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final String TITLE = "Select region";
	private static final String LOWLANDS = "coast";
	private static final String HILLS = "hills";
	private static final String MONTAINS = "mountains";
	private static final String CHANGE_PERMIT_TILES = "Change region permit tiles";
	private static final String ELECT_COUNCILLOR = "Elect a councillor";
	private static final String SEND_ASSISTANT_TO_ELECT_COUNCIL = "Send an assistant to elect a council";
	private static final String ACQUIRE_BUSINESS_PERMIT_TILE = "Acquire Business Permit tile";
	
	private static final int NUM_ROW = 2;
	private static final int NUM_COLUMS = 3;
	private static final int X_SIZE = 500;
	private static final int Y_SIZE = 200;
	
	private Listener listener = new Listener();
	private String actionString;
	private Boolean mainAction;
	private Boolean kingAction;
	private UserInterface ui;
	private JPanel regionChoice = new JPanel();
	private JButton montains;
	private JButton hill;
	private JButton coast;
	private Region regionChoosen = null;
	private DrawCouncil hillCouncilPanel, montainsCouncilPanel, lowlandsCouncilPanel;
	private JPanel lowlandsCouncil, montainsCouncil, hillCouncil;
	
	/**
	 * Builder
	 * 
	 * @param ui
	 * 			which I use to get information about region councils
	 * @param mainAction
	 * 					boolean which represents if is a main action or quick action i want to do
	 * @param actionString
	 * 					type of action i want to do
	 */
	public ChooseRegionFrame(UserInterface ui, Boolean mainAction, String actionString) {
		this.actionString = actionString;
		this.mainAction = mainAction;
		this.ui = ui;
		settingMethod();
		createFrame();
	}
	
	/**
	 * @see FrameUtilities
	 */
	@Override
	public void settingMethod() {
		settingMethodStandard(TITLE);
		this.setSize(X_SIZE,Y_SIZE);
	}

	/**
	 * Here I create a simple frame: I draw in the middle councils and below them the add respectives buttons
	 */
	@Override
	public void createFrame() {
		Container framePanel = getContentPane();
		framePanel.setLayout(new BoxLayout(framePanel, BoxLayout.X_AXIS));
		addChoice();
		framePanel.add(regionChoice);
	}
	
	/**
	 * Here I add coast, hill and mountain councils and below them I add respectives buttons
	 */
	private void addChoice() {
		regionChoice.setLayout(new GridLayout(NUM_ROW, NUM_COLUMS));
		for (Region region : ui.getModel().getMap().getRegions()) {
			if (region.getName().equalsIgnoreCase(MONTAINS)) {
				montainsCouncilPanel = new DrawCouncil (ui.getModel().getMap().getRegionReference(region).getCouncil());
				montainsCouncil = montainsCouncilPanel.getPanel();
				regionChoice.add(montainsCouncil);
			}
			else if (region.getName().equalsIgnoreCase(HILLS)) {
				hillCouncilPanel = new DrawCouncil (ui.getModel().getMap().getRegionReference(region).getCouncil());
				hillCouncil = hillCouncilPanel.getPanel();
				regionChoice.add(hillCouncil);
			}
			else if (region.getName().equalsIgnoreCase(LOWLANDS)) {
				lowlandsCouncilPanel = new DrawCouncil (ui.getModel().getMap().getRegionReference(region).getCouncil());
				lowlandsCouncil = lowlandsCouncilPanel.getPanel();
				regionChoice.add(lowlandsCouncil);	
			}
		}
		coast = addButton(LOWLANDS, regionChoice);
		coast.addActionListener(listener);
		hill = addButton(HILLS, regionChoice);
		hill.addActionListener(listener);
		montains = addButton(MONTAINS, regionChoice);
		montains.addActionListener(listener);
	}
	
	/**
	 * this method is used for complex bonus
	 * 
	 * @return listener to region
	 */
	public Listener getRegionListener() {
		return listener;
	}
	
	/**
	 * Inner class to manage actionlistener
	 *
	 */
	public class Listener extends StandardButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			
			JButton buttonPressed = (JButton) e.getSource();
			
			/*
			 * if the button is pressed is the king change councillor of king council
			 */
				for (Region region : ui.getModel().getMap().getRegions()) {
					if (region.getName().equalsIgnoreCase(LOWLANDS) && (buttonPressed.getText().equals(LOWLANDS))) {
						regionChoosen = region;
					}
					if (region.getName().equalsIgnoreCase(HILLS) && (buttonPressed.getText().equals(HILLS))) {
						regionChoosen = region;
					}
					if (region.getName().equalsIgnoreCase(MONTAINS) && (buttonPressed.getText().equals(MONTAINS))) {
						regionChoosen = region;
					}
				}
			/*
			 * if i have a main action
			 */
			if (mainAction) {
				if (actionString.equals(ELECT_COUNCILLOR)) {
					kingAction = false;
					new ChooseCouncillorFrame (ui, regionChoosen, mainAction, kingAction);
					exit();
				}
				else if (actionString.equals(ACQUIRE_BUSINESS_PERMIT_TILE)) {
					new ChoosePoliticCardsToUseFrame (ui, regionChoosen, actionString, null);
					exit();
				}
			}
			/*
			 * else if i have a quickaction
			 */
			else if (actionString.equals(CHANGE_PERMIT_TILES)) {
				ui.getMessageSender().changeBusinessPermitTiles(regionChoosen);
				exit();
			}
			else if (actionString.equals(SEND_ASSISTANT_TO_ELECT_COUNCIL)) {
				kingAction = false;
				new ChooseCouncillorFrame (ui, regionChoosen, mainAction, kingAction);
				exit();
			}
		}
		
		/**
		 * this method is used for complex bonus
		 * 
		 * @return region choosen
		 */
		public Region getRegion() {
			return regionChoosen;
		}
		
		/**
		 * @see StandardButtonListener
		 */
		@Override
		public void exit() {
			dispose();
		}
	}
}
