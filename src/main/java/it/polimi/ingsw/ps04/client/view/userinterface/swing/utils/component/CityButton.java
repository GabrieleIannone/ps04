package it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.component;

import java.awt.Container;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.SwingConstants;

import it.polimi.ingsw.ps04.client.view.userinterface.swing.GUIGame;
import it.polimi.ingsw.ps04.model.board.City;

public class CityButton extends JButton {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Builder of this custom JButton
	 * 
	 * @param name
	 * 			  of the city
	 * @param panel
	 * 			   in order to add this city button to this panel
	 * @param city
	 * 		       reference to the city
	 */
	public CityButton(String name, Container panel, City city) {
		this.setText(name);
		this.setIcon(new ImageIcon(GUIGame.IMAGES_PATH + "city_small.png"));
		this.setFont(new Font("Agency FB", Font.ITALIC, 20));
		this.setBackground(city.getColour());
		this.setHorizontalTextPosition(SwingConstants.CENTER);
		this.setVerticalTextPosition(SwingConstants.TOP);
		panel.add(this);
	}
}