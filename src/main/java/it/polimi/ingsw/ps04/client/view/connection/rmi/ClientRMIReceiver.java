package it.polimi.ingsw.ps04.client.view.connection.rmi;

import java.io.Serializable;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import it.polimi.ingsw.ps04.view.rmi.RMIViewInterface;

/**
 * This class represents the RMI connection used by the client. This class
 * manage only the reception of a message.
 * 
 * @see RMIClient for the sending of a message.
 */
public class ClientRMIReceiver extends UnicastRemoteObject implements ClientRMIReceiverInterface, Serializable {

	private static final long serialVersionUID = -2738117628848944462L;
	private RMIClient sender;

	public ClientRMIReceiver(RMIClient sender) throws RemoteException {
		this.sender = sender;
	}

	/**
	 * Called by the server. It sets the server stub of the RMIClient with the
	 * proper server RMIView.
	 * 
	 * @param IP
	 *            The server IP.
	 * 
	 * @param port
	 *            The server port.
	 * 
	 * @param gameName
	 *            The name of the RMIView saved in the server registry.
	 */
	@Override
	public void startGame(String IP, int port, String gameName) throws RemoteException, NotBoundException {
		Registry registry = LocateRegistry.getRegistry(IP, port);
		RMIViewInterface serverStub = (RMIViewInterface) registry.lookup(gameName);
		sender.setServerStub(serverStub);
	}

	/**
	 * This method will be called by server in order to send an object to
	 * client.
	 * 
	 * @param obj
	 *            The object sent.
	 */
	@Override
	public void receiveObject(Object obj) throws RemoteException {
		sender.notify(obj);
	}

}
