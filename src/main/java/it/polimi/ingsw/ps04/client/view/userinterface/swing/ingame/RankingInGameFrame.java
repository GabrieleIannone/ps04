package it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame;

import java.awt.Container;

import javax.swing.BoxLayout;

import it.polimi.ingsw.ps04.client.view.userinterface.UserInterface;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.FrameUtilities;
import it.polimi.ingsw.ps04.model.player.Player;

public class RankingInGameFrame extends FrameUtilities {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8536824953838080985L;

	private static final String TITLE = "Ranking";
	
	private UserInterface ui;
	private int rank = 0;

	/**
	 * Builder of the ranking in game frame
	 * 
	 * @param ui
	 * 			which I use to get ranking in game
	 */
	public RankingInGameFrame(UserInterface ui) {
		this.ui = ui;
		createFrame();
		settingMethod();
	}
	
	/**
	 * @see FrameUtilities
	 */
	@Override
	public void settingMethod() {
		settingMethodStandard(TITLE);
		this.pack();
	}
	
	/**
	 * Here i create a frame with the player ranking from higher to lower
	 */
	@Override
	public void createFrame() {
		Container framePanel = getContentPane();
		framePanel.setLayout(new BoxLayout(framePanel, BoxLayout.Y_AXIS));
		for (Player player : ui.getModel().getMatchRanking()) {
			rank++;
			addLabel (Integer.valueOf(rank) + ". " + player.getName() + " with " + Integer.valueOf(
			player.getPoints().getVictoryPoints()) + " victory points", framePanel);
		}
	}
}
