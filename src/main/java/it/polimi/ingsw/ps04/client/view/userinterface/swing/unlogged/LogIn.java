package it.polimi.ingsw.ps04.client.view.userinterface.swing.unlogged;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

import it.polimi.ingsw.ps04.client.view.userinterface.UserInterface;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.GUIGame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.loggedin.SuccessfullLoggedIn;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.FrameUtilities;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.StandardButtonListener;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.frames.WrongInformationFrame;

public class LogIn extends FrameUtilities {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final String TITLE = "Login";
	private static final String LOGIN_STRING = "Login";

	private static final int NUM_COLUMS = 2;
	private static final int NUM_ROWS = 3;

	private JPanel centralPanel = new JPanel();
	private JPanel southPanel = new JPanel();
	private JTextField username, password;
	private JButton logIn;
	private UserInterface ui;

	private GUIGame gg;

	/**
	 * Builder to log in frame
	 * 
	 * @param gg
	 *            which I use to do logIn
	 */
	public LogIn(GUIGame gg) {
		this.gg = gg;
		this.ui = gg;
		createFrame();
		settingMethod();
	}

	/**
	 * Create a simple frame and add central and south panel to it
	 */
	@Override
	public void createFrame() {
		Container framePanel = this.getContentPane();
		centralPanel.setLayout(new GridLayout(NUM_ROWS, NUM_COLUMS));
		addComponents(centralPanel, southPanel);
		framePanel.add(centralPanel, BorderLayout.CENTER);
		framePanel.add(southPanel, BorderLayout.SOUTH);
	}

	/**
	 * @see FrameUtilities
	 */
	@Override
	public void settingMethod() {
		settingMethodStandard(TITLE);
		this.pack();
	}

	/**
	 * Add central and south components to respective panels
	 * 
	 * @param centralPanel
	 *            where I will add labels and textfields
	 * @param southPanel
	 *            where I will add buttons
	 */
	private void addComponents(JPanel centralPanel, JPanel southPanel) {
		addComponentsCentral(centralPanel);
		addComponentsSouth(southPanel);
	}

	/**
	 * Add labels and text fields for username and password to central panel
	 * 
	 * @param frame
	 *            which is the container where I add labels and text fields
	 */
	private void addComponentsCentral(Container frame) {
		addLabel("Press Login button after you ", frame);
		addLabel("type username and password ", frame);
		addLabel("Username: ", frame);
		username = addTextField("", frame);
		username.addActionListener(new Listener());
		addLabel("Password: ", frame);
		password = addTextField("", frame);
		password.addActionListener(new Listener());
	}

	/**
	 * add exit and log in buttons to south panel
	 * 
	 * @param frame
	 *            which is where I add login and exit buttons
	 */
	private void addComponentsSouth(Container frame) {
		logIn = addButton(LOGIN_STRING, frame);
		logIn.addActionListener(new Listener());
		exit = addButton(EXIT, frame);
		exit.addActionListener(new Listener());
	}

	/**
	 * Inner class to manage actionlistener
	 *
	 */
	private class Listener extends StandardButtonListener implements ActionListener {

		/**
		 * Here i manage what to do if you type username, password or click on
		 * login or exit
		 */
		@Override
		public void actionPerformed(ActionEvent e) {

			/*
			 * if (e.getSource().equals(username)) { username(); } else if
			 * (e.getSource().equals(password)) { password(); } else
			 */if (e.getSource().equals(logIn)) {
				loggedIn();
			} else
				exit();
		}

		/**
		 * Here i try to login, first I check for missing information, then try
		 * to
		 */
		private void loggedIn() {
			if (!missingInformation()) {
				if (ui.doLogin(username.getText(), password.getText())) {
					closeAllWindows();
					new SuccessfullLoggedIn(gg);
				} else {
					new WrongInformationFrame();
				}
			} else {
				new WrongInformationFrame();
				exit();
			}
		}

		/**
		 * this method is used to check what you type
		 * 
		 * @return true if you miss to type password or username false otherwise
		 */
		private boolean missingInformation() {
			if ((password.getText().equals("")) || (username.getText().equals("")))
				return true;
			else
				return false;
		}

		/**
		 * @see StandardButtonListener
		 */
		@Override
		public void exit() {
			dispose();
		}
	}
}
