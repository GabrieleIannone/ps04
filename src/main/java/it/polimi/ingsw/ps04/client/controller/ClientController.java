package it.polimi.ingsw.ps04.client.controller;

import java.util.List;
import java.util.Observable;

import it.polimi.ingsw.ps04.client.view.connection.Connection;
import it.polimi.ingsw.ps04.controller.Controller;
import it.polimi.ingsw.ps04.model.GameModelView;
import it.polimi.ingsw.ps04.model.Model;
import it.polimi.ingsw.ps04.model.action.Action;
import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.model.player.PlayersManager;
import it.polimi.ingsw.ps04.utils.exception.ActionNotCompletedException;
import it.polimi.ingsw.ps04.utils.exception.ActivePlayerDisconnectedException;
import it.polimi.ingsw.ps04.utils.exception.NoActivePlayersException;
import it.polimi.ingsw.ps04.utils.message.ActionMessage;
import it.polimi.ingsw.ps04.utils.message.Message;
import it.polimi.ingsw.ps04.utils.message.ModelRequestMessage;
import it.polimi.ingsw.ps04.utils.message.PlayerDisconnectedMessage;
import it.polimi.ingsw.ps04.utils.message.RequestforBonus;
import it.polimi.ingsw.ps04.utils.message.login.SuccessMessage;
import it.polimi.ingsw.ps04.utils.message.market.MarketMessage;
import it.polimi.ingsw.ps04.utils.message.market.SellingPhaseMessage;
import it.polimi.ingsw.ps04.utils.message.market.UpdateMarketMessage;

/**
 * this class implements the controller of the Client. It observes the
 * connection and controls the model. Through the model it also notify the
 * UserInterface
 */
public class ClientController extends Controller {

	/**
	 * constructs a client controller with that specified model
	 * 
	 * @param clientModel
	 *            the model that you want to to assign to the controller
	 */
	public ClientController(Model clientModel) {
		super(clientModel);
	}

	/**
	 * updates the messages that come from the observed object
	 */
	@Override
	public void update(Observable o, Object arg) {
		if (!(o instanceof Connection)) {
			throw new IllegalArgumentException();
		}

		// I received remote model. I update my local model now
		else if (arg instanceof GameModelView) {
			GameModelView gmv = (GameModelView) arg;
			Model remoteModel = gmv.getModel();
			model.setModel(remoteModel);
			model.notifyModel();
			notifyActivePlayer();
		}

		// I received an action
		else if (arg instanceof ActionMessage) {
			Action action = ((ActionMessage) arg).getAction();
			try {
				executeAction(action);
			} catch (ActionNotCompletedException e) {
				requestModel();
			}
		}

		// I received a complex bonus
		else if (arg instanceof RequestforBonus) {
			model.executeBonus((RequestforBonus) arg);
		}

		// I received a market message
		else if (arg instanceof MarketMessage) {
			try {
				manageMarket((MarketMessage) arg);
			} catch (ActionNotCompletedException e) {
				requestModel();
			}
		}

		// The selling phase is closed. I synchronize market players manager
		// with server now
		else if (arg instanceof UpdateMarketMessage) {

			PlayersManager modelPlayersManager = model.getPlayersManager();
			List<Player> marketActivePlayer = model.getMarket().getPlayersManager().getActivePlayers();
			List<Player> newActivePlayer = ((UpdateMarketMessage) arg).getMarket().getPlayersManager()
					.getActivePlayers();
			model.getMarket().startBuyingPhase();
			marketActivePlayer.clear();
			for (Player playerIterator : newActivePlayer) {
				Player realPlayer = modelPlayersManager.getPlayerReference(playerIterator);
				marketActivePlayer.add(realPlayer);
			}
			model.getMarket().getPlayersManager().startRound();
			notifyActivePlayer();
		}

		// Server sends me an exception
		else if (arg instanceof Exception) {
			model.notifyException((Exception) arg);
		}

		// Someone has disconected
		else if (arg instanceof PlayerDisconnectedMessage) {
			PlayerDisconnectedMessage disconnessionMessage = (PlayerDisconnectedMessage) arg;
			model.notifyMessage(disconnessionMessage);
			try {
				model.getPlayersManager().disconnectPlayer(disconnessionMessage.getPlayer());
			} catch (ActivePlayerDisconnectedException e) {
				notifyActivePlayer();
			} catch (NoActivePlayersException e) {
			}
		} else if (arg instanceof SuccessMessage) {
			// do nothing because it is managed in the SocketCLient
		} else if (arg instanceof Message) {
			model.notifyMessage((Message) arg);
		}

	}

	/**
	 * notifies that you want to receive the model from the server
	 */
	private void requestModel() {
		model.notifyMessage(new ModelRequestMessage());

	}

	/**
	 * starts the market phase
	 */
	@Override
	public void startMarketPhase() {
		super.startMarketPhase();
		model.notifyMessage(new SellingPhaseMessage());
	}

	/**
	 * shows a message to the user. It notifies the message to the user
	 * interface
	 */
	@Override
	protected void showMessage(Message message) {
		model.notifyMessage(message);
	}

	/**
	 * stops the timer. In the client it does nothing
	 */
	@Override
	protected void stopTimer() {
		// do nothing
	}

	/**
	 * starts the timer. In the client I do nothing
	 */
	@Override
	protected void startTimer() {
	}

}
