package it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame.market.buy;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import it.polimi.ingsw.ps04.client.view.userinterface.UserInterface;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.GUIGame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.FrameUtilities;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.ImageColorReplacer;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.StandardButtonListener;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.component.TileButton;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.frames.BusinessPermitTileCardFrame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.frames.SomethingWentWrongFrame;
import it.polimi.ingsw.ps04.model.deck.business.BusinessPermitTile;
import it.polimi.ingsw.ps04.model.deck.politic.PoliticCard;
import it.polimi.ingsw.ps04.model.market.BusinessPermitTileonSale;
import it.polimi.ingsw.ps04.model.market.BuyingOrder;
import it.polimi.ingsw.ps04.model.market.PoliticCardonSale;
import it.polimi.ingsw.ps04.model.market.SellingOffers;
import it.polimi.ingsw.ps04.model.player.AssistantsCrew;
import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.utils.color.NamedColor;

public class BuyFromPlayerFrame extends FrameUtilities {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3858135632086943311L;

	private static final String CARD_IMAGE_PATH = GUIGame.IMAGES_PATH + "politics_card.png";

	private static final int NUM_COLUMNS = 3;
	private static final int NUM_ROW = 6;
	
	private static final String INFO = "Info here";
	
	private Player player;
	private UserInterface ui;
	private SellingOffers playerOffers;
	private JPanel assistantOnSale = new JPanel();
	private JPanel cardsOnSale = new JPanel();
	private JPanel tilesOnSale = new JPanel();
	private JPanel offersPanel = new JPanel();
	private JPanel buttonPanel = new JPanel();
	private int numberAssistant = 0;
	
	private BuyingOrder order = new BuyingOrder();

	/**
	 * Builder for Buy From Player Frame which has the player from who you want to buy something and the ui
	 * used to get information and send order
	 * 
	 * @param ui
	 * 			which is used to get information and send order
	 * @param player
	 * 				from who you want to buy something
	 */
	public BuyFromPlayerFrame(UserInterface ui, Player player) {
		this.ui = ui;
		this.player = player;
		createFrame();
		settingMethod();
	}
	
	/**
	 * @see FrameUtilities
	 */
	@Override
	public void settingMethod() {
		settingMethodStandard(player.getName());
		this.pack();
	}

	/**
	 * Here I create the frame with info panel in the north, offers panel in the middle and button panel in the south
	 */
	@Override
	public void createFrame() {
		Container framePanel = getContentPane();
		framePanel.setLayout(new BorderLayout());
		addComponentsToOfferPanel();
		addComponentsToButtonPanel();
		JLabel info = addLabel ("This player sell :", framePanel);
		framePanel.add(buttonPanel, BorderLayout.SOUTH);
		framePanel.add(info, BorderLayout.NORTH);
		framePanel.add(offersPanel, BorderLayout.CENTER);
	}

	/**
	 * Here I add exit and done button to button Panel
	 */
	private void addComponentsToButtonPanel() {
		buttonPanel.setLayout(new FlowLayout());
		exit = addButton(EXIT, buttonPanel);
		exit.addActionListener(new Listener());
		done = addButton (DONE, buttonPanel);
		done.addActionListener(new Listener());
	}

	/**
	 * Here I add components to offers Panel: in the center Politic Cards, to the left tiles and to the right
	 * assistants
	 */
	private void addComponentsToOfferPanel() {
		playerOffers = ui.getModel().getMarket().getOffers().get(player.getName());
		offersPanel.setLayout(new BorderLayout());
		addComponentsToAssistantsPanel();
		addComponentsToCardsPanel();
		addComponentsToTilesPanel();
		offersPanel.add(assistantOnSale, BorderLayout.EAST);
		offersPanel.add(cardsOnSale, BorderLayout.CENTER);
		offersPanel.add(tilesOnSale, BorderLayout.WEST);
	}

	/**
	 * Here I add some components about Politic Card like button to buy it and a label for the cost of it
	 */
	private void addComponentsToCardsPanel() {
		cardsOnSale.setLayout(new FlowLayout());
		for (PoliticCardonSale card : playerOffers.getCardsonSale()) {
			try {
				JButton cardButton;
				cardButton = new JButton(new ImageIcon(
						ImageColorReplacer.changeColor(new File(CARD_IMAGE_PATH), card.getCard().getColour())));
				cardButton.setText(card.getCard().getColour().getName());
				cardButton.setHorizontalTextPosition(SwingConstants.CENTER);
				cardButton.setVerticalTextPosition(SwingConstants.TOP);
				cardButton.addActionListener(new Listener());
				addLabel ("Card cost is: " + card.getCost(), cardsOnSale);
				cardsOnSale.add(cardButton);
			} catch (IOException e) {
				new SomethingWentWrongFrame();
			}
		}
	}

	/**
	 * Here I add some components about tile like info button and tile cost
	 */
	private void addComponentsToTilesPanel() {
		tilesOnSale.setLayout(new GridLayout(NUM_ROW, NUM_COLUMNS));
		for (BusinessPermitTileonSale tile : playerOffers.getTilesonSale()) {
			TileButton tileButton = new TileButton(tilesOnSale, tile.getTile());
			tileButton.addActionListener(new Listener());
			addLabel ("Cost of this tile is " + Integer.valueOf(tile.getCost()) + "Click on button to buy", tilesOnSale);
			JButton infoButton = addButton (INFO, tilesOnSale);
			infoButton.addActionListener(new InfoListener(tile));
		}
	}

	/**
	 * Here I add some labels about Assistants and a textfield in which you will write how many assistants you
	 * want to buy
	 */
	private void addComponentsToAssistantsPanel() {
		assistantOnSale.setLayout(new GridLayout(4, 1));
		addLabel ("Number of assistants on sale " + Integer.valueOf(playerOffers.getAssistantsonSale().getAssistantCrew().getMembersNumber()), assistantOnSale);
		addLabel ("Cost for assistants " + Integer.valueOf(playerOffers.getAssistantsonSale().getCost()), assistantOnSale);
		addLabel ("How many do yuo want to buy?", assistantOnSale);
		JTextField assistants = addTextField ("" ,assistantOnSale);
		assistants.addActionListener(new Listener());
	}
	
	/**
	 * I used this method in order to get player from who I buy something from other frames
	 * 
	 * @return player
	 * 				 from who I buy something
	 */
	public Player getPlayer() {
		return player;
	}
	
	/**
	 * I used this method in order to get order from each player from other frames
	 * 
	 * @return order
	 * 				which is what i buy
	 */
	public BuyingOrder getOrder() {
		return order;
	}
	
	/**
	 * Inner class to manage normal listener to attach to exit, done, tile, card or TextField buttons 
	 *
	 */
	private class Listener extends StandardButtonListener implements ActionListener {

		/**
		 * What to do if you click on exit, done or other kind of buttons
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			
			if (e.getSource().equals(exit)) {
				exit();
			}
			else if (e.getSource().equals(done)) {
				exit();
			}
			/*
			 * What to do if you click on a tile button
			 */
			else if (e.getSource() instanceof TileButton) {
				TileButton tileButtonPressed = (TileButton)e.getSource();
				BusinessPermitTile tileChoosen = tileButtonPressed.getTile();
				order.addTile(tileChoosen);
			}
			/*
			 * What to do if the button clicked is referred to a Politic card
			 */
			else if (e.getSource() instanceof JButton ) {
				JButton buttonPressed = (JButton) e.getSource();
				for (NamedColor color : ui.getModel().getPoliticDeck().getCorruptionColors()) {
					if (color.getName().equals(buttonPressed.getText())) {
						PoliticCard card = new PoliticCard(color);
						order.addCard(card);
					}
				}
			}
			/*
			 * What to do if you type in the textfield
			 */
			else if (e.getSource() instanceof JTextField) {
				JTextField textField = (JTextField) e.getSource();
				setAssistantNumber(Integer.valueOf(textField.getText()));
			}
		}
		
		/**
		 * Here I set assistants number and send the order 
		 * 
		 * @param assistantNumberInput
		 * 							  which is a value for how many assistants I want to buy
		 */
		private void setAssistantNumber (int assistantNumberInput) {
			numberAssistant = assistantNumberInput;
			AssistantsCrew assistantsToBuy = new AssistantsCrew(numberAssistant);
			order.setAssistants(assistantsToBuy);
		}
		
		/**
		 * @see StandardButtonListener
		 */
		@Override
		public void exit() {
			dispose();
		}
	}
	
	/**
	 * Another Inner class to manage the listener on tiles
	 *
	 */
	private class InfoListener implements ActionListener {

		private BusinessPermitTileonSale tile;

		/**
		 * Builder for this tile info listener
		 * 
		 * @param tile
		 * 			   which is the tile referred to this listener
		 */
		public InfoListener(BusinessPermitTileonSale tile) {
			this.tile = tile;
		}
		
		/**
		 * What to do if you click info button, it will display the right business permit tile frame
		 */
		@Override
		public void actionPerformed(ActionEvent arg0) {
			
			if (arg0.getSource() instanceof JButton) {
				new BusinessPermitTileCardFrame(tile.getTile());
			}	
		}
	}
}
