package it.polimi.ingsw.ps04.client.view.userinterface.swing.loggedin;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;

import it.polimi.ingsw.ps04.client.view.userinterface.UserInterface;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.GUIGame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.FrameUtilities;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.StandardButtonListener;

public class StatisticsFrame extends FrameUtilities{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5196970410929978354L;
	
	private static final String TITLE = "Statistics";
	private static final String RANKING = "All Ranking";
	private static final String YOUR_STATISTICS = "Your statistics";
	
	private UserInterface ui;
	
	private JButton ranking, yourStatistics;
	
	/**
	 * Builder of the statistics frame
	 * 
	 * @param ui
	 * 			which i use to call ranking or player statistics from database
	 */
	public StatisticsFrame(UserInterface ui) {
		this.ui = ui;
		createFrame();
		settingMethod();
	}
	
	/**
	 * @see FrameUtilities
	 */
	@Override
	public void settingMethod() {
		settingMethodStandard(TITLE);
		this.pack();
	}

	/**
	 * Here I create the frame with a background image and in the middle buttons
	 */
	@Override
	public void createFrame() {
		setLayout(new BorderLayout());
		JLabel background=new JLabel(new ImageIcon(GUIGame.IMAGES_PATH +"statistics.png"));
		background.setLayout(new BoxLayout (background, BoxLayout.Y_AXIS));
		addComponentsCentral(background);
		add(background);
	}

	/**
	 * add ranking, statistics and exit buttons to background
	 * 
	 * @param background
	 * 					which is the component where you add buttons
	 */
	private void addComponentsCentral(JLabel background) {
		yourStatistics = addButton (YOUR_STATISTICS, background);
		yourStatistics.addActionListener(new Listener());
		ranking = addButton (RANKING, background);
		ranking.addActionListener(new Listener());
		exit = addButton (EXIT, background);
		exit.addActionListener(new Listener());
	}
	
	/**
	 * Inner class used to manage listeners 
	 *
	 */
	private class Listener extends StandardButtonListener implements ActionListener {
	
		/**
		 * What to do if you click 
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource().equals(exit)) {
				exit();
			}
			if (e.getSource().equals(ranking)) {
				ranking();
			}
			if (e.getSource().equals(yourStatistics)) {
				yourStatistics();
			}
		}
		
		/**
		 * What to do if you click on ranking button
		 */
		private void ranking() {
			ui.getMessageSender().ranking();
			exit();
		}
		
		/**
		 * What to do if you click on your statistics button
		 */
		private void yourStatistics() {
			ui.getMessageSender().statistics(ui.getClientPlayer().getName());
			exit();
		}
	
		/**
		 * @see StandardButtonListener
		 */
		@Override
		public void exit() {
			dispose();
		}
	}
}
