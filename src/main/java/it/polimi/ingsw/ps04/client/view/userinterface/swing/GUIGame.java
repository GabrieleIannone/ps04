package it.polimi.ingsw.ps04.client.view.userinterface.swing;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import it.polimi.ingsw.ps04.client.view.MessageSender;
import it.polimi.ingsw.ps04.client.view.userinterface.UserInterface;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame.EndGameFrame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame.InGameFrame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame.ActionListFrame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame.action.ChooseRegionFrame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame.action.ChooseTileFromRegionFrame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame.action.ChooseYourTileFrame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame.map.DrawMap;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame.market.MarketBuyFrame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame.market.MarketSellFrame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.loggedin.GameNotReady;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.loggedin.PlayerStatisticsFrame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame.action.ChooseCityFrame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame.action.ChooseCityFrame.Listener;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.unlogged.BeginFrame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.frames.ExceptionFrame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.frames.RankingFrame;
import it.polimi.ingsw.ps04.model.Model;
import it.polimi.ingsw.ps04.model.board.City;
import it.polimi.ingsw.ps04.model.board.Region;
import it.polimi.ingsw.ps04.model.deck.business.BusinessPermitTile;
import it.polimi.ingsw.ps04.model.market.SellingOffers;
import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.utils.ConfigHelper;
import it.polimi.ingsw.ps04.utils.exception.ActionNotCompletedException;
import it.polimi.ingsw.ps04.utils.exception.NoPermitTileException;
import it.polimi.ingsw.ps04.utils.exception.NotYourTurnException;
import it.polimi.ingsw.ps04.utils.exception.PlayerHasAlreadyBuiltException;
import it.polimi.ingsw.ps04.utils.message.ActionMessage;
import it.polimi.ingsw.ps04.utils.message.GameFinishedMessage;
import it.polimi.ingsw.ps04.utils.message.Message;
import it.polimi.ingsw.ps04.utils.message.SimpleMessage;
import it.polimi.ingsw.ps04.utils.message.statistics.PlayerStatisticsMessage;
import it.polimi.ingsw.ps04.utils.message.statistics.RankingMessage;
import it.polimi.ingsw.ps04.utils.statistics.PlayerStatistics;
import it.polimi.ingsw.ps04.utils.statistics.Ranking;

public class GUIGame extends UserInterface {

	public static final String IMAGES_PATH = ConfigHelper.GUI_PATH + "images" + File.separator;
	
	private final static String NO_PERMIT_TILE_EXCEPTION = "No permit tile exception";
	private final static String NOT_YOUR_TURN_EXCEPTION = "Not your turn exception";
	private static final String PLAYER_HAS_BUILT = "Player has allready built exception";
	private static final String ACTION_NOT_COMPLETE_EXCEPTION = "Action is not completed";
	
	private InGameFrame inGameFrame;
	private ChooseCityFrame cityChooser;
	private ActionListFrame actionListFrame = null;

	private ArrayList<String> actionList = new ArrayList<>();

	/**
	 * Builder of the GUIGame with the reference to messagesender and clientModel
	 * 
	 * @param messageSender
	 * 					   which will be use during the game
	 * @param clientModel
	 * 					that will be use for create the game
	 */
	public GUIGame(MessageSender messageSender, Model clientModel) {
		super(messageSender, clientModel);
	}
	
	/**
	 *  This is the method I will 
	 */
	@Override
	public void launch() throws IOException {
		new BeginFrame(this);
	}
	
	/**
	 * 
	 */
	@Override
	public void showPlayer(Player activePlayer) {
		/*
		 * I do nothing, because I don't wont to show the player after the
		 * action is done
		 */
	}

	/**
	 * I will use this methods for complex bonus if I have to choose a city, will create the right frame and will 
	 * use the listener in order to get the user choice
	 */
	@Override
	public City insertCity() {
		cityChooser = new ChooseCityFrame(this);
		Listener listener = cityChooser.getCityListener();
		return listener.getCity();
	}

	/**
	 * I will use this methods for complex bonus if I have to choose a tile, will create the right frame and will 
	 * use the listener in order to get the user choice
	 */
	@Override
	public BusinessPermitTile insertMyTile() {
		ChooseYourTileFrame yourTileChooser = new ChooseYourTileFrame(this, null);
		it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame.action.ChooseYourTileFrame.Listener listener = yourTileChooser.getTileListener();
		return listener.getTile();
	}

	/**
	 * I will use this methods for complex bonus if I have to choose a tile froma a region, will create the right frame and will 
	 * use the listener in order to get the user choice
	 * 
	 * @param region
	 * 				the region from which i will choose tile
	 */
	@Override
	public int chooseTile(Region region) {
		ChooseTileFromRegionFrame yourTileChooser = new ChooseTileFromRegionFrame(this, region, null);
		it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame.action.ChooseTileFromRegionFrame.Listener listener = yourTileChooser.getTileListener();
		return listener.getTileNumberComplex();
	}
	
	/**
	 * I will use this methods for complex bonus if I have to choose a region, will create the right frame and will 
	 * use the listener in order to get the user choice
	 */
	@Override
	public Region insertRegion() {
		ChooseRegionFrame regionChooser = new ChooseRegionFrame(this, null, null);
		it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame.action.ChooseRegionFrame.Listener listener = regionChooser.getRegionListener();
		return listener.getRegion();
	}

	/**
	 * Here i will figure out what kind of message it is and do the right thing like open right frame
	 */
	@Override
	protected void showMessage(Message message) {
		
		/*
		 * will display the end game frame
		 */
		if (message instanceof GameFinishedMessage) {
			new EndGameFrame((GameFinishedMessage) message);
		}
		
		/*
		 * I will add action Message to the actionList and refresh it
		 */
		else if (message instanceof ActionMessage) {
			String action = new String (generateActionString(((ActionMessage) message).getAction()));
			actionList.add(action);
			displayMainFrame();
		}
		
		/*
		 * I will display Player Statistic Frame if a player request statistics
		 */
		else if (message instanceof PlayerStatisticsMessage) {
			PlayerStatistics playerStatistics = ((PlayerStatisticsMessage) message).getStatistics();
			new PlayerStatisticsFrame(playerStatistics); 
		}
		
		/*
		 * I will display Player Statistic Frame if a player request ranking
		 */
		else if (message instanceof RankingMessage ) {
			Ranking ranking = ((RankingMessage) message).getRanking();
			new RankingFrame(ranking); 
		}
		
		/*
		 * I will add SimpleMessage string to actionList and refresh it
		 */
		else if (message instanceof SimpleMessage) {
			String action = new String (((SimpleMessage) message).getMessage());
			actionList.add(action);
			displayMainFrame();
		}
	}

	/**
	 * Here I show in the right way exceptions 
	 */
	@Override
	protected void showException(Exception exception) {
		
		if (exception instanceof ActionNotCompletedException) {
			new ExceptionFrame(ACTION_NOT_COMPLETE_EXCEPTION, exception.getMessage());
		}
		if (exception instanceof NoPermitTileException) {
			new ExceptionFrame(NO_PERMIT_TILE_EXCEPTION, exception.getMessage());
		}
		if (exception instanceof NotYourTurnException) {
			new ExceptionFrame(NOT_YOUR_TURN_EXCEPTION, exception.getMessage());
		}
		if (exception instanceof PlayerHasAlreadyBuiltException) {
			new ExceptionFrame(PLAYER_HAS_BUILT, exception.getMessage());
		}
	}

	/**
	 * Here I do the update of the action list and game frame each time client receive an action message
	 * have the map final because i need to keep the city position
	 */
	@Override
	public void displayMainFrame() {
		try {
			final DrawMap drawMap = new DrawMap(this);
			if (inGameFrame != null)
				inGameFrame.dispose();
			new InGameFrame(this, drawMap);
			if (actionListFrame != null)
				actionListFrame.dispose();
			new ActionListFrame (actionList, this);
		} catch (Exception e) {
			new GameNotReady();
		}
	}
	
	/**
	 * Here I show the market buy frame if you are in the right phase
	 */
	@Override
	protected void showSellingOffers(Map<String, SellingOffers> offers) {
		new MarketBuyFrame(this);
	}

	/**
	 * Here I show the market sell frame if you are in the right phase
	 */
	@Override
	protected void showSellingInterface() {
		new MarketSellFrame(this);
	}
	
	/**
	 * Here I set the actionList reference in order to update it after an action message
	 * 
	 * @param actionListFrame
	 * 						  which has to be update 
	 */
	public void setActionListFrame (ActionListFrame actionListFrame) {
		this.actionListFrame = actionListFrame;
	}
	
	/**
	 * Here I set the inGameFrame reference in order to update it after an action message
	 * 
	 * @param inGameFrame
	 * 					 which has to be update 
	 */
	public void setInGameFrame(InGameFrame inGameFrame) {
		this.inGameFrame = inGameFrame;
	} 
}
