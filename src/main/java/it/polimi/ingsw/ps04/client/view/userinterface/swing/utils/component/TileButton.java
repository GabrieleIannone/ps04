package it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.component;

import java.awt.Container;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.SwingConstants;

import it.polimi.ingsw.ps04.client.view.userinterface.swing.GUIGame;
import it.polimi.ingsw.ps04.model.board.City;
import it.polimi.ingsw.ps04.model.deck.business.BusinessPermitTile;

public class TileButton extends JButton {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private  BusinessPermitTile tile;
	private String name = "";

	/**
	 * Builder for this bustom tile button
	 * 
	 * @param panel
	 * 				in order to add this tile button to this panel
	 * @param tile
	 * 			  reference to the tile
	 */
	public TileButton(Container panel, BusinessPermitTile tile) {
	this.tile = tile;
	sets();
	panel.add(this);
	}
	
	/**
	 * Here i set button name, which is represented by the first char of each city, font, and 
	 * image
	 */
	private void sets() {
		for (City city : tile.getCities()) {
			name = name + " ";
			name = name + city.getName().charAt(0);
		}
		this.setText(name);
		this.setIcon(new ImageIcon(GUIGame.IMAGES_PATH + "takevisibletile.png"));
		this.setFont(new Font("Agency FB", Font.ITALIC, 20));
		this.setHorizontalTextPosition(SwingConstants.CENTER);
		this.setVerticalTextPosition(SwingConstants.TOP);
	}
	
	/**
	 * This method will return tile which is represented by this tile button
	 * 
	 * @return tile
	 * 				is represented by this tile button
	 */
	public BusinessPermitTile getTile() {
		return tile;
	}
}
	

