package it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import it.polimi.ingsw.ps04.client.view.userinterface.swing.GUIGame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.FrameUtilities;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.component.TileButton;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.frames.BusinessPermitTileCardFrame;
import it.polimi.ingsw.ps04.model.deck.business.BusinessPermitTile;
import it.polimi.ingsw.ps04.model.player.Player;

public class OthersPlayerFrame extends FrameUtilities {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3446324110438112179L;

	private static final int NUM_ROW = 4;
	private static final int NUM_COLUMNS = 2;
	
	private Player player;
	private JPanel cardPanel = new JPanel();
	private JPanel resourcesPanel = new JPanel();
	private JPanel tilePanel = new JPanel();
	
	/**
	 * Builder of OthersPlayerFrame that display public resources of a certain player
	 * 
	 * @param player
	 * 				you want to know resources
	 */
	public OthersPlayerFrame(Player player) {
		this.player = player;
		createFrame();
		settingMethod();
	}
	
	/**
	 * @see FrameUtilities
	 */
	@Override
	public void settingMethod() {
		settingMethodStandard(player.getName());
		this.pack();
	}

	/**
	 * Here I create frame which has simple resources to the lift, Politic card panel in the center
	 * and tile panel to the right
	 */
	@Override
	public void createFrame() {
		Container framePanel = getContentPane();
		addSimpleResources();
		addCards();
		addTiles();
		framePanel.add(cardPanel, BorderLayout.CENTER);
		framePanel.add(tilePanel, BorderLayout.EAST);
		framePanel.add(resourcesPanel, BorderLayout.WEST);
	}

	/**
	 * Here I add players tile to tile panel
	 */
	private void addTiles() {
		tilePanel.setLayout(new FlowLayout());
		if (player.getBusinessPermitTilePool().getPermitTilesNumber() > 0) {
			for (BusinessPermitTile tile : player.getBusinessPermitTilePool().getPermitTiles()) {
				TileButton button = new TileButton(tilePanel, tile);
				button.addActionListener(new TileListener(tile));
			}	
		}
	}

	/**
	 * Here I add the number of politics Cards to cardPanel
	 */
	private void addCards() {
		cardPanel.setLayout(new FlowLayout());
		addLabel ("Politic Cards: " + player.getPoliticCardsHand().getHand().size(), cardPanel);
	}

	/**
	 * Here I add simple resources like points, assistants and coins to resources Panel
	 */
	private void addSimpleResources() {
		resourcesPanel.setLayout(new GridLayout(NUM_ROW, NUM_COLUMNS));
		JLabel assistantImage = new JLabel(new ImageIcon(GUIGame.IMAGES_PATH +"assistants.png"));
		resourcesPanel.add(assistantImage);
		addLabel (String.valueOf(player.getAssistantCrew().getMembersNumber()), resourcesPanel);
		JLabel coinImage = new JLabel(new ImageIcon(GUIGame.IMAGES_PATH +"coins.png"));
		resourcesPanel.add(coinImage);
		addLabel (String.valueOf(player.getCoins().getCoinsNumber()),resourcesPanel);
		JLabel victoryPointImage = new JLabel(new ImageIcon(GUIGame.IMAGES_PATH +"victorypointssmall.png"));
		resourcesPanel.add(victoryPointImage);
		addLabel (String.valueOf(player.getPoints().getVictoryPoints()),resourcesPanel);
		JLabel noobilityLevelImage = new JLabel(new ImageIcon(GUIGame.IMAGES_PATH +"nobilitypoints.png"));
		resourcesPanel.add(noobilityLevelImage);
		addLabel (String.valueOf(player.getNobilityLevel().getLevel()),resourcesPanel);
	}
	
	/**
	 * Inner listener to manage tile listener
	 *
	 */
	private class TileListener implements ActionListener {

		private BusinessPermitTile tile;

		/**
		 * Builder of the listener with the reference to tile
		 * 
		 * @param tile
		 * 			  related to the listener
		 */
		public TileListener(BusinessPermitTile tile) {
			this.tile = tile;
		}
		
		/**
		 * Here I display tile frame if you click on it
		 */
		@Override
		public void actionPerformed(ActionEvent arg0) {
			new BusinessPermitTileCardFrame(tile);
		}
		
	}
}
