package it.polimi.ingsw.ps04.client.view.connection;

import java.util.Observable;

import it.polimi.ingsw.ps04.utils.message.Message;
import it.polimi.ingsw.ps04.utils.message.login.CredentialsMessage;

/**
 * this class implements the connection of the client
 */
public abstract class Connection extends Observable {

	/**
	 * sends a message to the server
	 * 
	 * @param message
	 *            the message that you want to send
	 */
	public abstract void sendMessage(Message message);

	/**
	 * sends a credentials message to the server
	 * 
	 * @param credentialsMessage
	 *            the credentials message that you want to send
	 * @return
	 */
	public abstract boolean sendCredentialsMessage(CredentialsMessage credentialsMessage);

	/**
	 * starts the connection
	 */
	public abstract void startConnection();

	/**
	 * This method will be called by the concrete view. It notify the observers
	 * 
	 * @param object
	 *            the object that you want to notify
	 */
	public void notify(Object object) {
		setChanged();
		notifyObservers(object);
	}

}
