package it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame.market.sell;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;

import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.FrameUtilities;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.StandardButtonListener;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.component.TileButton;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.frames.BusinessPermitTileCardFrame;
import it.polimi.ingsw.ps04.model.deck.business.BusinessPermitTile;
import it.polimi.ingsw.ps04.model.market.SellingOffers;
import it.polimi.ingsw.ps04.model.player.Player;

public class SellTileFrame extends FrameUtilities {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9133104304654412373L;

	private static final String TITLE = "Sell tile";
	private static final int X_SIZE = 500;
	private static final int Y_SIZE = 300;
	private static final String INFO = "Info";
	
	private Player player;
	private JPanel tilePanel = new JPanel();
	private JPanel buttonPanel = new JPanel();
	private ArrayList <SellSingleTileFrame> sellSingleTileFrames = new ArrayList<>();

	private SellingOffers offers;
	
	/**
	 * Builder for Sell Tile Frame which has player who is the client player who is selling something and offers 
	 * reference in order to add some item to it
	 * 
	 * @param player
	 * 				who is the client player
	 * @param offers
	 * 				reference
	 */
	public SellTileFrame(Player player, SellingOffers offers) {
		this.offers = offers;
		this.player = player;
		createFrame();
		settingMethod();
	}
	
	/**
	 * @see FrameUtilities
	 */
	@Override
	public void settingMethod() {
		settingMethodStandard(TITLE);
		this.setSize(X_SIZE, Y_SIZE);
	}

	/**
	 * I create a simple frame with a button panel in the south and a tile panel in the center
	 */
	@Override
	public void createFrame() {
		addComponentsToButtonPanel();
		addComponentToTilePanel();
		add(buttonPanel, BorderLayout.SOUTH);
		add(tilePanel, BorderLayout.CENTER);
	}
	
	/**
	 * Here I add done and exit buttons to button Panel
	 */
	private void addComponentsToButtonPanel() {
		buttonPanel.setLayout(new FlowLayout());
		done = addButton (DONE, buttonPanel);
		done.addActionListener(new Listener());
		exit = addButton(EXIT, buttonPanel);
		exit.addActionListener(new Listener());
		
	}

	/**
	 * Here I add a tile button and a TextField for cost for each tile player got in his hand
	 */
	private void addComponentToTilePanel() {
		tilePanel.setLayout(new GridLayout(2, 4));
		for (BusinessPermitTile tile : player.getBusinessPermitTilePool().getPermitTiles()) {
			TileButton tileCard = new TileButton(tilePanel, tile);
			tileCard.addActionListener(new Listener());
			JButton info = addButton (INFO, tilePanel);
			info.addActionListener(new TileListener(tileCard.getTile()));
			tilePanel.add(tileCard);
			tilePanel.add(info);
		}
	}

	/**
	 * Inner class to manage this kind of listener
	 *
	 */
	public class Listener extends StandardButtonListener implements ActionListener {

		/**
		 * Here I manage what to do if you click on a button
		 */
		@Override
		public void actionPerformed(ActionEvent e) {

			/*
			 * What to do if you click on exit and done button
			 */
			if (e.getSource().equals(exit)) {
				exit();
			}
			else if (e.getSource().equals(done)) {
				exit();
			}
			/*
			 * If button istanceof tileButton 
			 * then open the right tileFrame to sell it
			 */
			else if (e.getSource() instanceof TileButton) {
				TileButton buttonTile = (TileButton) e.getSource();
				SellSingleTileFrame sellSingleTileFrame = new SellSingleTileFrame(buttonTile.getTile(), offers);
				sellSingleTileFrames.add(sellSingleTileFrame);
			}
		}
		
		/**
		 * @see StandardButtonListener
		 */
		@Override
		public void exit() {
			dispose();
		}
	}
	
	/**
	 * Inner class to manage this kind of listener which is reported to a tile 
	 *
	 */
	private class TileListener implements ActionListener {
		
		private BusinessPermitTile tile;

		/**
		 * Builder for this kind of listener which has the tile
		 * 
		 * @param tile
		 * 			  which is the tile to which is reported the listener
		 */
		public TileListener(BusinessPermitTile tile) {
			this.tile = tile;
		}
		
		/**
		 * What to do if you click on a button which has this kind of listener
		 */
		@Override
		public void actionPerformed(ActionEvent arg0) {
			new BusinessPermitTileCardFrame(tile);
		}
	}
}
