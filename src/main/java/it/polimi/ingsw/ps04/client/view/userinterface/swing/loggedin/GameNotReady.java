package it.polimi.ingsw.ps04.client.view.userinterface.swing.loggedin;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;

import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.FrameUtilities;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.StandardButtonListener;

public class GameNotReady extends FrameUtilities{


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final String TITLE = "Game not ready";
	private static final String INFO = "Game is not ready, please wait for others players ";
	
	/**
	 * simple builder for Game Not ready warning
	 */
	public GameNotReady() {
		createFrame();
		settingMethod();
	}

	/**
	 * @see FrameUtilities
	 */
	@Override
	public void settingMethod() {
		settingMethodStandard(TITLE);
		this.pack();
	}

	/**
	 * Here i create a simple frame with in the middle the information of the warning and in the south position an
	 * exit button
	 */
	@Override
	public void createFrame() {
		Container framePanel = this.getContentPane();
		framePanel.setLayout (new BoxLayout(framePanel, BoxLayout.Y_AXIS));
		addLabel (INFO, framePanel);
		exit = addButton (EXIT, framePanel);
		exit.addActionListener(new Listener());
	}
	
	/**
	 * Inner class to manage actionlistener
	 *
	 */
	private class Listener extends StandardButtonListener  implements ActionListener {

		JButton buttonPressed;
		
		/**
		 * Here i manage what to do if you click exit button
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			
			buttonPressed = (JButton) e.getSource();
			
			if (buttonPressed.equals(exit)) {
				exit();
			}
		}
		
		/**
		 * @see StandardButtonListener
		 */
		@Override
		public void exit() {
			dispose();
		}
	}
}
