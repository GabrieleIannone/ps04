package it.polimi.ingsw.ps04.client.view.userinterface.swing.utils;

import java.awt.Container;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;

/**
 * I use this interface to add some standard actions like addButton, or addLabel to panels
 *
 */
public interface ComponentUtilities {
	
	public JButton addButton(String string, Container frame);
	public JLabel addLabel(String string, Container frame);
	public void sets(JComponent component);
	
}
