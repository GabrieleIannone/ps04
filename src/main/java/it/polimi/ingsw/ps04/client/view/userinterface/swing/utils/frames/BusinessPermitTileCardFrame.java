package it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.frames;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import it.polimi.ingsw.ps04.client.view.userinterface.swing.GUIGame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.FrameUtilities;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.StandardButtonListener;
import it.polimi.ingsw.ps04.model.board.City;
import it.polimi.ingsw.ps04.model.bonus.Bonus;
import it.polimi.ingsw.ps04.model.bonus.simple.AssistantsBonus;
import it.polimi.ingsw.ps04.model.bonus.simple.CoinsBonus;
import it.polimi.ingsw.ps04.model.bonus.simple.NobilityBonus;
import it.polimi.ingsw.ps04.model.bonus.simple.VictoryPointsBonus;
import it.polimi.ingsw.ps04.model.deck.business.BusinessPermitTile;

public class BusinessPermitTileCardFrame extends FrameUtilities {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final String TITLE = "Tile";

	private static final int NUM_ROW = 3;
	private static final int NUM_COLUMNS = 4;
	
	private JPanel citiesPanel = new JPanel();
	private JPanel bonusesPanel = new JPanel();
	private BusinessPermitTile tile;
	
	/**
	 * Builder which takes tile and show its bonuses and cities where you can build
	 * 
	 * @param tile
	 * 			  you wanted to know 
	 */
	public BusinessPermitTileCardFrame(BusinessPermitTile tile) {
		this.tile = tile;
        createFrame();
		settingMethod();
	}
	
	/**
	 * Here i use standard settings
	 */
	@Override
	public void settingMethod() {
		settingMethodStandard(TITLE);
		this.pack();
	}

	/**
	 * here i create frame that show where you can build and which bonuses you will get.
	 * in the south area will create exit button
	 */
	@Override
	public void createFrame() {
		Container framePanel = getContentPane();
		framePanel.setLayout(new BorderLayout());
		exit = addButton("exit", framePanel);
		exit.addActionListener(new Listener());
		addComponentsToCitiesPanel();
		addComponentsToBonusesPanel();
		framePanel.add(citiesPanel, BorderLayout.NORTH);
		framePanel.add(bonusesPanel, BorderLayout.CENTER);
		framePanel.add(exit, BorderLayout.SOUTH);
	}
	
	private void addComponentsToBonusesPanel() {
		bonusesPanel.setLayout(new FlowLayout());
		addLabel ("", bonusesPanel);
		for (Bonus bonus : tile.getBonuses()) {
			if (CoinsBonus.class.isInstance(bonus)) {
				JLabel coinImage = new JLabel (new ImageIcon(GUIGame.IMAGES_PATH + "coins.png"));
				JLabel coinCount = new JLabel (" " + String.valueOf(bonus.getBonusIncrementer() + " "));
				sets(coinCount, coinImage);
			}
			if (AssistantsBonus.class.isInstance(bonus)) {
				JLabel assistantsImage = new JLabel (new ImageIcon(GUIGame.IMAGES_PATH + "assistants.png"));
				JLabel assistantsCount = new JLabel (" " + String.valueOf(bonus.getBonusIncrementer() +" "));
				sets(assistantsCount, assistantsImage);
			}
			if (VictoryPointsBonus.class.isInstance(bonus)) {
				JLabel victoryPointsImage = new JLabel (new ImageIcon(GUIGame.IMAGES_PATH + "victorypointssmall.png"));
				JLabel victoryPointsCount = new JLabel (" " + String.valueOf(bonus.getBonusIncrementer()+ " "));
				sets(victoryPointsCount, victoryPointsImage);
			}
			if (NobilityBonus.class.isInstance(bonus)) {
				JLabel nobilityTrackImage = new JLabel (new ImageIcon(GUIGame.IMAGES_PATH + "nobilitypoints.png"));
				JLabel nobilityTrackCount = new JLabel (" " +String.valueOf(bonus.getBonusIncrementer()+ " "));
				sets(nobilityTrackCount, nobilityTrackImage);
			}
		addLabel ("", bonusesPanel);
		}
	}

	private void addComponentsToCitiesPanel() {
		citiesPanel.setLayout(new FlowLayout());
		bonusesPanel.setLayout(new GridLayout(NUM_ROW, NUM_COLUMNS));
		for (City city: tile.getCities()) {		
			JLabel cityLabel = new JLabel (city.getName());
			citiesPanel.add(cityLabel);
		}	
	}

	/**
	 * Here i set component and add both, image and component, to bonus panel
	 * 
	 * @param component
	 * 				   to set 
	 * @param image
	 * 			   of the bonus to add close to it
	 */
	private void sets(JComponent component, JComponent image) {
		component.setFont(new Font("Agency FB", Font.ITALIC, 20));
		bonusesPanel.add(image);
		bonusesPanel.add(component);
	}
	
	/**
	 * Inner class to manage listener
	 *
	 */
	private class Listener extends StandardButtonListener implements ActionListener {
		
		/**
		 * Here i manage what to do if you click on exit
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			
			if (e.getSource().equals(exit)) {
				exit();
			}
		}
		@Override
		public void exit() {
			dispose();
		}
	}
}
