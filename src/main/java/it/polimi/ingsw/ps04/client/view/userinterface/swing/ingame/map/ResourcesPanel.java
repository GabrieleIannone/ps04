package it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame.map;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import it.polimi.ingsw.ps04.client.view.userinterface.UserInterface;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.GUIGame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame.ActionFrame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame.NobilityTrackFrame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame.RankingInGameFrame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.loggedin.StatisticsFrame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.ComponentUtilities;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.component.TileButton;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.frames.BonusesFrame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.frames.BusinessPermitTileCardFrame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.panels.ChoosePoliticCards;
import it.polimi.ingsw.ps04.model.deck.business.BusinessPermitTile;

public class ResourcesPanel extends JComponent implements ComponentUtilities{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final int NUM_ROW = 2;
	private static final int NUM_COLUMNS = 4;
	private static final String NOBILITY_TRACK = "Nobility Track";
	private static final String ACTION = "Action";
	private static final String RANKING = "Ranking";
	private static final String STATISTICS = "Statistics";
	private static final String BONUS = "Map Bonus";
		
	private UserInterface ui;
	private JPanel mainPanel = new JPanel();
	private JPanel buttonPanel = new JPanel();
	private JPanel resourcesPanel = new JPanel();
	private JPanel politicCard = new JPanel();
	private JPanel businessPermitCard = new JPanel();
	private JPanel subResourcesPanel = new JPanel();
	private ChoosePoliticCards politicCardPanel;
	private ArrayList<TileButton> handOfTiles = new ArrayList<>();
	private JButton nobilityTrack;
	private JButton action;
	private JButton ranking;
	private JButton statistics;
	private JButton bonus;
	
	/**
	 * Builder of resources Panel
	 * 
	 * @param ui
	 * 			which I use to get and print resources
	 */
	public ResourcesPanel(UserInterface ui) {
		this.ui = ui;
		createPanel();
		mainPanel.setVisible(true);
	}

	/**
	 * Here I create a panel with buttons like ranking, statistics, actions and nobility track to the left
	 * and resources like assistants, coins, tiles, politic cards to the right
	 */
	private void createPanel() {
		mainPanel.setLayout(new BorderLayout());
		addComponentToButtonPanel();
		addResourcesPanel();
		mainPanel.add(buttonPanel, BorderLayout.WEST);
		mainPanel.add(resourcesPanel, BorderLayout.EAST);
		add(mainPanel);
	}
	
	/**
	 * Here I add player resources to right panel: politics cards in south position, coins, points and assistants
	 * in the center and after them business permit tiles
	 */
	private void addResourcesPanel() {
		resourcesPanel.setLayout(new BorderLayout());
		addComponentsToresourcesPanel(resourcesPanel);
		politicCardPanel = new ChoosePoliticCards(ui.getClientPlayer());
		politicCard = politicCardPanel.getPanel();
		fillBusinessCardPanel ();
		resourcesPanel.add(politicCard, BorderLayout.SOUTH);
		resourcesPanel.add(businessPermitCard, BorderLayout.CENTER);
		resourcesPanel.add(subResourcesPanel, BorderLayout.WEST);
	}
	
	/**
	 * Here i add labels references to players points and resources like assistants and coins
	 * 
	 * @param resourcesPanel
	 * 						where I add those labels resources
	 */
	private void addComponentsToresourcesPanel(JPanel resourcesPanel) {
		subResourcesPanel.setLayout(new GridLayout(NUM_ROW, NUM_COLUMNS));
		JLabel assistantImage = new JLabel(new ImageIcon(GUIGame.IMAGES_PATH +"assistants.png"));
		subResourcesPanel.add(assistantImage);
		addLabel (String.valueOf(ui.getClientPlayer().getAssistantCrew().getMembersNumber()), subResourcesPanel);
		JLabel coinImage = new JLabel(new ImageIcon(GUIGame.IMAGES_PATH +"coins.png"));
		subResourcesPanel.add(coinImage);
		addLabel (String.valueOf(ui.getClientPlayer().getCoins().getCoinsNumber()),subResourcesPanel);
		JLabel victoryPointImage = new JLabel(new ImageIcon(GUIGame.IMAGES_PATH +"victorypointssmall.png"));
		subResourcesPanel.add(victoryPointImage);
		addLabel (String.valueOf(ui.getClientPlayer().getPoints().getVictoryPoints()),subResourcesPanel);
		JLabel noobilityLevelImage = new JLabel(new ImageIcon(GUIGame.IMAGES_PATH +"nobilitypoints.png"));
		subResourcesPanel.add(noobilityLevelImage);
		addLabel (String.valueOf(ui.getClientPlayer().getNobilityLevel().getLevel()),subResourcesPanel);
	}

	/**
	 * 
	 * @param buttonPanel
	 */
	private void addComponentToButtonPanel() {
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.Y_AXIS));
		action = addButton (ACTION, buttonPanel);
		nobilityTrack = addButton (NOBILITY_TRACK, buttonPanel);
		ranking = addButton (RANKING, buttonPanel);
		statistics = addButton (STATISTICS, buttonPanel);
		bonus = addButton (BONUS, buttonPanel);
		}
	
	/**
	 * I override a ComponentUtilities method adding a label with its string to a panel
	 * 
	 * @param string
	 * 				which I use to create JLabel
	 * @param panel
	 * 				where I add this JLabel
	 * @return JLabel
	 * 				 itself
	 */
	@Override
	public JLabel addLabel(String string, Container panel) {
		JLabel label = new JLabel (string);
		sets(label);
		panel.add(label);
		return label;
	}
		
	/**
	 * Here i manage to change font, colour and dimension
	 * 
	 * @param component
	 * 					which i want to set
	 */
	@Override
	public void sets(JComponent component) {
		component.setFont(new Font("Serif", Font.ITALIC, 20));
		component.setForeground(Color.RED);
		component.setBackground(Color.WHITE);
		component.setAlignmentX(CENTER_ALIGNMENT);
		component.setAlignmentY(CENTER_ALIGNMENT);
	}
	
	/**
	 * Here I draw tile buttons of tiles I got
	 * 
	 */
	private void fillBusinessCardPanel () {
		businessPermitCard.setLayout(new BorderLayout());
		JLabel infoBusiness = addLabel ("Your Business Permit Tile:", businessPermitCard);
		businessPermitCard.add(infoBusiness, BorderLayout.NORTH);
		JPanel subBusinessPermitCardPanel = new JPanel();
		subBusinessPermitCardPanel.setLayout(new GridLayout(3,5));
		try {
			for (BusinessPermitTile tile : ui.getClientPlayer().getBusinessPermitTilePool().getPermitTiles()){
				TileButton card = new TileButton (businessPermitCard, tile);
				card.addActionListener(new ListenerTile(tile));
				/*
				 * if i allready used a tile card his background is orange
				 */
				if (!tile.isFaceUp()) {
					card.setBackground(Color.ORANGE);
				}
				subBusinessPermitCardPanel.add(card);
				handOfTiles.add(card);
			}
		} catch (Exception NoPermitTileException) {
			addLabel("", subBusinessPermitCardPanel);
		}
		businessPermitCard.add(subBusinessPermitCardPanel, BorderLayout.CENTER);
	}
	
	/**
	 * this method will be used in others classes to get king and players informations
	 * 
	 * @return panel
	 * 				in order to add it in other frames
	 */
	public JPanel getResourcesPanel() {
		return mainPanel;
	}
	
	/**
	 * @see ComponentUtilities
	 */
	@Override
	public JButton addButton(String string, Container frame) {
		JButton button = new JButton(string);
		sets(button);
		button.addActionListener(new Listener());
		frame.add(button);
		return button;
	}
	
	/**
	 * Inner class to manage buttons listeners
	 *
	 */
	private class Listener implements ActionListener {
		
		/**
		 * manage what to do if a button is clicked and display the frame relative to it
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			
			if (e.getSource().equals(nobilityTrack)){
				new NobilityTrackFrame(ui.getModel().getNobilityTrack(), ui);
			}
			else if (e.getSource().equals(action)) {
				new ActionFrame(ui);
			}
			else if (e.getSource().equals(statistics)) {
				new StatisticsFrame(ui);
			}
			else if (e.getSource().equals(ranking)) {
				new RankingInGameFrame(ui);
			}
			else if (e.getSource().equals(bonus)) {
				new BonusesFrame(ui);
			}
		}
	}
	
	/**
	 * Another inner class to manage tile listeners 
	 *
	 */
	private class ListenerTile implements ActionListener {

		private BusinessPermitTile tile;

		/**
		 * Builder of tile listener 
		 * 
		 * @param tile
		 * 			  with is built the tile button
		 */
		public ListenerTile(BusinessPermitTile tile) {
			this.tile = tile;
		}

		/**
		 * Here i manage specific action performed
		 * if you click on a tile button will be displayed the frame relative to it
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			new BusinessPermitTileCardFrame(tile);
		}
	}
}
