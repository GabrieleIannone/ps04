package it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.frames;

import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.FrameUtilities;
import it.polimi.ingsw.ps04.utils.message.SimpleMessage;

public class SimpleMessageFrame extends FrameUtilities {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5811095450252390833L;
	
	private static final String TITLE = "Simple message";
	private static final int X_SIZE = 600;
	private static final int Y_SIZE = 300;
	
	private SimpleMessage message;
	
	/**
	 * Builder of a frame which inform user with a simple message 
	 * 
	 * @param message
	 * 				 i wanted to show
	 */
	public SimpleMessageFrame(SimpleMessage message) {
		this.message = message;
		createFrame();
		settingMethod();
	}
	
	
	/**
	 * will set this frame with standard properties defined in Frame Utilities
	 * 
	 */
	@Override
	public void settingMethod() {
		settingMethodStandard(TITLE);
		setSize(X_SIZE, Y_SIZE);
	}

	/**
	 * Here I create a frame for simple message
	 * 
	 */
	@Override
	public void createFrame() {
		addLabel (message.getMessage(), getContentPane());
	}
}
