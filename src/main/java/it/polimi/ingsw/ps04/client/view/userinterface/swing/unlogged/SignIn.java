package it.polimi.ingsw.ps04.client.view.userinterface.swing.unlogged;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.JTextField;

import it.polimi.ingsw.ps04.client.view.userinterface.UserInterface;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.FrameUtilities;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.StandardButtonListener;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.frames.WrongInformationFrame;

public class SignIn extends FrameUtilities {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L; 
	
	private JPanel centralPanel = new JPanel();
	private JPanel southPanel = new JPanel();
	private JTextField username, password,repassword, email;
	private UserInterface ui;
	
	private static final String TITLE = "Creating an Account";

	private static final int numColumn = 2;
	private static final int numRow = 5;

	
	/**
	 * Builder for sign up
	 * 
	 * @param ui 
	 * 			which is the UserInterfaace
	 */
	public SignIn (UserInterface ui) {
		this.ui = ui;
		createFrame();
		settingMethod();
	}
	
	/**
	 * @see FrameUtilities
	 */
	public void settingMethod() {
		settingMethodStandard(TITLE);
		this.pack();
	}
	
	/**
	 * Here I create a frame for sign up
	 * 
	 */
	@Override
	public void createFrame() {
		Container framePanel = this.getContentPane();
		centralPanel.setLayout(new GridLayout(numRow, numColumn));
		addComponents(centralPanel, southPanel);
		framePanel.add(centralPanel, BorderLayout.CENTER);
		framePanel.add(southPanel, BorderLayout.SOUTH);
	}

	/**
	 * Add components to panels
	 * 
	 * @param centralPanel
	 * 					  panel with labels and text fields
	 * @param southPanel
	 * 					panel with buttons
	 */
	private void addComponents(Container centralPanel, Container southPanel) {
		addComponentsCentral(centralPanel);
		addComponentsSouth (southPanel);
	}

	/**
	 * Add label and textfields to central panel 
	 * 
	 * @param frame
	 * 				which is central panel
	 */
	private void addComponentsCentral(Container panel) {
		addLabel ("Press Sign in button after you ", panel);
		addLabel ("type username and password ", panel);
		addLabel ("Name: ", panel);
		username = addTextField ("", panel);
		username.addActionListener(new Listener());
		addLabel ("Password: ", panel);
		password = addTextField ("", panel);
		password.addActionListener(new Listener());
		addLabel ("Re insert password: ", panel);
		repassword = addTextField ("", panel);
		repassword.addActionListener(new Listener());
		addLabel ("Insert your e-mail: ", panel);
		email = addTextField ("", panel);
		email.addActionListener(new Listener());
	}

	/**
	 * Here i add buttons to southpanel
	 * 
	 * @param frame
	 * 				is the container for south panel
	 */
	private void addComponentsSouth(Container panel) {
		done = addButton ("Sign in", panel);
		done.addActionListener(new Listener());
		exit = addButton (EXIT, panel);
		exit.addActionListener(new Listener());
	}

	/**
	 * Inner class to manage actionlistener
	 *
	 */
	private class Listener extends StandardButtonListener implements ActionListener {
		
		/**
		 * Manage different method for each button is pressed
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			
			if (e.getSource().equals(done) ) {
				done();
			}
			if (e.getSource().equals(exit)) {
				exit();
			}
		}

		/**
		 * this method will check informations you typed and if all is correct send it to database 
		 * 
		 */
		public void done() {
			if (matchPasswords()) {
				ui.doSignup(username.getText(), email.getText(), password.getText());
				new SuccessfullSignedIn();
				exit();
			}
			else {
				new WrongInformationFrame();
				exit();
			}
		}
		
		/**
		 * this method will verify if password and repassword match
		 * 
		 * @return 	true if passwords match and are not blank
		 * 		 	false otherwise
		 */
		private boolean matchPasswords() {
			if (password.getText().equals("")||repassword.getText().equals(""))
				return false;
				else 
					return password.getText().equalsIgnoreCase(repassword.getText());
		}
		
		/**
		 *	@see StandardButtonListener
		 */
		@Override
		public void exit() {
			dispose();
		}
	}
}
