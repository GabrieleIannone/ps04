package it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame.market.sell;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.JTextField;

import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.FrameUtilities;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.StandardButtonListener;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.component.TileButton;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.frames.BusinessPermitTileCardFrame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.frames.SomethingWentWrongFrame;
import it.polimi.ingsw.ps04.model.deck.business.BusinessPermitTile;
import it.polimi.ingsw.ps04.model.market.BusinessPermitTileonSale;
import it.polimi.ingsw.ps04.model.market.SellingOffers;

public class SellSingleTileFrame extends FrameUtilities{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6866178622938661353L;

	private static final String TITLE = "Sell Tile";
	private static final int NUM_ROW = 5;
	private static final int NUM_COLUMNS = 2;
	
	private BusinessPermitTile tile;
	private JPanel buttonPanel = new JPanel();
	private JPanel tilePanel = new JPanel();
	private JTextField cost;
	private int costTile = -1;
	private BusinessPermitTileonSale businessPermitTileonSale;
	private SellingOffers offers;
	
	/**
	 * Builder for Sell Single Tile Frame which take offers reference to update it and tile you wanted to sell
	 * 
	 * @param tile
	 * 			  you wanted to sell
	 * @param offers
	 * 				reference in order to pudate it
	 */
	public SellSingleTileFrame(BusinessPermitTile tile, SellingOffers offers) {
		this.offers = offers;
		this.tile = tile;
		createFrame();
		settingMethod();
	}

	/**
	 * @see FrameUtilities
	 */
	@Override
	public void settingMethod() {
		settingMethodStandard(TITLE);
		pack();
	}

	/**
	 * I create a simple frame with a button panel in the south and a tile panel in the center
	 */
	@Override
	public void createFrame() {
		Container framePanel = getContentPane();
		addComponentsToButtonPanel();
		addComponentsToCentralPanel();
		framePanel.add(buttonPanel, BorderLayout.SOUTH);
		framePanel.add(tilePanel, BorderLayout.CENTER);
	}
	
	/**
	 * Here I add a tile button to show what are you wanting to sell and a TextField for the cost
	 */
	private void addComponentsToCentralPanel() {
		tilePanel.setLayout(new GridLayout(NUM_ROW, NUM_COLUMNS));
		TileButton tileButton = new TileButton (tilePanel, tile);
		tileButton.addActionListener(new TileListener(tileButton.getTile()));
		cost = addTextField("", tilePanel);
		cost.addActionListener(new Listener());
	}

	/**
	 * Here I add done and exit buttons to button Panel
	 */
	private void addComponentsToButtonPanel() {
		buttonPanel.setLayout(new FlowLayout());
		done = addButton (DONE, buttonPanel);
		done.addActionListener(new Listener());
		exit = addButton (EXIT, buttonPanel);
		exit.addActionListener(new Listener());
	}

	/**
	 * Inner class to manage this kind of listener
	 *
	 */
	private class Listener extends StandardButtonListener implements ActionListener {

		/**
		 * Here I manage what to do if you click on a button
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			
			/**
			 * Here I manage what to do if you click exit and done button or fill cost TextField
			 */
			if (e.getSource().equals(exit)) {
				exit();
			}
			else if (e.getSource().equals(cost)) {
				setCost(Integer.valueOf(cost.getText()));
			}
			/*
			 * If you click on done and the cost is not set this will generate a Something Went Wrong Frame
			 * else you add your tile to offers
			 */
			else if (e.getSource().equals(done)) {
				if (costTile==-1) {
					new SomethingWentWrongFrame();
				}
				else {
					businessPermitTileonSale = new BusinessPermitTileonSale(tile, costTile);
					offers.addTileOnSale(businessPermitTileonSale);
					exit();
				}
			}
		}

		/**
		 * Here I set the cost for the tile
		 * 
		 * @param setCostTile
		 * 					 which is the cost of the tile
		 */
		private void setCost(Integer setCostTile) {
			costTile = setCostTile;
		}

		/**
		 * @see StandardButtonListener
		 */
		@Override
		public void exit() {
			dispose();
		}
		
	}
	
	/**
	 * Inner class to manage this kind of listener which is reported to a tile 
	 *
	 */
	private class TileListener implements ActionListener {
		
		private BusinessPermitTile tile;
		
		/**
		 * Builder for this kind of listener which has the tile
		 * 
		 * @param tile
		 * 			  which is to which is reported the listener
		 */
		public TileListener(BusinessPermitTile tile) {
			this.tile = tile;
		}

		/**
		 * What to do if you click on a button which has this kind of listener
		 */
		@Override
		public void actionPerformed(ActionEvent arg0) {
			new BusinessPermitTileCardFrame(tile);
		}
	}
}
