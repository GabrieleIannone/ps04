package it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame.market;


import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import it.polimi.ingsw.ps04.client.view.userinterface.UserInterface;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame.market.buy.BuyFromPlayerFrame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.FrameUtilities;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.StandardButtonListener;
import it.polimi.ingsw.ps04.model.market.BuyingOrder;
import it.polimi.ingsw.ps04.model.player.Player;

public class MarketBuyFrame extends FrameUtilities {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final String TITLE = "Market Sell Phase";
	private static final String SKIP_BUY_PHASE = "Skip buy phase";
	private static final int X_SIZE = 300;
	private static final int Y_SIZE = 400;
	private ArrayList<BuyFromPlayerFrame> buyFromPlayers = new ArrayList<>();
	private Map<String, BuyingOrder> orders = new HashMap<>();

	private JButton skipBuyPhase;
	private UserInterface ui;
	private JPanel buyPanel = new JPanel();
	private JPanel buttonPanel = new JPanel();
	
	/**
	 * Builder for Market Buy Frame which use ui in order to submit the order
	 * 
	 * @param ui
	 * 			which is used to submit the order
	 */
	public MarketBuyFrame(UserInterface ui) {
		this.ui = ui;
		createFrame();
		settingMethod();
	}
	
	/**
	 * @see FrameUtilities
	 */
	@Override
	public void settingMethod() {
		settingMethodStandard(TITLE);
		this.setSize(X_SIZE, Y_SIZE);
	}

	/**
	 * Here I create a frame which has an info label in the north position, buy Panel in the center and the 
	 * buttons Panel in the south
	 */
	@Override
	public void createFrame() {
		Container framePanel = getContentPane();
		framePanel.setLayout(new BorderLayout());
		JLabel info = addLabel ("From who you want to buy?", framePanel);
		addComponentToBuyPanel();
		addComponentToButtonPanel();
		framePanel.add(info, BorderLayout.NORTH);
		framePanel.add(buyPanel, BorderLayout.CENTER);
		framePanel.add(buttonPanel, BorderLayout.SOUTH);
	}

	/**
	 * Here I add a button for each player and add it to buy Panel
	 */
	private void addComponentToBuyPanel() {
		buyPanel.setLayout(new FlowLayout());
		for (Player player : ui.getModel().getPlayersManager().getAllPlayers()) {
			if (!player.equals(ui.getClientPlayer())) {
				JButton button = addButton (player.getName(), buyPanel);
				button.addActionListener(new Listener());
			}	
		}
	}
	
	/**
	 * Here I add done and skip Buy phase buttons to buttonPanel
	 */
	private void addComponentToButtonPanel() {
		buttonPanel.setLayout(new FlowLayout());
		done = addButton (DONE, buttonPanel);
		done.addActionListener(new Listener());
		skipBuyPhase = addButton (SKIP_BUY_PHASE, buttonPanel);
		skipBuyPhase.addActionListener(new Listener());
	}

	/**
	 * Inner class to manage listeners
	 *
	 */
	private class Listener extends StandardButtonListener implements ActionListener {
		
		/**
		 * Here I manage what to do if you click on a button
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			
			if (e.getSource().equals(done)) {
				submitOrderToBuy();
				exit();
			}
			else if (e.getSource().equals(skipBuyPhase)) {
				ui.getMessageSender().skipBuy();
				exit();
			}
			else {
				JButton buttonPressed = (JButton) e.getSource();
				for (Player player : ui.getModel().getPlayersManager().getAllPlayers()) {
					if (player.getName().equals(buttonPressed.getText())) {
						BuyFromPlayerFrame buyFromSinglePlayer = new BuyFromPlayerFrame(ui, player);
						buyFromPlayers.add(buyFromSinglePlayer);
					}	
				}
			}
		}
		
		/**
		 * I use this method at the end if you click on done button and you want to submit your order
		 */
		private void submitOrderToBuy() {
			for (BuyFromPlayerFrame buyFromSinglePlayerIterator : buyFromPlayers) {
				Player playerOrdering = buyFromSinglePlayerIterator.getPlayer();
				BuyingOrder order = buyFromSinglePlayerIterator.getOrder();
				orders.put(playerOrdering.getName(), order);
			}
			ui.getMessageSender().buy(orders);
		}

		/**
		 * @see StandardButtonListener
		 */
		@Override
		public void exit() {
			dispose();
		}
	}
}

