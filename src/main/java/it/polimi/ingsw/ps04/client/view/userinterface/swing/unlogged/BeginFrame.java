package it.polimi.ingsw.ps04.client.view.userinterface.swing.unlogged;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;

import it.polimi.ingsw.ps04.client.view.userinterface.swing.GUIGame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.FrameUtilities;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.StandardButtonListener;

public class BeginFrame extends FrameUtilities{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L; 
	private JButton signIn, credits, logIn;
	private GUIGame gg;
	
	private static final String TITLE = "Welcome to council of four";
	private static final String SIGN_IN = "SignIn";
	private static final String CREDITS = "Credits";
	private static final String LOGIN = "Login";
	
	private static final int NUM_COLUMNS = 1;
	private static final int NUM_ROW = 6;
	
	/**
	 * Builder of the begin frame with the reference to GUIGame which is very usefull if you want to start a game
	 * 
	 * @param gg
	 * 			which is the reference to GUIGame
	 */
	public BeginFrame(GUIGame gg) {
		this.gg = gg;
		createFrame();
		settingMethod();
	}
	
	/**
	 * @see FrameUtilities
	 */
	@Override
	public void settingMethod() {
		settingMethodStandard(TITLE);
		this.pack();
	}

	/**
	 * Here I create the frame using a background image and add to it buttons
	 */
	@Override
	public void createFrame() {
		setLayout(new BorderLayout());
		JLabel background=new JLabel(new ImageIcon(GUIGame.IMAGES_PATH +"councilOfFour1.jpg"));
		background.setLayout(new GridLayout(NUM_ROW, NUM_COLUMNS));
		addComponentsToFramePanel(background);
		add(background);
	}
	
	/**
	 * this method is used to add login, signin, credits and exit buttons to the background image
	 * 
	 * @param frame
	 * 				which is the image used as frame where I add buttons
	 */
	private void addComponentsToFramePanel(Container frame) {
		addButton("", frame);
		logIn = addButton(LOGIN, frame);
		logIn.addActionListener(new ButtonListener()); 
		signIn = addButton(SIGN_IN, frame);
		signIn.addActionListener(new ButtonListener()); 
		credits = addButton(CREDITS, frame);
		credits.addActionListener(new ButtonListener());
		exit = addButton(EXIT, frame);
		exit.addActionListener(new ButtonListener()); 
	}

	/**
	 * Inner class to manage actionlistener
	 *
	 */
	private class ButtonListener extends StandardButtonListener implements ActionListener {

		JButton buttonPressed;
		
		/**
		 * Here i manage what to do if you click on signin, credits, login and exit button
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			
			buttonPressed = (JButton) e.getSource();
			
			if (buttonPressed.equals(signIn)) {
				signIn();
			}
			
			if (buttonPressed.equals(credits)) {
				credits();
			}
			
			if (buttonPressed.equals(logIn)) {
				logIn();
			}
			
			if (buttonPressed.equals(exit)) {
				exit();
			}
		}
	
		/**
		 * Here i explain what to do in case of credits, signin and login
		 */
		private void logIn() {
			new LogIn(gg);
		}
	
		private void credits() {
			new Credits ();
		}
	
		private void signIn() {
			new SignIn(gg);
		}

		/**
		 * @see StandardButtonListener
		 */
		@Override
		public void exit() {
			dispose();
		}
	}

}
