package it.polimi.ingsw.ps04.client.view.userinterface.swing.utils;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

public class ImageColorReplacer {

	/**
	 * Builder for Image Color Replacer with the file you wanted to change and the color you want to paint
	 * 
	 * @param file
	 * 			  which is the image file
	 * @param color
	 * 				you want to use
	 * @throws IOException
	 * 					  is thrown if something would go wrong
	 */
	public static BufferedImage changeColor(File file, Color color) throws IOException {
		return changeColor(ImageIO.read(file), color);
	}

	/**
	 * This method will replace image old with new color you wanted to use
	 * 
	 * @param image
	 * 				you want to change color
	 * @param color
	 * 				you want to use
	 * @return image
	 * 				re-colored with color you choose
	 */
	public static BufferedImage changeColor(BufferedImage image, Color color) {
		int width = image.getWidth();
		int height = image.getHeight();
		WritableRaster raster = image.getRaster();

		for (int xx = 0; xx < width; xx++) {
			for (int yy = 0; yy < height; yy++) {
				int[] pixels = raster.getPixel(xx, yy, (int[]) null);
				pixels[0] = color.getRed();
				pixels[1] = color.getGreen();
				pixels[2] = color.getBlue();
				raster.setPixel(xx, yy, pixels);
			}
		}
		return image;
	}
}