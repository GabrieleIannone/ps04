package it.polimi.ingsw.ps04.client.view.connection.rmi;

import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * This class represents the public interface used by RMI. It collects all the
 * methods that could be called by the server remotely.
 */
public interface ClientRMIReceiverInterface extends Remote {

	/**
	 * @see ClientRMIReceiver#startGame(String, int, String)
	 */
	public void startGame(String IP, int port, String gameName) throws RemoteException, NotBoundException;

	/**
	 * @see ClientRMIReceiver#receiveObject(Object)
	 */
	public void receiveObject(Object obj) throws RemoteException;

}
