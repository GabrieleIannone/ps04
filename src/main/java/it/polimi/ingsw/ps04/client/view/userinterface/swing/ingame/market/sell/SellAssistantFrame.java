package it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame.market.sell;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.JTextField;

import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.FrameUtilities;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.StandardButtonListener;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.frames.SomethingWentWrongFrame;
import it.polimi.ingsw.ps04.model.market.AssistantsonSale;
import it.polimi.ingsw.ps04.model.market.SellingOffers;
import it.polimi.ingsw.ps04.model.player.AssistantsCrew;
import it.polimi.ingsw.ps04.model.player.Player;

public class SellAssistantFrame extends FrameUtilities {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final String TITLE = "Sell Assistants";
	private static final int NUM_ROW = 3;
	private static final int NUM_COLUMNS = 2;
	private static final int X_SIZE = 500;
	private static final int Y_SIZE = 300;
	
	private int number;
	private int cost;
	private JPanel assistantPane = new JPanel();
	private JPanel buttonPanel = new JPanel();
	private Player player;
	private JTextField numberTextField, costTextField;
	private AssistantsonSale assistantsonSale;
	private SellingOffers offers;
	
	/**
	 * Builder for Sell Assistants Frame which has player who is the client player and who is selling something 
	 * and offers reference in order to add some item to it
	 * 
	 * @param player
	 * 				who is the client player
	 * @param offers
	 * 				reference
	 */
	public SellAssistantFrame(Player player, SellingOffers offers) {
		this.offers = offers;
		this.player = player;
		createFrame();
		settingMethod();
	}
	
	/**
	 * @see FrameUtilities
	 */
	@Override
	public void settingMethod() {
		settingMethodStandard(TITLE);
		this.setSize(X_SIZE,Y_SIZE);
	}

	/**
	 * I create a simple frame with a button panel in the south and assistants panel in the center 
	 */
	@Override
	public void createFrame() {
		Container framePanel = getContentPane();
		addComponentToAssistantPanel();
		addComponentToButtonPanel();
		framePanel.add(assistantPane, BorderLayout.CENTER);
		framePanel.add(buttonPanel, BorderLayout.SOUTH);
	}

	/**
	 * Here I add done and exit buttons to button Panel
	 */
	private void addComponentToButtonPanel() {
		buttonPanel.setLayout(new FlowLayout());
		done = addButton (DONE, buttonPanel);
		done.addActionListener(new Listener());
		exit = addButton (EXIT, buttonPanel);
		exit.addActionListener(new Listener());
	}

	/**
	 * Here I add some labels and a textfield to get how many assistants player wants to sell and how much
	 */
	private void addComponentToAssistantPanel() {
		assistantPane.setLayout(new GridLayout(NUM_ROW, NUM_COLUMNS));
		addLabel ("You have" + player.getAssistantCrew().getMembersNumber() + "assistants", assistantPane);
		addLabel ("", assistantPane);
		addLabel ("How many do you want to sell?", assistantPane);
		numberTextField = addTextField ("", assistantPane);
		numberTextField.addActionListener(new Listener());
		addLabel ("How much?", assistantPane);
		costTextField = addTextField ("", assistantPane);
		costTextField.addActionListener(new Listener());
	}
	
	/**
	 * Inner class to manage listeners
	 *
	 */
	private class Listener extends StandardButtonListener implements ActionListener {

		/**
		 * Manage what to do if you click on a button or type in the TextField
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
					
			if (e.getSource().equals(exit)) {
				exit();
			}		
			else if (e.getSource().equals(numberTextField)) {
				setNumber();
			}
			else if (e.getSource().equals(costTextField)) {
				setCost();
			}
			/*
			 * If you click on done and the cost is not set this will generate a Something Went Wrong Frame
			 * else you add your assistantsonSale to offers
			 */
			else if (e.getSource().equals(done)) {
				if (cost == -1) {
					new SomethingWentWrongFrame();
				}
				else {
					AssistantsCrew assistants = new AssistantsCrew(number);
					assistantsonSale = new AssistantsonSale(assistants, cost);
					offers.setAssistantsonSale(assistantsonSale);
					exit();
				}
			}
		}

		/**
		 * Here I set the cost for assistants
		 */
		private void setCost() {
			cost = Integer.valueOf(costTextField.getText());
		}
		
		/**
		 * Here I set the assistants number
		 */
		private void setNumber() {
			number = Integer.valueOf(numberTextField.getText());
		}
		
		/**
		 * @see StandardButtonListener
		 */
		@Override
		public void exit() {
			dispose();
		}
	}
}
