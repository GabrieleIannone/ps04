package it.polimi.ingsw.ps04.client.view.userinterface.cli;

import asg.cliche.Command;
import asg.cliche.Param;
import asg.cliche.Shell;
import asg.cliche.ShellDependent;
import asg.cliche.ShellFactory;
import it.polimi.ingsw.ps04.client.view.MessageSender;
import it.polimi.ingsw.ps04.client.view.userinterface.UserInterface;
import it.polimi.ingsw.ps04.model.Model;
import it.polimi.ingsw.ps04.model.action.Action;
import it.polimi.ingsw.ps04.model.board.City;
import it.polimi.ingsw.ps04.model.board.Region;
import it.polimi.ingsw.ps04.model.deck.business.BusinessPermitTile;
import it.polimi.ingsw.ps04.model.market.SellingOffers;
import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.utils.message.ActionMessage;
import it.polimi.ingsw.ps04.utils.message.Message;
import it.polimi.ingsw.ps04.utils.string.StringHelper;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 * the first shell that is opened
 */
public class InitialCli extends UserInterface implements ShellDependent {

	private Shell shell;
	private String user;
	private Scanner scanner = new Scanner(System.in);

	/**
	 * constructs an initial cli with the specifies message sender and client
	 * model
	 * 
	 * @param messageSender
	 *            the sender of the messages of the user
	 * @param clientModel
	 *            the model of the client
	 */
	public InitialCli(MessageSender messageSender, Model clientModel) {
		super(messageSender, clientModel);
	}

	/**
	 * allows the user to login
	 * 
	 * @param user
	 *            the username of the player
	 * @param psw
	 *            the password of the player
	 */
	@Command(description = "login entering username and password")
	public void login(@Param(name = "username", description = "your username") String user,
			@Param(name = "password", description = "your password") String psw) {
		this.user = user;
		if (doLogin(user, psw)) {
			showLoggedInInterface();
		}
	}

	/**
	 * allows the user to sign up
	 * 
	 * @param username
	 *            the username of the player
	 * @param email
	 *            the email of the player
	 * @param password
	 *            the password of the player
	 */
	@Command
	public void signUp(@Param(name = "username", description = "your username") String username,
			@Param(name = "email", description = "your email") String email,
			@Param(name = "password", description = "your password") String password) {
		if (doSignup(username, email, password)) {
			System.out.println(username + " is registered now");
		}
	}

	/**
	 * allows the user to delete himself from the server
	 * 
	 * @param username
	 *            the username of the player
	 * @param password
	 *            the password of the player
	 */
	@Command
	public void deleteUser(@Param(name = "username", description = "your username") String username,
			@Param(name = "password", description = "your password") String password) {
		if (doDeleteUser(username, password)) {
			System.out.println(username + " was deleted");
		}
	}

	/**
	 * launches the initial cli
	 */
	public void launch() throws IOException {
		ShellFactory.createConsoleShell("menu", GAME_TITLE, this).commandLoop();
	}

	/**
	 * sets the shell
	 */
	@Override
	public void cliSetShell(Shell theShell) {
		this.shell = theShell;
	}

	/**
	 * prints the player
	 */
	@Override
	protected void showPlayer(Player player) {
		System.out.println(player.getPublicElementsString());
	}

	/**
	 * allows the user to insert a city from the cli
	 */
	@Override
	public City insertCity() {
		System.out.println("insert city: ");
		String cityName = scanner.nextLine();
		return new City(cityName);
	}

	/**
	 * allows the user to insert a tile from the cli
	 */
	@Override
	public BusinessPermitTile insertMyTile() {
		List<BusinessPermitTile> myTiles = getClientPlayer().getBusinessPermitTilePool().getPermitTiles();
		System.out.println("your tiles:\n" + StringHelper.getEnumeratedObjectsString(myTiles));
		System.out.println("choose tile:");
		int tileNumber = scanner.nextInt();
		return getClientPlayer().getBusinessPermitTilePool().getBusinessPermitTileAt(tileNumber);
	}

	/**
	 * allows the user to insert a region from the cli
	 */
	@Override
	public Region insertRegion() {
		System.out.println("insert region: ");
		String regionName = scanner.nextLine();
		return new Region(regionName);
	}

	/**
	 * allows the user to choose a tile of a region from the cli
	 */
	@Override
	public int chooseTile(Region region) {
		Scanner in = new Scanner(System.in);
		System.out.println("region tiles:\n" + StringHelper.getEnumeratedObjectsString(
				getModel().getMap().getRegionReference(region).getBusinessDeck().getRevealedTiles()));
		System.out.println("choose tile:");
		int tileNumber = in.nextInt();
		in.close();
		return tileNumber;
	}

	/**
	 * prints a message
	 */
	@Override
	protected void showMessage(Message message) {
		if (message instanceof ActionMessage) {
			ActionMessage am = (ActionMessage) message;
			Action action = am.getAction();
			System.out.println(generateActionString(action));
		}
		System.out.println("Message: " + message);

	}

	/**
	 * prints an exception
	 */
	@Override
	protected void showException(Exception exception) {
		System.err.println("Error Message: " + exception.getMessage());
	}

	/**
	 * prints the model
	 */
	@Override
	protected void displayMainFrame() {
		System.out.println(getModel());
	}

	/**
	 * opens the logged in shell
	 */
	private void showLoggedInInterface() {
		try {
			ShellFactory.createSubshell(user, shell, "you are logged in", new LoggedCli(this, scanner)).commandLoop();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * prints the selling offers
	 */
	@Override
	protected void showSellingOffers(Map<String, SellingOffers> offers) {
		System.out.println("selling offers: " + offers);
	}

	/**
	 * prints the selling interface
	 */
	@Override
	protected void showSellingInterface() {
		System.out.println("selling phase started");
	}
}
