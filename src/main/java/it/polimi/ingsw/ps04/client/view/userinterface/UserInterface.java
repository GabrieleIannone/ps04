package it.polimi.ingsw.ps04.client.view.userinterface;

import java.io.IOException;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import it.polimi.ingsw.ps04.client.view.MessageSender;
import it.polimi.ingsw.ps04.model.market.SellingOffers;
import it.polimi.ingsw.ps04.model.Model;
import it.polimi.ingsw.ps04.model.action.Action;
import it.polimi.ingsw.ps04.model.board.City;
import it.polimi.ingsw.ps04.model.board.Region;
import it.polimi.ingsw.ps04.model.deck.business.BusinessPermitTile;
import it.polimi.ingsw.ps04.model.player.Player;
import it.polimi.ingsw.ps04.utils.message.ActionMessage;
import it.polimi.ingsw.ps04.utils.message.Message;
import it.polimi.ingsw.ps04.utils.message.ModelRequestMessage;
import it.polimi.ingsw.ps04.utils.message.market.SellingPhaseMessage;
import it.polimi.ingsw.ps04.utils.message.market.UpdateMarketMessage;

/**
 * a class that represents the user interface
 */
public abstract class UserInterface implements Observer {

	protected static final String GAME_TITLE = "Council of Four Online";

	/*
	 * private Boolean loginResultArrived = false; private boolean loginResult;
	 */
	private Player clientPlayer;
	protected Model model;
	protected MessageSender messageSender;

	/**
	 * constructs a user interface with a given message sender and client model
	 * 
	 * @param messageSender
	 *            the sender of the messages of the user
	 * @param clientModel
	 *            the model of the client
	 */
	public UserInterface(MessageSender messageSender, Model clientModel) {
		this.messageSender = messageSender;
		this.model = clientModel;
	}

	/**
	 * Returns the message sender
	 * 
	 * @return the message sender
	 */
	public MessageSender getMessageSender() {
		return messageSender;
	}

	/**
	 * Returns the model
	 * 
	 * @return the model
	 */
	public Model getModel() {
		return model;
	}

	/**
	 * Returns the client player
	 * 
	 * @return the client player
	 */
	public Player getClientPlayer() {
		return model.getPlayersManager().getPlayerReference(clientPlayer);
	}

	/**
	 * Changes the client player of the user interface to be equal to the
	 * argument player
	 * 
	 * @param player
	 *            the player that you want to set
	 */
	private void setClientPlayer(Player player) {
		this.clientPlayer = player;
	}

	/**
	 * asks to the message sender to send a login request
	 * 
	 * @param username
	 *            the username of the player
	 * @param password
	 *            the password of the player
	 * @return true if the user logged in
	 */
	public boolean doLogin(String username, String password) {
		/*
		 * I do this before the login request because I don't want that the
		 * model arrives before I set my player. If the login is not alright it
		 * does not matter, because when you do the login again you will
		 * override the client player
		 */
		setClientPlayer(new Player(username));
		return messageSender.login(username, password);
	}

	/**
	 * asks to the message sender to send a sig nup request
	 * 
	 * @param username
	 *            the username of the player
	 * @param email
	 *            the email of the player
	 * @param password
	 *            the password of the player
	 * @return true if the sign up was successful
	 */
	public boolean doSignup(String username, String email, String password) {
		return messageSender.signup(username, email, password);
	}

	/**
	 * 
	 * @param username
	 *            the username of the player
	 * @param password
	 *            the password of the player
	 * @return true if the user was deleted
	 */
	public boolean doDeleteUser(String username, String password) {
		return messageSender.deleteUser(username, password);
	}

	/**
	 * launches the user interface
	 * 
	 * @throws IOException
	 *             if a problem with the I/O occurred
	 */
	public abstract void launch() throws IOException;

	/**
	 * updates the notify from the observed object
	 */
	@Override
	public synchronized void update(Observable o, Object arg1) {
		if (!(o instanceof Model)) {
			throw new IllegalArgumentException();
		}
		if (arg1 == null) {
			model = (Model) o;
			displayMainFrame();
		} else if (arg1 instanceof ActionMessage) {
			ActionMessage am = (ActionMessage) arg1;
			showMessage(am);
			try {
				showPlayer(model.getPlayersManager().getActivePlayer());
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (arg1 instanceof SellingPhaseMessage) {
			showSellingInterface();
		} else if (arg1 instanceof UpdateMarketMessage) {
			UpdateMarketMessage umm = (UpdateMarketMessage) arg1;
			showSellingOffers(umm.getMarket().getOffers());
		} else if (arg1 instanceof Exception) {
			showException((Exception) arg1);
		} else if (arg1 instanceof ModelRequestMessage) {
			messageSender.requestModel();
		} else if (arg1 instanceof Message) {
			Message msg = (Message) arg1;
			showMessage(msg);
		}
	}

	/**
	 * shows the selling interface
	 */
	protected abstract void showSellingInterface();

	/**
	 * displays the model
	 */
	protected abstract void displayMainFrame();

	/**
	 * shows an exception
	 * 
	 * @param exception
	 *            the exception that you want to show
	 */
	protected abstract void showException(Exception exception);

	/**
	 * generates a string that represents the given action
	 * 
	 * @param action
	 *            the action that you want to generate the string
	 * @return a string that represents the given action
	 */
	protected String generateActionString(Action action) {
		return model.getPlayersManager().getActivePlayer().getName() + " " + action.toString();
	}

	/**
	 * shows selling offers
	 * 
	 * @param offers
	 *            the selling offers that you want to show
	 */
	protected abstract void showSellingOffers(Map<String, SellingOffers> offers);

	/**
	 * shows a message
	 * 
	 * @param message
	 *            the message that you want to show
	 */
	protected abstract void showMessage(Message message);

	/**
	 * shows a player
	 * 
	 * @param player
	 *            the player that you want to show
	 */
	protected abstract void showPlayer(Player player);

	/**
	 * allows the user to insert a city
	 * 
	 * @return the city that was inserted by the user
	 */
	public abstract City insertCity();

	/**
	 * allows the user to insert a tile
	 * 
	 * @return the tile that was inserted by the user
	 */
	public abstract BusinessPermitTile insertMyTile();

	/**
	 * allows the user to insert a region
	 * 
	 * @return the region that was inserted by the user
	 */
	public abstract Region insertRegion();

	/**
	 * allows the user to choose the number of a tile of the business permit
	 * tiles deck of a given region
	 * 
	 * @param region
	 *            the region of the business permit tiles that contains the tile
	 *            that you have to choose
	 * @return the number of the tile that was chosen by the user
	 */
	public abstract int chooseTile(Region region);

}
