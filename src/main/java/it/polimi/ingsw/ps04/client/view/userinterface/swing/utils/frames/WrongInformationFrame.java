package it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.frames;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;

import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.FrameUtilities;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.StandardButtonListener;

public class WrongInformationFrame extends FrameUtilities{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final String TITLE = "Missing information";

	/**
	 * Builder i called methods for create this frame and set it
	 */
	public WrongInformationFrame() {
		createFrame();
		settingMethod();
	}
	
	/**
	 * Here i use standard settings
	 */
	@Override
	public void settingMethod() {
		settingMethodStandard(TITLE);
		this.pack();
	}
	
	/**
	 * Here i create this simple frame with only a label which inform you that you miss information
	 * 
	 */
	@Override
	public void createFrame() {
		Container framePanel = this.getContentPane();
		framePanel.setLayout (new BoxLayout(framePanel, BoxLayout.Y_AXIS));
		addLabel ("Something went wrong! Take care about the information you enter in  ", framePanel);
		exit = addButton (EXIT, framePanel);
		exit.addActionListener(new Listener());
	}

	/**
	 * Inner class to manage actionlistener
	 *
	 */
	private class Listener extends StandardButtonListener implements ActionListener {

		/**
		 * Manage what to do if you click the button "exit"
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			
			if (e.getSource().equals(exit)) {
				exit();
			}
		}
		
		@Override
		public void exit() {
			dispose();
		}
	}
}
