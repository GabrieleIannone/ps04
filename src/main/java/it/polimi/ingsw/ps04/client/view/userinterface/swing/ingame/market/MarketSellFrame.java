package it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame.market;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import it.polimi.ingsw.ps04.client.view.userinterface.UserInterface;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame.market.sell.SellAssistantFrame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame.market.sell.SellPoliticFrame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame.market.sell.SellTileFrame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.FrameUtilities;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.StandardButtonListener;
import it.polimi.ingsw.ps04.model.market.SellingOffers;

public class MarketSellFrame extends FrameUtilities {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final String TITLE = "Market Sell Phase";
	private static final String ASSISTANTS = "Assistants";
	private static final String TILES = "Business permit tiles";
	private static final String POLITIC_CARDS = "Politic cards";
	private static final String SKIP_SELL_PHASE = "Skip sell phase";
	private static final int X_SIZE = 600;
	private static final int Y_SIZE = 300;

	private JButton assistantButton, chooseTileButton, politicCardButton;
	private UserInterface ui;
	private JPanel sellPanel = new JPanel();
	private JPanel buttonPanel = new JPanel();
	private JButton skipSellPhase;
	private SellingOffers offers = new SellingOffers();
	
	/**
	 * Builder for Market sell frame which will use ui in order to send your offers 
	 * 
	 * @param ui
	 * 			which is used to send your offers
	 */
	public MarketSellFrame(UserInterface ui) {
		this.ui = ui;
		createFrame();
		settingMethod();
	}
	
	/**
	 * @see FrameUtilities
	 */
	@Override
	public void settingMethod() {
		settingMethodStandard(TITLE);
		this.setSize(X_SIZE, Y_SIZE);
	}

	/**
	 * Here I create a frame which has an info label in the north position, sell Panel in the center and the 
	 * buttons Panel in the south
	 */
	@Override
	public void createFrame() {
		Container framePanel = getContentPane();
		framePanel.setLayout(new BorderLayout());
		JLabel info = addLabel ("What you want to sell?", framePanel);
		addComponentToButtonPanel();
		addComponentToSellPanel();
		framePanel.add(info, BorderLayout.NORTH);
		framePanel.add(sellPanel, BorderLayout.CENTER);
		framePanel.add(buttonPanel, BorderLayout.SOUTH);
	}

	/**
	 * Here I add done and skip sell phase buttons to button Panel
	 */
	private void addComponentToButtonPanel() {
		buttonPanel.setLayout(new FlowLayout());
		done = addButton (DONE, buttonPanel);
		done.addActionListener(new Listener());
		skipSellPhase = addButton (SKIP_SELL_PHASE, buttonPanel);
		skipSellPhase.addActionListener(new Listener());
	}

	/**
	 * Here I add assistants, politic card and tile button, you can choose what you want to sell
	 */
	private void addComponentToSellPanel() {
		sellPanel.setLayout(new FlowLayout());
		assistantButton = addButton (ASSISTANTS, sellPanel);
		assistantButton.addActionListener(new Listener());
		politicCardButton = addButton (POLITIC_CARDS, sellPanel);
		politicCardButton.addActionListener(new Listener());
		chooseTileButton = addButton (TILES, sellPanel);
		chooseTileButton.addActionListener(new Listener());
	}

	/**
	 * Inner listener class in order to manage listeners
	 *
	 */
	private class Listener extends StandardButtonListener implements ActionListener {
		
		/**
		 * Here I manage what to do if you click on a button
		 */
		@Override
		public void actionPerformed(ActionEvent e) {

			/*
			 * if you click on a tile, assistants, politiccard button the right frame is displayed
			 * if you click on done button ui will send your offers and 
			 * if you click on skip sell phase you will skip this phase
			 */
			if (e.getSource().equals(chooseTileButton)) {
				new SellTileFrame (ui.getClientPlayer(), offers);
			}
			else if (e.getSource().equals(politicCardButton)) {
				new SellPoliticFrame(ui.getClientPlayer(), offers);
			}
			else if (e.getSource().equals(assistantButton)) {
				new SellAssistantFrame (ui.getClientPlayer(), offers);
			}
			else if (e.getSource().equals(done)) {
				ui.getMessageSender().sell(offers);
				exit();
			}
			else if (e.getSource().equals(skipSellPhase)) {
				ui.getMessageSender().skipSell();
				exit();
			}	
		}
		
		/**
		 * @see StandardButtonListener
		 */
		@Override
		public void exit() {
			dispose();
		}
	}
}
