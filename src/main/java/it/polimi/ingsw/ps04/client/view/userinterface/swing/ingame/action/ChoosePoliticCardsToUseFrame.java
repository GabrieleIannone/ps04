package it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame.action;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import it.polimi.ingsw.ps04.client.view.userinterface.UserInterface;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.FrameUtilities;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.StandardButtonListener;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.frames.WrongInformationFrame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.panels.ChoosePoliticCards;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.panels.DrawCouncil;
import it.polimi.ingsw.ps04.model.board.City;
import it.polimi.ingsw.ps04.model.board.Region;
import it.polimi.ingsw.ps04.model.deck.politic.PoliticCard;

public class ChoosePoliticCardsToUseFrame extends FrameUtilities {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final String TITLE = "Choose Politic Cards";
	private static final String CHOOSE_CARDS = "Now choose which cards do you want to use: ";
	private static final String ACQUIRE_BUSINESS_PERMIT_TILE = "Acquire Business Permit tile";
	private static final String BUILD_WITH_KING = "Build with king";
	private static final int X_SIZE = 900;
	private static final int Y_SIZE = 300;
	
	private UserInterface ui;
	private Region region;
	private DrawCouncil councilRegionPanel;

	private ArrayList<PoliticCard> cardUsed = new ArrayList<PoliticCard>(); 
	private JPanel councilRegion;
	private JPanel chooseYourCardPanel = new JPanel();
	private JPanel buttonPanel = new JPanel();
	private ChoosePoliticCards displayPoliticCards;
	private String actionString;
	private City city;

	/**
	 * Builder for ChoosePoliticCardsToUseFrame which has UserInterface to send action, the region of
	 * which I want to draw council, the actionstring in order to do the right action and the city 
	 * where I want to build if the action is related with king
	 * 
	 * @param ui
	 * 			which is used to send action
	 * @param region
	 * 				of which I want to draw council
	 * @param actionString
	 * 					  the string name for action
	 * @param city
	 * 			  where I want to build in with king
	 */
	public ChoosePoliticCardsToUseFrame(UserInterface ui, Region region, String actionString, City city) {
		this.city = city;
		this.actionString = actionString;
		this.ui = ui;
		this.region = region;
		settingMethod();
		createFrame();
	}
	
	/**
	 * @see FrameUtilities
	 */
	@Override
	public void settingMethod() {
		settingMethodStandard(TITLE);
		this.setSize(X_SIZE,Y_SIZE);
	}

	/**
	 * Here I create the frame with the region or king council and your Politic card in the middle
	 * and button panel in south position
	 */
	@Override
	public void createFrame() {
		Container framePanel = this.getContentPane();
		framePanel.setLayout(new BorderLayout());
		if (actionString.equals(BUILD_WITH_KING))
			councilRegionPanel = new DrawCouncil(ui.getModel().getMap().getKing().getCouncil());
		else {
			councilRegionPanel = new DrawCouncil(region.getCouncil());
		}
		councilRegion = councilRegionPanel.getPanel();
		chooseYourCard();
		addButtonsComponents();
		framePanel.add(buttonPanel, BorderLayout.SOUTH);
		framePanel.add(chooseYourCardPanel, BorderLayout.CENTER);
		framePanel.add(councilRegion, BorderLayout.NORTH);
	}

	/**
	 * Here i create exit and done buttons to Button panel
	 */
	private void addButtonsComponents() {
		buttonPanel.setLayout(new BorderLayout());
		done = new JButton(DONE);
		done.addActionListener(new Listener());
		exit = new JButton (EXIT);
		exit.addActionListener(new Listener());
		buttonPanel.add(done, BorderLayout.EAST);
		buttonPanel.add(exit, BorderLayout.WEST);
	}

	/**
	 * Here i display Politic cards you have in your hand
	 */
	private void chooseYourCard() {
		chooseYourCardPanel.setLayout(new BorderLayout());
		JLabel label = addLabel(CHOOSE_CARDS, chooseYourCardPanel);
		chooseYourCardPanel.add(label, BorderLayout.NORTH);
		displayPoliticCards = new ChoosePoliticCards(ui.getClientPlayer());
		JPanel cardPanel = displayPoliticCards.getPanel();
		chooseYourCardPanel.add(cardPanel, BorderLayout.CENTER);
	}
	
	/**
	 * Inner class to manage actionlistener
	 *
	 */
	private class Listener extends StandardButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			/**
			 * Here I manage what to do if you click on exit or done button
			 */
			if (e.getSource().equals(exit)) {
				exit();
			} 
			/*
			 * If i didn't get cardUsed then WrongInformationFrame
			 */
			else if (e.getSource().equals(done)) {
					cardUsed = displayPoliticCards.getCardsChoosen();
					if (cardUsed.isEmpty())
						new WrongInformationFrame();
					else {
						done();
					}
				}
			}
		
		/**
		 * @see StandardButtonListener
		 */
		@Override
		public void exit() {
			dispose();
		}
		
		/**
		 * Here I manage what to do after you click done button: if you want to acquire a tile or 
		 * you want to build with king
		 */
		public void done(){
			if (actionString.equals(ACQUIRE_BUSINESS_PERMIT_TILE)) {
				new ChooseTileFromRegionFrame(ui, region, cardUsed);
				exit();
			}
			else if (actionString.equals(BUILD_WITH_KING)) {
				ui.getMessageSender().buildEmporiumWithKing(cardUsed, city);
				exit();
			}
		}
	}
}

