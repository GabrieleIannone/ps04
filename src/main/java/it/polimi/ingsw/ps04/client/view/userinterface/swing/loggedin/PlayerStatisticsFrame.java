package it.polimi.ingsw.ps04.client.view.userinterface.swing.loggedin;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.FrameUtilities;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.StandardButtonListener;
import it.polimi.ingsw.ps04.utils.statistics.PlayerStatistics;

public class PlayerStatisticsFrame extends FrameUtilities{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final String GAME_WON = "You have won ";
	private static final String GAME = " games";
	private static final String GAME_MINUTES = "You have played for ";
	private static final String MINUTES = " minutes";
	private static final String GAME_PLAYED = "You have played " ;
	
	private PlayerStatistics playerStatistics;
	private JPanel playerStatistcsPanel = new JPanel();

	/**
	 * Builder for player statistics frame
	 * 
	 * @param playerStatistics
	 * 						  which I use to get information about this single player
	 */
	public PlayerStatisticsFrame(PlayerStatistics playerStatistics) {
		this.playerStatistics = playerStatistics;
		createFrame();
		settingMethod();
	}

	/**
	 * @see FrameUtilities
	 */
	@Override
	public void settingMethod() {
		settingMethodStandard(playerStatistics.getPlayerName());
		this.pack();
	}

	/**
	 * Here I create a simple frame which has in center position info about statistics and in the south an exit button
	 */
	@Override
	public void createFrame() {
		Container framePanel = getContentPane();
		framePanel.setLayout(new BorderLayout());
		exit = addButton (EXIT, framePanel);
		exit.addActionListener(new Listener());
		addComponentsToPlayerStatistcsPanel();
		framePanel.add(playerStatistcsPanel, BorderLayout.CENTER);
		framePanel.add(exit, BorderLayout.SOUTH);
	}
	
	/**
	 * Here i add labels with game played, game won and minutes spent
	 */
	private void addComponentsToPlayerStatistcsPanel() {
		playerStatistcsPanel.setLayout(new BoxLayout(playerStatistcsPanel, BoxLayout.Y_AXIS));
		addLabel (GAME_PLAYED +Integer.valueOf(playerStatistics.getPlayedGames())+ GAME, playerStatistcsPanel );
		addLabel (GAME_WON +Integer.valueOf(playerStatistics.getWonGames())+ GAME, playerStatistcsPanel );
		addLabel (GAME_MINUTES +Integer.valueOf(playerStatistics.getGameMinutes())+ MINUTES, playerStatistcsPanel );
	}

	/**
	 * Inner class to manage actionlistener
	 *
	 */
	private class Listener extends StandardButtonListener implements ActionListener {

		/**
		 * Here i manage what to do if you click exit button
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource().equals(exit)) {
				exit();
			}
		}
		
		/**
		 * @see StandardButtonListener
		 */
		@Override
		public void exit() {
			dispose();
		}
	}
}
