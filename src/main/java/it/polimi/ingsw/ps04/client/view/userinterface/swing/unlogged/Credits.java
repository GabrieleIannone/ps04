package it.polimi.ingsw.ps04.client.view.userinterface.swing.unlogged;

import java.awt.Container;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;

import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.FrameUtilities;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.StandardButtonListener;

public class Credits extends FrameUtilities{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final String TITLE = "Credits";
	
	private static final int NUMROW = 5;
	private static final int NUMCOLUMN = 2;
	
	private JLabel credits;
	
	/**
	 * Builder for credits frame
	 */
	public Credits (){
		createFrame();
		settingMethod();
	}

	/**
	 * I create a frame with three labels and one exit button
	 */
	@Override
	public void createFrame() {
		Container framePanel = this.getContentPane();
		framePanel.setLayout(new GridLayout(NUMROW, NUMCOLUMN));
		credits = addLabel("Thanks to", framePanel);
		credits.setFont(new Font("Serif", Font.BOLD, 30));
		addLabel("Iannone Gabriele", framePanel);
		addLabel("Ieni Marco", framePanel);
		addLabel("Lamonaca Francesco", framePanel);
		exit = addButton ("Exit", framePanel);
		exit.addActionListener(new Listener());
	}
	
	/**
	 * @see FrameUtilities
	 */
	public void settingMethod() {
		settingMethodStandard(TITLE);
		this.pack();
	}

	/**
	 * Inner class to manage listener
	 *
	 */
	private class Listener extends StandardButtonListener implements ActionListener {
		
		/**
		 * Here i manage what to do if you click on exit button
		 */
		@Override
		public void actionPerformed(ActionEvent e) {

			if (e.getSource().equals(exit)) {
				exit();
			}
		}
		
		/**
		 * @see StandardButtonListener
		 */
		@Override
		public void exit() {
			dispose();
		}
	}
}
