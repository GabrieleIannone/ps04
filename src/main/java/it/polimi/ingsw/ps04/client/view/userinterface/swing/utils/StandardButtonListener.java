package it.polimi.ingsw.ps04.client.view.userinterface.swing.utils;

import java.awt.Window;

/**
 * This class is used for standard actions like dispose a single window or all windows
 *
 */
public abstract class StandardButtonListener{

	/**
	 * this method will close only the windows in which it's called
	 */
	public abstract void exit();
	/**
	 * this method will close all windows
	 */
	public void closeAllWindows(){
		for (Window window : Window.getWindows()) {
		    window.dispose();
			}
	}
	
}
