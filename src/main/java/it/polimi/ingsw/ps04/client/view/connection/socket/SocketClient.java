package it.polimi.ingsw.ps04.client.view.connection.socket;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;
import javax.security.auth.login.LoginException;

import it.polimi.ingsw.ps04.client.view.connection.Connection;
import it.polimi.ingsw.ps04.utils.message.Message;
import it.polimi.ingsw.ps04.utils.message.login.CredentialsMessage;
import it.polimi.ingsw.ps04.utils.message.login.LoginMessage;
import it.polimi.ingsw.ps04.utils.message.login.SuccessMessage;

/**
 * this class implements the socket connection for the client
 */
public class SocketClient extends Connection implements Runnable {
	private String serverAddress;
	private int port;
	private Socket socket;
	private boolean active = true;
	private ObjectInputStream objectInputStream;
	private ObjectOutputStream objectOutputStream;

	/**
	 * constructs a SocketClient with ip: "127.0.01" and port: 12345
	 */
	public SocketClient() {
		this("127.0.01", 12345);
	}

	/**
	 * constructs a SocketClient with a given address and port
	 * 
	 * @param serverAddress
	 *            the server address you want to connect
	 * @param port
	 *            the port of the server
	 */
	public SocketClient(String serverAddress, int port) {
		this.serverAddress = serverAddress;
		this.port = port;
	}

	/**
	 * tells if the socket connection is active
	 * 
	 * @return true if it is active
	 */
	private synchronized boolean isActive() {
		return active;
	}

	/**
	 * send an object to the server
	 * 
	 * @param obj
	 *            the object that you want to send
	 */
	private void sendObject(Object obj) {
		try {
			objectOutputStream.writeObject(obj);
		} catch (IOException e) {
			System.err.println("connession error");
		}
	}

	/**
	 * receive an object from the server
	 * 
	 * @return the received object
	 * @throws ClassNotFoundException
	 *             if the client did not found definition for the class of the
	 *             received object
	 * @throws IOException
	 *             if there were problems in the connection
	 */
	public Object receiveObject() throws ClassNotFoundException, IOException {
		try {
			return objectInputStream.readObject();
		} catch (SocketException e) {
			closeConnection();
			throw new IOException();
		}
	}

	/**
	 * starts the socket connection
	 */
	@Override
	public void startConnection() {
		try {
			socket = new Socket(serverAddress, port);
		} catch (IOException e) {
			System.err.println("server not available");
		}
		try {
			objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
			objectInputStream = new ObjectInputStream(socket.getInputStream());
		} catch (IOException e) {
			System.err.println("communication error");
			e.printStackTrace();
		}
	}

	/**
	 * sends a message
	 */
	@Override
	public void sendMessage(Message message) {
		sendObject(message);
	}

	/**
	 * sends a credentials message
	 */
	@Override
	public boolean sendCredentialsMessage(CredentialsMessage credentialsMessage) {
		sendMessage(credentialsMessage);
		SuccessMessage successMessage = new SuccessMessage(false);
		try {
			Object receivedObject = receiveObject();

			if (receivedObject instanceof LoginException) {
				notify(receivedObject);
				return false;
			} else {
				successMessage = (SuccessMessage) receivedObject;

				Boolean operationSuccess = successMessage.getResult();
				if (operationSuccess && credentialsMessage instanceof LoginMessage)
					(new Thread(this)).start();
				return operationSuccess;
			}
		} catch (IOException | ClassNotFoundException e1) {
			System.err.println("connection error");
			return false;
		}

	}

	/**
	 * runs the socket client thread
	 */
	@Override
	public void run() {
		while (isActive()) {
			try {
				notify(receiveObject());
			} catch (ClassNotFoundException | IOException e) {
				System.err.println("connection error");
				e.printStackTrace();
			}
		}
	}

	/**
	 * closes the connection correctly
	 */
	private synchronized void closeConnection() {
		try {
			socket.close();
		} catch (IOException e) {
		}
		active = false;
		System.out.println("socket connection closed");
	}
}
