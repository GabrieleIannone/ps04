package it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame.market.sell;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import it.polimi.ingsw.ps04.client.view.userinterface.swing.GUIGame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.FrameUtilities;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.ImageColorReplacer;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.StandardButtonListener;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.frames.SomethingWentWrongFrame;
import it.polimi.ingsw.ps04.model.deck.politic.PoliticCard;
import it.polimi.ingsw.ps04.model.market.SellingOffers;
import it.polimi.ingsw.ps04.model.player.Player;

public class SellPoliticFrame extends FrameUtilities {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3590478876243472656L;
	
	private static final String TITLE = "Sell Politics Cards";
	private static final String INFO = "If you want to sell a card just click on it";
	private static final String CARD_IMAGE_PATH = GUIGame.IMAGES_PATH + "politics_card.png";
	private static final int X_SIZE = 400;
	private static final int Y_SIZE = 600;
	private static final int NUM_COLUMNS = 2;
	private static final int NUM_ROW = 10;
	
	private Player player;
	private JPanel cardsPanel = new JPanel();
	private JPanel buttonPanel = new JPanel();
	private SellingOffers offers;
	
	/**
	 * Builder for Sell Tile Frame which has clientPlayer who is selling something and offers 
	 * reference in order to add some item to it
	 * 
	 * @param clientPlayer
	 * 					  who is the client player
	 * @param offers
	 * 				reference
	 */
	public SellPoliticFrame(Player clientPlayer, SellingOffers offers) {
		this.offers = offers;
		this.player = clientPlayer;
		createFrame();
		settingMethod();
	}

	/**
	 * @see FrameUtilities
	 */
	@Override
	public void settingMethod() {
		settingMethodStandard(TITLE);
		this.setSize(X_SIZE,Y_SIZE);
	}

	/**
	 * I create a simple frame with a button panel in the south, card panel in the center and info
	 * label in the north
	 */
	@Override
	public void createFrame() {
		Container framePanel = getContentPane();
		JLabel infoLabel = addLabel (INFO, framePanel);
		addComponentToCardsPanel();
		addComponentToButtonPanel();
		framePanel.add(infoLabel, BorderLayout.NORTH);
		framePanel.add(cardsPanel, BorderLayout.CENTER);
		framePanel.add(buttonPanel, BorderLayout.SOUTH);
	}
	
	/**
	 * Here I add done and exit buttons to button Panel
	 */
	private void addComponentToButtonPanel() {
		buttonPanel.setLayout(new FlowLayout());
		exit = addButton (EXIT, buttonPanel);
		exit.addActionListener(new Listener());
		done = addButton (DONE, buttonPanel);
		done.addActionListener(new Listener());
	}
	
	/**
	 * Here I add a card button and a TextField for cost for each card player got in his hand
	 */
	private void addComponentToCardsPanel() {
		cardsPanel.setLayout(new GridLayout(NUM_ROW, NUM_COLUMNS));
		for (PoliticCard card : player.getPoliticCardsHand().getHand()) {
			try {
				JButton cardButton = new JButton (new ImageIcon(
						ImageColorReplacer.changeColor(new File(CARD_IMAGE_PATH), card.getColour())));
				cardButton.setText(card.getColour().getName());
				cardButton.addActionListener(new CardListener (card));
				cardsPanel.add(cardButton);
			} catch (IOException e) {
				new SomethingWentWrongFrame();
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Inner class to manage this kind of listener
	 *
	 */
	private class Listener extends StandardButtonListener implements ActionListener {
		
		/**
		 * Here I manage what to do if you click on exit or done button
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			
			if (e.getSource().equals(exit)) {
				exit();
			}
			else if (e.getSource().equals(done)){
				exit();
				}
			}
		
		/**
		 * @see StandardButtonListener
		 */
		@Override
		public void exit() {
			dispose();
		}
	}
	
	/**
	 * Inner class to manage this kind of listener which is reported to a tile 
	 *
	 */
	private class CardListener implements ActionListener {
		
		private PoliticCard card;

		/**
		 * Builder for this kind of listener which has the tile
		 * 
		 * @param card
		 * 			  which is the card to which is reported the listener
		 */
		public CardListener(PoliticCard card) {
			this.card = card;
		}

		/**
		 * What to do if you click on a button which has this kind of listener
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			new SellSinglePoliticCardFrame(card, offers);
		}
	}
}