package it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame.action;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.JTextField;

import it.polimi.ingsw.ps04.client.view.userinterface.UserInterface;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.FrameUtilities;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.StandardButtonListener;
import it.polimi.ingsw.ps04.model.board.City;

public class ChooseCityFrame extends FrameUtilities {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9045240783607763311L;
	
	private static final String TITLE = "Choose a city";
	private static final String INFO = "Which city do you want to choose? ";
	
	private Listener listener = new Listener();
	private UserInterface ui;
	private City cityChoosen;
	private JTextField cityText;

	/**
	 * Builder of the ChooseCity frame which has the UserInterface in order to get the right city from city name
	 * 
	 * @param ui
	 * 			which I use to get the city from city name
	 */
	public ChooseCityFrame(UserInterface ui) {
		this.ui = ui;
		createFrame();
		settingMethod();
	}
	
	/**
	 * @see FrameUtilities
	 */
	@Override
	public void settingMethod() {
		settingMethodStandard(TITLE);
		pack();
	}

	/**
	 * Here I create a frame with and exit button in the south, info label and city name textField in the center
	 */
	@Override
	public void createFrame() {
		setLayout(new BorderLayout());
		exit = addButton (EXIT, getContentPane());
		add(exit, BorderLayout.SOUTH);
		JPanel cityPanel = new JPanel();
		cityPanel.setLayout(new FlowLayout());
		addLabel (INFO, cityPanel);
		cityText = addTextField ("", cityPanel);
		cityText.addActionListener(listener);
		add(cityPanel, BorderLayout.CENTER);
	}
	
	/**
	 * this method will return the listener to the textfield in order to get the right information for the 
	 * complex bonus
	 * 
	 * @return listener
	 * 				   which is reported to the textfield				
	 */
	public Listener getCityListener() {
		return listener;
	}
	
	/**
	 * Inner class to manage listeners 
	 *
	 */
	public class Listener extends StandardButtonListener implements ActionListener {

		/**
		 * Manage what to do if you click on exit button or city TextField 
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource().equals(exit)) {
				exit();
			}
			if (e.getSource().equals(cityText)){
				for (City city : ui.getModel().getMap().getCities()){
					if (city.getName().equals(cityText.getText()))
						cityChoosen = city;
				}
			}
		}
		
		/**
		 * this method is used to get city for complex bonus
		 * 
		 * @return city
		 * 				which is choosen
		 */
		public City getCity(){
			return cityChoosen;
		}
		
		/**
		 * @see StandardButtonListener
		 */
		@Override
		public void exit() {
			dispose();
		}
	}
}
