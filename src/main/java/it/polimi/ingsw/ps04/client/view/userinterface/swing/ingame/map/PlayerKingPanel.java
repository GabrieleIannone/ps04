package it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame.map;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import it.polimi.ingsw.ps04.client.view.userinterface.UserInterface;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.GUIGame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame.OthersPlayerFrame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.ComponentUtilities;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.panels.DrawCouncil;
import it.polimi.ingsw.ps04.model.player.Player;

public class PlayerKingPanel extends JComponent implements ComponentUtilities{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7388072186819047861L;
	
	private UserInterface ui;
	private JPanel mainPanel = new JPanel();
	private JPanel playersPanel = new JPanel();
	private JPanel kingPanel = new JPanel();
	
	/**
	 * costructor of PlayerKingPanel
	 * 
	 * @param ui
	 * 			Userinterface of the fame
	 */
	public PlayerKingPanel(GUIGame gg) {
		this.ui = gg;
		createPanel();
	}
	
	/**
	 * Create mainPanel and add players and king components to it
	 * 
	 */
	private void createPanel() {
		mainPanel.setLayout(new BorderLayout());
		addComponentsToPlayerPanel();
		addComponentsToKingPanel();
		mainPanel.add(playersPanel, BorderLayout.EAST);
		mainPanel.add(kingPanel, BorderLayout.WEST);
	}
	
	/**
	 * Add one button for each other player and remember you who you are
	 * 
	 */
	private void addComponentsToPlayerPanel() {
		playersPanel.setLayout(new BoxLayout(playersPanel, BoxLayout.Y_AXIS));
		addLabel ("Players:", playersPanel);
		for (Player player: ui.getModel().getPlayersManager().getAllPlayers()) {
			JButton button = addButton (player.getName(), playersPanel);
			button.addActionListener(new Listener());
		}
		addLabel("You are : " + ui.getClientPlayer().getName(), playersPanel);
	}
	
	/**
	 * Display where king is and his council
	 */
	private void addComponentsToKingPanel() {
		kingPanel.setLayout(new BoxLayout (kingPanel, BoxLayout.Y_AXIS));
		addLabel ("King is in: " + ui.getModel().getMap().getKingPosition().getName(), kingPanel);
		addLabel ("King council: ", kingPanel);
		JPanel kingCouncilPanel = new JPanel();
		DrawCouncil kingCouncil = new DrawCouncil(ui.getModel().getMap().getKing().getCouncil());
		kingCouncilPanel = kingCouncil.getPanel();
		kingPanel.add(kingCouncilPanel);
	}
	
	/**
	 * this method will be used in others classes to get king and players informations
	 * 
	 * @return panel
	 * 				in order to add it in other frames
	 */
	public JPanel getPlayerKingPanel () {
		return mainPanel;
	}
	
	/**
	 * create, set and add a button
	 * 
	 * @param string
	 * 				which is the button text
	 * @param frame
	 * 				which is where i add this button
	 */
	@Override
	public JButton addButton(String string, Container frame) {
		JButton button = new JButton (string);
		sets(button);
		frame.add(button);
		return button;
	}

	/**
	 * create, set and add a label
	 * 
	 * @param string
	 * 				which is the label text
	 * @param frame
	 * 				which is where i add this label
	 */
	@Override
	public JLabel addLabel(String string, Container frame) {
		JLabel label = new JLabel (string);
		sets(label);
		frame.add(label);
		return label;
	}

	/**
	 * Set standard font, background, foreground and alignments of a component
	 * 
	 * @param component
	 * 					which i want to set
	 */
	@Override
	public void sets(JComponent component) {
		component.setFont(new Font("Serif", Font.ITALIC, 20));
		component.setForeground(Color.RED);
		component.setBackground(Color.WHITE);
		component.setAlignmentX(CENTER_ALIGNMENT);
		component.setAlignmentY(CENTER_ALIGNMENT);
	}
	
	/**
	 * Inner class to manage players button listeners
	 *
	 */
	private class Listener implements ActionListener {
		
		/**
		 * if you click on a player button it will show you his resources
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			
			JButton buttonPressed = (JButton) e.getSource();
			
			for (Player player : ui.getModel().getPlayersManager().getAllPlayers()) {
				if (player.getName().equals(buttonPressed.getText())) {
					new OthersPlayerFrame(player);
				}
			}
		}
	}
}
