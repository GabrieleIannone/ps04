package it.polimi.ingsw.ps04.client.view.userinterface.swing.utils;

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.*;

public abstract class FrameUtilities extends JFrame implements ComponentUtilities{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String EXIT = "exit";
	public static final String DONE = "done";
	
	public JButton exit;
	public JButton done;
	
	/**
	 * Those methods are used in all frames in order to create and set the new frame
	 */
	public abstract void settingMethod();
	public abstract void createFrame();
	
	/**
	 * Add single JTextField
	 * 
	 * @param frame
	 * 			    where i want to add button
	 * @param string
	 * 				 which is the text for the text field 
	 * @return JTextfield
	 * 				      created in order to add the proper listener
	 */
	public JTextField addTextField(String string, Container frame){
		JTextField textField = new JTextField(string);
		sets(textField);
		frame.add(textField);
		return textField;
	}
	
	/**
	 * Add single JButton
	 * 
	 * @param frame
	 * 				which is the frame where i want to add button
	 * @param string
	 * 				which is the text for the button
	 * @return button
	 * 				 created in order to add the proper listener
	 */
	@Override
	public JButton addButton(String string, Container frame) {
		JButton button = new JButton (string);
		sets(button);
		button.setContentAreaFilled(false);
		button.setBorderPainted(false);
		button.setOpaque(false);
		frame.add(button);
		return button;
	}
	
	/**
	 * Add single Label 
	 * 
	 * @param frame
	 * 			    where i want to add button
	 * @param string
	 * 				 which is the text for the button
	 * @return label
	 * 				 created 
	 */
	@Override
	public JLabel addLabel(String string, Container panel) {
		JLabel label = new JLabel (string);
		sets(label);
		panel.add(label);
		return label;
	}
	
	/**
	 * Here i manage to change font, colour and dimension
	 * 
	 * @param component
	 * 					i want to set
	 */
	@Override
	public void sets(JComponent component) {
		component.setFont(new Font("Serif", Font.ITALIC, 20));
		component.setForeground(Color.RED);
		component.setBackground(Color.WHITE);
		component.setAlignmentX(CENTER_ALIGNMENT);
		component.setAlignmentY(CENTER_ALIGNMENT);
	}
	
	/**
	 * I use this method for default frame settings like title, visibility, close operation 
	 * and location
	 * 
	 * @param title
	 * 				of the frame
	 */
	public void settingMethodStandard(String title) {
		this.setTitle(title);
		this.setVisible(true);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
	}
	
	protected void paintComponent(Graphics g) {
		super.paintComponents(g);
	}
}
