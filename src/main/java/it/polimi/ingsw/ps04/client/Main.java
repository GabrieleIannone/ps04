package it.polimi.ingsw.ps04.client;

import java.io.IOException;
import java.util.Scanner;

import it.polimi.ingsw.ps04.client.view.MessageSender;
import it.polimi.ingsw.ps04.client.view.connection.Connection;
import it.polimi.ingsw.ps04.client.view.connection.rmi.RMIClient;
import it.polimi.ingsw.ps04.client.view.connection.socket.SocketClient;
import it.polimi.ingsw.ps04.client.view.userinterface.UserInterface;
import it.polimi.ingsw.ps04.client.view.userinterface.cli.InitialCli;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.GUIGame;
import it.polimi.ingsw.ps04.model.Model;
import it.polimi.ingsw.ps04.model.bonus.bonusVisitor.ClientBonusVisitor;
import it.polimi.ingsw.ps04.client.controller.ClientController;

/**
 * this class runs the client
 */
public class Main {
	/**
	 * this is the main that starts the client
	 * 
	 * @param args
	 *            not used
	 */
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int choice;
		do {
			System.out.println("choose:\n1. cli - socket\n2. cli - rmi\n3. gui - socket\n4. gui - rmi");
			choice = in.nextInt();
		} while (choice < 1 && choice > 4);
		Connection connection;
		if (choice == 1 || choice == 3) {
			connection = new SocketClient();
		} else {
			connection = new RMIClient();
		}
		connection.startConnection();
		MessageSender messageSender = new MessageSender(connection);
		UserInterface ui;
		Model clientModel = new Model();
		if (choice == 1 || choice == 2) {
			ui = new InitialCli(messageSender, clientModel);
		} else {
			ui = new GUIGame(messageSender, clientModel);
		}
		clientModel.setBonusVisitor(new ClientBonusVisitor(ui));

		ClientController controller = new ClientController(clientModel);
		clientModel.addObserver(ui);
		connection.addObserver(controller);
		try {
			ui.launch();
		} catch (IOException e) {
			e.printStackTrace();
		}
		in.close();
	}
}
