package it.polimi.ingsw.ps04.client.view.userinterface.swing.ingame;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JPanel;

import it.polimi.ingsw.ps04.client.view.userinterface.swing.GUIGame;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.FrameUtilities;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.StandardButtonListener;

public class ActionListFrame extends FrameUtilities {

	/**
	 * 
	 */
	private static final long serialVersionUID = -297291428669186012L;

	private static final String TITLE = "List Action";
	private static final int NUM_ROW = 30;
	private static final int NUM_COLUMNS = 2;

	private static final String NO_ACTION_DONE = "No one have done actions";
	
	private JPanel listActionPanel = new JPanel();
	
	private ArrayList <String> actionList = new ArrayList <> ();
	
	/**
	 * Builder of Action list frame that will set itself and will take an arrayList in order to print it
	 * 
	 * @param actionList
	 * 					which is an ArrayList of the actions that I will print 
	 * @param gg
	 * 			which I use for setActionListFrame in order to update it after
	 */
	public ActionListFrame(ArrayList<String> actionList, GUIGame gg) {
		this.actionList = actionList;
		gg.setActionListFrame(this);
		createFrame();
		settingMethod();
	}

	/**
	 * @see FrameUtilities
	 */
	@Override
	public void settingMethod() {
		setTitle(TITLE);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setVisible(true);
		pack();
	}
	
	/**
	 * Here I create a simple frame in which i will print all actions and put an exit button in south position
	 */
	@Override
	public void createFrame() {
		Container framePanel = getContentPane();
		framePanel.setLayout(new BorderLayout());
		exit = addButton (EXIT, framePanel);
		exit.addActionListener(new Listener());
		listActionPanel.setLayout(new GridLayout(NUM_ROW, NUM_COLUMNS));
		addActionToList();
		framePanel.add(exit, BorderLayout.SOUTH);
		framePanel.add(listActionPanel, BorderLayout.CENTER);
	}
	
	/**
	 * Here I print all action list 
	 */
	public void addActionToList() {
		if (actionList.isEmpty()) {
			addLabel (NO_ACTION_DONE, listActionPanel);
		}
		else for (String string : actionList) {
			addLabel (string, listActionPanel);
		}
	}

	/**
	 * Inner class to manage listener
	 *
	 */
	private class Listener extends StandardButtonListener implements ActionListener {

		/**
		 * Here I manage what to do if you click on exit button
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
		
			if (e.getSource().equals(exit)) {
				exit();
			}
		}
		
		/**
		 * @see StandardButtonListener
		 */
		@Override
		public void exit() {
			dispose();
		}
	}
}
