package it.polimi.ingsw.ps04.client.view.userinterface.swing.utils.panels;

import java.awt.FlowLayout;
import java.awt.Font;
import java.util.Set;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import it.polimi.ingsw.ps04.model.bonus.Bonus;
import it.polimi.ingsw.ps04.model.bonus.complex.BonusfromCityBonus;
import it.polimi.ingsw.ps04.model.bonus.complex.BonusfromTileBonus;
import it.polimi.ingsw.ps04.model.bonus.complex.BusinessPermitTileBonus;
import it.polimi.ingsw.ps04.model.bonus.simple.AdditionalMainActionBonus;
import it.polimi.ingsw.ps04.model.bonus.simple.AssistantsBonus;
import it.polimi.ingsw.ps04.model.bonus.simple.CoinsBonus;
import it.polimi.ingsw.ps04.model.bonus.simple.NobilityBonus;
import it.polimi.ingsw.ps04.model.bonus.simple.PoliticCardsBonus;
import it.polimi.ingsw.ps04.model.bonus.simple.VictoryPointsBonus;
import it.polimi.ingsw.ps04.client.view.userinterface.swing.GUIGame;

public class BonusesPanel {

	private Set<Bonus> bonuses;
	private JPanel bonusPanel = new JPanel();

	/**
	 * Builder for BonusesPanel which has a bonuses set to draw
	 * 
	 * @param bonuses
	 * 				 I wanted to draw
	 */
	public BonusesPanel(Set<Bonus> bonuses) {
		this.bonuses = bonuses;
		createPanel();
	}
	
	/**
	 * Here I figure out what kind of bonus is and create two labels: one for amount and one for the image, after
	 * that I added both to bonus Panel
	 * 
	 */
	private void createPanel() {
		bonusPanel.setLayout(new FlowLayout());
		JLabel emptyLabel = new JLabel ("");
		bonusPanel.add(emptyLabel);
		for (Bonus bonus : bonuses) {
			if (CoinsBonus.class.isInstance(bonus)) {
				JLabel coinImage = new JLabel (new ImageIcon(GUIGame.IMAGES_PATH + "coins.png"));
				JLabel coinCount = new JLabel (" " + String.valueOf(bonus.getBonusIncrementer() + " "));
				sets(coinCount, coinImage);
			}
			if (AssistantsBonus.class.isInstance(bonus)) {
				JLabel assistantsImage = new JLabel (new ImageIcon(GUIGame.IMAGES_PATH + "assistants.png"));
				JLabel assistantsCount = new JLabel (" " + String.valueOf(bonus.getBonusIncrementer() + " "));
				sets(assistantsCount, assistantsImage);
			}
			if (VictoryPointsBonus.class.isInstance(bonus)) {
				JLabel victoryPointsImage = new JLabel (new ImageIcon(GUIGame.IMAGES_PATH + "victorypointssmall.png"));
				JLabel victoryPointsCount = new JLabel (" " +String.valueOf(bonus.getBonusIncrementer()+ " "));
				sets(victoryPointsCount, victoryPointsImage);
			}
			if (NobilityBonus.class.isInstance(bonus)) {
				JLabel nobilityTrackImage = new JLabel (new ImageIcon(GUIGame.IMAGES_PATH + "nobilitypoints.png"));
				JLabel nobilityTrackCount = new JLabel (" " + String.valueOf(bonus.getBonusIncrementer()+ " "));
				sets(nobilityTrackCount, nobilityTrackImage);
			} 
			if (BusinessPermitTileBonus.class.isInstance(bonus)) {
				JLabel businessPermitTileBonusImage = new JLabel (new ImageIcon(GUIGame.IMAGES_PATH + "takevisibletile.png"));
				JLabel businessPermitTileBonusCount = new JLabel (" " + String.valueOf(bonus.getBonusIncrementer() +" "));
				sets(businessPermitTileBonusCount, businessPermitTileBonusImage);
			} 
			if (BonusfromCityBonus.class.isInstance(bonus)) {
				JLabel bonusFromCityImage = new JLabel (new ImageIcon(GUIGame.IMAGES_PATH + "rewardtokenagain.png"));
				JLabel bonusFromCityCount = new JLabel (" " +String.valueOf(bonus.getBonusIncrementer() + " "));
				sets(bonusFromCityCount, bonusFromCityImage);
			} 
			if (BonusfromTileBonus.class.isInstance(bonus)) {
				JLabel bonusFromTileAgainImage = new JLabel (new ImageIcon(GUIGame.IMAGES_PATH + "tilebonusagain.png"));
				JLabel bonusFromTileAgainCount = new JLabel (" " +String.valueOf(bonus.getBonusIncrementer() + " "));
				sets(bonusFromTileAgainCount, bonusFromTileAgainImage);
			} 
			if (AdditionalMainActionBonus.class.isInstance(bonus)) {
				JLabel additionaMainActionImage = new JLabel (new ImageIcon(GUIGame.IMAGES_PATH + "mainactions.png"));
				JLabel additionaMainActionCount = new JLabel (" " +String.valueOf(bonus.getBonusIncrementer()+ " "));
				sets(additionaMainActionCount, additionaMainActionImage);
			} 
			if (PoliticCardsBonus.class.isInstance(bonus)) {
				JLabel politicCardsBonusImage = new JLabel (new ImageIcon(GUIGame.IMAGES_PATH + "politicscardsbonus.png"));
				JLabel politicCardsBonusCount = new JLabel (" " +String.valueOf(bonus.getBonusIncrementer() + " "));
				sets(politicCardsBonusCount, politicCardsBonusImage);
			} 
		JLabel label = new JLabel ("");
		bonusPanel.add(label);
		}
	}

	/**
	 * Here I set component and add both, image and component, to bonus panel
	 * 
	 * @param component
	 * 				   to set 
	 * @param image
	 * 			   of the bonus to add close to it
	 */
	private void sets(JComponent component, JComponent image) {
		component.setFont(new Font("Agency FB", Font.ITALIC, 20));
		bonusPanel.add(image);
		bonusPanel.add(component);
	}
	
	/**
	 * Here I return this panel in order to use it in other classes
	 * 
	 * @return bonusPanel
	 */
	public JPanel getPanel() {
		return bonusPanel;
	}
}
