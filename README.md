# Council of Four - PS04 #

this class starts the server:

```
ps04/src/main/java/it/polimi/ingsw/ps04/view/Server.java


```

this class starts the client:

```
ps04/src/main/java/it/polimi/ingsw/ps04/client/Main.java


```


## Info ##

* in the cli you can always insert `?list`to see the commands and their argouments;
* we implemented the login feature, but the database is started when you run the server, so you don't need XAMPP or similars;
* if you want to store the changes of the database you have to select "stop server" in the cli of the server, so you have to insert `2` or `stop`.